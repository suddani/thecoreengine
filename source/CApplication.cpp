#include <core/IGraphicManager.h>
#include <core/IInputManager.h>
#include <core/IConsole.h>
#include <core/SProfiler.h>
#include <core/IProcess.h>

#include <support/CMySkin.h>

#include "CApplication.h"


irr::core::map<AppFrame::string, clock_t> AppFrame::SProfiler::Times;
AppFrame::u32 AppFrame::SProfiler::TotalTime = 0;
AppFrame::u32 AppFrame::IProcess::IPROCESS_ID = 0;

CApplication::CApplication(int argc, char* argv[])
{
    #ifndef DEBUG
    //AppFrame::ILogger::setLogLevel(1);
    nlogLevel = 1;
    #endif
}

CApplication::~CApplication(void)
{
    Manager->getConsole()->unRegisterConsoleCommand("/exit", this);
    nlog<<"Clean Input...";
    Input->drop();
    nlog<<"Clean Graphic...";
    Graphic->drop();
    nlog<<"Clean ProcessManager...";
    Manager->drop();
    nlog<<"Clean Irrlicht...";
    Device->drop();

    nlog<<"Cleaned everything"<<nlflush;
}

void CApplication::Init(void)
{
    //init gamemanager
    Device  = AppFrame::graphic::createIrrlichtDeviceFromFile("config.xml");

    Input = AppFrame::input::createInputManager(Device);

    Graphic = AppFrame::graphic::createGraphicManager(Device);

    Manager = AppFrame::createProcessManager(Graphic, Input);

    Manager->getConsole()->RegisterConsoleCommand("/exit", CONSOLE_CALLBACK_METHOD(CApplication, exitProcess), "Kills the currently active Process", true);

}

void CApplication::PrintGameVersion(const AppFrame::c8* name)
{
}

void CApplication::Run(void)
{
    bool run = true;
    while (run)
    {
        //do logic stuff
        run = Manager->Run() && Device->run() ? true : false;

        if (run)
        {
            //draw stuff
            Device->getVideoDriver()->beginScene(true, true, irr::video::SColor(255,0,0,0));
            Graphic->draw();
            Device->getGUIEnvironment()->drawAll();
            Device->getVideoDriver()->endScene();
        }
    }
    Device->closeDevice();
    Device->run();
}

void CApplication::exitProcess(const AppFrame::SCommand& command)
{
    Manager->killActiveProcess();
}
