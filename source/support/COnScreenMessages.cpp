#include "support/COnScreenMessages.h"
#include <ILogger.h>

COnScreenMessages::COnScreenMessages(irr::gui::IGUIEnvironment* environment, irr::gui::IGUIElement* parent, irr::s32 id, irr::core::rect<irr::s32> rectangle) : IGUIElement(irr::gui::EGUIET_ELEMENT, environment, parent, id, rectangle)
{
    //ctor
    Font = NULL;
    MaxLines = 0;
    setFont(Environment->getBuiltInFont());
    LastTime = 0;
    Top2Bottom = true;
    VisTime = 1000;
    FadeTime = 1.0f;
}

COnScreenMessages::~COnScreenMessages()
{
    //dtor
    setFont(NULL);
}

void COnScreenMessages::calc_maxlines(void)
{
    if (!Font)
        return;
    YDim = Font->getDimension(L"QWERTZUIOPASDFGHJKLYXCVBNM").Height;
    YDim += 0;
    irr::s32 Yspacing = AbsoluteRect.LowerRightCorner.Y-AbsoluteRect.UpperLeftCorner.Y;
    MaxLines = Yspacing / YDim;
}

void COnScreenMessages::draw()
{
    if (!IsVisible)
        return;

    irr::core::rect<irr::s32> option = AbsoluteRect;

    if (Top2Bottom)
    {
        option.LowerRightCorner.Y = AbsoluteRect.UpperLeftCorner.Y+YDim;
        irr::core::list<SMessage>::Iterator it = Messages.begin();
        while (it != Messages.end())
        {
            (*it).Color.setAlpha(255 * (*it).Visibility);
            irr::gui::IGUIFont* dFont = (*it).Font;
            if (!dFont)
                dFont = Font;
            dFont->draw((*it).Message.c_str(), option, (*it).Color, true, true, &AbsoluteClippingRect);
            option.LowerRightCorner.Y += YDim;
            option.UpperLeftCorner.Y  += YDim;
            ++it;
        }
    }
    else
    {
        option.UpperLeftCorner.Y = AbsoluteRect.LowerRightCorner.Y-YDim;
        irr::core::list<SMessage>::Iterator it = Messages.begin();
        while (it != Messages.end())
        {
            (*it).Color.setAlpha(255 * (*it).Visibility);
            irr::gui::IGUIFont* dFont = (*it).Font;
            if (!dFont)
                dFont = Font;
            dFont->draw((*it).Message.c_str(), option, (*it).Color, true, true);//, &AbsoluteClippingRect);
            option.LowerRightCorner.Y -= YDim;
            option.UpperLeftCorner.Y  -= YDim;
            ++it;
        }
    }

    irr::gui::IGUIElement::draw();
}

void COnScreenMessages::OnPostRender(irr::u32 timeMs)
{
    irr::u32 CurrentTime = timeMs;

    if (LastTime == 0)
        LastTime = CurrentTime;

    irr::f32 diff = (CurrentTime-LastTime) / 1000.f;

    irr::core::array<irr::core::list<SMessage>::Iterator> del;
    irr::core::list<SMessage>::Iterator it = Messages.begin();
    while (it != Messages.end())
    {
        if ((*it).StartTime + VisTime <= CurrentTime)
        {
            (*it).Visibility -= diff/FadeTime;
            if ((*it).Visibility <= 0.f)
                del.push_back(it);
        }
        ++it;
    }

    for (irr::u32 i=0;i<del.size();++i)
        Messages.erase(del[i]);


    irr::gui::IGUIElement::OnPostRender(timeMs);
    LastTime = CurrentTime;
}

void COnScreenMessages::setFadeTime(const irr::f32& fadeTime)
{
    FadeTime = fadeTime;

    if (FadeTime < 0.f)
        FadeTime *= -1.f;

    if (FadeTime == 0.f)
        FadeTime = 0.001f;
}

void COnScreenMessages::setVisibleTime(const irr::u32& visTime)
{
    VisTime = visTime;
}

void COnScreenMessages::setFont(irr::gui::IGUIFont* font)
{
    if (Font)
        Font->drop();
    Font = font;
    if (Font)
        Font->grab();

    calc_maxlines();
}

void COnScreenMessages::addMessage(const irr::c8* message, const irr::video::SColor& color, irr::gui::IGUIFont* font)
{
    SMessage m;
    m.Message = irr::core::stringw(message);
    m.StartTime = LastTime;
    m.Color = color;
    m.Visibility = 1.0f;
    m.Font = font;
    Messages.push_front(m);

    if (Messages.getSize() > MaxLines)
    {
        irr::core::list<SMessage>::Iterator it;
        if (Top2Bottom)
            it = Messages.getLast();
        else
            it = Messages.begin();
        Messages.erase(it);
    }
}
