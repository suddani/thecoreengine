#include "support/CProgressBar.h"

CProgressBar::CProgressBar(irr::gui::IGUIEnvironment* environment, irr::gui::IGUIElement* parent, irr::s32 id, irr::core::rect<irr::s32> rectangle) : IGUIElement(irr::gui::EGUIET_ELEMENT, environment, parent, id, rectangle)
{
    //ctor
    LastTime = 0;
    Border = 10;
    Progress = -1.f;
    Font = NULL;

    OverrideColor = irr::video::SColor(255,0,0,0);

    BorderColor = irr::video::SColor(255,0,0,0);

    BarColor[0] = irr::video::SColor(128,255,0,0);
    BarColor[1] = irr::video::SColor(128,255,0,0);
    BarColor[2] = irr::video::SColor(128,0,255,0);
    BarColor[3] = irr::video::SColor(128,0,255,0);

    FadeOutTime = 3.f;
    CurrentFadeOutTime = 3.f;
}

CProgressBar::~CProgressBar()
{
    //dtor
    if (Font)
        Font->drop();
    Font = NULL;
}

void CProgressBar::setFadeOutTime(const irr::f32& time)
{
    FadeOutTime = time;
    CurrentFadeOutTime = time;
}

void CProgressBar::draw(void)
{
    if (!IsVisible || CurrentFadeOutTime <= 0.f)
        return;

    if (Progress >= 0.f)
    {
        irr::video::IVideoDriver* driver = Environment->getVideoDriver();
        irr::core::rect<irr::s32> option = AbsoluteRect;

        irr::video::SColor color = BorderColor;
        color.setAlpha(color.getAlpha()*(CurrentFadeOutTime/FadeOutTime));

        //draw upper border
        option.LowerRightCorner.Y = option.UpperLeftCorner.Y+Border;
        driver->draw2DRectangle(color, option, &AbsoluteClippingRect);

        //draw lower border
        option = AbsoluteRect;
        option.UpperLeftCorner.Y = option.LowerRightCorner.Y-Border;
        driver->draw2DRectangle(color, option, &AbsoluteClippingRect);

        //draw left border
        option = AbsoluteRect;
        option.UpperLeftCorner.Y += Border;
        option.LowerRightCorner.Y -= Border;
        option.LowerRightCorner.X = option.UpperLeftCorner.X+Border;
        driver->draw2DRectangle(color, option, &AbsoluteClippingRect);

        //draw right border
        option = AbsoluteRect;
        option.UpperLeftCorner.X = option.LowerRightCorner.X-Border;
        option.UpperLeftCorner.Y += Border;
        option.LowerRightCorner.Y -= Border;
        driver->draw2DRectangle(color, option, &AbsoluteClippingRect);

        //draw progress
        option = AbsoluteRect;
        option.UpperLeftCorner.X += Border;
        option.UpperLeftCorner.Y += Border;
        option.LowerRightCorner.Y -= Border;
        option.LowerRightCorner.X -= Border;
        irr::f32 range = option.LowerRightCorner.X-option.UpperLeftCorner.X;
        irr::core::rect<irr::s32> p = option;
        p.LowerRightCorner.X = option.UpperLeftCorner.X + (irr::s32)(range*Progress);
        irr::video::SColor color1 = BarColor[0];
        irr::video::SColor color2 = BarColor[1];
        irr::video::SColor color3 = BarColor[2];
        irr::video::SColor color4 = BarColor[3];
        color1.setAlpha(color1.getAlpha()*(CurrentFadeOutTime/FadeOutTime));
        color2.setAlpha(color2.getAlpha()*(CurrentFadeOutTime/FadeOutTime));
        color3.setAlpha(color3.getAlpha()*(CurrentFadeOutTime/FadeOutTime));
        color4.setAlpha(color4.getAlpha()*(CurrentFadeOutTime/FadeOutTime));
        driver->draw2DRectangle(option, BarColor[0], BarColor[2], BarColor[1], BarColor[3], &p);

        //draw Text
        if (Text.size() > 0)
        {
            irr::gui::IGUIFont* font = Font;
            if (!font)
                font = Environment->getBuiltInFont();
            color = OverrideColor;
            color.setAlpha(color.getAlpha()*(CurrentFadeOutTime/FadeOutTime));
            font->draw(Text.c_str(), option, OverrideColor, true, true, &AbsoluteClippingRect);
        }
    }

    irr::gui::IGUIElement::draw();
}

void CProgressBar::OnPostRender(irr::u32 timeMs)
{
    if (LastTime == 0)
        LastTime = timeMs;
    irr::f32 diff = timeMs-LastTime;
    diff /= 1000;
    LastTime = timeMs;
    if (getProgress() <= 0.f && FadeOutTime > 0.f && CurrentFadeOutTime > 0.f)
    {
        CurrentFadeOutTime -= diff;
    }

    irr::gui::IGUIElement::OnPostRender(timeMs);
}

irr::s32 CProgressBar::getProgressPercent(void)
{
    return (irr::s32)(Progress*100);
}

const irr::f32& CProgressBar::getProgress(void)
{
    return Progress;
}

void CProgressBar::setProgress(irr::s32 progress)
{
    setProgress((irr::f32)(progress/100.f));
}

void CProgressBar::setProgress(irr::f32 progress)
{
    if (progress > 0.f)
        CurrentFadeOutTime = FadeOutTime;
    Progress = progress;
    if (Progress > 1.f)
        Progress = 1.f;
}

void CProgressBar::setOverrideFont(irr::gui::IGUIFont* font)
{
    if (Font)
        Font->drop();
    Font = font;
    if (Font)
        Font->grab();
}

void CProgressBar::setBorder(irr::s32 border)
{
    Border = border;
    if (Border < 0)
        Border *= -1;
}

void CProgressBar::setOverrideColor(irr::video::SColor color)
{
    OverrideColor = color;
}

void CProgressBar::setBorderColor(irr::video::SColor color)
{
    BorderColor = color;
}

void CProgressBar::setBarColor(irr::video::SColor color[4])
{
    BarColor[0] = color[0];

    BarColor[1] = color[1];

    BarColor[2] = color[2];

    BarColor[3] = color[3];
}
