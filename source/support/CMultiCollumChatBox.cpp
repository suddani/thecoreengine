#include "support/CMultiCollumChatBox.h"

const irr::u32 MAX_SAVED_LINES = 100;
CMultiCollumChatBox::CMultiCollumChatBox(irr::gui::IGUIEnvironment* environment, irr::gui::IGUIElement* parent, irr::s32 id, irr::core::rect<irr::s32> rectangle) : IGUIElement(irr::gui::EGUIET_ELEMENT, environment, parent, id, rectangle)
{
    //ctor
    FadeTime = 1.f;
    MessageTimeVisible = 1000;
    Font = NULL;
    HeaderFont = NULL;
    MaxLines = 0;
    CollumBarHeight = 0;
    setFont(Environment->getBuiltInFont());
    setHeaderFont(Environment->getBuiltInFont());
    LastTime = 0;
    Top2Bottom = true;
    CollumBarVisible =  false;
    ScrollBarSize = 10;
    CollumCount = 1;
    CollumNames.push_back("Data");
    CollumSpacings.push_back(1.f);
    CollumCenter.push_back(false);
    SelectedElement = -1;
    AutoScroll = true;

    ScrollBar = Environment->addScrollBar(false, irr::core::rect<irr::s32>(0,0,0,0),this, -1);
    ScrollBar->setVisible(false);
    ScrollBar->setSmallStep (1);
    ScrollBar->setLargeStep (5);
}

CMultiCollumChatBox::~CMultiCollumChatBox()
{
    //dtor
    setFont(NULL);
    setHeaderFont(NULL);
}

void CMultiCollumChatBox::setAutoScrollEnabled (bool scroll)
{
    AutoScroll = scroll;
}

void CMultiCollumChatBox::calc_maxlines(void)
{
    if (!Font)
        return;
    YDim = Font->getDimension(L"QWERTZUIOPASDFGHJKLYXCVBNM").Height;
    YDim += 0;
    irr::s32 Yspacing = AbsoluteRect.LowerRightCorner.Y-AbsoluteRect.UpperLeftCorner.Y-CollumBarHeight;
    MaxLines = Yspacing / YDim;
    //if (CollumBarVisible)
    //MaxLines--;
}

void CMultiCollumChatBox::draw(void)
{
    irr::core::rect<irr::s32> option = AbsoluteRect;

    //draw background here
    if (IsVisible)
    {
        if (CollumBarVisible)
            option.UpperLeftCorner.Y += CollumBarHeight;
        //option.UpperLeftCorner.Y -= 2;
        //option.UpperLeftCorner.X -= 2;
        option.LowerRightCorner.X -= ScrollBarSize;
        //option.LowerRightCorner.Y += 2;
        Environment->getSkin()->draw3DSunkenPane(this, irr::video::SColor(255,128,128,128), false, true, option);//, &AbsoluteClippingRect);
    }
    option = AbsoluteRect;
    //draws Messages
    if (IsVisible || MessageTimeVisible > 0)
    {
        if (Top2Bottom)
        {
            option.LowerRightCorner.Y = AbsoluteRect.UpperLeftCorner.Y+YDim;

            //makes space for the CollumHeader
            if (CollumBarVisible)
            {
                option.UpperLeftCorner.Y  += CollumBarHeight;
                option.LowerRightCorner.Y += CollumBarHeight;
            }

            //irr::core::list<SMultiMessage>::Iterator it = Messages.begin();
            //while (it != Messages.end())
            irr::s32 s = ScrollBar->getPos();
            for (irr::u32 i=s; i<(MaxLines+s) && i<Messages.size();++i)
            {
                irr::video::SColor TheColor = Messages[i].Color;
                if (!IsVisible)
                    TheColor.setAlpha(255 * Messages[i].Visibility);

                irr::gui::IGUIFont* dFont = NULL;
                if (!dFont)
                    dFont = Font;

                //draw actual data
                option.UpperLeftCorner.X = AbsoluteRect.UpperLeftCorner.X;
                option.LowerRightCorner.X = AbsoluteRect.LowerRightCorner.X-ScrollBarSize;

                //draw is it is selcted and Visible
                if (IsVisible && SelectedElement == i)
                    Environment->getSkin()->draw3DMenuPane(this, option, &AbsoluteClippingRect);


                option.LowerRightCorner.X = AbsoluteRect.UpperLeftCorner.X;

                for (irr::u32 a=0;a<CollumCount;++a)
                {
                    option.UpperLeftCorner.X  = option.LowerRightCorner.X;
                    option.LowerRightCorner.X = option.UpperLeftCorner.X+((irr::s32)((AbsoluteRect.LowerRightCorner.X-AbsoluteRect.UpperLeftCorner.X-ScrollBarSize)*CollumSpacings[a]));

                    if (!CollumCenter[a])
                        option.UpperLeftCorner.X += dFont->getDimension(L"A").Width;

                    dFont->draw(Messages[i].Message[a].c_str(), option, TheColor, CollumCenter[a], true, &AbsoluteClippingRect);
                }

                option.LowerRightCorner.Y += YDim;
                option.UpperLeftCorner.Y  += YDim;
            }
        }
        else
        {
            option.UpperLeftCorner.Y = AbsoluteRect.LowerRightCorner.Y-YDim;
            //irr::core::list<SMultiMessage>::Iterator it = Messages.begin();
            //while (it != Messages.end())
            irr::s32 s = ScrollBar->getMax()-ScrollBar->getPos();
            for (irr::u32 i=s; i<(MaxLines+s) && i<Messages.size();++i)
            {
                irr::video::SColor TheColor = Messages[i].Color;
                if (!IsVisible)
                    TheColor.setAlpha(255 * Messages[i].Visibility);

                irr::gui::IGUIFont* dFont = NULL;
                if (!dFont)
                    dFont = Font;

                //draw actual data
                option.UpperLeftCorner.X = AbsoluteRect.UpperLeftCorner.X;
                option.LowerRightCorner.X = AbsoluteRect.LowerRightCorner.X-ScrollBarSize;

                //draw is it is selcted and Visible
                if (IsVisible && SelectedElement == i)
                    Environment->getSkin()->draw3DMenuPane(this, option, &AbsoluteClippingRect);


                option.LowerRightCorner.X = AbsoluteRect.UpperLeftCorner.X;

                for (irr::u32 a=0;a<CollumCount;++a)
                {
                    option.UpperLeftCorner.X  = option.LowerRightCorner.X;
                    option.LowerRightCorner.X = option.UpperLeftCorner.X+((irr::s32)((AbsoluteRect.LowerRightCorner.X-AbsoluteRect.UpperLeftCorner.X-ScrollBarSize)*CollumSpacings[a]));

                    if (!CollumCenter[a])
                        option.UpperLeftCorner.X += dFont->getDimension(L"A").Width;

                    dFont->draw(Messages[i].Message[a].c_str(), option, TheColor, CollumCenter[a], true, &AbsoluteClippingRect);
                }

                option.LowerRightCorner.Y -= YDim;
                option.UpperLeftCorner.Y  -= YDim;
            }
        }
    }

    if (!IsVisible)
        return;

    ScrollBar->setRelativePosition(irr::core::rect<irr::s32>(AbsoluteRect.LowerRightCorner.X-AbsoluteRect.UpperLeftCorner.X-ScrollBarSize,
                                   CollumBarVisible ? CollumBarHeight : 0,
                                   AbsoluteRect.LowerRightCorner.X-AbsoluteRect.UpperLeftCorner.X,
                                   AbsoluteRect.LowerRightCorner.Y-AbsoluteRect.UpperLeftCorner.Y));
    //draw CollumBar if visibile
    if (CollumBarVisible)
    {
        option = AbsoluteRect;
        option.LowerRightCorner.Y = option.UpperLeftCorner.Y+CollumBarHeight;
        option.LowerRightCorner.X = option.UpperLeftCorner.X;
        for (irr::u32 i=0;i<CollumCount;++i)
        {
            option.UpperLeftCorner.X = option.LowerRightCorner.X;
            option.LowerRightCorner.X = option.UpperLeftCorner.X+((irr::s32)((AbsoluteRect.LowerRightCorner.X-AbsoluteRect.UpperLeftCorner.X-ScrollBarSize)*CollumSpacings[i]));

            //draw background here
            Environment->getSkin()->draw3DMenuPane(this, option, &AbsoluteClippingRect);

            //draw Header
            HeaderFont->draw(CollumNames[i].c_str(), option, irr::video::SColor(255,255,0,0), true, true, &AbsoluteClippingRect);
        }
    }

    //draw seperator
    irr::core::position2d<irr::s32> start(AbsoluteRect.UpperLeftCorner.X, AbsoluteRect.UpperLeftCorner.Y);
    if (CollumBarVisible)
        start.Y += CollumBarHeight;
    irr::core::position2d<irr::s32> end(AbsoluteRect.UpperLeftCorner.X, AbsoluteRect.LowerRightCorner.Y);
    for (irr::u32 i=0;i<CollumCount;++i)
    {
        start.X += ((irr::s32)((AbsoluteRect.LowerRightCorner.X-AbsoluteRect.UpperLeftCorner.X-ScrollBarSize)*CollumSpacings[i]));
        end.X   += ((irr::s32)((AbsoluteRect.LowerRightCorner.X-AbsoluteRect.UpperLeftCorner.X-ScrollBarSize)*CollumSpacings[i]));
        Environment->getVideoDriver()->draw2DLine(start, end, irr::video::SColor(255,0,0,0));
    }
    irr::gui::IGUIElement::draw();
}

void CMultiCollumChatBox::OnPostRender(irr::u32 timeMs)
{
    irr::u32 CurrentTime = timeMs;
    if (LastTime == 0)
        LastTime = CurrentTime;
    irr::f32 diff = (CurrentTime-LastTime)/1000.f;

    //do stuff
    for (irr::u32 i=0;i<Messages.size();++i)
    {
        if (Messages[i].StartTime + MessageTimeVisible <= CurrentTime)
        {
            Messages[i].Visibility -= diff/FadeTime;
            if (Messages[i].Visibility <= 0.f)
                Messages[i].Visibility = 0.f;
        }
    }

    irr::gui::IGUIElement::OnPostRender(timeMs);
    LastTime = CurrentTime;
}

void CMultiCollumChatBox::setFont(irr::gui::IGUIFont* font)
{
    if (Font)
        Font->drop();
    Font = font;
    if (Font)
        Font->grab();

    calc_maxlines();
}

void CMultiCollumChatBox::setHeaderFont(irr::gui::IGUIFont* font)
{
    if (HeaderFont)
        HeaderFont->drop();
    HeaderFont = font;
    if (HeaderFont)
    {
        irr::u32 h = HeaderFont->getDimension(L"QWERTZUIOPASDFGHJKLYXCVBNM").Height;
        if (h > CollumBarHeight)
            CollumBarHeight = h;
        HeaderFont->grab();
    }

    calc_maxlines();
}

void CMultiCollumChatBox::setCollumNameVisible(bool visible)
{
    CollumBarVisible = visible;
    calc_maxlines();
}

const irr::u32& CMultiCollumChatBox::getCollumCount(void)
{
    return CollumCount;
}

void CMultiCollumChatBox::setCollums(irr::u32 count, irr::c8* name[], irr::f32 spacing[], bool allignCenter[])
{
    CollumCount = count;
    CollumNames.clear();
    CollumSpacings.clear();
    CollumCenter.clear();
    for (irr::u32 i=0;i<CollumCount;++i)
    {
        if (name)
            CollumNames.push_back(name[i]);
        else
            CollumNames.push_back("");

        if (allignCenter)
            CollumCenter.push_back(allignCenter[i]);
        else
            CollumCenter.push_back(false);

        if (spacing)
            CollumSpacings.push_back(spacing[i]);
        else
            CollumSpacings.push_back(1.f);
    }
}

void CMultiCollumChatBox::setCollumBarHeight(irr::u32 height)
{
    CollumBarHeight = height;
}

void CMultiCollumChatBox::setMessageVisibleTime(irr::s32 timeMs)
{
    MessageTimeVisible = timeMs;
}

void CMultiCollumChatBox::setMessageFadeTime(irr::f32 fadetime)
{
    FadeTime = fadetime;
    if (FadeTime < 0.f)
        FadeTime *= -1;
    if (FadeTime == 0.f)
        FadeTime = 0.001f;
}

void CMultiCollumChatBox::setTop2Bottom(bool t2b)
{
    Top2Bottom = t2b;
}

irr::u32 CMultiCollumChatBox::getItemCount(void)
{
    return Messages.size();
}

CMultiCollumChatBox::SMultiMessage& CMultiCollumChatBox::getItem(irr::u32 id)
{
    if (id >= Messages.size())
        return *((SMultiMessage*)NULL);
    return Messages[id];
}

void CMultiCollumChatBox::addMessage(const irr::c8* message, const irr::video::SColor& color)
{
    irr::core::stringc data = message;
    irr::core::array<irr::core::stringc> m;

    if (CollumCount == 1)
    {
        irr::s32 d = data.findFirstChar(";", 1);
        if (d > 0)
            m.push_back(data.subString(0, d));
        else
            m.push_back(message);
        addMessage(m, color);
        return;
    }

    for (irr::u32 i=0;i<CollumCount;++i)
    {
        irr::s32 d = data.findFirstChar(";", 1);
        m.push_back(data.subString(0, d));
        data = data.subString(d+1, data.size()-d);
    }
    addMessage(m, color);
}

void CMultiCollumChatBox::addMessage(const irr::core::array<irr::core::stringc>& message, const irr::video::SColor& color)
{
    if (message.size() < CollumCount)
        return;

    SMultiMessage m;
    m.StartTime = LastTime;
    m.Color = color;
    m.Visibility = 1.0;
    for (irr::u32 i=0;i<CollumCount;++i)
    {
        m.Message.push_back(message[i]);
    }
    Messages.push_front(m);

    /*if (Messages.size() > MAX_SAVED_LINES)
    {
        Messages.erase(Messages.size()-1);
    }*/

    if (Messages.size() > MaxLines)
    {
        ScrollBar->setVisible(true);
        ScrollBar->setMax(Messages.size()-MaxLines+1);
        if (AutoScroll)
        {
            if (Top2Bottom)
                ScrollBar->setPos(0);
            else
            {
                ScrollBar->setPos(ScrollBar->getMax());
            }
        }
    }
    else
    {
        ScrollBar->setMax(0);
        if (Top2Bottom)
            ScrollBar->setPos(0);
        else
        {
            ScrollBar->setPos(ScrollBar->getMax());
        }
        ScrollBar->setVisible(false);
    }
}

irr::s32 CMultiCollumChatBox::getItem(irr::u32 x, irr::u32 y, irr::s32& collum)
{
    irr::core::rect<irr::s32> option = AbsoluteRect;

    if (CollumBarVisible)
    {
        option.LowerRightCorner.Y = option.UpperLeftCorner.Y+CollumBarHeight;
        option.LowerRightCorner.X = option.UpperLeftCorner.X;
        for (irr::u32 i=0;i<CollumCount;++i)
        {
            option.UpperLeftCorner.X = option.LowerRightCorner.X;
            option.LowerRightCorner.X = option.UpperLeftCorner.X+((irr::s32)((AbsoluteRect.LowerRightCorner.X-AbsoluteRect.UpperLeftCorner.X-ScrollBarSize)*CollumSpacings[i]));

            //check for Mouse
            if (option.isPointInside(irr::core::position2d<irr::s32>(x,y)))
            {
                collum = i;
                return -1;
            }
        }
    }

    option = AbsoluteRect;

    if (Top2Bottom)
    {
        option.LowerRightCorner.Y = AbsoluteRect.UpperLeftCorner.Y+YDim;

        //makes space for the CollumHeader
        if (CollumBarVisible)
        {
            option.UpperLeftCorner.Y  += CollumBarHeight;
            option.LowerRightCorner.Y += CollumBarHeight;
        }

        irr::s32 s = ScrollBar->getPos();
        for (irr::u32 i=s; i<(MaxLines+s) && i<Messages.size();++i)
        {
            option.UpperLeftCorner.X = AbsoluteRect.UpperLeftCorner.X;
            option.LowerRightCorner.X = AbsoluteRect.LowerRightCorner.X-ScrollBarSize;

            if (option.isPointInside(irr::core::position2d<irr::s32>(x,y)))
            {
                collum = -1;
                return i;
            }
            option.LowerRightCorner.Y += YDim;
            option.UpperLeftCorner.Y  += YDim;
        }
    }
    else
    {
        option.UpperLeftCorner.Y = AbsoluteRect.LowerRightCorner.Y-YDim;

        irr::s32 s = ScrollBar->getMax()-ScrollBar->getPos();
        for (irr::u32 i=s; i<(MaxLines+s) && i<Messages.size();++i)
        {
            option.UpperLeftCorner.X = AbsoluteRect.UpperLeftCorner.X;
            option.LowerRightCorner.X = AbsoluteRect.LowerRightCorner.X-ScrollBarSize;

            if (option.isPointInside(irr::core::position2d<irr::s32>(x,y)))
            {
                collum = -1;
                return i;
            }
            option.LowerRightCorner.Y -= YDim;
            option.UpperLeftCorner.Y  -= YDim;
        }
    }
    return -1;
}

bool CMultiCollumChatBox::OnEvent(const irr::SEvent& event)
{
    switch (event.EventType)
    {
    case irr::EET_MOUSE_INPUT_EVENT:
        switch (event.MouseInput.Event)
        {
        case irr::EMIE_MOUSE_WHEEL:
            if (event.MouseInput.Wheel < 0.f)
                ScrollBar->setPos(ScrollBar->getPos()+1);
            else
                ScrollBar->setPos(ScrollBar->getPos()-1);
            break;
        case irr::EMIE_LMOUSE_PRESSED_DOWN:
        {
            irr::s32 c = -1;
            irr::s32 oldItem = SelectedElement;
            SelectedElement = getItem(event.MouseInput.X, event.MouseInput.Y, c);
            //if (SelectedElement >= 0)
            {
                irr::SEvent MyGUIEvent;
                MyGUIEvent.EventType = irr::EET_GUI_EVENT;
                MyGUIEvent.GUIEvent.Caller = this;
                MyGUIEvent.GUIEvent.Element = this;
                if (oldItem != SelectedElement)
                    MyGUIEvent.GUIEvent.EventType = irr::gui::EGET_LISTBOX_CHANGED;
                else if (SelectedElement >= 0)
                    MyGUIEvent.GUIEvent.EventType = irr::gui::EGET_LISTBOX_SELECTED_AGAIN;
                Environment->postEventFromUser(MyGUIEvent);
            }
            //TODO: sort if selected is a collum header
            break;
        }
        }
        break;
    }
    return false;
}
