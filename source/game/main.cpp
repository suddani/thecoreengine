#include "CApplication.h"

int main(int argumentcount, char* arguments[])
{
    CApplication app(argumentcount, arguments);
    app.Init();
    app.registerProcess();
    app.Run();
    return 0;
}
