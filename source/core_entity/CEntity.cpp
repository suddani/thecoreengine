/*
  Copyright (C) 2011 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/
#include "CEntity.h"
#include "CEntityManager.h"

#ifdef USE_SLB
#include <SLB/SLB.hpp>
const char* getType(CEntity* e)
{
    return e->getType().c_str();
}
const char* getIdent(CEntity* e)
{
    return e->getIdent().c_str();
}

IComponent* addComponent(CEntity* e, const char* type)
{
    return e->addComponent(type);
}

void registerCEntity(SLB::Manager* m)
{
    SLB::Class<CEntity, SLB::Instance::NoCopyNoDestroy>("Entity", m)
        .constructor()
        .set("getType", getType)
        .set("getIdent", getIdent)
        .set("addComponent", addComponent);

}
#endif
CEntity::CEntity()
{
    EntityIDGenerator::getGenerator().add(this);
    nlogl(1)<<"Entity created"<<nlendl;
    type = "none";
}
CEntity::~CEntity()
{
    clearList();
    EntityIDGenerator::getGenerator().remove(this);
    nlogl(1)<<"Entity destroyed"<<nlendl;
}

void CEntity::init(AppFrame::SharedPointer<CEntityManager> m)
{
    manager = m;
}

IComponent* CEntity::getComponent(const int& comp_id)
{
    RunnableListIt it = this->Runnables.begin();
    while(it != Runnables.end())
    {
        if ((*it)->getComponentTypeID() == comp_id)
        {
            return (*it);
        }
        it++;
    }
    return NULL;
}

IComponent* CEntity::getComponent(const irr::core::stringc& comp_type)
{
    RunnableListIt it = this->Runnables.begin();
    while(it != Runnables.end())
    {
        if ((*it)->getComponentType() == comp_type)
        {
            return (*it);
        }
        it++;
    }
    return NULL;
}

/*
void CEntity::onUpdate(const float& delta, const unsigned int& timeMs)
{
    lockList();
    unlockList();
}
*/
CEntityManager* CEntity::getManager()
{
    return manager;
}

void CEntity::remove()
{
    getManager()->removeEntity(this);
}


IComponent* CEntity::addComponent(IComponent* comp)
{
    addHandler(comp);

    RunnableListIt it = this->Runnables.begin();
    while(it != Runnables.end())
    {
        (*it)->entityChanged();
        it++;
    }
    return comp;
}

IComponent* CEntity::addComponent(const irr::core::stringc& type)
{
    IComponent* comp = manager->create(type);
    if (comp)
        return addComponent(comp);
    return NULL;
}

const irr::core::stringc& CEntity::getType() const
{
    return type;
}

const irr::core::stringc& CEntity::getIdent() const
{
    return ident;
}

void CEntity::setIdent(const irr::core::stringc& i)
{
    ident = i;
}


void CEntity::onAdd(ComponentPtr comp)
{
    comp->onAdd(this);
}

void CEntity::onRemove(ComponentPtr comp)
{
    comp->onRemove(this);
}
