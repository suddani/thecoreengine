/*
  Copyright (C) 2011 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/
#include "CEntityManager.h"
#include "IComponent.h"
#include <core/XMLread.h>

#ifdef USE_SLB
#include <SLB/SLB.hpp>

CEntity* createEntity(CEntityManager* m)
{
    return m->createEntity();
}

CEntity* spawnEntity(CEntityManager* m, const char* type)
{
    return m->spawnEntity(type);
}

void registerCEntityManager(SLB::Manager* m)
{
    SLB::Class<CEntityManager, SLB::Instance::NoCopyNoDestroy>("EntityManager", m)
        .set("spawnEntity", spawnEntity)
        .set("createEntity", createEntity);
}
#endif
CEntityManager::CEntityManager(irr::ITimer* t)
{
    //ctor
    nlogl(1)<<"EntityManager created"<<nlendl;

    timer = t;
    if (timer)
        lasttime = t->getTime();
}

CEntityManager::~CEntityManager()
{
    //dtor
    clearList();
    componentSystems.clearList();

    nlogl(1)<<"EntityManager destroyed"<<nlendl;
}

void CEntityManager::update()
{
    /// TODO (sudi#1#): All Entity operations must take place in here!
    irr::u32 currentTime = timer ? timer->getTime() : lasttime;
    irr::f32 diff = (currentTime-lasttime)/1000.f;
    lasttime = currentTime;

    lockList();
    componentSystems.lockList();
    AppFrame::CRunnableList<ComponentSystemPtr>::RunnableListIt ct = componentSystems.Runnables.begin();
    while(ct != componentSystems.Runnables.end())
    {
        (*ct)->onUpdate(diff, currentTime);
        ct++;
    }
    componentSystems.unlockList();
    unlockList();
}

EntityPtr CEntityManager::spawnEntity(const irr::core::stringc& type){
    /// TODO (sudi#1#): Have to add a spawn system
    AppFrame::xml::XMLread xml((irr::core::stringc("gos/")+type+irr::core::stringc(".xml")).c_str());
    return spawnEntity(type, xml);
}

EntityPtr CEntityManager::spawnEntity(const irr::core::stringc& type, AppFrame::xml::XMLread& xml)
{
    AppFrame::xml::XMLread::Node* node = xml.getNode("entity");

    if (!node)
        return 0;

    CEntity* e = 0;

    irr::core::stringc parent = node->getValue("parent");

    irr::core::stringc ident = node->getValue("ident");

    if (parent != "")
        e = spawnEntity(parent);

    if (!e)
        e = createEntity();

    e->setIdent(ident);

    e->type = type;

    ///load components
    for (irr::u32 i=0;i<node->Nodes.size();++i)
    {
        AppFrame::xml::XMLread::Node* component = node->Nodes[i];
        IComponent* comp = e->getComponent(component->Name);
        if (!comp)
        {
            comp = create(component->Name);
            e->addComponent(comp);
        }
        if (!comp)
            continue;

        comp->deserialize(component);
    }
    return e;
}

EntityPtr CEntityManager::createEntity()
{
    EntityPtr e = new CEntity();
    addHandler(e);
    e->init(this);
    return e;
}

void CEntityManager::removeEntity(EntityPtr e)
{
    removeHandler(e);
}

EntityPtr CEntityManager::getEntity(const irr::core::stringc& ident)
{
    RunnableListIt it = Runnables.begin();
    while (it != Runnables.end())
    {
        if ((*it)->getIdent() == ident)
            return *it;
        it++;
    }
    return 0;
}

std::vector<EntityPtr> CEntityManager::getEntities(const irr::core::stringc& ident)
{
	std::vector<EntityPtr> collect;
    RunnableListIt it = Runnables.begin();
    while (it != Runnables.end())
    {
        if ((*it)->getIdent() == ident)
            collect.push_back(*it);
        it++;
    }
    return collect;
}


void CEntityManager::addComponentSystem(IComponentSystem* system)
{
    componentSystems.addHandler(system);
    registerType(system);
    system->setEntityManager(this);
}

void CEntityManager::onAdd(EntityPtr handler)
{
    nlogl(1)<<"Entity with id: "<<EntityID(handler).Id<<" added."<<nlendl;
}

void CEntityManager::onRemove(EntityPtr handler)
{
    nlogl(1)<<"Entity with id: "<<EntityID(handler).Id<<" removed."<<nlendl;
}
