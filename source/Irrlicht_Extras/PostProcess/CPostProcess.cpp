/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#include "Irrlicht_Extras/PostProcess/CPostProcess.h"
//#include <ILogger.h>
#include <ILog.h>
namespace AppFrame
{
    namespace graphic
    {
        static irr::core::stringc StandardVertShader =
            "varying vec2 vTexCoord;"
            "void main(void)"
            "{"
            " vec2 Position;"
            " Position.xy = sign(gl_Vertex.xy);"
            " gl_Position = vec4(Position.xy, 0.0, 1.0);"
            " vTexCoord = Position.xy *.5 + .5;"
            "}";
        static irr::core::stringc StandardFragShader =
            "uniform sampler2D tex0;"
            "uniform sampler2D tex1;"
            "uniform sampler2D tex2;"
            "uniform sampler2D tex3;"
            "varying vec2 vTexCoord;"
            "void main(void)"
            "{"
            "	gl_FragColor = texture2D(tex0, vTexCoord);"//+texture2D(texture1, vTexCoord)+texture2D(texture2, vTexCoord)+texture2D(texture3, vTexCoord);"
            "}";

		static irr::core::stringc DepthVertShader =
			"struct VS_OUTPUT"
            "{"
            "	vec4 Position;"
            "   vec4 Color;"
            "};"
            "uniform mat4 mWorldViewProj;"
            "VS_OUTPUT vertexMain( in vec3 Position )"
            "{"
            "    vec4 hpos;"
            "    VS_OUTPUT OUT;"
            "    hpos = (mWorldViewProj * vec4( Position.x , Position.y , Position.z , 1.00000) );"
			"   OUT.Color = hpos;"
			"   OUT.Position = hpos;"
			"   return OUT;"
            "}"
            "void main() "
            "{"
            "    VS_OUTPUT xlat_retVal;"
            "    xlat_retVal = vertexMain( vec3(gl_Vertex));"
            "    gl_Position = vec4( xlat_retVal.Position);"
            "    gl_TexCoord[0] = vec4( xlat_retVal.Color);"
            "}";
		static irr::core::stringc DepthFragShader =
			"uniform float MaxD;"
			"void main()"
			"{"
			"	float depth = gl_TexCoord[0].z / MaxD;"
			"	float mulDepth = depth * 256.0f;"
			"	float flooredDepth = floor(mulDepth);"
			"	gl_FragColor = vec4(flooredDepth / 256.0f, (mulDepth - flooredDepth),0.0,0.0);"
			//"	gl_FragColor = vec4(depth, depth, depth, 1.0);"
			"}";
        class CopyCallback : public irr::video::IShaderConstantSetCallBack
        {
        public:
            virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32 userData)
            {
                //set Textures
                irr::u32 id0 = 0;
                irr::u32 id1 = 1;
                irr::u32 id2 = 2;
                irr::u32 id3 = 3;
                services->setVertexShaderConstant("tex0", reinterpret_cast<irr::f32*>(&id0),1);
                services->setVertexShaderConstant("tex1", reinterpret_cast<irr::f32*>(&id1),1);
                services->setVertexShaderConstant("tex2", reinterpret_cast<irr::f32*>(&id2),1);
                services->setVertexShaderConstant("tex3", reinterpret_cast<irr::f32*>(&id3),1);
            }
        };

        class DepthShaderCB : public irr::video::IShaderConstantSetCallBack
        {
        public:
            DepthShaderCB(void) : FarValue(new irr::f32(0)) {};
            ~DepthShaderCB()
            {
                delete FarValue;
            };
            irr::f32* FarValue;
            irr::core::matrix4 worldViewProj;

            virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32 userData)
            {
                irr::video::IVideoDriver* driver = services->getVideoDriver();

                worldViewProj = driver->getTransform(irr::video::ETS_PROJECTION);
                worldViewProj *= driver->getTransform(irr::video::ETS_VIEW);
                worldViewProj *= driver->getTransform(irr::video::ETS_WORLD);

                services->setVertexShaderConstant("mWorldViewProj", worldViewProj.pointer(), 16);

                services->setPixelShaderConstant("MaxD", FarValue, 1);
            }
        };

        void CPostProcess::SetupCopyMaterial(void)
        {
            irr::video::IVideoDriver* driver = Manager->getVideoDriver();
            irr::video::IGPUProgrammingServices* gpu = driver->getGPUProgrammingServices();

            CopyCallback* p = new CopyCallback();

            irr::video::E_MATERIAL_TYPE mat = (irr::video::E_MATERIAL_TYPE)gpu->addHighLevelShaderMaterial
                                              (
                                                  StandardVertShader.c_str(), "main", irr::video::EVST_VS_3_0,
                                                  StandardFragShader.c_str(), "main", irr::video::EPST_PS_3_0,
                                                  p, irr::video::EMT_SOLID_2_LAYER
                                              );
            CopyMaterial.MaterialType = mat;
            CopyMaterial.setTexture(0, getRenderTarget());
            CopyMaterial.setTexture(1, getRenderTarget());
            CopyMaterial.setTexture(2, getRenderTarget());
            CopyMaterial.setTexture(3, getRenderTarget());
            CopyMaterial.Lighting = false;


            DepthShaderCB* depthCall = new DepthShaderCB();

            FarValue = depthCall->FarValue;

            DepthMaterial = gpu->addHighLevelShaderMaterial
                            (
                                DepthVertShader.c_str(), "main", irr::video::EVST_VS_2_0,
                                DepthFragShader.c_str(), "main", irr::video::EPST_PS_2_0,
                                depthCall, irr::video::EMT_SOLID
                            );

            Vertices[0] = irr::video::S3DVertex(-1.0f, -1.0f, 0.0f,1,1,0, irr::video::SColor(255,0,255,255), 0.0f, 1.0f);
            Vertices[1] = irr::video::S3DVertex(-1.0f,  1.0f, 0.0f,1,1,0, irr::video::SColor(255,0,255,255), 0.0f, 0.0f);
            Vertices[2] = irr::video::S3DVertex( 1.0f,  1.0f, 0.0f,1,1,0, irr::video::SColor(255,0,255,255), 1.0f, 0.0f);
            Vertices[3] = irr::video::S3DVertex( 1.0f, -1.0f, 0.0f,1,1,0, irr::video::SColor(255,0,255,255), 1.0f, 1.0f);
            Vertices[4] = irr::video::S3DVertex(-1.0f, -1.0f, 0.0f,1,1,0, irr::video::SColor(255,0,255,255), 0.0f, 1.0f);
            Vertices[5] = irr::video::S3DVertex( 1.0f,  1.0f, 0.0f,1,1,0, irr::video::SColor(255,0,255,255), 1.0f, 0.0f);
        }

        CPostProcess::CPostProcess(irr::u32 width, irr::u32 height, const irr::c8* effectName, irr::IrrlichtDevice* device, const irr::video::ECOLOR_FORMAT& cf) : EffectName(effectName), Manager(device->getSceneManager()), Width(width), Height(height), Device(device), Active(false), RenderDepthPass(false), DepthPassTexture(NULL)
        {
            //ctor
            //FinalRenderTarget = Manager->getVideoDriver()->createRenderTargetTexture(irr::core::dimension2d< irr::s32 >(width, height), effectName);
            addRenderTarget(effectName, width, height, true, false, cf);
            Camera = NULL;
            SetupCopyMaterial();
        }

        irr::video::ECOLOR_FORMAT getColorFormatFromString(const irr::core::stringc& format)
        {
            if (format == "A1R5G5B5")
                return irr::video::ECF_A1R5G5B5;
            else if (format == "R5G6B5")
                return irr::video::ECF_R5G6B5;
            else if (format == "R8G8B8")
                return irr::video::ECF_R8G8B8;
            else if (format == "A8R8G8B8")
                return irr::video::ECF_A8R8G8B8;
            else if (format == "R16F")
                return irr::video::ECF_R16F;
            else if (format == "G16R16F")
                return irr::video::ECF_G16R16F;
            else if (format == "A16B16G16R16F")
                return irr::video::ECF_A16B16G16R16F;
            else if (format == "R32F")
                return irr::video::ECF_R32F;
            else if (format == "G32R32F")
                return irr::video::ECF_G32R32F;
            else if (format == "A32B32G32R32F")
                return irr::video::ECF_A32B32G32R32F;

            return irr::video::ECF_UNKNOWN;
        }

        CPostProcess::CPostProcess(const irr::c8* effectFile, irr::IrrlichtDevice* device)
        {
            //FinalRenderTarget = NULL;
            Camera = NULL;
            DepthPassTexture = NULL;

            Manager = device->getSceneManager();
            Device = device;
            Active = false;
            RenderDepthPass = false;

            irr::io::IXMLReaderUTF8* reader = NULL;

            nlog<<"Load EffecFile: "<<effectFile<<nlflush;

            reader = device->getFileSystem()->createXMLReaderUTF8(effectFile);

            if (!reader)
            {
                nlog<<"Could not load file"<<nlflush;
                return;
            }

            bool postprocess = false;
            bool pipline = false;

            nlog<<"Load PostProcess Effect...";

            while (reader && reader->read())
            {
                //printf("begin read\n");
                switch (reader->getNodeType())
                {
                case irr::io::EXN_TEXT:
                    // in this xml file, the only text which
                    // occurs is the messageText
                    //messageText = xml->getNodeData();
                    //xml->getAttributeValue("caption");
                    break;
                case irr::io::EXN_ELEMENT:
                    if (irr::core::stringc("PostProcess") == reader->getNodeName())
                    {
                        irr::core::stringc name = reader->getAttributeValueSafe("name");
                        irr::u32 width = reader->getAttributeValueAsInt("width");
                        irr::u32 height = reader->getAttributeValueAsInt("height");
                        irr::core::stringc color = reader->getAttributeValueSafe("colorformat");

                        //TODO: ok this is a problem i will change it to the filename now
                        //TODO: ok changed it back to name bc of inconsitensy
                        EffectName = name;//effectFile;

                        //TODO: ok this almost killed me
                        addRenderTarget(name.c_str(), width, height, true, true, getColorFormatFromString(color));

                        SetupCopyMaterial();

                        postprocess = true;
                    }

                    if (postprocess)
                    {
                        if (irr::core::stringc("RenderTarget") == reader->getNodeName())
                        {
                            irr::core::stringc name = reader->getAttributeValueSafe("name");
                            irr::u32 width = reader->getAttributeValueAsInt("width");
                            irr::u32 height = reader->getAttributeValueAsInt("height");
                            irr::core::stringc color = reader->getAttributeValueSafe("colorformat");

                            irr::core::stringc render2targetS = reader->getAttributeValueSafe("render2target");
                            bool render2target = false;
                            if (render2targetS == "true")
                                render2target = true;

                            irr::core::stringc copy2targetS = reader->getAttributeValueSafe("copy2target");
                            bool copy2target = false;
                            if (copy2targetS == "true")
                                copy2target = true;

                            addRenderTarget(name.c_str(), width, height, render2target, copy2target, getColorFormatFromString(color));
                            break;
                        }
                        else if (irr::core::stringc("Pipeline") == reader->getNodeName())
                        {
                        	pipline = true;
                            irr::core::stringc vertex = reader->getAttributeValueSafe("vertex");

                            irr::core::stringc fragment = reader->getAttributeValueSafe("fragment");

                            irr::core::stringc tex0 = reader->getAttributeValueSafe("tex0");
                            irr::core::stringc tex1 = reader->getAttributeValueSafe("tex1");
                            irr::core::stringc tex2 = reader->getAttributeValueSafe("tex2");
                            irr::core::stringc tex3 = reader->getAttributeValueSafe("tex3");
                            irr::core::stringc target = reader->getAttributeValueSafe("target");

                            addRenderPipeline(vertex, fragment, target, tex0, tex1, tex2, tex3);
                            break;
                        }
                        else if (irr::core::stringc("DepthPass") == reader->getNodeName())
                        {
                            irr::core::stringc allnodesS = reader->getAttributeValueSafe("allnodes");
                            bool allnodes = false;
                            irr::core::stringc target = reader->getAttributeValueSafe("target");
                            if (allnodesS == "true")
                                allnodes = true;

                            setDepthPass(getRenderTarget(target.c_str()), allnodes, reader->getAttributeValueAsFloat("maxfar"));
                            break;
                        }
                        if (pipline)
                        {
                        	if (irr::core::stringc("Constant") == reader->getNodeName())
                        	{
                        		RenderPipelines[RenderPipelines.size()-1]->addConstant(new ShaderConstant(reader->getAttributeValueSafe("name"), reader->getAttributeValueSafe("type"), reader->getAttributeValueSafe("value"), Manager));
                        	}
                        }
                    }
                    break;
                case irr::io::EXN_ELEMENT_END:
                    if (irr::core::stringc("PostProcess") == reader->getNodeName())
                    {
                        postprocess = false;
                    }
                    if (postprocess)
						if (irr::core::stringc("Pipeline") == reader->getNodeName())
                        	pipline = false;
                    break;
                default:
                    break;
                }
                //printf("end read\n");

                //printf("0\n");
            }

            //printf("Delete Reader\n");

            if (reader)
                delete reader;
            nlog<<"Done\n";
        }

        CPostProcess::~CPostProcess()
        {
            //dtor
            //if (FinalRenderTarget)
            //FinalRenderTarget->drop();

            for (irr::u32 i = 0; i < RenderTargets.size(); i++)
            {
                //RenderTargets[i]->drop();
                i++;
            }

            //for (irr::u32 i = 0; i < RenderPipelines.size(); i++)
            //RenderPipelines[0]->drop();
        }

        void CPostProcess::addRenderTarget(const irr::c8* name, irr::u32 width, irr::u32 height, bool render2target, bool copy2target, const irr::video::ECOLOR_FORMAT& cf)
        {
            //RenderTargets.push_back(Manager->getVideoDriver()->createRenderTargetTexture(irr::core::dimension2d< irr::s32 >(width, height), name));
            RenderTargets.push_back(Manager->getVideoDriver()->addRenderTargetTexture(irr::core::dimension2d< irr::u32 >(width, height), name, cf));

            RenderTargetNames.push_back(name);
            Render2Target.push_back(render2target);
            Copy2Target.push_back(copy2target);

            nlog<<"added RenderTarget"<<nlflush;
        }

        IPostProcess::Pipline* CPostProcess::addRenderPipeline(irr::core::stringc vert, irr::core::stringc frag, irr::core::stringc target, irr::video::ITexture* tex0, irr::video::ITexture* tex1, irr::video::ITexture* tex2, irr::video::ITexture* tex3)
        {
        	irr::video::IVideoDriver* driver = Manager->getVideoDriver();
            irr::video::IGPUProgrammingServices* gpu = driver->getGPUProgrammingServices();

            irr::video::ITexture* rtarget = getRenderTarget(target.c_str());

            Pipline* p = new Pipline(rtarget, tex0, tex1, tex2, tex3);

            //check shader
            irr::io::IReadFile* vertF = NULL;
            bool vertFD = false;
            irr::io::IReadFile* fragF = NULL;
            bool fragFD = false;

            if (vert == "standard")
            {
                vertF = Device->getFileSystem()->createMemoryReadFile ((void*)StandardVertShader.c_str(), StandardVertShader.size(), "standard", false);
                vertFD = true;
            }
            else
            {
                if (vert[vert.size()-4] == 'v' && vert[vert.size()-3] == 'e' && vert[vert.size()-2] == 'r' && vert[vert.size()-1] == 't')
                    vertF = Device->getFileSystem()->createAndOpenFile(vert.c_str());
                else
                {
                    vertF = Device->getFileSystem()->createMemoryReadFile ((void*)vert.c_str(), vert.size(), "nstandard", false);
                    vertFD = true;
                }
            }

            if (frag == "standard")
            {
                fragF = Device->getFileSystem()->createMemoryReadFile ((void*)StandardFragShader.c_str(), StandardFragShader.size(), "standard", false);
                fragFD = true;
            }
            else
            {
                if (frag[frag.size()-4] == 'f' && frag[frag.size()-3] == 'r' && frag[frag.size()-2] == 'a' && frag[frag.size()-1] == 'g')
                    fragF = Device->getFileSystem()->createAndOpenFile(frag.c_str());
                else
                {
                    fragF = Device->getFileSystem()->createMemoryReadFile ((void*)frag.c_str(), frag.size(), "nstandard", false);
                    fragFD = true;
                }
            }

            irr::video::E_MATERIAL_TYPE mat = (irr::video::E_MATERIAL_TYPE)gpu->addHighLevelShaderMaterialFromFiles
                                              (
                                                  vertF, "main", irr::video::EVST_VS_3_0,
                                                  fragF, "main", irr::video::EPST_PS_3_0,
                                                  p, irr::video::EMT_SOLID
                                              );
            p->setShader(mat);

            RenderPipelines.push_back(p);

            //p->drop();

            if (fragFD)
                fragF->drop();
            if (vertFD)
                vertF->drop();

            nlog<<"added RenderPipeline"<<nlflush;
            return p;
        }

        IPostProcess::Pipline* CPostProcess::addRenderPipeline(irr::core::stringc vert, irr::core::stringc frag, irr::core::stringc target, irr::core::stringc tex0, irr::core::stringc tex1, irr::core::stringc tex2, irr::core::stringc tex3)
        {
        	irr::video::IVideoDriver* driver = Manager->getVideoDriver();

            irr::video::ITexture* texture0 = getRenderTarget(tex0.c_str());
            if (!texture0)
                texture0 = driver->getTexture(tex0.c_str());

            irr::video::ITexture* texture1 = getRenderTarget(tex1.c_str());
            if (!texture1)
                texture1 = driver->getTexture(tex1.c_str());

            irr::video::ITexture* texture2 = getRenderTarget(tex2.c_str());
            if (!texture2)
                texture2 = driver->getTexture(tex2.c_str());

            irr::video::ITexture* texture3 = getRenderTarget(tex3.c_str());
            if (!texture3)
                texture3 = driver->getTexture(tex3.c_str());

			return addRenderPipeline(vert, frag, target, texture0, texture1, texture2, texture3);
        }

        irr::video::ITexture* CPostProcess::getRenderTarget(const irr::c8* name)
        {
            for (irr::u32 i = 0; i < RenderTargets.size(); i++)
            {
                if (RenderTargetNames[i] == name)
                {
                    nlog<<"found RenderTarget"<<nlflush;
                    return RenderTargets[i];
                }
            }
            return NULL;
        }

        irr::video::ITexture* CPostProcess::getRenderTarget(void)
        {
            if (RenderTargets.size() > 0)
                return RenderTargets[0];

            return NULL;
        }

        irr::video::ITexture* CPostProcess::getDepthPass(void)
        {
            return DepthPassTexture;
        }

        void CPostProcess::setMainCamera(irr::scene::ICameraSceneNode* camera)
        {
        	Camera = camera;
        }

        void CPostProcess::Render(irr::video::ITexture* renderTarget, bool renderScene)
        {
            if (RenderTargets.size() == 0)
                return;

            irr::u16 indices[] = {0,1,2,3,4,5};
            irr::core::matrix4 mat;
            mat.makeIdentity();
            irr::video::IVideoDriver* driver = Manager->getVideoDriver();

            if (RenderDepthPass)
                renderDepthPass();

            //get scene
            if (renderScene)
            {
                irr::scene::ICameraSceneNode* sceneCamera = Manager->getActiveCamera();
                if (Camera)
                    Manager->setActiveCamera(Camera);
                driver->setRenderTarget(RenderTargets[0], true,true, irr::video::SColor(255,0,0,0));
                Manager->drawAll();
                //Device->getGUIEnvironment()->drawAll();

                Manager->setActiveCamera(sceneCamera);

                for (irr::u32 i = 1; i < RenderTargets.size(); i++)
                {
                    if (Render2Target[i])
                    {
                        driver->setRenderTarget(RenderTargets[i], true,true, irr::video::SColor(255,0,0,0));
                        Manager->drawAll();
                        //printf("render Scene\n");
                    }
                }
            }

            //driver->setTransform(irr::video::ETS_WORLD, mat);

            //copy image to all RenderTargets
            for (irr::u32 i = 1; i < RenderTargets.size(); i++)
            {
                if (!Render2Target[i] && renderScene && Copy2Target[i])
                {
                    //printf("Copy Image\n");
                    driver->setRenderTarget(RenderTargets[i], true,true, irr::video::SColor(255,0,0,0));
                    driver->setMaterial(CopyMaterial);
                    driver->drawIndexedTriangleList(&Vertices[0], 6, &indices[0], 2);
                }
            }

            //Render Pipelines
            for (irr::u32 i = 0;i<RenderPipelines.size();i++)
            {
                driver->setRenderTarget(RenderPipelines[i]->getTarget(), true,true, irr::video::SColor(255,0,0,0));
                RenderPipelines[i]->setCam(Camera);
                driver->setMaterial(RenderPipelines[i]->getMaterial());
                driver->drawIndexedTriangleList(&Vertices[0], 6, &indices[0], 2);

                //printf("render Pipeline\n");

                //printf("Draw Pipeline\n");

                if (i == RenderPipelines.size()-1)
                {
                    driver->setRenderTarget(renderTarget, true,true, irr::video::SColor(255,0,0,0));
                    driver->setMaterial(RenderPipelines[i]->getMaterial());
                    driver->drawIndexedTriangleList(&Vertices[0], 6, &indices[0], 2);
                    //printf("Render FinalImage\n");
                }
            }
            if (RenderPipelines.size() == 0)
            {
                //AppFrame::ILogger::log("COPY");
                driver->setRenderTarget(renderTarget, true,true, irr::video::SColor(255,0,0,0));
                driver->setMaterial(CopyMaterial);
                driver->drawIndexedTriangleList(&Vertices[0], 6, &indices[0], 2);
            }
        }

        bool CPostProcess::isActive(void)
        {
            return Active;
        }

        void CPostProcess::setActive(bool active)
        {
            Active = active;
        }

        void CPostProcess::addNode2DepthPass(irr::scene::ISceneNode* node)
        {
        	if (node->getType() != irr::scene::ESNT_BILLBOARD && node->getType() != irr::scene::ESNT_SHADOW_VOLUME
				&& node->getType() != irr::scene::ESNT_PARTICLE_SYSTEM && node->getType() != irr::scene::ESNT_CAMERA
				&& node->getType() != irr::scene::ESNT_CAMERA_FPS && node->getType() != irr::scene::ESNT_UNKNOWN)
			{
			}
			else
				return;
            for (irr::u32 i=0;i<DepthNodes.size();i++)
                if (DepthNodes[i].getNode() == node)
                    return;
            node->grab();
            DepthNodes.push_back(DepthNode(node));
        }

        void CPostProcess::removeNodeFromDepthPass(irr::scene::ISceneNode* node)
        {
            for (irr::u32 i=0;i<DepthNodes.size();i++)
                if (DepthNodes[i].getNode() == node)
                {
                    node->drop();
                    DepthNodes.erase(i);
                    return;
                }
        }

        void iter(CPostProcess* post, irr::scene::ISceneNode* Node)
        {
            irr::core::list<irr::scene::ISceneNode*>::ConstIterator it = Node->getChildren().begin();
            while (it!=Node->getChildren().end())
            {
                post->addNode2DepthPass(*it);
                iter(post, *it);
                it++;
            }
        }

        void CPostProcess::addAllNodes2DepthPass(void)
        {
            iter(this, Manager->getRootSceneNode());
        }

        void CPostProcess::renderDepthPass(void)
        {
            irr::scene::ICameraSceneNode* cam = Camera;



			//printf("render depthpass with ");

            if (!cam)
            {
                cam = Manager->getActiveCamera();
				//printf("scene Camera ");
            }
            else
            {
				//printf("effect Camera ");
            }

            if (!cam)
                return;

			//printf("now\n");

            Manager->getVideoDriver()->setTransform(irr::video::ETS_VIEW, cam->getViewMatrix());
            Manager->getVideoDriver()->setTransform(irr::video::ETS_PROJECTION, cam->getProjectionMatrix());
            Manager->getVideoDriver()->setTransform(irr::video::ETS_WORLD, cam->getAbsoluteTransformation());

            Manager->getVideoDriver()->setRenderTarget(DepthPassTexture,true,true,irr::video::SColor(255,128,128,128));

            for (irr::u32 i = 0;i < DepthNodes.size();++i)
            {
                DepthNodes[i].getNode()->setMaterialType((irr::video::E_MATERIAL_TYPE)DepthMaterial);

                DepthNodes[i].getNode()->render();

                for (irr::u32 a = 0;a < DepthNodes[i].getNode()->getMaterialCount();++a)
					DepthNodes[i].getNode()->getMaterial(a).MaterialType = (irr::video::E_MATERIAL_TYPE)DepthNodes[i].getMaterialType(a);
            }
        }

        void CPostProcess::setDepthPass(irr::video::ITexture* target, bool allnodes, float maxfar)
        {
        	RenderDepthPass = true;
			if (allnodes)
				addAllNodes2DepthPass();
			DepthPassTexture = target;
			*FarValue = maxfar;
        }

        const irr::core::stringc& CPostProcess::getName(void)
        {
            return EffectName;
        }
    }
}
