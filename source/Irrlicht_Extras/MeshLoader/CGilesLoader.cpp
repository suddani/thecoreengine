/*
Copyright (c) 2009 Daniel Sudmann

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
   2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
   3. This notice may not be removed or altered from any source distribution.
*/
#include "Irrlicht_Extras/MeshLoader/CGilesLoader.h"
#include "Irrlicht_Extras/MeshLoader/GilesTypes.h"
namespace irr
{
    namespace scene
    {
        CGilesLoader::CGilesLoader(IrrlichtDevice* device) : Driver(device->getVideoDriver()), Logger(device->getLogger())
        {
            //ctor
        }

        CGilesLoader::~CGilesLoader()
        {
            //dtor
            clear();
        }

        IAnimatedMesh* CGilesLoader::createMesh (io::IReadFile *file)
        {
            clear();

            if (!readHeader(file))
                return NULL;

            s32 ChunkID = 0;
            s32 Size = 0;
            while (file->getPos() < file->getSize())
            {
                file->read(&ChunkID, 4);
                file->read(&Size, 4);
                switch (ChunkID)
                {
                case giles::GLS_AUTHOR:
                {
                    if (skipNoParent(giles::GLS_HEADER, Size, file))
                        break;
                    core::stringc Author;
                    readString(Author, file);
                    //AppFrame::ILogger::log("Author: %s", Author.c_str());
                    break;
                }
                case giles::GLS_MATERIALS:
                {
                    if (skipNoParent(giles::GLS_HEADER, Size, file))
                        break;
                    //AppFrame::ILogger::log("Materials");
                    pushParent(giles::GLS_MATERIALS);
                    loadMaterials(file, Size);
                    pullParent();
                    break;
                }
                case giles::GLS_TEXTURES:
                {
                    if (skipNoParent(giles::GLS_HEADER, Size, file))
                        break;
                    //AppFrame::ILogger::log("Textures");
                    pushParent(giles::GLS_TEXTURES);
                    loadTextures(file, Size);
                    pullParent();
                    break;
                }
                case giles::GLS_LIGHTMAPS:
                {
                    if (skipNoParent(giles::GLS_HEADER, Size, file))
                        break;
                    //AppFrame::ILogger::log("Lightmaps");
                    pushParent(giles::GLS_LIGHTMAPS);
                    loadLightmaps(file, Size);
                    pullParent();
                    break;
                }
                case giles::GLS_MODELS:
                {
                    if (skipNoParent(giles::GLS_HEADER, Size, file))
                        break;
                    //AppFrame::ILogger::log("Models");
                    pushParent(giles::GLS_MODELS);
                    loadModels(file, Size);
                    pullParent();
                    break;
                }
                default:
                    //skip unhandled chunks
                    skip(Size, file);
                    break;
                }
            }

            SAnimatedMesh*  AnimMesh = NULL;
            if (Mesh)
            {
                Mesh->recalculateBoundingBox();
                AnimMesh = new SAnimatedMesh(Mesh);
                AnimMesh->recalculateBoundingBox();
            }
            clear();
            return AnimMesh;
        }

        bool CGilesLoader::isALoadableFileExtension(const irr::core::string<irr::c8, irr::core::irrAllocator<irr::c8> >& fileName) const
        {
            return strstr(fileName.c_str(), ".gls");
        }

        bool CGilesLoader::readHeader(io::IReadFile *file)
        {
            s32 ID = 0;
            s32 Size = 0; //actually this shouldn't be here
            f32 Version = 0.0;
            core::stringc Name;
            file->read(&ID, 4);
            file->read(&Size, 4); //actually this shouldn't be here
            //AppFrame::ILogger::log("ID: %i", ID);
            readString(Name, file);
            //AppFrame::ILogger::log("Name: %s", Name.c_str());
            file->read(&Version, 4);
            //AppFrame::ILogger::log("Version: %f", Version);

            if (ID != giles::GLS_HEADER)
            {
                //AppFrame::ILogger::log("No Header");
                return false;
            }
            if (Version != 1.01f)
            {
                //AppFrame::ILogger::log("Wrong Version");
                return false;
            }
            if (Name != "gile[s]")
            {
                //AppFrame::ILogger::log("Wrong File");
                return false;
            }

            ParentChunk.push_back(giles::GLS_HEADER);

            return true;
        }

        void CGilesLoader::loadMaterials(io::IReadFile *file, s32 ChunkSize)
        {
            s32 newPos = file->getPos()+ChunkSize;
            s32 ChunkID = 0;
            s32 Size = 0;
            video::SMaterial CurrentMaterial;
            while (file->getPos() < newPos)
            {
                file->read(&ChunkID, 4);
                file->read(&Size, 4);
                switch (ChunkID)
                {
                case giles::GLS_MAT:
                    if (currentParent() == giles::GLS_MAT)
                    {
                        //push Material
                        Materials.push_back(CurrentMaterial);
                        pullParent();
                        CurrentMaterial = video::SMaterial();
                    }
                    pushParent(giles::GLS_MAT);
                    break;
                case giles::GLS_MAT_RED:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;
                    u32 data;
                    file->read(&data, 4);
                    CurrentMaterial.DiffuseColor.setRed(data);
                    break;
                }
                case giles::GLS_MAT_GREEN:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;
                    u32 data;
                    file->read(&data, 4);
                    CurrentMaterial.DiffuseColor.setGreen(data);
                    break;
                }
                case giles::GLS_MAT_BLUE:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;
                    u32 data;
                    file->read(&data, 4);
                    CurrentMaterial.DiffuseColor.setBlue(data);
                    break;
                }
                case giles::GLS_MAT_ALPHA:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;
                    u32 data;
                    file->read(&data, 4);
                    CurrentMaterial.DiffuseColor.setAlpha(data);
                    break;
                }
                case giles::GLS_MAT_SHININESS:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;
                    f32 data;
                    file->read(&data, 4);
                    CurrentMaterial.Shininess = data;
                    break;
                }
                case giles::GLS_MAT_BLEND:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;
                    s32 data;
                    file->read(&data, 4);
                    if (data == 1)
                        CurrentMaterial.MaterialType = video::EMT_TRANSPARENT_ALPHA_CHANNEL;
                    /*
                    else if (data == 2)
                        CurrentMaterial.MaterialType = video::EMT_TRANSPARENT_VERTEX_ALPHA;
                    else if (data == 3)
                        CurrentMaterial.MaterialType = video::EMT_TRANSPARENT_ADD_COLOR;
                    */

                    break;
                }
                case giles::GLS_MAT_FX:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;
                    s32 data;
                    file->read(&data, 4);
                    if (data & 16)
                        CurrentMaterial.BackfaceCulling = false;
                    break;
                }
                case giles::GLS_MAT_LIGHTMETHOD:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;
                    c8 data;
                    file->read(&data, 1);
                    switch (data)
                    {
                    case 0:
                        CurrentMaterial.Lighting = false;
                        break;
                    case 1:
                        break;
                    case 2:
                        CurrentMaterial.MaterialType = video::EMT_LIGHTMAP;
                        CurrentMaterial.Lighting = true;
                        break;
                    default:
                        //AppFrame::log("Unknown LightningMethod: %i", data);
                        break;
                    }
                    break;
                }
                case giles::GLS_MAT_LIGHTMAP:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;
                    s16 data;
                    file->read(&data, 2);
                    data -=1;
                    if ((s16)Lightmaps.size() > data)
                    {
                        CurrentMaterial.setTexture(1, Lightmaps[data]);
                    }
                    break;
                }
                case giles::GLS_MAT_TEXLAYER:
                {
                    if (skipNoParent(giles::GLS_MAT, Size, file))
                        break;

                    c8 layer;
                    s16 data;
                    file->read(&layer, 1);
                    file->read(&data, 2);
                    if (layer == 0 && (s16)Textures.size() > data)
                    {
                        ////AppFrame::ILogger::log("SetTextureLayer 0");
                        CurrentMaterial.setTexture(0, Textures[data]);
                    }
                    break;
                }
                default:
                    skip(Size, file);
                    break;
                }
            }
            if (currentParent() == giles::GLS_MAT)
            {
                //push Material
                Materials.push_back(CurrentMaterial);
                pullParent();
            }
        }

        void CGilesLoader::loadTextures(io::IReadFile *file, s32 ChunkSize)
        {
            s32 newPos = file->getPos()+ChunkSize;
            s32 ChunkID = 0;
            s32 Size = 0;
            video::ITexture* CurrentTexture = NULL;
            while (file->getPos() < newPos)
            {
                file->read(&ChunkID, 4);
                file->read(&Size, 4);
                switch (ChunkID)
                {
                case giles::GLS_TEX:
                    if (currentParent() == giles::GLS_TEX)
                    {
                        //push Material
                        if (CurrentTexture)
                            Textures.push_back(CurrentTexture);
                        pullParent();
                        CurrentTexture = NULL;
                    }
                    pushParent(giles::GLS_TEX);
                    break;
                case giles::GLS_TEX_FILE:
                {
                    if (skipNoParent(giles::GLS_TEX, Size, file))
                        break;
                    core::stringc data;
                    readString(data, file);
                    CurrentTexture = Driver->getTexture(data.c_str());
                    if (!CurrentTexture)
                    {
                        s32 pos = data.findLastChar("/\\", 2);
                        core::stringc filename = data.subString(pos+1, data.size()-pos-1);
                        if (pos > 0)
                        {
                            CurrentTexture = Driver->getTexture(filename.c_str());
                        }
                        if (!CurrentTexture)
                        {
                            data = file->getFileName();
                            pos = data.findLastChar("/\\", 2);
                            data = data.subString(0, pos+1);
                            data.append(filename);
                            CurrentTexture = Driver->getTexture(data.c_str());
                        }
                    }
                    break;
                }
                default:
                    skip(Size, file);
                    break;
                }
            }
            if (currentParent() == giles::GLS_TEX)
            {
                //push Material
                if (CurrentTexture)
                    Textures.push_back(CurrentTexture);
                pullParent();
            }
        }

        void CGilesLoader::loadLightmaps(io::IReadFile *file, s32 ChunkSize)
        {
            s32 newPos = file->getPos()+ChunkSize;
            s32 ChunkID = 0;
            s32 Size = 0;

            s16 Width = 0;
            s16 Height = 0;
            core::stringc Name;
            video::ITexture* CurrentTexture = NULL;
            while (file->getPos() < newPos)
            {
                file->read(&ChunkID, 4);
                file->read(&Size, 4);
                switch (ChunkID)
                {
                case giles::GLS_LMAP:
                    if (currentParent() == giles::GLS_LMAP)
                    {
                        //push Material
                        if (CurrentTexture)
                            Lightmaps.push_back(CurrentTexture);
                        pullParent();
                        CurrentTexture = NULL;
                    }
                    pushParent(giles::GLS_LMAP);
                    break;
                case giles::GLS_LMAP_WIDTH:
                {
                    if (skipNoParent(giles::GLS_LMAP, Size, file))
                        break;
                    file->read(&Width, 2);
                    break;
                }
                case giles::GLS_LMAP_HEIGHT:
                {
                    if (skipNoParent(giles::GLS_LMAP, Size, file))
                        break;
                    file->read(&Height, 2);
                    break;
                }
                case giles::GLS_LMAP_NAME:
                {
                    if (skipNoParent(giles::GLS_LMAP, Size, file))
                        break;
                    Name = "";
                    readString(Name, file);
                    break;
                }
                case giles::GLS_LMAP_DATA:
                {
                    if (skipNoParent(giles::GLS_LMAP, Size, file))
                        break;
                    core::stringc LightmapFilename = file->getFileName();
                    LightmapFilename.append(".");
                    LightmapFilename.append(Name.c_str());
                    CurrentTexture = Driver->getTexture(LightmapFilename.c_str());

                    c8* data = new c8[Width*Height*3];
                    file->read(data, Width*Height*3);
                    if (!CurrentTexture)
                    {
                        video::IImage* picture = Driver->createImageFromData(video::ECF_R8G8B8, core::dimension2d< u32 >(Width, Height), data, true, true);

                        CurrentTexture = Driver->addTexture(LightmapFilename.c_str(), picture);

                        //free image data
                        picture->drop();
                    }
                    else
                        delete [] data;

                    //CurrentTexture->regenerateMipMapLevels();
                    break;
                }
                default:
                    skip(Size, file);
                    break;
                }
            }
            if (currentParent() == giles::GLS_LMAP)
            {
                //push Material
                if (CurrentTexture)
                    Lightmaps.push_back(CurrentTexture);
                CurrentTexture = NULL;
                pullParent();
            }
        }

        void CGilesLoader::loadModels(io::IReadFile *file, s32 ChunkSize)
        {
            s32 newPos = file->getPos()+ChunkSize;
            s32 ChunkID = 0;
            s32 Size = 0;
            while (file->getPos() < newPos)
            {
                file->read(&ChunkID, 4);
                file->read(&Size, 4);
                switch (ChunkID)
                {
                case giles::GLS_MODEL:
                    pushParent(giles::GLS_MODEL);
                    VertexTransfrom.makeIdentity();
                    loadModel(file, Size);
                    pullParent();
                    break;
                    /*
                    case giles::GLS_TEX_FILE:
                    {
                        if (skipNoParent(this, giles::GLS_TEX, Size, file))
                            break;
                        core::stringc data;
                        readString(data, file);
                        CurrentTexture = Driver->getTexture(data.c_str());
                        break;
                    }
                    */
                default:
                    skip(Size, file);
                    break;
                }
            }
        }

        void CGilesLoader::loadModel(io::IReadFile *file, s32 ChunkSize)
        {
            s32 newPos = file->getPos()+ChunkSize;
            s32 ChunkID = 0;
            s32 Size = 0;
            while (file->getPos() < newPos)
            {
                file->read(&ChunkID, 4);
                file->read(&Size, 4);
                switch (ChunkID)
                {
                case giles::GLS_MODEL:
                    if (currentParent() != giles::GLS_MODEL && currentParent() != giles::GLS_PIVOT)
                    {
                        VertexTransfrom.makeIdentity();
                    }
                    pushParent(giles::GLS_MODEL);
                    loadModel(file, Size);
                    pullParent();
                    break;
                case giles::GLS_PIVOT:
                    //AppFrame::log("Load a GLS_PIVOT");
                    pushParent(giles::GLS_PIVOT);
                    loadModel(file, Size);
                    pullParent();
                    break;
                    /*
                    case giles::GLS_LIGHT:
                        AppFrame::log("Load a GLS_LIGHT");
                        pushParent(giles::GLS_LIGHT);
                        loadModel(file, Size);
                        pullParent();
                        break;
                    */
                case giles::GLS_MESH:
                    pushParent(giles::GLS_MESH);
                    loadMesh(file, Size);
                    pullParent();
                    break;
                case giles::GLS_MODEL_POSITION:
                {
                    f32 vec[3];
                    file->read(vec, 12);
                    core::matrix4 mat;
                    mat.setTranslation(irr::core::vector3df(vec[0], vec[1], vec[2]));
                    VertexTransfrom = VertexTransfrom*mat;
                    break;
                }
                case giles::GLS_MODEL_ROTATION:
                {
                    f32 vec[3];
                    file->read(vec, 12);
                    vec[1] = -vec[1];
                    core::matrix4 mat1;
                    mat1.makeIdentity();
                    mat1.setRotationDegrees(irr::core::vector3df(vec[0], vec[1], vec[2]));

                    /*core::quaternion quad1;
                    quad1.fromAngleAxis(vec[0], core::vector3df(1.0,0.0,0.0));
                    core::quaternion quad2;
                    quad2.fromAngleAxis(-vec[1]/2, core::vector3df(0.0,1.0,0.0));
                    core::quaternion quad3;
                    quad3.fromAngleAxis(vec[2], core::vector3df(0.0,0.0,1.0));

                    quad1 *= quad2;
                    quad1 *= quad3;
                    quad1.getMatrix(mat1);
                    */

                    VertexTransfrom = VertexTransfrom*mat1;
                    break;
                }
                case giles::GLS_MODEL_SCALE:
                {
                    f32 vec[3];
                    file->read(vec, 12);
                    core::matrix4 mat;
                    mat.setScale(irr::core::vector3df(vec[0], vec[1], vec[2]));
                    VertexTransfrom = VertexTransfrom*mat;
                    break;
                }
                default:
                    skip(Size, file);
                    break;
                }
            }
        }

        void CGilesLoader::loadMesh(io::IReadFile *file, s32 ChunkSize)
        {
            s32 newPos = file->getPos()+ChunkSize;
            s32 ChunkID = 0;
            s32 Size = 0;
            while (file->getPos() < newPos)
            {
                file->read(&ChunkID, 4);
                file->read(&Size, 4);
                switch (ChunkID)
                {
                case giles::GLS_MESH_SURFACES:
                    pushParent(giles::GLS_MESH_SURFACES);
                    loadSurfaces(file, Size);
                    pullParent();
                    break;
                default:
                    skip(Size, file);
                    break;
                }
            }
        }

        void CGilesLoader::loadSurfaces(io::IReadFile *file, s32 ChunkSize)
        {
            s32 newPos = file->getPos()+ChunkSize;
            s32 ChunkID = 0;
            s32 Size = 0;

            s16 Vertices = 0;
            s16 PolyCount = 0;

            s32 VertexFormat = 0;

            SMeshBufferLightMap* Buffer = NULL;
            while (file->getPos() < newPos)
            {
                file->read(&ChunkID, 4);
                file->read(&Size, 4);
                switch (ChunkID)
                {
                case giles::GLS_MESH_SURF:
                    if (currentParent() == giles::GLS_MESH_SURF)
                    {
                        //add MeshBuffer
                        if (!Mesh)
                        {
                            Mesh = new SMesh;
                        }
                        if (Buffer)
                            Mesh->addMeshBuffer(Buffer);

                        pullParent();

                        Vertices = 0;
                        PolyCount = 0;
                    }
                    pushParent(giles::GLS_MESH_SURF);
                    //Make new Buffer
                    Buffer = new SMeshBufferLightMap;
                    VertexFormat = 0;
                    break;
                case giles::GLS_MESH_SURFVERTS:
                    if (skipNoParent(giles::GLS_MESH_SURF, Size, file))
                        break;
                    file->read(&Vertices, 2);
                    break;
                case giles::GLS_MESH_SURFPOLYS:
                    if (skipNoParent(giles::GLS_MESH_SURF, Size, file))
                        break;
                    file->read(&PolyCount, 2);
                    break;
                case giles::GLS_MESH_SURFMATERIAL:
                {
                    if (skipNoParent(giles::GLS_MESH_SURF, Size, file))
                        break;
                    s32 MatID = 0;
                    file->read(&MatID , 4);
                    if ((s32)Materials.size() > MatID)
                        Buffer->Material = Materials[MatID];
                    break;
                }
                case giles::GLS_MESH_SURFVERTFORMAT:
                    if (skipNoParent(giles::GLS_MESH_SURF, Size, file))
                        break;
                    file->read(&VertexFormat, 4);
                    break;
                case giles::GLS_MESH_SURFVERTDATA:
                    if (skipNoParent(giles::GLS_MESH_SURF, Size, file))
                        break;
                    //AppFrame::ILogger::log("Load Vertices with Type: %i", VertexFormat);
                    //load vertices
                    for (s32 i=0;i<Vertices;i++)
                    {
                        core::vector3df position;
                        core::vector3df normal;
                        video::SColor color4;
                        c8 color3[3];
                        core::vector2df lightCoord;
                        core::vector2df texCoord;

                        //read data
                        file->read(&position.X, 12);
                        if (VertexFormat & 1)
                            file->read(&normal.X, 12);
                        if (VertexFormat & 2)
                            file->read(&color4.color, 4);
                        if (VertexFormat & 4)
                            file->read(&color3[0], 3);
                        if (VertexFormat & 8)
                            file->read(&lightCoord.X, 8);
                        if (VertexFormat & 16)
                            file->read(&texCoord.X, 8);

                        VertexTransfrom.transformVect(position);
                        video::S3DVertex2TCoords vert(position, normal, color4, texCoord, core::vector2df(lightCoord.Y, lightCoord.X));
                        Buffer->Vertices.push_back(vert);
                    }
                    break;
                case giles::GLS_MESH_SURFPOLYDATA:
                    if (skipNoParent(giles::GLS_MESH_SURF, Size, file))
                        break;
                    //load indices
                    for (s32 i=0;i<PolyCount;i++)
                    {
                        s16 index;
                        file->read(&index, 2);
                        Buffer->Indices.push_back(index);
                        file->read(&index, 2);
                        Buffer->Indices.push_back(index);
                        file->read(&index, 2);
                        Buffer->Indices.push_back(index);
                    }
                    break;
                default:
                    skip(Size, file);
                    break;
                }
            }
            if (currentParent() == giles::GLS_MESH_SURF)
            {
                //add MeshBuffer
                if (!Mesh)
                {
                    Mesh = new SMesh;
                }
                if (Buffer)
                    Mesh->addMeshBuffer(Buffer);
                pullParent();
            }
        }

        //util functions
        void CGilesLoader::clear(void)
        {
            ParentChunk.clear();
            Materials.clear();
            Lightmaps.clear();
            Textures.clear();
            Mesh = NULL;
        }

        void CGilesLoader::pushParent(s32 ID)
        {
            ParentChunk.push_back(ID);
        }

        void CGilesLoader::pullParent(void)
        {
            ParentChunk.erase(ParentChunk.size()-1);
        }

        s32 CGilesLoader::currentParent(void)
        {
            if (ParentChunk.size() > 0)
                return ParentChunk[ParentChunk.size()-1];
            return 0;
        }

        void CGilesLoader::readString(core::stringc& data, io::IReadFile *file)
        {
            u8 byte = ' ';
            while (byte != '\0')
            {
                file->read(&byte, 1);
                if (byte != '\0')
                    data.append(byte);
            }
        }

        void CGilesLoader::skip(s32 Size, io::IReadFile *file)
        {
            file->seek(Size, true);
        }

        bool CGilesLoader::skipNoParent(s32 ID, s32 Size, io::IReadFile *file)
        {
            if (ID != currentParent())
            {
                file->seek(Size, true);
                return true;
            }
            return false;
        }
    }
}
