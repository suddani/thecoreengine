#include "Irrlicht_Extras/ShaderMaterial/CShaderMaterial.h"
#include <iostream>
#include <fstream>
namespace AppFrame
{
    namespace graphic
    {
        CShaderMaterial::CShaderMaterial(irr::IrrlichtDevice* device)
        {
            //ctor
            Device = device;
            Device->grab();
            SceneManager = Device->getSceneManager();
            SceneManager->grab();
            start();
        }

        CShaderMaterial::CShaderMaterial(const irr::core::stringc& name, irr::IrrlichtDevice* device)
        {
            Device = device;
            Device->grab();
            SceneManager = Device->getSceneManager();
            SceneManager->grab();
            start();

            irr::core::stringc ext;
            irr::core::getFileNameExtension(ext, name);

            nlog<<"Fileextension: "<<ext.c_str()<<nlfendl;
            ext.make_lower();
            if (ext == ".material")
            {
                Name = name;
                load(name);
            }
            else
            {
                Name = name;
                InternalName = name;
            }
        }

        void CShaderMaterial::start(void)
        {
            Compiled = false;
            NewMaterial = -1;
            Driver = SceneManager->getVideoDriver();
            Driver->grab();
            VertexShader = NULL;
            PixelShader = NULL;
            VertexShaderType = irr::video::EVST_VS_1_1;
            PixelShaderType = irr::video::EPST_PS_1_1;
            BaseMaterial = irr::video::EMT_SOLID;
        }

        CShaderMaterial::~CShaderMaterial()
        {
            //dtor
            SceneManager->drop();
            Driver->drop();


            if (PixelShader)
                PixelShader->drop();
            PixelShader = NULL;


            if (VertexShader)
                VertexShader->drop();
            VertexShader = NULL;

            for (irr::u32 i=0;i<ShaderConstants.size();i++)
                ShaderConstants[i]->drop();
            ShaderConstants.clear();
        }

        void CShaderMaterial::load(const irr::core::stringc& filename)
        {
            if (isCompiled())
                return;
            irr::io::IXMLReaderUTF8* xml = Device->getFileSystem()->createXMLReaderUTF8(filename.c_str());
            Name = filename;
            load(xml);
        }

        void CShaderMaterial::load(irr::io::IXMLReaderUTF8* xml)
        {
            if (isCompiled())
                return;
            nlog<<"load shadermaterial from file"<<nlflush;
            if (xml == NULL)
                return;

            bool Material = false;
            //bool Pixel = false;
            //bool Vertex = false;

            while (xml->read())
            {
                switch (xml->getNodeType())
                {
                case irr::io::EXN_CDATA:
                    break;
                case irr::io::EXN_ELEMENT:
                    if (irr::core::stringc("Material") == xml->getNodeName())
                    {
                        Material = true;
                        InternalName = xml->getAttributeValueSafe("name");
                        //TODO: ok doing this to match the PostProcess convetion
                        Name = InternalName;
                        setBaseMaterial(xml->getAttributeValueSafe("type"));
                    }
                    if (Material)
                    {
                        if (irr::core::stringc("PixelShader") == xml->getNodeName())
                        {
                            //Pixel = true;
                            setPixelShader(xml->getAttributeValueSafe("data"));
                            setPixelShaderType(xml->getAttributeValueSafe("type"));
                        }
                        if (irr::core::stringc("VertexShader") == xml->getNodeName())
                        {
                            //Vertex = true;
                            setVertexShader(xml->getAttributeValueSafe("data"));
                            setVertexShaderType(xml->getAttributeValueSafe("type"));
                        }
                        if (irr::core::stringc("ShaderConstant") == xml->getNodeName())
                        {
                            addShaderConstant(xml->getAttributeValueSafe("name"), xml->getAttributeValueSafe("type"), xml->getAttributeValueSafe("value"));
                        }
                    }
                    break;
                case irr::io::EXN_ELEMENT_END:
                    if (irr::core::stringc("Material") == xml->getNodeName())
                        Material = false;
                    /*
                    if (Material)
                    {
                        if (irr::core::stringc("PixelShader") == xml->getNodeName())
                            Pixel = false;
                        if (irr::core::stringc("VertexShader") == xml->getNodeName())
                            Vertex = true;
                    }
                    */
                    break;
                }
            }
            CompileMaterial();
            delete xml;
        }

        void CShaderMaterial::setBaseMaterial(irr::video::E_MATERIAL_TYPE baseMaterial)
        {
            if (isCompiled())
                return;
            BaseMaterial = baseMaterial;
        }

        void CShaderMaterial::setBaseMaterial(const irr::core::stringc& baseMaterial)
        {
            if (isCompiled())
                return;
            for (irr::u32 i=0;i<24;i++)//24 is the number of builtinMaterials
            {
                if (baseMaterial == irr::video::sBuiltInMaterialTypeNames[i])
                {
                    BaseMaterial = (irr::video::E_MATERIAL_TYPE)i;
                    return;
                }
            }
            BaseMaterial = irr::video::EMT_SOLID;
        }

        irr::video::E_MATERIAL_TYPE CShaderMaterial::getMaterial(void) const
        {
            return (NewMaterial > 0) ? (irr::video::E_MATERIAL_TYPE)NewMaterial : BaseMaterial;
        }

        bool CShaderMaterial::isCompiled(void) const
        {
            return Compiled;
        }

        bool CShaderMaterial::CompileMaterial(void)
        {
            if (isCompiled())
                return true;
            //check if supported here
            NewMaterial = Driver->getGPUProgrammingServices()->addHighLevelShaderMaterialFromFiles(VertexShader, "main", VertexShaderType, PixelShader, "main", PixelShaderType, this, BaseMaterial, 0);
            Compiled = (NewMaterial > 0) ? true : false;
            NewMaterial = (NewMaterial > 0) ? NewMaterial : BaseMaterial;
            if (Compiled)
            {
                if (PixelShader)
                    PixelShader->drop();
                PixelShader = NULL;


                if (VertexShader)
                    VertexShader->drop();
                VertexShader = NULL;
            }
            return Compiled;
        }

        void CShaderMaterial::addShaderConstant(ShaderConstant* constant)
        {
            for (irr::u32 i=0;i<ShaderConstants.size();i++)
            {
                if (ShaderConstants[i]->getName() == constant->getName())
                    return;
            }
            ShaderConstants.push_back(constant);
            constant->grab();
        }

        void CShaderMaterial::addShaderConstant(const irr::core::stringc& name, const irr::core::stringc& type, const irr::core::stringc& value)
        {
            for (irr::u32 i=0;i<ShaderConstants.size();i++)
            {
                if (ShaderConstants[i]->getName() == name)
                    return;
            }
            ShaderConstants.push_back(new ShaderConstant(name.c_str(), type.c_str(), value.c_str(), SceneManager));
        }

        bool CShaderMaterial::setShaderConstant(const irr::core::stringc& name, const ShaderConstant::E_SHADER_CONSTANT_TYPE& type, const irr::core::stringc& value)
        {
            ShaderConstant c(name.c_str(), ESCT_STRINGS[type].c_str(), value.c_str(), SceneManager);
            for (irr::u32 i=0;i<ShaderConstants.size();i++)
            {
                if (ShaderConstants[i]->getName() == name && ShaderConstants[i]->getType() == type && ShaderConstants[i]->getIsPointer())
                {
                    *ShaderConstants[i] = c;
                    return true;
                }
            }
            return false;
        }

        bool CShaderMaterial::setShaderConstant(const irr::core::stringc& name, const irr::u32& value)
        {
            ShaderConstant c(name.c_str(), (irr::u32*)&value, SceneManager);
            for (irr::u32 i=0;i<ShaderConstants.size();i++)
            {
                if (*ShaderConstants[i] == c)
                {
                    *ShaderConstants[i] = c;
                    return true;
                }
            }
            return false;
        }

        bool CShaderMaterial::setShaderConstant(const irr::core::stringc& name, const irr::f32& value)
        {
            ShaderConstant c(name.c_str(), (irr::f32*)&value, SceneManager);
            for (irr::u32 i=0;i<ShaderConstants.size();i++)
            {
                if (*ShaderConstants[i] == c)
                {
                    *ShaderConstants[i] = c;
                    return true;
                }
            }
            return false;
        }

        bool CShaderMaterial::setShaderConstant(const irr::core::stringc& name, const irr::core::vector3df& value)
        {
            ShaderConstant c(name.c_str(), (irr::core::vector3df*)&value, SceneManager);
            for (irr::u32 i=0;i<ShaderConstants.size();i++)
            {
                if (*ShaderConstants[i] == c)
                {
                    *ShaderConstants[i] = c;
                    return true;
                }
            }
            return false;
        }

        bool CShaderMaterial::setShaderConstant(const irr::core::stringc& name, const irr::core::vector2df& value)
        {
            ShaderConstant c(name.c_str(), (irr::core::vector2df*)&value, SceneManager);
            for (irr::u32 i=0;i<ShaderConstants.size();i++)
            {
                if (*ShaderConstants[i] == c)
                {
                    *ShaderConstants[i] = c;
                    return true;
                }
            }
            return false;
        }

        bool CShaderMaterial::setShaderConstant(const irr::core::stringc& name, const irr::core::matrix4& value)
        {
            ShaderConstant c(name.c_str(), (irr::core::matrix4*)&value, SceneManager);
            for (irr::u32 i=0;i<ShaderConstants.size();i++)
            {
                if (*ShaderConstants[i] == c)
                {
                    *ShaderConstants[i] = c;
                    return true;
                }
            }
            return false;
        }

        void CShaderMaterial::setPixelShader(const irr::core::stringc& name)
        {
            if (isCompiled())
                return;
            if (PixelShader)
                PixelShader->drop();
            PixelShader = NULL;

            irr::core::stringc Ending;
            irr::core::getFileNameExtension(Ending, name);

            if (name.size() == 0)
                return;

            if (Ending == ".frag")//load from file
            {
                PixelShader = Device->getFileSystem()->createAndOpenFile(name.c_str());
            }
            else
            {
                irr::c8* data = new irr::c8[name.size()];
                strcpy(data, name.c_str());
                PixelShader = Device->getFileSystem()->createMemoryReadFile((void*)data, name.size(), "PixelShader", true);
            }
        }

        void CShaderMaterial::setPixelShader(irr::io::IReadFile* shader)
        {
            if (isCompiled())
                return;
            if (PixelShader)
                PixelShader->drop();
            PixelShader = NULL;

            if (!shader)
                return;
            PixelShader = shader;
            PixelShader->grab();
        }

        void CShaderMaterial::setPixelShaderType(irr::video::E_PIXEL_SHADER_TYPE type)
        {
            if (isCompiled())
                return;
            PixelShaderType = type;
        }

        void CShaderMaterial::setPixelShaderType(const irr::core::stringc& type)
        {
            if (isCompiled())
                return;
            for (irr::u32 i=0;i<irr::video::EPST_COUNT;i++)
                if (type == irr::video::PIXEL_SHADER_TYPE_NAMES[i])
                {
                    setPixelShaderType((irr::video::E_PIXEL_SHADER_TYPE)i);
                    return;
                }
        }

        void CShaderMaterial::setVertexShader(const irr::core::stringc& name)
        {
            if (isCompiled())
                return;
            if (VertexShader)
                VertexShader->drop();
            VertexShader = NULL;

            irr::core::stringc Ending;
            irr::core::getFileNameExtension(Ending, name.c_str());

            if (name.size() == 0)
                return;

            if (Ending == ".vert")//load from file
            {
                VertexShader = Device->getFileSystem()->createAndOpenFile(name.c_str());
            }
            else
            {
                irr::c8* data = new irr::c8[name.size()];
                strcpy(data, name.c_str());
                VertexShader = Device->getFileSystem()->createMemoryReadFile((void*)data, name.size(), "VertexShader", true);
            }
        }

        void CShaderMaterial::setVertexShader(irr::io::IReadFile* shader)
        {
            if (isCompiled())
                return;
            if (VertexShader)
                VertexShader->drop();
            VertexShader = NULL;

            if (!shader)
                return;
            VertexShader = shader;
            VertexShader->grab();
        }

        void CShaderMaterial::setVertexShaderType(irr::video::E_VERTEX_SHADER_TYPE type)
        {
            if (isCompiled())
                return;
            VertexShaderType = type;
        }

        void CShaderMaterial::setVertexShaderType(const irr::core::stringc& type)
        {
            if (isCompiled())
                return;
            for (irr::u32 i=0;i<irr::video::EVST_COUNT;i++)
                if (type == irr::video::VERTEX_SHADER_TYPE_NAMES[i])
                {
                    setVertexShaderType((irr::video::E_VERTEX_SHADER_TYPE)i);
                    return;
                }
        }

        void CShaderMaterial::OnSetMaterial(const irr::video::SMaterial& material)
        {
            CurrentMaterial = material;
        }

        void CShaderMaterial::OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32 userData)
        {
            //irr::video::IVideoDriver* driver = services->getVideoDriver();

            //set textures
            irr::u32 id0 = 0;
            irr::u32 id1 = 1;
            irr::u32 id2 = 2;
            irr::u32 id3 = 3;
            if (CurrentMaterial.getTexture(id0))
                services->setVertexShaderConstant("Texture0", reinterpret_cast<irr::f32*>(&id0),1);
            if (CurrentMaterial.getTexture(id1))
                services->setVertexShaderConstant("Texture1", reinterpret_cast<irr::f32*>(&id1),1);
            if (CurrentMaterial.getTexture(id2))
                services->setVertexShaderConstant("Texture2", reinterpret_cast<irr::f32*>(&id2),1);
            if (CurrentMaterial.getTexture(id3))
                services->setVertexShaderConstant("Texture3", reinterpret_cast<irr::f32*>(&id3),1);

            //set material constants
            services->setVertexShaderConstant("MaterialTypeParam", reinterpret_cast<irr::f32*>(&CurrentMaterial.MaterialTypeParam), 1);
            services->setVertexShaderConstant("MaterialTypeParam2", reinterpret_cast<irr::f32*>(&CurrentMaterial.MaterialTypeParam2), 1);

            //set shader constants
            for (irr::u32 i=0;i<ShaderConstants.size();i++)
                ShaderConstants[i]->setConstant(services, NULL);

            /*
            // set clip matrix at register 4
            irr::core::matrix4 worldViewProj(driver->getTransform(irr::video::ETS_PROJECTION));
            worldViewProj *= driver->getTransform(irr::video::ETS_VIEW);
            worldViewProj *= driver->getTransform(irr::video::ETS_WORLD);
            services->setVertexShaderConstant("mWorldViewProj", &worldViewProj.pointer()[0], 16);

            // set some light color at register 9
            irr::video::SColorf col(0.0f,1.0f,1.0f,0.0f);
            services->setVertexShaderConstant("myColor", reinterpret_cast<irr::f32*>(&col), 4);
            */
        }

        const irr::core::stringc& CShaderMaterial::getName(void) const
        {
            nlog<<"Shadername: "<<Name.c_str()<<nlflush;
            return Name;
        }
    }
}
