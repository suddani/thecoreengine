#include "Irrlicht_Extras/ObjectSceneNode/CObjectSceneNode.h"
//#include <ILogger.h>
#include <ILog.h>
#include <IGraphicManager.h>

CObjectSceneNode::CObjectSceneNode(scene::ISceneNode *parent, scene::ISceneManager *mgr, s32 id, const core::vector3df &position, const core::vector3df &rotation, const core::vector3df &scale) : /*ISceneNode*/IObjectSceneNode (parent, mgr, id, position, rotation, scale)
{
    //ctor
    GraphicManager = NULL;
    Node = NULL;
    CurrentAnimation = -1;
    NextAnimation = -1;
    MaxFrame = 0;
}

CObjectSceneNode::~CObjectSceneNode()
{
    //dtor
    cleanUp();
}

void CObjectSceneNode::render(void)
{
    irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();
    if (Node)
        driver->setTransform(irr::video::ETS_WORLD, Node->getAbsoluteTransformation());
    else
        driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);
}

void CObjectSceneNode::cleanUp(void)
{
    if (Node)
    {
        Node->remove();
        Node->drop();
    }
    Node = NULL;

    for (u32 i=0;i<RenderPipeline.size();i++)
    {
        RenderPipeline[i]->remove();
        RenderPipeline[i]->drop();
    }
    RenderPipeline.clear();

    Animations.clear();
    Clamps.clear();

    CurrentAnimation = -1;
    NextAnimation = -1;
    MaxFrame = 0;
    MeshPath = "";
}

void CObjectSceneNode::OnAnimationEnd(irr::scene::IAnimatedMeshSceneNode *node)
{
    if (NextAnimation >= 0)
    {
        //AppFrame::ILogger::log("set New Animation\n");
        nlog<<"set New Animation\n"<<nlflush;
        s32 I = NextAnimation;
        NextAnimation = -1;
        CurrentAnimation = -1;
        setAnimation(I);
    }
    else
    {
        CurrentAnimation = -1;
        NextAnimation = -1;
    }
}

irr::u32 CObjectSceneNode::getMaterialCount(void) const
{
    if (Node)
        return Node->getMaterialCount();
    return 0;
}

irr::video::SMaterial& CObjectSceneNode::getMaterial(const irr::u32& i)
{
    if (Node)
        return Node->getMaterial(i);
    return SomeMaterial;
}

const irr::core::aabbox3d<irr::f32 >& CObjectSceneNode::getBoundingBox(void) const
{
    return BoundingBox;
}

s32 CObjectSceneNode::findAnimation(const irr::c8* name)
{
    irr::s32 i = -1;
    for (irr::u32 a=0;a<Animations.size();a++)
    {
        if (Animations[a] == name)
        {
            i = a;
            break;
        }
    }
    return i;
}

const irr::s32& CObjectSceneNode::getCurrentAnimation(void)
{
    return CurrentAnimation;
}

const irr::c8* CObjectSceneNode::getAnimationName(const u32& i)
{
    if (i>=Animations.size())
        return "";
    return Animations[i].Name.c_str();
}

const scene::ISceneNode* CObjectSceneNode::getClamp(const irr::c8* name) const
{
    return CObjectSceneNode::getClamp(name);
}

const scene::ISceneNode* CObjectSceneNode::getClamp(const u32& i) const
{
    return CObjectSceneNode::getClamp(i);
}

scene::ISceneNode* CObjectSceneNode::getClamp(const irr::c8* name)
{
    for (u32 i=0;i<Clamps.size();++i)
        if (Clamps[i].Name == name)
        {
			return Clamps[i].Node;
		}
    return NULL;
}

scene::ISceneNode* CObjectSceneNode::getClamp(const u32& i)
{
    if (i<Clamps.size())
    {
		return Clamps[i].Node;
	}
    return NULL;
}

s32 CObjectSceneNode::getClampCount(void) const
{
    return Clamps.size();
}

void CObjectSceneNode::setAnimation(const u32& start, const u32& end, const f32& speed, const bool& loop)
{
    if (Node)
    {
        Node->setFrameLoop(start, end);
        Node->setCurrentFrame(start);
        Node->setAnimationSpeed(speed);
        Node->setLoopMode(loop);
        for (u32 i=0;i<RenderPipeline.size();i++)
        {
            RenderPipeline[i]->setFrameLoop(start, end);
            RenderPipeline[i]->setCurrentFrame(start);
            RenderPipeline[i]->setAnimationSpeed(speed);
            RenderPipeline[i]->setLoopMode(loop);
        }
        CurrentAnimation = -1;
        NextAnimation = -1;
    }
}

void CObjectSceneNode::setAnimationFrame(const u32& frame)
{
    if (Node)
    {
        Node->setFrameLoop(frame, frame);
        for (u32 i=0;i<RenderPipeline.size();i++)
            RenderPipeline[i]->setFrameLoop(frame, frame);
        CurrentAnimation = -1;
        NextAnimation = -1;
    }
}

u32 CObjectSceneNode::getCurrentAnimationFrame(void)
{
    if (Node)
    {
        return Node->getFrameNr();
    }
    return 0;
}

u32 CObjectSceneNode::getMaxAnimationFrame(void)
{
    if (!Node)
        return 0;
    return MaxFrame;
}

s32 CObjectSceneNode::getAnimationCount(void) const
{
    return Animations.size();
}

f32 CObjectSceneNode::setAnimation(const irr::c8* name, bool now)
{
    irr::s32 i = findAnimation(name);
    //printf("set Animation: %s ID: %i\n", name.c_str(), i);
    if ((CurrentAnimation >= 0 && Animations[CurrentAnimation].Loop) || CurrentAnimation == -1 || now)
        return setAnimation(i, now);
    else if (i>=0)
    {
        NextAnimation = i;
        return 0.0;
    }

    return -1.0;
}

void CObjectSceneNode::setGraphicManager(AppFrame::graphic::IGraphicManager* graphic)
{
    GraphicManager = graphic;
}

f32 CObjectSceneNode::setAnimation(const s32& i, bool now, bool same)
{
    if (i>=0 && (i != CurrentAnimation || same) && Node && i < Animations.size())
    {
        Node->setFrameLoop(Animations[i].Start, Animations[i].End);
        Node->setAnimationSpeed(Animations[i].Speed);
        Node->setLoopMode(Animations[i].Loop);
        Node->setAnimationEndCallback(this);

        for (u32 r=0;r<RenderPipeline.size();r++)
        {
            RenderPipeline[r]->setFrameLoop(Animations[i].Start, Animations[i].End);
            RenderPipeline[r]->setAnimationSpeed(Animations[i].Speed);
            RenderPipeline[r]->setLoopMode(Animations[i].Loop);
            RenderPipeline[r]->setAnimationEndCallback(this);
        }
        CurrentAnimation = i;
        if (Animations[CurrentAnimation].Next.size() > 0)
            NextAnimation = findAnimation(Animations[CurrentAnimation].Next[rand()%Animations[CurrentAnimation].Next.size()].c_str());
        else
            NextAnimation = -1;
        s32 l = Animations[CurrentAnimation].Start-Animations[CurrentAnimation].End;
        l = abs(l);

        return (f32)l/Animations[CurrentAnimation].Speed;
    }
    else
        return -1.0;
}

void CObjectSceneNode::setAnimationSpeed(const f32& speed)
{
    if (Node)
        Node->setAnimationSpeed(1.0);
    for (u32 i=0;i<RenderPipeline.size();i++)
        RenderPipeline[i]->setAnimationSpeed(1.0);
}

void CObjectSceneNode::removeNext2Animation(const irr::c8* name, const irr::c8* next)
{
    for (irr::u32 i=0;i<Animations.size();i++)
        if (Animations[i] == name)
        {
            for (irr::u32 a=0;a<Animations[i].Next.size();a++)
                if (Animations[i].Next[a] == next)
                {
                    Animations[i].Next.erase(a);
                    return;
                }
            return;
        }
}

void CObjectSceneNode::addNext2Animation(const irr::c8* name, const irr::c8* next)
{
    nlog<<"Add "<<next<<" as Next Animation to "<<name<<nlflush;
    for (irr::u32 i=0;i<Animations.size();i++)
        if (Animations[i] == name)
        {
            for (irr::u32 a=0;a<Animations[i].Next.size();a++)
                if (Animations[i].Next[a] == next)
                    return;
            Animations[i].Next.push_back(next);
            return;
        }
}

void CObjectSceneNode::removeAnimation(const irr::c8* name)
{
    for (irr::u32 i=0;i<Animations.size();i++)
        if (Animations[i] == name)
        {
            Animations.erase(i);
            return;
        }
}

void CObjectSceneNode::addAnimation(const irr::c8* name, const u32& start, const u32& end, const irr::f32& speed, const bool& loop)
{
    AnimationSQ ani(name);
    ani.Start = start;
    ani.End = end;
    ani.Loop = loop;
    ani.Speed = speed;
    for (irr::u32 i=0;i<Animations.size();i++)
        if (Animations[i] == name)
            return;
    Animations.push_back(ani);
}

//Loading
s32 checkMaterialID(const irr::core::stringc& mat)
{
    for (irr::s32 i=0;i<24;i++)
    {
        if (mat == irr::video::sBuiltInMaterialTypeNames[i])
            return i;
    }
    return -1;
}
//getFileNameExtension(dest, source)
void CObjectSceneNode::loadObject(const irr::c8* file)
{
    nlog<<"Start loading Object"<<nlflush;
    cleanUp();

    //first check if the object is a simple mesh file
    irr::scene::IAnimatedMesh* MyNewMesh = NULL;
    MyNewMesh = SceneManager->getMesh(file);
    if (MyNewMesh)
    {
        nlog<<"Object is a simple Mesh"<<nlflush;
        Node = SceneManager->addAnimatedMeshSceneNode(MyNewMesh, this);
        Node->setAnimationEndCallback(this);
        Node->grab();
        MaxFrame = Node->getEndFrame();
        MeshPath = file;
        return;
    }

    irr::io::IrrXMLReader* xml = irr::io::createIrrXMLReader(file);

    if (xml == NULL)
        return;

    bool Object = false;

    bool Material = false;

    bool Animation = false;

    bool Clamp = false;

    bool Attachment = false;

    s32  MaterialPipelineID = -2;

    bool MaterialPipeline = false;

    s32 Material_ID = 0;

    while (xml->read())
    {
        switch (xml->getNodeType())
        {
        case irr::io::EXN_ELEMENT:
            if (irr::core::stringc("Object") == xml->getNodeName() && !Object)
                Object = true;
            else if (Object)
            {
                if (Animation)
                {
                    if (irr::core::stringc("Range") == xml->getNodeName())
                    {
                        Animations[Animations.size()-1].Start = xml->getAttributeValueAsInt("value");
                        Animations[Animations.size()-1].End = xml->getAttributeValueAsInt("value1");
                    }
                    else if (irr::core::stringc("Speed") == xml->getNodeName())
                    {
                        Animations[Animations.size()-1].Speed = xml->getAttributeValueAsFloat("value");
                    }
                    else if (irr::core::stringc("Loop") == xml->getNodeName())
                    {
                        core::stringc data = xml->getAttributeValueSafe("value");
                        if (data == "true")
                            Animations[Animations.size()-1].Loop = true;
                        else
                            Animations[Animations.size()-1].Loop = false;
                    }
                    else if (irr::core::stringc("Next") == xml->getNodeName())
                    {
                        Animations[Animations.size()-1].Next.push_back(xml->getAttributeValueSafe("value"));
                    }
                }
                else if (Attachment)
                {
                }
                else if (Clamp)
                {
                    if (irr::core::stringc("Position") == xml->getNodeName())
                    {
                        irr::core::stringc pos = xml->getAttributeValueSafe("value");
                        irr::core::vector3df p;
                        sscanf(pos.c_str(),"%f, %f, %f", &p.X, &p.Y, &p.Z);
                        Clamps[Clamps.size()-1].Position = p;
                    }
                    else if (irr::core::stringc("Rotation") == xml->getNodeName())
                    {
                        irr::core::stringc pos = xml->getAttributeValueSafe("value");
                        irr::core::vector3df p;
                        sscanf(pos.c_str(),"%f, %f, %f", &p.X, &p.Y, &p.Z);
                        Clamps[Clamps.size()-1].Rotation = p;
                    }
                    else if (irr::core::stringc("Scale") == xml->getNodeName())
                    {
                        irr::core::stringc pos = xml->getAttributeValueSafe("value");
                        irr::core::vector3df p;
                        sscanf(pos.c_str(),"%f, %f, %f", &p.X, &p.Y, &p.Z);
                        Clamps[Clamps.size()-1].Scale = p;
                    }
                    else if (irr::core::stringc("Bone") == xml->getNodeName())
                    {
                        Clamps[Clamps.size()-1].Bone = xml->getAttributeValueSafe("value");
                    }
                }
                else if (irr::core::stringc("Mesh") == xml->getNodeName())
                {
                    MeshPath = xml->getAttributeValueSafe("value");
                    irr::scene::IAnimatedMesh* mesh = SceneManager->getMesh(xml->getAttributeValueSafe("value"));
                    if (!Node)
                    {
                        Node = SceneManager->addAnimatedMeshSceneNode(mesh, this);
                        Node->setAnimationEndCallback(this);
                        Node->grab();
                    }
                    else
                    {
                        Node->remove();
                        Node->drop();
                        Node = SceneManager->addAnimatedMeshSceneNode(mesh, this);
                        Node->setAnimationEndCallback(this);
                        Node->grab();
                        for (u32 i=0;i<RenderPipeline.size();i++)
                        {
                            RenderPipeline[i]->remove();
                            RenderPipeline[i]->drop();
                        }
                        RenderPipeline.clear();
                    }
                    MaxFrame = Node->getEndFrame();
                }
                else if (irr::core::stringc("Position") == xml->getNodeName())
                {
                    irr::core::stringc pos = xml->getAttributeValueSafe("value");
                    irr::core::vector3df p;
                    sscanf(pos.c_str(),"%f, %f, %f", &p.X, &p.Y, &p.Z);
                    if (Node)
                        Node->setPosition(p);
                }
                else if (irr::core::stringc("Rotation") == xml->getNodeName())
                {
                    irr::core::stringc pos = xml->getAttributeValueSafe("value");
                    irr::core::vector3df p;
                    sscanf(pos.c_str(),"%f, %f, %f", &p.X, &p.Y, &p.Z);
                    if (Node)
                        Node->setRotation(p);
                }
                else if (irr::core::stringc("Scale") == xml->getNodeName())
                {
                    irr::core::stringc pos = xml->getAttributeValueSafe("value");
                    irr::core::vector3df p;
                    sscanf(pos.c_str(),"%f, %f, %f", &p.X, &p.Y, &p.Z);
                    if (Node)
                        Node->setScale(p);
                }
                else if (irr::core::stringc("MaterialPipeline") == xml->getNodeName())
                {
                    MaterialPipeline = true;
                    MaterialPipelineID++;
                    if (MaterialPipelineID >= 0 && Node)
                    {
                        nlog<<"add MaterialPipeline"<<nlflush;
                        RenderPipeline.push_back((irr::scene::IAnimatedMeshSceneNode*)Node->clone(this, SceneManager));
                        nlog<<"Node is Parent: "<<(RenderPipeline[RenderPipeline.size()-1]->getParent() == Node ? "true" : "false")<<nlflush;
                        //if (RenderPipeline[RenderPipeline.size()-1]->getReferenceCount() == 1)
                        RenderPipeline[RenderPipeline.size()-1]->grab();
                    }
                }
                else if (irr::core::stringc("Animation") == xml->getNodeName())
                {
                    Animation = true;
                    Animations.push_back(AnimationSQ(xml->getAttributeValueSafe("name")));
                    //printf("Add Animation: %s\n", xml->getAttributeValueSafe("name"));
                }
                else if (irr::core::stringc("Clamp") == xml->getNodeName())
                {
                    Clamp = true;
                    Clamps.push_back(ClampSQ(xml->getAttributeValueSafe("name")));
                    //printf("Add Animation: %s\n", xml->getAttributeValueSafe("name"));
                }
                else if (irr::core::stringc("Attachment") == xml->getNodeName())
                {
                    Attachment = true;
                }
                else if (Material)
                {
                    if (irr::core::stringc("Lightning") == xml->getNodeName())
                    {
                        //printf("MaterialCount: %i\n", Node->getMaterialCount());
                        irr::core::stringc data = xml->getAttributeValueSafe("value");
                        if (data != "true")
                        {
                            if (MaterialPipelineID < 0 && Node && Node->getMaterialCount() > Material_ID)
                            {
                                Node->getMaterial(Material_ID).Lighting = false;
                            }
                            else if (MaterialPipelineID >= 0 && MaterialPipeline < RenderPipeline.size() && RenderPipeline[MaterialPipelineID]->getMaterialCount() > Material_ID)
                            {
                                RenderPipeline[MaterialPipelineID]->getMaterial(Material_ID).Lighting = false;
                            }
                        }
                    }
                    else if (irr::core::stringc("BackfaceCulling") == xml->getNodeName())
                    {
                        irr::core::stringc data = xml->getAttributeValueSafe("value");
                        if (data != "true")
                        {
                            if (MaterialPipelineID < 0 && Node && Node->getMaterialCount() > Material_ID)
                            {
                                Node->getMaterial(Material_ID).BackfaceCulling = false;
                            }
                            else if (MaterialPipelineID >= 0 && MaterialPipelineID < RenderPipeline.size() && RenderPipeline[MaterialPipelineID]->getMaterialCount() > Material_ID)
                            {
                                RenderPipeline[MaterialPipelineID]->getMaterial(Material_ID).BackfaceCulling = false;
                            }
                        }
                    }
                    else if (irr::core::stringc("FogEnable") == xml->getNodeName())
                    {
                        irr::core::stringc data = xml->getAttributeValueSafe("value");
                        if (data == "true")
                        {
                            if (MaterialPipelineID < 0 && Node && Node->getMaterialCount() > Material_ID)
                            {
                                Node->getMaterial(Material_ID).FogEnable = true;
                            }
                            else if (MaterialPipelineID >= 0 && MaterialPipelineID < RenderPipeline.size() && RenderPipeline[MaterialPipelineID]->getMaterialCount() > Material_ID)
                            {
                                RenderPipeline[MaterialPipelineID]->getMaterial(Material_ID).FogEnable = true;
                            }
                        }
                    }
                    else if (irr::core::stringc("FrontfaceCulling") == xml->getNodeName())
                    {
                        irr::core::stringc data = xml->getAttributeValueSafe("value");
                        if (data == "true")
                        {
                            if (MaterialPipelineID < 0 && Node && Node->getMaterialCount() > Material_ID)
                            {
                                Node->getMaterial(Material_ID).FrontfaceCulling = true;
                            }
                            else if (MaterialPipelineID >= 0 && MaterialPipelineID < RenderPipeline.size() && RenderPipeline[MaterialPipelineID]->getMaterialCount() > Material_ID)
                            {
                                RenderPipeline[MaterialPipelineID]->getMaterial(Material_ID).FrontfaceCulling = true;
                            }
                        }
                    }
                    else if (irr::core::stringc("MaterialTypeParam") == xml->getNodeName())
                    {
                        f32 data = xml->getAttributeValueAsFloat("value");
                        if (MaterialPipelineID < 0 && Node && Node->getMaterialCount() > Material_ID)
                            Node->getMaterial(Material_ID).MaterialTypeParam = data;
                        else if (MaterialPipelineID >= 0 && MaterialPipelineID < RenderPipeline.size() && RenderPipeline[MaterialPipelineID]->getMaterialCount() > Material_ID)
                            RenderPipeline[MaterialPipelineID]->getMaterial(Material_ID).MaterialTypeParam = data;
                    }
                    else if (irr::core::stringc("MaterialTypeParam2") == xml->getNodeName())
                    {
                        f32 data = xml->getAttributeValueAsFloat("value");
                        if (MaterialPipelineID < 0 && Node && Node->getMaterialCount() > Material_ID)
                            Node->getMaterial(Material_ID).MaterialTypeParam2 = data;
                        else if (MaterialPipelineID >= 0 && MaterialPipelineID < RenderPipeline.size() && RenderPipeline[MaterialPipelineID]->getMaterialCount() > Material_ID)
                            RenderPipeline[MaterialPipelineID]->getMaterial(Material_ID).MaterialTypeParam2 = data;
                    }
                    else if (irr::core::stringc("MaterialType") == xml->getNodeName())
                    {
                        irr::core::stringc data = xml->getAttributeValueSafe("value");
                        s32 Mat = checkMaterialID(data);
                        if (Mat > 0)
                        {
                            if (MaterialPipelineID < 0 && Node && Node->getMaterialCount() > Material_ID)
                                Node->getMaterial(Material_ID).MaterialType = (irr::video::E_MATERIAL_TYPE)Mat;
                            else if (MaterialPipelineID >= 0 && MaterialPipelineID < RenderPipeline.size() && RenderPipeline[MaterialPipelineID]->getMaterialCount() > Material_ID)
                                RenderPipeline[MaterialPipelineID]->getMaterial(Material_ID).MaterialType = (irr::video::E_MATERIAL_TYPE)Mat;
                        }
                        else
                        {
                            //load Material
                            //TODO: load the material from the material manager..DONE
                            //TODO: Now just integate the NewShaderMaterial into the Engine...DONE
                            if (GraphicManager)
                            {
                                if (MaterialPipelineID < 0 && Node && Node->getMaterialCount() > Material_ID)
                                    Node->getMaterial(Material_ID).MaterialType = GraphicManager->getMaterial(data.c_str());
                                else if (MaterialPipelineID >= 0 && MaterialPipelineID < RenderPipeline.size() && RenderPipeline[MaterialPipelineID]->getMaterialCount() > Material_ID)
                                    RenderPipeline[MaterialPipelineID]->getMaterial(Material_ID).MaterialType = GraphicManager->getMaterial(data.c_str());
                            }
                        }
                    }
                    else if (irr::core::stringc("Texture") == xml->getNodeName())
                    {
                        irr::core::stringc data = xml->getAttributeValueSafe("value");
                        s32 id = xml->getAttributeValueAsInt("id");
                        if (MaterialPipelineID < 0 && Node && Node->getMaterialCount() > Material_ID && id < 4)
                        {
                            Node->getMaterial(Material_ID).setTexture(id, SceneManager->getVideoDriver()->getTexture(data.c_str()));
                        }
                        else if (MaterialPipelineID >= 0 && MaterialPipelineID < RenderPipeline.size() && RenderPipeline[MaterialPipelineID]->getMaterialCount() > Material_ID)
                        {
                            RenderPipeline[MaterialPipelineID]->getMaterial(Material_ID).setTexture(id, SceneManager->getVideoDriver()->getTexture(data.c_str()));
                        }
                    }
                }
                else if (MaterialPipeline)
                {
                    if (irr::core::stringc("Material") == xml->getNodeName())
                    {
                        //printf("Found Material..");
                        Material = true;
                        Material_ID = xml->getAttributeValueAsInt("id");
                        //printf("End\n");
                    }
                }
            }
            break;
        case irr::io::EXN_ELEMENT_END:
            if (irr::core::stringc("Object") == xml->getNodeName() && Object)
                Object = false;
            if (Object)
            {
                if (irr::core::stringc("MaterialPipeline") == xml->getNodeName() && Object)
                    MaterialPipeline = false;
                if (irr::core::stringc("Material") == xml->getNodeName() && Object)
                    Material = false;
                else if (irr::core::stringc("Animation") == xml->getNodeName())
                    Animation = false;
                else if (irr::core::stringc("Clamp") == xml->getNodeName())
                {
                    scene::ISceneNode* bone = this;
                    if (Node)
                    {
						Node->setJointMode(irr::scene::EJUOR_READ);
                        bone = Node->getJointNode(Clamps[Clamps.size()-1].Bone.c_str());
                        if (bone == 0)
                            bone = this;
                    }
                    Clamps[Clamps.size()-1].Make(SceneManager, bone);
                    Clamp = false;
                }
                else if (irr::core::stringc("Attachment") == xml->getNodeName())
                    Attachment = false;
            }
            break;
        }
    }

    delete xml;


    nlog<<"DONE loading Object"<<nlflush;
}

void CObjectSceneNode::saveObject(const irr::c8* file)
{
    irr::core::stringc data;
    if (Node)
    {
        FILE* datei = fopen(file, "wt+");
        if (datei)
        {
            fwrite("<Object>\n", 9,1,datei);

            //write Mesh
            data = "\t<Mesh value=\"";
            data.append(MeshPath);
            data.append("\"/>\n");
            fwrite(data.c_str(), data.size(),1,datei);

            //write Position
            data = "\t<Position value=\"";
            data.append(irr::core::stringc(Node->getPosition().X));
            data.append(", ");
            data.append(irr::core::stringc(Node->getPosition().Y));
            data.append(", ");
            data.append(irr::core::stringc(Node->getPosition().Z));
            data.append("\"/>\n");
            fwrite(data.c_str(), data.size(),1,datei);

            //write Rotation
            data = "\t<Rotation value=\"";
            data.append(irr::core::stringc(Node->getRotation().X));
            data.append(", ");
            data.append(irr::core::stringc(Node->getRotation().Y));
            data.append(", ");
            data.append(irr::core::stringc(Node->getRotation().Z));
            data.append("\"/>\n");
            fwrite(data.c_str(), data.size(),1,datei);

            //write Scale
            data = "\t<Scale value=\"";
            data.append(irr::core::stringc(Node->getScale().X));
            data.append(", ");
            data.append(irr::core::stringc(Node->getScale().Y));
            data.append(", ");
            data.append(irr::core::stringc(Node->getScale().Z));
            data.append("\"/>\n\n");
            fwrite(data.c_str(), data.size(),1,datei);

            //write Materials
            data = "\t<MaterialPipeline>\n";
            fwrite(data.c_str(), data.size(),1,datei);
            for (irr::u32 i=0;i<getMaterialCount();i++)
            {
                data = "\t\t<Material id=\"";
                data.append(irr::core::stringc(i));
                data.append("\">\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //write MaterialType
                if (GraphicManager)
                {
                    data = "\t\t\t<MaterialType value=\"";
                    data.append(GraphicManager->getMaterialName(getMaterial(i).MaterialType));
                    data.append("\"/>\n");
                    fwrite(data.c_str(), data.size(),1,datei);
                }

                //write MaterialTypeParam
                data = "\t\t\t<MaterialTypeParam value=\"";
                data.append(irr::core::stringc(getMaterial(i).MaterialTypeParam));
                data.append("\"/>\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //write MaterialTypeParam2
                data = "\t\t\t<MaterialTypeParam2 value=\"";
                data.append(irr::core::stringc(getMaterial(i).MaterialTypeParam2));
                data.append("\"/>\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //write Lightning
                data = "\t\t\t<Lightning value=\"";
                if (getMaterial(i).Lighting)
                    data.append("true");
                else
                    data.append("false");
                data.append("\"/>\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //write BackfaceCulling
                data = "\t\t\t<BackfaceCulling value=\"";
                if (getMaterial(i).BackfaceCulling)
                    data.append("true");
                else
                    data.append("false");
                data.append("\"/>\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //write FrontfaceCulling
                data = "\t\t\t<FrontfaceCulling value=\"";
                if (getMaterial(i).FrontfaceCulling)
                    data.append("true");
                else
                    data.append("false");
                data.append("\"/>\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //write Textures
                for (irr::u32 t=0;t<4;t++)
                {
                    if (getMaterial(i).getTexture(t))
                    {
                        data = "\t\t\t<Texture value=\"";
                        irr::core::stringc n = getMaterial(i).getTexture(t)->getName();
                        data.append(n);
                        data.append("\" id=\"");
                        data.append(irr::core::stringc(t));
                        data.append("\"/>\n");
                        fwrite(data.c_str(), data.size(),1,datei);
                    }
                }
                data = "\t\t</Material>\n\n";
                fwrite(data.c_str(), data.size(),1,datei);
            }
            data = "\t</MaterialPipeline>\n\n";
            fwrite(data.c_str(), data.size(),1,datei);

            //write RenderPipeline Materials
            for (u32 r=0;r<RenderPipeline.size();r++)
            {
                data = "\t<MaterialPipeline>\n";
                fwrite(data.c_str(), data.size(),1,datei);
                for (irr::u32 i=0;i<RenderPipeline[r]->getMaterialCount();i++)
                {
                    data = "\t\t<Material id=\"";
                    data.append(irr::core::stringc(i));
                    data.append("\">\n");
                    fwrite(data.c_str(), data.size(),1,datei);

                    //write MaterialType
                    if (GraphicManager)
                    {
                        data = "\t\t\t<MaterialType value=\"";
                        data.append(GraphicManager->getMaterialName(RenderPipeline[r]->getMaterial(i).MaterialType));
                        data.append("\"/>\n");
                        fwrite(data.c_str(), data.size(),1,datei);
                    }

                    //write MaterialTypeParam
                    data = "\t\t\t<MaterialTypeParam value=\"";
                    data.append(irr::core::stringc(RenderPipeline[r]->getMaterial(i).MaterialTypeParam));
                    data.append("\"/>\n");
                    fwrite(data.c_str(), data.size(),1,datei);

                    //write MaterialTypeParam2
                    data = "\t\t\t<MaterialTypeParam2 value=\"";
                    data.append(irr::core::stringc(RenderPipeline[r]->getMaterial(i).MaterialTypeParam2));
                    data.append("\"/>\n");
                    fwrite(data.c_str(), data.size(),1,datei);

                    //write Lightning
                    data = "\t\t\t<Lightning value=\"";
                    if (RenderPipeline[r]->getMaterial(i).Lighting)
                        data.append("true");
                    else
                        data.append("false");
                    data.append("\"/>\n");
                    fwrite(data.c_str(), data.size(),1,datei);

                    //write BackfaceCulling
                    data = "\t\t\t<BackfaceCulling value=\"";
                    if (RenderPipeline[r]->getMaterial(i).BackfaceCulling)
                        data.append("true");
                    else
                        data.append("false");
                    data.append("\"/>\n");
                    fwrite(data.c_str(), data.size(),1,datei);

                    //write FrontfaceCulling
                    data = "\t\t\t<FrontfaceCulling value=\"";
                    if (RenderPipeline[r]->getMaterial(i).FrontfaceCulling)
                        data.append("true");
                    else
                        data.append("false");
                    data.append("\"/>\n");
                    fwrite(data.c_str(), data.size(),1,datei);

                    //write Textures
                    for (irr::u32 t=0;t<4;t++)
                    {
                        if (RenderPipeline[r]->getMaterial(i).getTexture(t))
                        {
                            data = "\t\t\t<Texture value=\"";
                            irr::core::stringc n = RenderPipeline[r]->getMaterial(i).getTexture(t)->getName();
                            data.append(n);
                            data.append("\" id=\"");
                            data.append(irr::core::stringc(t));
                            data.append("\"/>\n");
                            fwrite(data.c_str(), data.size(),1,datei);
                        }
                    }
                    data = "\t\t</Material>\n\n";
                    fwrite(data.c_str(), data.size(),1,datei);
                }
                data = "\t</MaterialPipeline>\n\n";
                fwrite(data.c_str(), data.size(),1,datei);
            }

            //write Animations
            for (irr::u32 i=0;i<Animations.size();i++)
            {
                data = "\t<Animation name=\"";
                data.append(Animations[i].Name);
                data.append("\">\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //Range
                data = "\t\t<Range value=\"";
                data.append(irr::core::stringc(Animations[i].Start));
                data.append("\" value1=\"");
                data.append(irr::core::stringc(Animations[i].End));
                data.append("\">\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //Speed
                data = "\t\t<Speed value=\"";
                data.append(irr::core::stringc(Animations[i].Speed));
                data.append("\">\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //Loop
                data = "\t\t<Loop value=\"";
                if (Animations[i].Loop)
                    data.append("true");
                else
                    data.append("false");
                data.append("\">\n");
                fwrite(data.c_str(), data.size(),1,datei);

                //Next animation(only makes sense to save if the animation is non looping
                if (!Animations[i].Loop)
                    for (irr::u32 n=0;n<Animations[i].Next.size();n++)
                    {
                        data = "\t\t<Next value=\"";
                        data.append(Animations[i].Next[n]);
                        data.append("\"/>\n");
                        fwrite(data.c_str(), data.size(),1,datei);
                    }

                data = "\t</Animation>\n\n";
                fwrite(data.c_str(), data.size(),1,datei);
            }

            fwrite("</Object>", 9,1,datei);
            fclose(datei);
        }
    }
}
