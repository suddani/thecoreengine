#include "CThread.h"
namespace AppFrame
{
namespace thread
{
#ifdef APPFRAME_USING_THREADS
CThread::CThread(unsigned int mutexcount) : MutexCount(mutexcount)
{
    StopThread = true;

    Mutexes = NULL;

    if (MutexCount > 0)
    {
        Mutexes = new pthread_mutex_t[MutexCount];
        for (unsigned int i=0; i<MutexCount; ++i)
            pthread_mutex_init(&Mutexes[i], NULL);
    }
}

CThread::~CThread()
{
    stopThread();
    if (Mutexes)
    {
        for (unsigned int i=0; i<MutexCount; ++i)
            pthread_mutex_destroy(&Mutexes[i]);
        delete [] Mutexes;
    }
    Mutexes = NULL;
}

void CThread::startThread(void)
{
    if (!isThreadStoppped())
        return;
    StopThread = false;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&ThreadId, &attr, CThread::thread_Main, (void *)this);
}

bool CThread::stopThread(void)
{
    StopThread = true;
    void* status;
    //printf("Join thread\n");
    pthread_join(ThreadId, &status);
    //printf("Join thread done\n");
    return StopThread;
}

bool CThread::isThreadStoppped(void)
{
    return StopThread;
}

bool CThread::lockMutex(const unsigned int& id)
{
    if (id >= MutexCount)
        return false;
    pthread_mutex_lock (&Mutexes[id]);
    return true;
}

bool CThread::trylockMutex(const unsigned int& id)
{
    if (id >= MutexCount)
        return false;
    return pthread_mutex_trylock (&Mutexes[id]) == 0 ? true : false;
}

bool CThread::unlockMutex(const unsigned int& id)
{
    if (id >= MutexCount)
        return false;
    pthread_mutex_unlock (&Mutexes[id]);
    return true;
}

void* CThread::thread_Main(void* data)
{
    CThread* thread = (CThread*)data;
    while(thread->thread_main())
    {
        if (thread->isThreadStoppped())
            break;
    }
    thread->StopThread = true;
    pthread_exit(NULL);
}
#endif
}
}
