#include "CProcessManager.h"
#include "IProcess.h"
#include "IGraphicManager.h"
#include "IInputManager.h"
#include "IConsole.h"
namespace AppFrame
{
    IProcessManager* createProcessManager(graphic::IGraphicManager* graphic, input::IInputManager* input)
    {
        #ifndef DEBUG
        //AppFrame::ILogger::setLogLevel(1);
        nlogLevel = 1;
        #endif
        return new CProcessManager(graphic, input);
    }

    CProcessManager::CProcessManager(graphic::IGraphicManager* graphic, input::IInputManager* input)
    {
        //ctor
        MainProcess = NULL;
        Graphic     = graphic;
        Graphic->grab();
        Input       = input;
        Input->addListener(this);
        Input->grab();

        Console = input::createConsole(graphic, input);
        Console->RegisterConsoleCommand("/ls", CONSOLE_CALLBACK_METHOD(CProcessManager, ls), "shows all running Processes");
        Console->RegisterConsoleCommand("/kill", CONSOLE_CALLBACK_METHOD(CProcessManager, kill), "kills the specified process");
        Console->RegisterConsoleCommand("/run", CONSOLE_CALLBACK_METHOD(CProcessManager, run), "runs the specified process");
        Console->RegisterConsoleCommand("/setMain", CONSOLE_CALLBACK_METHOD(CProcessManager, setMain), "sets the specified process to be the main process");
        if (graphic->getDevice()->getVideoDriver()->getDriverType() == irr::video::EDT_NULL)
            Console->setIsConsoleApplication(true);
    }

    CProcessManager::~CProcessManager(void)
    {
        //dtor
        Graphic->drop();
        Input->removeListener(this);
        Input->drop();
        Console->drop();
    }

    void CProcessManager::setMainProcess(const u32& id)
    {
        irr::core::list<IProcess*>::Iterator it = RunningProcesses.begin();
        while (it != RunningProcesses.end())
        {
            if ((*it)->getProcessID() == id)
            {
                if (*it == MainProcess)
                    return;
                if (MainProcess)
                    MainProcess->makeSlave();
                MainProcess = (*it);
                MainProcess->makeMain();
                return;
            }
            ++it;
        }
    }

    void CProcessManager::gui_event(const irr::SEvent::SGUIEvent& event)
    {
        if (!MainProcess)
            return;

        MainProcess->OnEvent(event);
    }

    bool CProcessManager::Run(void)
    {
        if (RunningProcesses.getSize() <= 0)
            return false;

        //update console
        Console->UpdateConsoleInput();

        if (!MainProcess)
        {
            nlog<<"No MainProcess set"<<nlendl;
            if (RunningProcesses.getSize() > 0)
            {
                nlog<<"Set new MainProcess"<<nlendl;
                irr::core::list<IProcess*>::Iterator it = RunningProcesses.begin();
                MainProcess = (*it);
                MainProcess->makeMain();
            }
        }

        //update process
        irr::core::list<IProcess*>::Iterator it = RunningProcesses.begin();
        while (it != RunningProcesses.end())
        {
            if ((*it)->alwaysRun() || MainProcess == *it)
                (*it)->Update();
            ++it;
        }

        //Kill process
        it = KillProcesses.begin();
        while (it != KillProcesses.end())
        {
            detachProcess((*it));
            ++it;
        }
        KillProcesses.clear();

        return true;
    }

    void CProcessManager::registerProcess(const u32& id, IProcessFactory* factory)
    {
        irr::core::map<u32, IProcessFactory*>::Node* node = RegisteredProcesses.find(id);
        if (node)
        {
            node->getValue()->drop();
        }
        RegisteredProcesses[id] = factory;
    }

    IProcess* CProcessManager::createProcess(const u32& id, const SCommand& command)
    {
        //dtor
        irr::core::map<u32, IProcessFactory*>::Node* node = RegisteredProcesses.find(id);
        if (node)
        {
            IProcess* process = node->getValue()->create(command);
            if (attachProcess(process))
            {
                nlog<<"Attached Process"<<nlfendl;
                process->drop();
                return process;
            }
            if (process)
            {
                nlog<<"Drop Process"<<nlfendl;
                process->drop();
                process = NULL;
            }
            return NULL;
        }
        return NULL;
    }

    bool CProcessManager::attachProcess(IProcess* process)
    {
        //dtor
        if (!process)
            return false;
        irr::core::list<IProcess*>::Iterator it = RunningProcesses.begin();
        while (it != RunningProcesses.end())
        {
            if ((*it) == process || (*it)->getProcessID() == process->getProcessID())
            {
                nlog<<"Process already exists"<<nlfendl;
                return false;
            }
            ++it;
        }

        RunningProcesses.push_back(process);
        process->AttachProcess(this);
        process->grab();
        return true;
    }

    bool CProcessManager::detachProcess(IProcess* process)
    {
        if (!process)
            return false;
        irr::core::list<IProcess*>::Iterator it = RunningProcesses.begin();
        while (it != RunningProcesses.end())
        {
            if ((*it) == process)
            {
                RunningProcesses.erase(it);
                if (MainProcess == process)
                {
                    MainProcess->makeSlave();
                    MainProcess = NULL;
                }
                process->DetachProcess(this);
                process->drop();
                break;
            }
            ++it;
        }
        return true;
    }

    void CProcessManager::killProcess(const u32& id)
    {
        //dtor
        IProcess* p = NULL;

        irr::core::list<IProcess*>::Iterator it = RunningProcesses.begin();
        while (it != RunningProcesses.end())
        {
            if ((*it)->getProcessID() == id)
            {
                p = (*it);
                break;
            }
            ++it;
        }

        if (p)
            KillProcesses.push_back(p);
    }

    void CProcessManager::killAll(void)
    {
        irr::core::list<IProcess*>::Iterator it = RunningProcesses.begin();
        while (it != RunningProcesses.end())
        {
            killProcess((*it)->getProcessID());
            ++it;
        }
        killActiveProcess();
    }

    void CProcessManager::killActiveProcess(void)
    {
        KillProcesses.push_back(MainProcess);
    }

    input::IConsole* CProcessManager::getConsole(void)
    {
        //dtor
        return Console;
    }

    input::IInputManager* CProcessManager::getInputManager(void)
    {
        return Input;
    }

    graphic::IGraphicManager* CProcessManager::getGraphicManager(void)
    {
        return Graphic;
    }

    void CProcessManager::run(const AppFrame::SCommand& command)
    {
        if (command.Parameter.size() <= 0)
        {
            nlogl(1)<<"ID missing - try: /run ID"<<nlfendl;
            return;
        }
        createProcess(ProcessID(command.Parameter[0].Value.c_str()), "");
    }

    void CProcessManager::kill(const AppFrame::SCommand& command)
    {
        if (command.Parameter.size() <= 0)
        {
            nlogl(1)<<"ID missing - try: /kill ID"<<nlfendl;
            return;
        }
        killProcess(command.Parameter[0].toInt());
    }

    void CProcessManager::setMain(const AppFrame::SCommand& command)
    {
        if (command.Parameter.size() <= 0)
        {
            nlogl(1)<<"ID missing - try: /setMain ID"<<nlfendl;
            return;
        }
        setMainProcess(command.Parameter[0].toInt());
    }

    void CProcessManager::ls(const AppFrame::SCommand& command)
    {
        if (MainProcess)
            nlogl(1)<<"Running Process["<<ProcessIDstr(MainProcess->getProcessID()).c_str()<<" - "<<MainProcess->getProcessName()<<"]"<<nlfendl;
        irr::core::stringc info = nlinfo;
        nlinfo = "";
        nlogl(1)<<"\t [ID] - [Name]"<<nlfendl;
        irr::core::list<IProcess*>::Iterator it = RunningProcesses.begin();
        while (it != RunningProcesses.end())
        {
            nlogl(1)<<"\t "<<(*it)->getProcessID()<<" - "<<(*it)->getProcessName()<<nlfendl;
            ++it;
        }
        nlinfo = info;
    }
}
