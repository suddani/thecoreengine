/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#include "CInputManager.h"
#include "InputListener.h"
//#include "core/ILogger.h"
#include "core/ILog.h"

#ifdef USE_GLFW
#include <GL/glfw.h>
#include "../../../../3DGraphic/braindamage3d/include/ILogger.h"
#endif

namespace AppFrame
{
    namespace input
    {
        IInputManager* createInputManager(irr::IrrlichtDevice* device)
        {
            #ifndef DEBUG
            //AppFrame::ILogger::setLogLevel(1);
            nlogLevel = 1;
            #endif
            CInputManager* manager = new CInputManager;
            device->setEventReceiver(manager);
            manager->Device = device;
            return manager;
        }
        #ifdef USE_GLFW
        void PollEventsThread(void* arg)
        {
            nlog<<"Started InputThread"<<nlfendl;
            CInputManager* input = (CInputManager*)arg;
            while(input->RunPollEvents)
            {
                input->pollEvents();
                glfwSleep(0.0);
            }
        }
        #endif

        CInputManager::CInputManager(void)
        {
            #ifdef USE_GLFW
            UpdateINPUT = true;
            RunPollEvents = true;
            ILogger::log("Start InputThread...");
            EventThread = glfwCreateThread(PollEventsThread, (void*)this );
            ILogger::log("EventThread: %i", EventThread);
            KeyMutex = glfwCreateMutex();
            MouseMutex = glfwCreateMutex();
            JoystickMutex = glfwCreateMutex();
            #endif
            Restricted_Key	= NULL;
            Restricted_Mouse	= NULL;
            Restricted_Joystick	= NULL;
        }

        CInputManager::~CInputManager(void)
        {
            //dtor
            #ifdef USE_GLFW
            RunPollEvents = false;
            glfwWaitThread( EventThread, GLFW_WAIT );
            glfwDestroyThread( EventThread );
            glfwDestroyMutex(KeyMutex);
            glfwDestroyMutex(MouseMutex);
            glfwDestroyMutex(JoystickMutex);
            #endif
        }

        const E_KEY_STATE& CInputManager::getKeyState(irr::EKEY_CODE key)
        {
            return keymap[key].pressed;
        }

        const KeyState* CInputManager::getKey(irr::EKEY_CODE key)
        {
            #ifndef USE_GLFW
            keymap[key].Name = key2string((irr::EKEY_CODE)key).c_str();
            #endif
            return &keymap[key];
        }

        irr::EKEY_CODE CInputManager::getKeyCode(const char* key)
        {
            for (u32 i=0;i<irr::KEY_KEY_CODES_COUNT;++i)
            {
                if (getKey((irr::EKEY_CODE)i)->Name == key)
                    return (irr::EKEY_CODE)i;
            }
            return irr::KEY_KEY_CODES_COUNT;
        }

        #ifdef USE_GLFW
        void CInputManager::Update(void)
        {
            glfwLockMutex(KeyMutex);
            list<KeyInputEvent>::Iterator it = KeyInputEvents.begin();
            while (it != KeyInputEvents.end())
            {
                //E_KEY_STATE tmp = keymap[(*it).Key].pressed;
                //keymap[(*it).Key].pressed = (*it).State;
                updateKeyListener((*it).Key, (*it).State);
                //keymap[(*it).Key].pressed = tmp;
                ++it;
            }
            KeyInputEvents.clear();
            glfwUnlockMutex(KeyMutex);
        }
        void CInputManager::pollEvents(void)
        {
            //poll keystates
            u32 i = 0;
            glfwLockMutex(KeyMutex);
            while (i<EKK_COUNT)
            {
                if (keymap[i].pressed != (E_KEY_STATE)glfwGetKey(i))
                {
                    keymap[i].pressed = (E_KEY_STATE)glfwGetKey(i);

                    KeyInputEvents.push_back(KeyInputEvent((E_KEYBOARD_KEY)i, keymap[i]));

                    //updateKeyListener((E_KEYBOARD_KEY)i, &keymap[i]);
                }
                i++;
            }
            glfwUnlockMutex(KeyMutex);
        }
        #else
        bool CInputManager::OnEvent(const irr::SEvent& event)
        {
            switch (event.EventType)
            {
            case irr::EET_LOG_TEXT_EVENT:
                {
                    AppFrame::string lText = nlinfo.c_str();
                    nlinfo = "Irrlicht: ";
                    nlogl(1)<<event.LogEvent.Text;
                    nlinfo = lText.c_str();
                    return true;
                }
                break;
            case irr::EET_GUI_EVENT:
                updateGUIListener(event.GUIEvent);
                break;
            case irr::EET_KEY_INPUT_EVENT:
                if ((keymap[event.KeyInput.Key].pressed == EKS_UP && event.KeyInput.PressedDown == true )||(keymap[event.KeyInput.Key].pressed == EKS_DOWN && event.KeyInput.PressedDown == false ))
                {
                    if (event.KeyInput.PressedDown)
                        keymap[event.KeyInput.Key].pressed = EKS_DOWN;
                    else
                        keymap[event.KeyInput.Key].pressed = EKS_UP;
                    updateKeyListener(event.KeyInput.Key, keymap[event.KeyInput.Key]);
                }
                break;
            case irr::EET_MOUSE_INPUT_EVENT:
                {
                    Mouse.LeftPressed = event.MouseInput.isLeftPressed() ? EKS_DOWN : EKS_UP;
                    Mouse.RightPressed = event.MouseInput.isRightPressed() ? EKS_DOWN : EKS_UP;
                    Mouse.MiddlePressed = event.MouseInput.isMiddlePressed() ? EKS_DOWN : EKS_UP;
                    Mouse.MouseWheel = event.MouseInput.Wheel;
                    E_MOUSE_EVENT_TYPE eventType;
                    switch (event.MouseInput.Event)
                    {
                        case irr::EMIE_LMOUSE_LEFT_UP:
                        case irr::EMIE_LMOUSE_PRESSED_DOWN:
                            eventType = EMET_LEFT;
                            break;
                        case irr::EMIE_RMOUSE_LEFT_UP:
                        case irr::EMIE_RMOUSE_PRESSED_DOWN:
                            eventType = EMET_RIGHT;
                            break;
                        case irr::EMIE_MMOUSE_LEFT_UP:
                        case irr::EMIE_MMOUSE_PRESSED_DOWN:
                            eventType = EMET_MIDDLE;
                            break;
                        case irr::EMIE_MOUSE_MOVED:
                            eventType = EMET_MOVE;
                            Mouse.AbsolutePos[0] = event.MouseInput.X;
                            Mouse.AbsolutePos[1] = event.MouseInput.Y;
                            break;
                        case irr::EMIE_MOUSE_WHEEL:
                            eventType = EMET_WHEEL;
                            break;
                    }
                    Mouse.RelativePos[0] = Device->getCursorControl()->getRelativePosition().X;
                    Mouse.RelativePos[1] = Device->getCursorControl()->getRelativePosition().Y;
                    updateMouseListener(eventType, &Mouse);
                }
                break;
            }
            return false;//m_frame->OnEvent(event);
        }
        #endif

        void CInputManager::addListener(IGUIListener* listener)
        {
            if (listener->IGUIListener_Manager != NULL)
                return;

            #ifdef USE_GLFW
            list<IGUIListener*>::Iterator it = GUIListener.begin();
            #else
            irr::core::list<IGUIListener*>::Iterator it = GUIListener.begin();
            #endif
            while (it != GUIListener.end())
            {
                if (*it == listener)
                {
                    return;
                }
                ++it;
            }
            listener->IGUIListener_Manager = this;
            GUIListener.push_back(listener);
        }

        void CInputManager::removeListener(IGUIListener* listener)
        {
            #ifdef USE_GLFW
            list<IGUIListener*>::Iterator it = GUIListener.begin();
            #else
            irr::core::list<IGUIListener*>::Iterator it = GUIListener.begin();
            #endif
            while (it != GUIListener.end())
            {
                if (*it == listener)
                {
                    (*it)->IGUIListener_Manager = NULL;
                    GUIListener.erase(it);
                    break;
                }
                ++it;
            }
        }

        void CInputManager::addListener(IKeyListener* listener)
        {
            if (listener->IKeyListener_Manager != NULL)
                return;

            #ifdef USE_GLFW
            list<IKeyListener*>::Iterator it = KeyListener.begin();
            #else
            irr::core::list<IKeyListener*>::Iterator it = KeyListener.begin();
            #endif
            while (it != KeyListener.end())
            {
                if (*it == listener)
                {
                    return;
                }
                ++it;
            }
            listener->IKeyListener_Manager = this;
            KeyListener.push_back(listener);
        }

        void CInputManager::removeListener(IKeyListener* listener)
        {
            #ifdef USE_GLFW
            list<IKeyListener*>::Iterator it = KeyListener.begin();
            #else
            irr::core::list<IKeyListener*>::Iterator it = KeyListener.begin();
            #endif
            while (it != KeyListener.end())
            {
                if (*it == listener)
                {
                    (*it)->IKeyListener_Manager = NULL;
                    KeyListener.erase(it);
                    break;
                }
                ++it;
            }
        }

        void CInputManager::restrictListener(IKeyListener* listener)
        {
            Restricted_Key = listener;
        }

        void CInputManager::addListener(IMouseListener* listener)
        {
            if (listener->IMouseListener_Manager != NULL)
                return;

            #ifdef USE_GLFW
            list<IMouseListener*>::Iterator it = MouseListener.begin();
            #else
            irr::core::list<IMouseListener*>::Iterator it = MouseListener.begin();
            #endif
            while (it != MouseListener.end())
            {
                if (*it == listener)
                {
                    return;
                }
                ++it;
            }
            listener->IMouseListener_Manager = this;
            MouseListener.push_back(listener);
        }

        void CInputManager::removeListener(IMouseListener* listener)
        {
            #ifdef USE_GLFW
            list<IMouseListener*>::Iterator it = MouseListener.begin();
            #else
            irr::core::list<IMouseListener*>::Iterator it = MouseListener.begin();
            #endif
            while (it != MouseListener.end())
            {
                if (*it == listener)
                {
                    (*it)->IMouseListener_Manager = NULL;
                    MouseListener.erase(it);
                    break;
                }
                ++it;
            }
        }

        void CInputManager::restrictListener(IMouseListener* listener)
        {
            Restricted_Mouse = listener;
        }

        void CInputManager::addListener(IJoystickListener* listener)
        {
            if (listener->IJoystickListener_Manager != NULL)
                return;
            #ifdef USE_GLFW
            list<IJoystickListener*>::Iterator it = JoystickListener.begin();
            #else
            irr::core::list<IJoystickListener*>::Iterator it = JoystickListener.begin();
            #endif
            while (it != JoystickListener.end())
            {
                if (*it == listener)
                {
                    return;
                }
                ++it;
            }
            listener->IJoystickListener_Manager = this;
            JoystickListener.push_back(listener);
        }

        void CInputManager::removeListener(IJoystickListener* listener)
        {
            #ifdef USE_GLFW
            list<IJoystickListener*>::Iterator it = JoystickListener.begin();
            #else
            irr::core::list<IJoystickListener*>::Iterator it = JoystickListener.begin();
            #endif
            while (it != JoystickListener.end())
            {
                if (*it == listener)
                {
                    (*it)->IJoystickListener_Manager = NULL;
                    JoystickListener.erase(it);
                    break;
                }
                ++it;
            }
        }

        void CInputManager::restrictListener(IJoystickListener* listener)
        {
            Restricted_Joystick = listener;
        }

        void CInputManager::updateGUIListener(const irr::SEvent::SGUIEvent& event)
        {
            //TODO: don't update when console is active
            #ifdef USE_GLFW
            list<IGUIListener*>::Iterator it = GUIListener.begin();
            #else
            irr::core::list<IGUIListener*>::Iterator it = GUIListener.begin();
            #endif
            while (it != GUIListener.end())
            {
                (*it)->gui_event(event);
                ++it;
            }
        }

        void CInputManager::updateKeyListener(irr::EKEY_CODE key, KeyState& state)
        {
            //TODO: don't update when console is active
            #ifdef USE_GLFW
            list<IKeyListener*>::Iterator it = KeyListener.begin();
            #else
            irr::core::list<IKeyListener*>::Iterator it = KeyListener.begin();
            #endif
            while (it != KeyListener.end())
            {
                if (Restricted_Key == *it || Restricted_Key == NULL)
                	(*it)->key_event(key, state);
                ++it;
            }
        }

        void CInputManager::updateMouseListener(E_MOUSE_EVENT_TYPE key, MouseState* state)
        {
            //TODO: don't update when console is active
            #ifdef USE_GLFW
            list<IMouseListener*>::Iterator it = MouseListener.begin();
            #else
            irr::core::list<IMouseListener*>::Iterator it = MouseListener.begin();
            #endif
            while (it != MouseListener.end())
            {
                if (Restricted_Mouse == *it || Restricted_Mouse == NULL)
                	(*it)->mouse_event(key, *state);
                ++it;
            }
        }

        void CInputManager::updateJoystickListener(E_JOYSTICK_EVENT_TYPE key, JoystickState* state)
        {
            //TODO: don't update when console is active
            #ifdef USE_GLFW
            list<IJoystickListener*>::Iterator it = JoystickListener.begin();
            #else
            irr::core::list<IJoystickListener*>::Iterator it = JoystickListener.begin();
            #endif
            while (it != JoystickListener.end())
            {
                if (Restricted_Joystick == *it || Restricted_Joystick == NULL)
                	(*it)->joystick_event(key, *state);
                ++it;
            }
        }
        #ifndef USE_GLFW
        irr::core::stringc CInputManager::key2string(irr::EKEY_CODE  key)
        {
            switch (key)
            {
            case irr::KEY_LBUTTON:
                return "lbutton";
                break;
            case irr::KEY_RBUTTON:
                return "rbutton";
                break;
            case irr::KEY_CANCEL:
                return "cancel";
                break;
            case irr::KEY_MBUTTON:
                return "mbutton";
                break;
            case irr::KEY_XBUTTON1:
                return "xbutton1";
                break;
            case irr::KEY_XBUTTON2:
                return "xbutton2";
                break;
            case irr::KEY_BACK:
                return "back";
                break;
            case irr::KEY_TAB:
                return "tab";
                break;
            case irr::KEY_CLEAR:
                return "clear";
                break;
            case irr::KEY_RETURN:
                return "return";
                break;
            case irr::KEY_SHIFT:
                return "shift";
                break;
            case irr::KEY_CONTROL:
                return "control";
                break;
            case irr::KEY_MENU:
                return "menu";
                break;
            case irr::KEY_PAUSE:
                return "pause";
                break;
            case irr::KEY_CAPITAL:
                return "capital";
                break;
                //case irr::KEY_KANA:
                //return "kana";
                //break;
                //case irr::KEY_HANGUEL:
                //return "hanguel";
                //break;
            case irr::KEY_HANGUL:
                return "hangul";
                break;
            case irr::KEY_JUNJA:
                return "junja";
                break;
            case irr::KEY_FINAL:
                return "final";
                break;
                //case irr::KEY_HANJA:
                //return "hanja";
                //break;
            case irr::KEY_KANJI:
                return "kanji";
                break;
            case irr::KEY_ESCAPE:
                return "escape";
                break;
            case irr::KEY_CONVERT:
                return "convert";
                break;
            case irr::KEY_NONCONVERT:
                return "nonconvert";
                break;
            case irr::KEY_ACCEPT:
                return "accept";
                break;
            case irr::KEY_MODECHANGE:
                return "modechange";
                break;
            case irr::KEY_SPACE:
                return "space";
                break;
            case irr::KEY_PRIOR:
                return "prior";
                break;
            case irr::KEY_NEXT:
                return "next";
                break;
            case irr::KEY_END:
                return "end";
                break;
            case irr::KEY_HOME:
                return "home";
                break;
            case irr::KEY_LEFT:
                return "left";
                break;
            case irr::KEY_UP:
                return "up";
                break;
            case irr::KEY_RIGHT:
                return "right";
                break;
            case irr::KEY_DOWN:
                return "down";
                break;
            case irr::KEY_SELECT:
                return "select";
                break;
            case irr::KEY_PRINT:
                return "print";
                break;
            case irr::KEY_EXECUT:
                return "execut";
                break;
            case irr::KEY_SNAPSHOT:
                return "snapshot";
                break;
            case irr::KEY_INSERT:
                return "insert";
                break;
            case irr::KEY_DELETE:
                return "delete";
                break;
            case irr::KEY_HELP:
                return "help";
                break;
            case irr::KEY_KEY_0:
                return "0";
                break;
            case irr::KEY_KEY_1:
                return "1";
                break;
            case irr::KEY_KEY_2:
                return "2";
                break;
            case irr::KEY_KEY_3:
                return "3";
                break;
            case irr::KEY_KEY_4:
                return "4";
                break;
            case irr::KEY_KEY_5:
                return "5";
                break;
            case irr::KEY_KEY_6:
                return "6";
                break;
            case irr::KEY_KEY_7:
                return "7";
                break;
            case irr::KEY_KEY_8:
                return "8";
                break;
            case irr::KEY_KEY_9:
                return "9";
                break;
            case irr::KEY_KEY_A:
                return "a";
                break;
            case irr::KEY_KEY_B:
                return "b";
                break;
            case irr::KEY_KEY_C:
                return "c";
                break;
            case irr::KEY_KEY_D:
                return "d";
                break;
            case irr::KEY_KEY_E:
                return "e";
                break;
            case irr::KEY_KEY_F:
                return "f";
                break;
            case irr::KEY_KEY_G:
                return "g";
                break;
            case irr::KEY_KEY_H:
                return "h";
                break;
            case irr::KEY_KEY_I:
                return "i";
                break;
            case irr::KEY_KEY_J:
                return "j";
                break;
            case irr::KEY_KEY_K:
                return "k";
                break;
            case irr::KEY_KEY_L:
                return "l";
                break;
            case irr::KEY_KEY_M:
                return "m";
                break;
            case irr::KEY_KEY_N:
                return "n";
                break;
            case irr::KEY_KEY_O:
                return "o";
                break;
            case irr::KEY_KEY_P:
                return "p";
                break;
            case irr::KEY_KEY_Q:
                return "q";
                break;
            case irr::KEY_KEY_R:
                return "r";
                break;
            case irr::KEY_KEY_S:
                return "s";
                break;
            case irr::KEY_KEY_T:
                return "t";
                break;
            case irr::KEY_KEY_U:
                return "u";
                break;
            case irr::KEY_KEY_V:
                return "v";
                break;
            case irr::KEY_KEY_W:
                return "w";
                break;
            case irr::KEY_KEY_X:
                return "x";
                break;
            case irr::KEY_KEY_Y:
                return "y";
                break;
            case irr::KEY_KEY_Z:
                return "z";
                break;
            case irr::KEY_LWIN:
                return "lwin";
                break;
            case irr::KEY_RWIN:
                return "rwin";
                break;
            case irr::KEY_APPS:
                return "apps";
                break;
            case irr::KEY_SLEEP:
                return "sleep";
                break;
            case irr::KEY_NUMPAD0:
                return "numpad0";
                break;
            case irr::KEY_NUMPAD1:
                return "numpad1";
                break;
            case irr::KEY_NUMPAD2:
                return "numpad2";
                break;
            case irr::KEY_NUMPAD3:
                return "numpad3";
                break;
            case irr::KEY_NUMPAD4:
                return "numpad4";
                break;
            case irr::KEY_NUMPAD5:
                return "numpad5";
                break;
            case irr::KEY_NUMPAD6:
                return "numpad6";
                break;
            case irr::KEY_NUMPAD7:
                return "numpad7";
                break;
            case irr::KEY_NUMPAD8:
                return "numpad8";
                break;
            case irr::KEY_NUMPAD9:
                return "numpad9";
                break;
            case irr::KEY_MULTIPLY:
                return "*";
                break;
            case irr::KEY_ADD:
                return "+";
                break;
            case irr::KEY_SEPARATOR:
                return "_";
                break;
            case irr::KEY_SUBTRACT:
                return "-";
                break;
            case irr::KEY_DECIMAL:
                return ",";
                break;
            case irr::KEY_DIVIDE:
                return "/";
                break;
            case irr::KEY_F1:
                return "f1";
                break;
            case irr::KEY_F2:
                return "f2";
                break;
            case irr::KEY_F3:
                return "f3";
                break;
            case irr::KEY_F4:
                return "f4";
                break;
            case irr::KEY_F5:
                return "f5";
                break;
            case irr::KEY_F6:
                return "f6";
                break;
            case irr::KEY_F7:
                return "f7";
                break;
            case irr::KEY_F8:
                return "f8";
                break;
            case irr::KEY_F9:
                return "f9";
                break;
            case irr::KEY_F10:
                return "f10";
                break;
            case irr::KEY_F11:
                return "f11";
                break;
            case irr::KEY_F12:
                return "f12";
                break;
            case irr::KEY_F13:
                return "f13";
                break;
            case irr::KEY_F14:
                return "f14";
                break;
            case irr::KEY_F15:
                return "f15";
                break;
            case irr::KEY_F16:
                return "f16";
                break;
            case irr::KEY_F17:
                return "f17";
                break;
            case irr::KEY_F18:
                return "f18";
                break;
            case irr::KEY_F19:
                return "f19";
                break;
            case irr::KEY_F20:
                return "f20";
                break;
            case irr::KEY_F21:
                return "f21";
                break;
            case irr::KEY_F22:
                return "f22";
                break;
            case irr::KEY_F23:
                return "f23";
                break;
            case irr::KEY_F24:
                return "f24";
                break;
            case irr::KEY_NUMLOCK:
                return "numlock";
                break;
            case irr::KEY_SCROLL:
                return "scroll";
                break;
            case irr::KEY_LSHIFT:
                return "lshift";
                break;
            case irr::KEY_RSHIFT:
                return "rshift";
                break;
            case irr::KEY_LCONTROL:
                return "lcontrol";
                break;
            case irr::KEY_RCONTROL:
                return "rcontrol";
                break;
            case irr::KEY_LMENU:
                return "lmenu";
                break;
            case irr::KEY_RMENU:
                return "rmenu";
                break;
            case irr::KEY_PLUS:
                return "+";
                break;
            case irr::KEY_COMMA:
                return ",";
                break;
            case irr::KEY_MINUS:
                return "-";
                break;
            case irr::KEY_PERIOD:
                return ".";
                break;
            case irr::KEY_ATTN:
                return "attn";
                break;
            case irr::KEY_CRSEL:
                return "crsel";
                break;
            case irr::KEY_EXSEL:
                return "exsel";
                break;
            case irr::KEY_EREOF:
                return "ereof";
                break;
            case irr::KEY_PLAY:
                return "play";
                break;
            case irr::KEY_ZOOM:
                return "zoom";
                break;
            case irr::KEY_PA1:
                return "pal";
                break;
            case irr::KEY_OEM_CLEAR:
                return "oem_clear";
                break;
            default:
                return "^";
                break;
            };
            return "NULL";
        }
        #endif
    }
}
