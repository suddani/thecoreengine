/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef CINPUTMANAGER_H
#define CINPUTMANAGER_H

#ifdef USE_GLFW
#include "../../../../3DGraphic/braindamage3d/include/appList.h"
#else
#include <irrlicht.h>
#endif

#include "IInputManager.h"

#ifdef USE_GLFW
typedef int GLFWthread;
typedef void* GLFWmutex;
#endif

namespace AppFrame
{
	class IAppFrame;
    namespace input
    {
        #ifdef USE_GLFW
        class CInputManager : public IInputManager
        #else
        class CInputManager : public IInputManager, public irr::IEventReceiver
        #endif
        {
        public:
            CInputManager(void);
            virtual ~CInputManager(void);

            #ifndef USE_GLFW
            virtual bool OnEvent(const irr::SEvent& event);
            #else
            void Update(void);
            #endif

            const E_KEY_STATE& getKeyState(irr::EKEY_CODE key);

            const KeyState* getKey(irr::EKEY_CODE key);

            irr::EKEY_CODE getKeyCode(const char* key);

            #ifndef USE_GLFW
            irr::core::stringc key2string(irr::EKEY_CODE  key);
            #endif

            void addListener(IGUIListener* listener);
            void removeListener(IGUIListener* listener);

            void addListener(IKeyListener* listener);
            void removeListener(IKeyListener* listener);
            void restrictListener(IKeyListener* listener);

            void addListener(IMouseListener* listener);
            void removeListener(IMouseListener* listener);
            void restrictListener(IMouseListener* listener);

            void addListener(IJoystickListener* listener);
            void removeListener(IJoystickListener* listener);
            void restrictListener(IJoystickListener* listener);

            irr::IrrlichtDevice* Device;

        protected:
            #ifdef USE_GLFW
            struct KeyInputEvent
            {
                KeyInputEvent(E_KEYBOARD_KEY key, KeyState& state)
                {
                    Key = key;
                    State = state;
                }
                KeyInputEvent(void)
                {
                    Key = EKK_0;
                }
                E_KEYBOARD_KEY Key;
                KeyState State;
            };
            list<KeyInputEvent> KeyInputEvents;

            friend void PollEventsThread(void* arg);

            bool RunPollEvents;

            bool UpdateINPUT;

            GLFWthread EventThread;

            GLFWmutex KeyMutex;
            GLFWmutex MouseMutex;
            GLFWmutex JoystickMutex;
            void pollEvents(void);
            #endif
			KeyState keymap[irr::KEY_KEY_CODES_COUNT];

			MouseState Mouse;

			IKeyListener* Restricted_Key;
			IMouseListener* Restricted_Mouse;
			IJoystickListener* Restricted_Joystick;

			#ifdef USE_GLFW
			list<IGUIListener*> GUIListener;
			#else
			irr::core::list<IGUIListener*> GUIListener;
			#endif
			void updateGUIListener(const irr::SEvent::SGUIEvent& event);

			#ifdef USE_GLFW
			list<IKeyListener*> KeyListener;
			#else
			irr::core::list<IKeyListener*> KeyListener;
			#endif
			void updateKeyListener(irr::EKEY_CODE key, KeyState& state);


			#ifdef USE_GLFW
			list<IMouseListener*> MouseListener;
			#else
			irr::core::list<IMouseListener*> MouseListener;
			#endif
			void updateMouseListener(E_MOUSE_EVENT_TYPE key, MouseState* state);

			#ifdef USE_GLFW
			list<IJoystickListener*> JoystickListener;
			#else
			irr::core::list<IJoystickListener*> JoystickListener;
			#endif
			void updateJoystickListener(E_JOYSTICK_EVENT_TYPE key, JoystickState* state);
        private:
        };
    }
}
#endif // CINPUTMANAGER_H
