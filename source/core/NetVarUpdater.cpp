#include "NetVarUpdater.h"
#include "SNetVar.h"
#include "ILogger.h"
namespace AppFrame
{
namespace network
{
NetVarUpdater::NetVarUpdater()
{
    //ctor
    State = EVC_NOTHING;
    NextUpdate = 0;
}

NetVarUpdater::~NetVarUpdater()
{
    //dtor
    Vars.clear();
}

void NetVarUpdater::registerNetVar(BaseNetVar* var)
{
    Vars.push_back(var);
}

void NetVarUpdater::receiveNetVarUpdate(BaseNetVar* var, const E_VAR_CHANGED& type)
{
    bool r = true;
    for (AppFrame::u32 i=0;i<Vars.size();++i)
        if (Vars[i] == var)
            r = false;
    if (r)
        return;

    switch(type)
    {
        case EVC_SEND:
            State = EVC_SEND;
            break;
        case EVC_SEND_ALL:
            State = EVC_SEND;
            break;
        case EVC_SEND_MAYBE:
            if (State == EVC_SEND_MAYBE)
                State = EVC_SEND;
            else if (State == EVC_NOTHING)
                State = EVC_SEND_MAYBE;
            break;
    }
    if (State == EVC_SEND && NextUpdate < 1)
    {
        //register for next update in X miliseconds
        NextUpdate = 1;
    }
}

void NetVarUpdater::sendUpdate(IDataStream* stream)
{
    if (State != EVC_SEND)
        return;

    //Send Data
    for (AppFrame::u32 i=0;i<Vars.size();++i)
    {

        if (Vars[i]->changed() || State == EVC_SEND_ALL /*&& is sending*/)
        {
            stream->Write((const AppFrame::c8)1);
            Vars[i]->serialize(stream);
        }
        else
        {
            stream->Write((const AppFrame::c8)0);
        }
    }

    State = EVC_NOTHING;
}

void NetVarUpdater::receiveUpdate(IDataStream* stream)
{
    AppFrame::c8 isData;
    for (AppFrame::u32 i=0;i<Vars.size();++i)
    {
        stream->Read(isData);
        if (isData == 1)
        {
            Vars[i]->deserialize(stream);
        }
    }
}
}
}
