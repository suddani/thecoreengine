#ifndef CPROCESSMANAGER_H
#define CPROCESSMANAGER_H

#include <IProcessManager.h>
#include <InputListener.h>

namespace AppFrame
{
    class CProcessManager : public IProcessManager, public input::IGUIListener
    {
    public:
        CProcessManager(graphic::IGraphicManager* graphic, input::IInputManager* input);

        ~CProcessManager();

        void setMainProcess(const u32& id);

        void gui_event(const irr::SEvent::SGUIEvent& event);

        bool Run(void);

        void registerProcess(const u32& id, IProcessFactory* factory);

        IProcess* createProcess(const u32& id, const SCommand& command);

        bool attachProcess(IProcess* process);

        bool detachProcess(IProcess* process);

        void killProcess(const u32& id);

        void killActiveProcess(void);

        void killAll(void);

        input::IConsole* getConsole(void);

        input::IInputManager* getInputManager(void);

        graphic::IGraphicManager* getGraphicManager(void);
    protected:
        IProcess* MainProcess;
        graphic::IGraphicManager* Graphic;
        input::IInputManager* Input;
        input::IConsole* Console;
        irr::core::list<IProcess*> RunningProcesses;
        irr::core::list<IProcess*> KillProcesses;
        irr::core::map<u32, IProcessFactory*> RegisteredProcesses;

        void ls(const AppFrame::SCommand& command);
        void kill(const AppFrame::SCommand& command);
        void run(const AppFrame::SCommand& command);
        void setMain(const AppFrame::SCommand& command);
    private:
    };
}
#endif // CPROCESSMANAGER_H
