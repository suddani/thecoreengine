/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#include "CGraphicManager.h"
#include "CGUIChatQueue.h"
#include "EntitySceneNodePlugin.h"
#include "Irrlicht_Extras/PostProcess/CPostProcess.h"
#include "Irrlicht_Extras/SceneNodes/CBeamSceneNode.h"
#include "Irrlicht_Extras/SceneNodes/CBoltSceneNode.h"
#include "Irrlicht_Extras/ObjectSceneNode/CObjectSceneNode.h"
#include "Irrlicht_Extras/ShaderMaterial/CShaderMaterial.h"
#include "Irrlicht_Extras/MeshLoader/CGilesLoader.h"
#include "Irrlicht_Extras/ParticleEngine/IParticleSystem.h"
#include "CAttributeNode.h"
#include "Config.h"

namespace AppFrame
{
    namespace graphic
    {
        irr::IrrlichtDevice* createIrrlichtDeviceFromFile(const irr::c8* file)
        {
            #ifndef DEBUG
            //AppFrame::ILogger::setLogLevel(1);
            nlogLevel = 1;
            #endif
            CConfig conf((irr::c8*)file);
            return irr::createDeviceEx(conf.getIrrlichtParameter());
        }

        IGraphicManager* createGraphicManager(irr::IrrlichtDevice* device)
        {
            return new CGraphicManager(device);
        }

        CGraphicManager::CGraphicManager(irr::IrrlichtDevice* device)
        {
            //ctor
            m_device = device;
            m_device->grab();
            m_sceneNodeFactory = NULL;
            registerExternalMeshLoader();
        }

        CGraphicManager::~CGraphicManager()
        {
            //dtor
            if (m_sceneNodeFactory)
                m_sceneNodeFactory->drop();

            //clear shader materials
            for (u32 i = 0;i<ShaderMaterials.size();i++)
            {
                ShaderMaterials[i]->drop();
            }
            ShaderMaterials.clear();

            //clear postprocess effects
            deactivateAllPostProcessEffects();
            for(u32 i=0;i<PostProcessEffects.size();i++)
            {
                PostProcessEffects[i]->drop();
            }
            PostProcessEffects.clear();

            m_device->drop();
        }

        irr::IrrlichtDevice* CGraphicManager::getDevice(void)
        {
            return m_device;
        }

        void CGraphicManager::draw(bool post)
        {
            if (!post || ActivePostProcessEffects.size() <= 0)
            {
                m_device->getSceneManager()->drawAll();
                return;
            }

            for(u32 i=0;i<ActivePostProcessEffects.size();i++)
            {
                if (i+1<ActivePostProcessEffects.size())
                    ActivePostProcessEffects[i]->Render(ActivePostProcessEffects[i+1]->getRenderTarget(), (i==0) ? true : false);
                else
                    ActivePostProcessEffects[i]->Render(NULL, (i==0) ? true : false);
            }
            m_device->getVideoDriver()->setMaterial(m_device->getVideoDriver()->getMaterial2D());
        }

        bool CGraphicManager::isPostProcessActive(const irr::c8* effectName)
        {
            for(u32 i=0;i<ActivePostProcessEffects.size();i++)
            {
                if (ActivePostProcessEffects[i]->getName() == effectName)
                    return true;
            }
            return false;
        }

        bool CGraphicManager::isPostProcessActive(IPostProcess* post)
        {
            for(u32 i=0;i<ActivePostProcessEffects.size();i++)
            {
                if (ActivePostProcessEffects[i] == post)
                    return true;
            }
            return false;
        }

        void CGraphicManager::deactivateAllPostProcessEffects(void)
        {
            ActivePostProcessEffects.clear();
        }

        void CGraphicManager::deactivatePostProcessEffect(const irr::c8* effectName)
        {
            for(u32 i=0;i<ActivePostProcessEffects.size();i++)
            {
                if (ActivePostProcessEffects[i]->getName() == effectName)
                    ActivePostProcessEffects.erase(i);
            }
        }

        void CGraphicManager::deactivatePostProcessEffect(IPostProcess* post)
        {
            for(u32 i=0;i<ActivePostProcessEffects.size();i++)
            {
                if (ActivePostProcessEffects[i] == post)
                    ActivePostProcessEffects.erase(i);
            }
        }

        void CGraphicManager::activatePostProcessEffect(const irr::c8* effectName)
        {
            IPostProcess* post = getPostProcess(effectName);
            ActivePostProcessEffects.push_back(post);
        }

        void CGraphicManager::activatePostProcessEffect(IPostProcess* post)
        {
            if (!post)
                return;
            IPostProcess* p = getPostProcess(post->getName().c_str());
            ActivePostProcessEffects.push_back(p);
        }

        bool CGraphicManager::addPostProcess(IPostProcess* post)
        {
            for(u32 i=0;i<PostProcessEffects.size();i++)
            {
                if (PostProcessEffects[i] == post)
                    return false;
            }
            PostProcessEffects.push_back(post);
            post->grab();
            return true;
        }

        IPostProcess* CGraphicManager::getPostProcess(const irr::c8* filename)
        {
            for(u32 i=0;i<PostProcessEffects.size();i++)
            {
                if (PostProcessEffects[i]->getName() == filename)
                    return PostProcessEffects[i];
            }
            IPostProcess* post = createPostProcess(filename);
            addPostProcess(post);
            post->drop();
            return post;
        }

        IPostProcess* CGraphicManager::createPostProcess(const irr::c8* filename)
        {
            //dtor
            nlog<<"Create new PostProcess"<<nlfendl;
            return new CPostProcess(filename, m_device);
        }

        IPostProcess* CGraphicManager::createPostProcess(irr::u32 width, irr::u32 height, const irr::c8* effectName)
        {
            //dtor
            return new CPostProcess(width, height, effectName, m_device);
        }

        IBeamNode* CGraphicManager::createBeamNode(const irr::c8* side, const irr::c8* front, irr::scene::ISceneNode* parent, irr::scene::ISceneManager* smgr)
        {
            //dtor
            if (smgr == NULL)
                smgr = m_device->getSceneManager();

            if (parent == NULL)
                parent = smgr->getRootSceneNode();

            return new irr::scene::CBeamNode(parent, smgr, -1, (char*)side, (char*)front);
        }

        IBoltNode* CGraphicManager::createBoltNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* smgr)
        {
            //dtor
            if (smgr == NULL)
                smgr = m_device->getSceneManager();

            if (parent == NULL)
                parent = smgr->getRootSceneNode();

            return new irr::scene::CBoltSceneNode(parent, smgr, -1);
        }

        irr::scene::IEntityNode* CGraphicManager::createEntityNode(irr::scene::ISceneNode* parent, irr::scene::ISceneManager* smgr, irr::s32 id)
        {
            if (smgr == NULL)
                smgr = m_device->getSceneManager();

            if (parent == NULL)
                parent = smgr->getRootSceneNode();

            return new irr::scene::CAttributeNode(parent, m_device, id);
        }

        irr::scene::IParticleSystem* CGraphicManager::createParticleSystem(irr::scene::ISceneNode* parent, irr::s32 id, const irr::core::vector3df& position, const irr::core::vector3df& rotation, const irr::core::vector3df& scale)
        {
            if (parent == NULL)
                parent = m_device->getSceneManager()->getRootSceneNode();

            return irr::scene::createParticleSystem(parent, m_device->getSceneManager(), id, position, rotation, scale);
        }

        irr::gui::IGUIChatQueue* CGraphicManager::createChatBox(irr::core::rect<irr::s32> rect, irr::gui::IGUIElement* parent, irr::s32 id, irr::gui::IGUIFont* font, irr::s32 life, irr::u32 FadeTime, bool wordwrap)
        {
            if (!m_device)
                return NULL;
            irr::gui::IGUIElement* p = parent;
            if (!p)
                p = m_device->getGUIEnvironment()->getRootGUIElement();
            irr::gui::CGUIChatQueue* box = new irr::gui::CGUIChatQueue(m_device->getGUIEnvironment(), p, id, rect, m_device->getTimer(), font, life, FadeTime, wordwrap);
            box->drop();
            return box;
        }

        irr::scene::IObjectSceneNode* CGraphicManager::addObjectSceneNode(irr::scene::ISceneNode* parent, const irr::s32& id, const irr::core::vector3df& position, const irr::core::vector3df& rotation, const irr::core::vector3df& scale)
        {
            if (!parent)
                parent = m_device->getSceneManager()->getRootSceneNode();
            irr::scene::IObjectSceneNode* obj = new CObjectSceneNode(parent, m_device->getSceneManager(), id, position, rotation, scale);
            obj->setGraphicManager(this);
            obj->drop();
            return obj;
        }

        irr::scene::ISceneNodeFactory* CGraphicManager::getSceneNodeFactory(void)
        {
            if (m_sceneNodeFactory == NULL && m_device != NULL)
            {
                m_sceneNodeFactory = new irr::scene::CEntitySceneNodeFactory(m_device->getSceneManager(), m_device);
                m_device->getSceneManager()->registerSceneNodeFactory(m_sceneNodeFactory);
            }
            return m_sceneNodeFactory;
        }

        irr::core::stringc getFileName(const irr::c8* file)
        {
            irr::core::stringc name = file;
            irr::core::stringc ret;
            for (irr::u32 i=0;i<name.size();i++)
            {
                if (name[i] == '.')
                    break;
                else
                {
                    printf("%c", name[i]);
                    ret.append(name[i]);
                }
            }

            return ret;
        }

        irr::video::E_MATERIAL_TYPE CGraphicManager::getMaterial(const irr::c8* mat)
        {
            for (u32 i = 0;i<ShaderMaterials.size();i++)
            {
                if (ShaderMaterials[i]->getName() == mat)
                    return ShaderMaterials[i]->getMaterial();
            }
            //now try to load and compile
            IShaderMaterial* newMat = new CShaderMaterial(mat, m_device);
            if (!newMat->isCompiled())
            {
                newMat->drop();
                return irr::video::EMT_SOLID;
            }
            ShaderMaterials.push_back(newMat);
            return newMat->getMaterial();
        }

        IShaderMaterial* CGraphicManager::getShaderMaterial(const irr::c8* mat)
        {
            for (u32 i = 0;i<ShaderMaterials.size();i++)
            {
                if (ShaderMaterials[i]->getName() == mat)
                    return ShaderMaterials[i];
            }
            //now try to load and compile
            IShaderMaterial* newMat = new CShaderMaterial(mat, m_device);
            if (!newMat->isCompiled())
            {
                newMat->drop();
                return NULL;
            }
            ShaderMaterials.push_back(newMat);
            return newMat;
        }

        const irr::core::stringc NoMaterialNameFound = "NoMaterialNaeFound";

        const irr::core::stringc checkMaterialName(irr::video::E_MATERIAL_TYPE type)
        {
            /*
            for (irr::s32 i=0;i<24;i++)
            {
                if (mat == irr::video::sBuiltInMaterialTypeNames[i])
                    return i;
            }
            */
            if (type < 24)
                return irr::video::sBuiltInMaterialTypeNames[type];
            return NoMaterialNameFound;
        }

        const irr::core::stringc CGraphicManager::getMaterialName(irr::video::E_MATERIAL_TYPE type)
        {
            for (u32 i = 0;i<ShaderMaterials.size();i++)
            {
                if (ShaderMaterials[i]->getMaterial() == type)
                {
                    nlog<<"Is ShaderMaterial"<<nlfendl;
                    return ShaderMaterials[i]->getName();
                }
            }
            irr::core::stringc name = checkMaterialName(type);
            return name;
        }

        void CGraphicManager::registerExternalMeshLoader(void)
        {
            irr::scene::IMeshLoader* loader = new irr::scene::CGilesLoader(m_device);
            m_device->getSceneManager()->addExternalMeshLoader(loader);
            loader->drop();
        }
    }
}
