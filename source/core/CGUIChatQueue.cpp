/*
ChatQueue is copyright g0dsoft 2008
Version: 0.3.1
Code by: Mark Laprairie
Contact: webmaster@g0dsoft.com
Url: www.g0dsoft.com

CGUIChatQueue.cpp
ChatQueue is a object for simulating half-life/starcraft style chat display
capable of colors, special effects and any custom font.
*/

#include "CGUIChatQueue.h"
#ifdef _IRR_COMPILE_WITH_GUI_

#include "IGUISkin.h"
#include "IGUIEnvironment.h"
#include "IVideoDriver.h"
#include "IGUIFont.h"
//#include "os.h"

namespace irr
{
    namespace gui
    {

        CGUIChatQueue::CGUIChatQueue(IGUIEnvironment* environment, IGUIElement* parent, s32 id,const core::rect<s32> &drawArea, ITimer* timer, IGUIFont* font, s32 textLife,
                                     u32 fadeTime, bool setWordWrap): IGUIChatQueue(environment, parent, id, drawArea)
        {
#ifdef _DEBUG
            setDebugName("CGUIChatQueue");
#endif

            //Initialize ChatQueue
            chatQueue_env = environment;

            m_TTB = false;

            if (!font)
                chatQueue_font =  environment->getBuiltInFont();
            else
                chatQueue_font = font;

            chatQueue_wordWrap = setWordWrap;
            chatQueue_life = textLife;
            setVisible(true);
            chatQueue_debug = false;
            chatQueue_fadeTime = fadeTime;
            chatQueue_defaultStyle = CQS_PLAIN;
            m_fontHeight = 0;
            chatQueue_calculateRect(drawArea);
            this->setFont(chatQueue_font);

            m_timer = timer;
        }

        CGUIChatQueue::~CGUIChatQueue()
        {
            chatQueue_list.empty();
        }

        void CGUIChatQueue::addMessage(core::stringw text,chatQueueStyle useStyle,video::SColor color1,video::SColor color2)
        {
            chatQueueMessage lmsg;
            s32 l_cutPosition = chatQueue_font->getCharacterFromPos(text.c_str(),m_xWidth);
            if (l_cutPosition != -1)
                wcscpy(lmsg.message,text.subString(0,l_cutPosition).c_str());
            else
                wcscpy(lmsg.message,text.subString(0,text.size()).c_str());

            lmsg.color1 = color1;
            lmsg.color2 = color2;
            lmsg.fade = 0;
            //Assign Style
            if (useStyle == CQS_USEDEFAULT)
                useStyle = chatQueue_defaultStyle;
            lmsg.Style = useStyle;
            lmsg.created = m_timer->getTime();//os::Timer::getTime();
            //Word preserving word wrapping, adapted from BlindSide's code
            if (l_cutPosition != -1 && chatQueue_wordWrap)
            {
                s32 spacepos = text.subString(0,l_cutPosition).findLast(32);
                if (spacepos != -1)
                    l_cutPosition = spacepos;
                wcscpy(lmsg.message,text.subString(0,l_cutPosition).c_str());
                chatQueue_list.push_front(lmsg);
                addMessage(text.subString(l_cutPosition,text.size()),useStyle,color1,color2);
            }
            else
                chatQueue_list.push_front(lmsg);
        }

        void CGUIChatQueue::addMessage(wchar_t* text,chatQueueStyle useStyle,video::SColor color1,video::SColor color2)
        {
            addMessage((const wchar_t*) text,useStyle,color1,color2);
        }

        void CGUIChatQueue::addMessage(const wchar_t* text,chatQueueStyle useStyle,video::SColor color1,video::SColor color2)
        {
            //I'm sure there is a better way to do this conversion, Ideas?
            core::stringw text2;
            text2 = "";
            text2.append(text);
            addMessage(text2,useStyle,color1,color2);
        }

        void CGUIChatQueue::draw()
        {
            //Debug
            if (chatQueue_debug && IsVisible)
                chatQueue_env->getVideoDriver()->draw2DRectangle(video::SColor(100,155,155,255),
                        core::rect<s32>(m_x, m_y - m_yHeight,m_x + m_xWidth,m_y));
            //Main Update/Draw
            core::list<chatQueueMessage>::Iterator iter;
            u16 count = 0;
            u32 m_y_tmp = m_y;
            u32 m_yStart_tmp = m_yStart;
            u32 l_time = m_timer->getTime();//os::Timer::getTime();
            if (!m_TTB)
            {
                for (iter=chatQueue_list.begin(); iter != chatQueue_list.end(); iter++)
                {
                    //Max lines means no fade, special case delete
                    if (count > chatQueue_maxLines)
                    {
                        //STL deprecation, I need to fix this..
                        //chatQueue_list.erase(iter,chatQueue_list.end());
                        chatQueue_list.erase(iter);
                        return;
                    }
                    //Update
                    if (chatQueue_life >= 0 && (iter->created + chatQueue_life) <= l_time)
                    {
                        if (iter->fade == 0 && chatQueue_fadeTime > 0)
                        {
                            iter->fade = l_time;
                            iter->color1Fade = iter->color1;
                            iter->color2Fade = iter->color2;
                            if (iter->Style == CQS_SHADOW)
                                iter->Style = CQS_INTERNALUSEONLY_FADE2;
                            else
                                iter->Style = CQS_INTERNALUSEONLY_FADE;
                        }
                        //Kill text that has faded away.
                        if ( ((l_time - iter->fade) >= chatQueue_fadeTime) || chatQueue_fadeTime <= 0 )
                        {
                            //STL deprecation, could cause problems. Needs extensive testing
                            //chatQueue_list.erase(iter,chatQueue_list.end());
                            chatQueue_list.erase(iter);
                            return;
                        }
                        // Old Fade code, left incase you would rather fade to a color
                        // Here it fades to black, w/ alpha fade.
                        // f32 l_alpha = (   (f32)(l_time - iter->fade)/(f32)chatQueue_fadeTime );
                        // iter->color1 = iter->color1Fade.getInterpolated(video::SColor(0,0,0,0),1-l_alpha);
                        // iter->color2 = iter->color2Fade.getInterpolated(video::SColor(0,0,0,0),1-l_alpha);

                        // Proposed alpha fade code, does not work with irrlicht 1.1
                        f32 l_alpha = (   (f32)(l_time - iter->fade)/(f32)chatQueue_fadeTime );
                        iter->color1.setAlpha((s32)(iter->color1Fade.getAlpha() * (1-l_alpha)));
                        iter->color2.setAlpha((s32)(iter->color2Fade.getAlpha() * (1-l_alpha)));

                    }
                    //Draw
                    if (IsVisible && chatQueue_font)
                    {
                        s16 l_offSetX = 0;
                        s16 l_offSetY = 0;
                        bool l_render2 = false;
                        //Apply style
                        switch (iter->Style)
                        {
                        case CQS_SHADOW:
                            l_offSetX = 1;
                            l_offSetY = 1;
                            l_render2 = true;
                            break;
                        case CQS_RAINBOW:
                            iter->color1 =video::SColor(255,(l_time/2)%255,(l_time/3)%255,(l_time)%255);
                            break;
                        case CQS_MULTIRAINBOW:
                            l_offSetX = 1;
                            l_offSetY = 1;
                            iter->color1 =video::SColor(255,(l_time/2)%255,(l_time/3)%255,(l_time)%255);
                            iter->color2 =video::SColor(255,(l_time)%255,(l_time/2)%255,(l_time/3)%255);
                            l_render2 = true;
                            break;
                        case CQS_BACKGROUND:

                            chatQueue_env->getVideoDriver()->draw2DRectangle(iter->color2,
                                    core::rect<s32>(m_x,m_y_tmp - m_fontHeight,m_x+m_xWidth,m_y_tmp));

                            break;
                        case CQS_CROPBACKGROUND:
                        {

                            core::dimension2d<u32> l_fontDimensions = chatQueue_font->getDimension(iter->message);

                            chatQueue_env->getVideoDriver()->draw2DRectangle(iter->color2,
                                    core::rect<s32>(m_x,m_y_tmp - m_fontHeight,m_x+l_fontDimensions.Width,m_y_tmp));

                            break;
                        }
                        case CQS_PULSE:
                            iter->color1Fade = iter->color1;
                            iter->Style = CQS_INTERNALUSEONLY_PULSE;
                        case CQS_INTERNALUSEONLY_PULSE:
                            iter->color1 = iter->color1Fade.getInterpolated(iter->color2,(l_time%1000)/(f32)999.9);
                            break;
                        case CQS_INTERNALUSEONLY_FADE2:
                            l_offSetX = 1;
                            l_offSetY = 1;
                            l_render2 = true;
                            break;
                        default:
                        {
                            break;
                        }
                        }
                        if (l_render2)
                        {
                            chatQueue_font->draw(iter->message,
                                                 core::rect<s32>(m_x + l_offSetX,m_y_tmp - m_fontHeight+ l_offSetY,m_x+m_xWidth + l_offSetX,m_y_tmp + l_offSetY),
                                                 iter->color2);

                        }

                        chatQueue_font->draw(iter->message,
                                             core::rect<s32>(m_x,m_y_tmp - m_fontHeight,m_x+m_xWidth,m_y_tmp),
                                             iter->color1);

                        m_y_tmp -= m_fontHeight;
                    }
                    count++;
                }
            }
            else
            {
                for (iter=chatQueue_list.getLast(); iter != chatQueue_list.begin(); iter--)
                {
                    //Max lines means no fade, special case delete
                    if (count > chatQueue_maxLines)
                    {
                        //STL deprecation, I need to fix this..
                        //chatQueue_list.erase(iter,chatQueue_list.end());
                        chatQueue_list.erase(iter);
                        return;
                    }
                    //Update

                    //AppFrame::ILogger::log("Time aive: %i[TIME: %i, CREATED: %i, LIFETIME: %i]\n", l_time - (iter->created + chatQueue_life), l_time, iter->created, chatQueue_life);
                    if (chatQueue_life >= 0 && (iter->created + chatQueue_life) < l_time)
                    {
                        if (iter->fade == 0 && chatQueue_fadeTime > 0)
                        {
                            iter->fade = l_time;
                            iter->color1Fade = iter->color1;
                            iter->color2Fade = iter->color2;
                            if (iter->Style == CQS_SHADOW)
                                iter->Style = CQS_INTERNALUSEONLY_FADE2;
                            else
                                iter->Style = CQS_INTERNALUSEONLY_FADE;
                        }
                        //Kill text that has faded away.
                        if ( ((l_time - iter->fade) >= chatQueue_fadeTime) || chatQueue_fadeTime <= 0 )
                        {
                            //AppFrame::ILogger::log("erase bc faded away\n");
                            //STL deprecation, could cause problems. Needs extensive testing
                            //chatQueue_list.erase(iter,chatQueue_list.end());
                            chatQueue_list.erase(iter);
                            return;
                        }
                        // Old Fade code, left incase you would rather fade to a color
                        // Here it fades to black, w/ alpha fade.
                        // f32 l_alpha = (   (f32)(l_time - iter->fade)/(f32)chatQueue_fadeTime );
                        // iter->color1 = iter->color1Fade.getInterpolated(video::SColor(0,0,0,0),1-l_alpha);
                        // iter->color2 = iter->color2Fade.getInterpolated(video::SColor(0,0,0,0),1-l_alpha);

                        // Proposed alpha fade code, does not work with irrlicht 1.1
                        f32 l_alpha = (   (f32)(l_time - iter->fade)/(f32)chatQueue_fadeTime );
                        iter->color1.setAlpha((s32)(iter->color1Fade.getAlpha() * (1-l_alpha)));
                        iter->color2.setAlpha((s32)(iter->color2Fade.getAlpha() * (1-l_alpha)));

                    }
                    //Draw
                    if (IsVisible && chatQueue_font)
                    {
                        s16 l_offSetX = 0;
                        s16 l_offSetY = 0;
                        bool l_render2 = false;
                        //Apply style
                        switch (iter->Style)
                        {
                        case CQS_SHADOW:
                            l_offSetX = 1;
                            l_offSetY = 1;
                            l_render2 = true;
                            break;
                        case CQS_RAINBOW:
                            iter->color1 =video::SColor(255,(l_time/2)%255,(l_time/3)%255,(l_time)%255);
                            break;
                        case CQS_MULTIRAINBOW:
                            l_offSetX = 1;
                            l_offSetY = 1;
                            iter->color1 =video::SColor(255,(l_time/2)%255,(l_time/3)%255,(l_time)%255);
                            iter->color2 =video::SColor(255,(l_time)%255,(l_time/2)%255,(l_time/3)%255);
                            l_render2 = true;
                            break;
                        case CQS_BACKGROUND:
                            chatQueue_env->getVideoDriver()->draw2DRectangle(iter->color2,
                                    core::rect<s32>(m_x,m_yStart_tmp,m_x+m_xWidth,m_y_tmp + m_fontHeight));

                            break;
                        case CQS_CROPBACKGROUND:
                        {

                            core::dimension2d<u32> l_fontDimensions = chatQueue_font->getDimension(iter->message);
                            chatQueue_env->getVideoDriver()->draw2DRectangle(iter->color2,
                                    core::rect<s32>(m_x,m_yStart_tmp,m_x+l_fontDimensions.Width,m_yStart_tmp + m_fontHeight));

                            break;
                        }
                        case CQS_PULSE:
                            iter->color1Fade = iter->color1;
                            iter->Style = CQS_INTERNALUSEONLY_PULSE;
                        case CQS_INTERNALUSEONLY_PULSE:
                            iter->color1 = iter->color1Fade.getInterpolated(iter->color2,(l_time%1000)/(f32)999.9);
                            break;
                        case CQS_INTERNALUSEONLY_FADE2:
                            l_offSetX = 1;
                            l_offSetY = 1;
                            l_render2 = true;
                            break;
                        default:
                        {
                            break;
                        }
                        }
                        if (l_render2)
                        {
                            chatQueue_font->draw(iter->message,
                                                 core::rect<s32>(m_x + l_offSetX,m_yStart_tmp + l_offSetY,m_x+m_xWidth + l_offSetX,m_yStart_tmp + m_fontHeight + l_offSetY),
                                                 iter->color2);

                        }
                        chatQueue_font->draw(iter->message,
                                             core::rect<s32>(m_x,m_yStart_tmp,m_x+m_xWidth,m_yStart_tmp + m_fontHeight),
                                             iter->color1);

                        m_yStart_tmp += m_fontHeight;

                    }
                    count++;
                }
            }
        }

//!-- Start Helpers

        void CGUIChatQueue::chatQueue_calculateRect(const core::rect<s32> drawArea)
        {
            m_xWidth  = drawArea.getWidth();
            m_yHeight = drawArea.getHeight();
            core::position2d<s32> l_positionTmp = drawArea.LowerRightCorner;
            m_y = l_positionTmp.Y;
            l_positionTmp = drawArea.UpperLeftCorner;
            m_x = l_positionTmp.X;
            m_yStart = l_positionTmp.Y;
        }

        void CGUIChatQueue::chatQueue_calculateFontDimensions(irr::gui::IGUIFont* font)
        {
            core::dimension2d<u32> l_fontDimensions = font->getDimension(L"W");
            m_fontHeight = l_fontDimensions.Height;
            chatQueue_calculateMaxLines();
        }

        void CGUIChatQueue::chatQueue_calculateMaxLines()
        {
            chatQueue_maxLines = ((m_yHeight - m_fontHeight) / m_fontHeight);
        }

//!-- End Helpers

//Set Functions

        void CGUIChatQueue::setFadeTime(u32 fadeTime)
        {
            chatQueue_fadeTime = fadeTime;
        }

        void CGUIChatQueue::setMaxLines(u16 maxLines)
        {
            chatQueue_calculateMaxLines();
            if (maxLines < chatQueue_maxLines)
                chatQueue_maxLines = maxLines;
        }

        void CGUIChatQueue::setFont(IGUIFont* font)
        {
            chatQueue_font = font;
            chatQueue_calculateFontDimensions(font);
        }

        void CGUIChatQueue::setLife(u32 setLife)
        {
            chatQueue_life = setLife;
        }

        void CGUIChatQueue::setVisible(bool setVisible)
        {
            IsVisible = setVisible;
        }

        void CGUIChatQueue::setWordWrap(bool setWordWrap)
        {
            chatQueue_wordWrap = setWordWrap;
        }

        void CGUIChatQueue::setDrawArea(const core::rect<s32> drawArea)
        {
            chatQueue_calculateRect(drawArea);
            setFont(chatQueue_font);
        }

        void CGUIChatQueue::setDebug(bool setDebug)
        {
            chatQueue_debug = setDebug;
        }

        void CGUIChatQueue::setDefaultStyle(chatQueueStyle useStyle)
        {
            if (useStyle == CQS_USEDEFAULT)
                useStyle = CQS_PLAIN;
            chatQueue_defaultStyle = useStyle;
        }

//Get Functions
        chatQueueStyle CGUIChatQueue::getDefaultStyle()
        {
            return chatQueue_defaultStyle;
        }

        u32 CGUIChatQueue::getFadeTime()
        {
            return chatQueue_fadeTime;
        }
        bool CGUIChatQueue::isDebug()
        {
            return chatQueue_debug;
        }
        irr::gui::IGUIFont* CGUIChatQueue::getFont()
        {
            if (chatQueue_font)
                return chatQueue_font;
            else
                return NULL;
        }

        void CGUIChatQueue::drawFromTopToBottom(bool TTB)
        {
            m_TTB = TTB;
        }

    } // end namespace gui
} // end namespace irr

#endif
