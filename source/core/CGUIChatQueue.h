/*
ChatQueue is copyright g0dsoft 2008
Version: 0.3.1
Code by: Mark Laprairie
Contact: webmaster@g0dsoft.com
Url: www.g0dsoft.com

CGUIChatQueue.h
ChatQueue is a object for simulating half-life/starcraft style chat display
capable of colors, special effects and any custom font.
*/

#ifndef __C_GUI_CHAT_QUEUE_H_INCLUDED__
#define __C_GUI_CHAT_QUEUE_H_INCLUDED__

#include "IrrCompileConfig.h"
#ifdef _IRR_COMPILE_WITH_GUI_

#include "IGUIFont.h"
#include <ITimer.h>
#include "IGUIChatQueue.h"
#include <irrList.h>

namespace irr
{
    namespace gui
    {
#define chatQueue_STRINGLENGTH 200

        //! Multi-Line chat queue
        class CGUIChatQueue : public virtual IGUIChatQueue
        {
        public:

            //! constructor
            CGUIChatQueue(IGUIEnvironment* environment, IGUIElement* parent, s32 id,const core::rect<s32> &drawArea, ITimer* timer, IGUIFont* font = NULL,
                          s32 textLife = 5000, u32 fadeTime = 500.00, bool setWordWrap = true);

            //! destructor
            ~CGUIChatQueue();

            //! Add a message to the Chat Queue
            /** \param text: Irrlicht stringw, wchar_t* or const wchar_t*
            refrencing the text you want displayed
            \param color1: The main color for your text
            \param color2: The special color, used for styles
            ex. if you apply CQS_SHADOW as the style this will
            be the color for the background shadow.
            \param chatQueueStyle: The style the text is to be renderd
            with defaults to a style set with setDefaultStyle or if
            not set,CQS_PLAIN*/
            virtual void addMessage(core::stringw text,chatQueueStyle useStyle =  CQS_USEDEFAULT,video::SColor color1 =video::SColor(255,255,255,255),video::SColor color2 =video::SColor(255,0,0,0));

            virtual void addMessage(wchar_t* text,chatQueueStyle useStyle =  CQS_USEDEFAULT,video::SColor color1 =video::SColor(255,255,255,255),video::SColor color2 =video::SColor(255,0,0,0));
            virtual void addMessage(const wchar_t* text,chatQueueStyle useStyle =  CQS_USEDEFAULT,video::SColor color1 =video::SColor(255,255,255,255),video::SColor color2 =video::SColor(255,0,0,0));

            //! draws the element and its children
            virtual void draw();

            //! Sets how long text should fade for before removing
            virtual void setFadeTime(u32 fadeTime = 1000);

            //! Sets the font to be used by Chat Queue
            virtual void setFont(IGUIFont* font);

            //! Sets a max number of lines that can be visible at a given time
            virtual void setMaxLines(u16 maxLines);

            //! Makes Chat Queue visible, or invisible
            virtual void setVisible(bool setVisible = true);

            //! Sets how long a text entry should be fully visible
            virtual void setLife(u32 setLife = 1000);

            //! Enables/Disables word wrapping
            virtual void setWordWrap(bool setWordWrap = true);

            //! Modifies the draw area
            virtual void setDrawArea(const core::rect<s32> areaArea);

            //! Sets a debug mode that makes the draw area visible
            virtual void setDebug(bool setDebug = true);

            //! Sets a default style to be used when no other style is present
            virtual void setDefaultStyle(chatQueueStyle useStyle);

            //! Is the current ChatQueue object in debug mode?
            virtual bool isDebug();

            //! Returns the current fade time
            virtual u32 getFadeTime();

            //! Returns the current font
            virtual IGUIFont* getFont();

            //! Returns the current, default style
            virtual chatQueueStyle getDefaultStyle();

            //! Change drawing Direction
            virtual void drawFromTopToBottom(bool TTB);

        private:
            //Helper functions
            void chatQueue_calculateRect(const core::rect<s32> drawArea);
            void chatQueue_calculateFontDimensions(IGUIFont* font);
            void chatQueue_calculateMaxLines();

            //Style and Font
            chatQueueStyle chatQueue_defaultStyle;
            IGUIFont* chatQueue_font;
            //Rect replacment
            u32 m_xWidth;
            u32 m_yHeight;
            u32 m_y;
            u32 m_x;
            u32 m_yStart;
            //Font height
            u16 m_fontHeight;

            //Draw top to bottom
            bool m_TTB;

            //Chat Queue message container
            struct chatQueueMessage
            {
                wchar_t message[chatQueue_STRINGLENGTH];
                chatQueueStyle Style;
                video::SColor color1;
                video::SColor color2;
                video::SColor color1Fade;
                video::SColor color2Fade;
                u32 created;
                u32 fade;
            };
            IGUIEnvironment* chatQueue_env;
            bool chatQueue_debug;
            bool chatQueue_wordWrap;
            u32 chatQueue_fadeTime;
            s32 chatQueue_life;
            u16 chatQueue_maxLines;
            core::list<chatQueueMessage> chatQueue_list;
            ITimer* m_timer;
        };

    } // end namespace gui
} // end namespace irr

#endif // _IRR_COMPILE_WITH_GUI_
#endif // __C_GUI_CHAT_QUEUE_H_INCLUDED__
