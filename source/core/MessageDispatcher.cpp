/*
  Copyright (C) 2010 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/
#include "MessageDispatcher.h"
namespace AppFrame
{
namespace actions
{
/////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////Subscriber////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
Subscriber::~Subscriber(void)
{
    std::list<Entry*>::iterator it = MessageSignals.begin();
    while (it != MessageSignals.end())
    {
        Entry* ent = *it;
        delete ent;
        ++it;
    }
    MessageSignals.clear();
}

void Subscriber::addSignal(std::map<Subscriber*, smartPoint>* EventMap, std::list<Subscriber*>* List)
{
    std::list<Subscriber*>::iterator ListIt;
    std::map<Subscriber*, smartPoint>::iterator EventMapIt;

    //check if same configuration already exits
    std::list<Entry*>::iterator it = MessageSignals.begin();
    while (it != MessageSignals.end())
    {
        if ((*it)->EventMap == EventMap && (*it)->List == List)
            return; //already added this signal why calling it again????
        ++it;
    }

    //Event iterator
    EventMapIt = EventMap->find(this);
    if (EventMapIt == EventMap->end())
        return;

    //list iterator
    List->push_front(this);
    ListIt = List->begin();

    //add configuration
    Entry* ent = new Entry;
    ent->EventMap = EventMap;
    ent->List = List;
    ent->EventMapIt = EventMapIt;
    ent->ListIt = ListIt;
    MessageSignals.push_back(ent);
}

void Subscriber::removeSignal(std::map<Subscriber*, smartPoint>* EventMap)
{
    std::list<Entry*>::iterator it = MessageSignals.begin();
    while (it != MessageSignals.end())
    {
        if ((*it)->EventMap == EventMap)
        {
            Entry* ent = *it;
            delete ent;
            MessageSignals.erase(it);
            return;
        }
        ++it;
    }
}

void Subscriber::removeSignal(std::list<Subscriber*>* List)
{
    std::list<std::list<Entry*>::iterator> DeleteList;

    std::list<Entry*>::iterator it = MessageSignals.begin();
    while (it != MessageSignals.end())
    {
        if ((*it)->List == List)
        {
            Entry* ent = *it;
            delete ent;
            DeleteList.push_back(it);
        }
        ++it;
    }

    std::list<std::list<Entry*>::iterator>::iterator del = DeleteList.begin();
    while(del != DeleteList.end())
    {
        MessageSignals.erase(*del);
        ++del;
    }
}
/////////////////////////////////////////////////////////////////////////////////////////
////////////////////////MessageDispatcher////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

MessageDispatcher::~MessageDispatcher(void)
{
    std::map<int, std::list<Subscriber*> >::iterator it = ListenerMap.begin();
    while(it != ListenerMap.end())
    {
        std::list<Subscriber*>& List = (*it).second;
        while (List.begin() != List.end())
        {
            (*List.begin())->removeSignal(&List);
        }
        List.clear();
        it++;
    }
}

void MessageDispatcher::send(Message* message)
{
    std::list<Subscriber*>* List = &ListenerMap[message->getID()];
    std::list<Subscriber*>::iterator it = List->begin();
    while (it != List->end())
    {
        EventHandlerBase* base = EventHandlerMap[message->getID()][*it];
        base->call(message);

        it++;
    }
}
}
}
