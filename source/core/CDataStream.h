#ifndef CDATASTREAM_H
#define CDATASTREAM_H

#include <core/IDataStream.h>
namespace AppFrame
{
class CDataStream : public IDataStream
{
    public:
        /** Default constructor */
        CDataStream(AppFrame::c8* data, AppFrame::u32 size, bool copy = true);
        CDataStream(AppFrame::u32 reserve);
        CDataStream(const CDataStream& data);
        CDataStream(void);
        /** Default destructor */
        ~CDataStream(void);

        void reset(void);
        void ResetReader(void);
        void ResetWriter(void);

        void encrypt(const AppFrame::string& key);
        void dencrypt(const AppFrame::string& key);
        bool isEncrypted(void);

        AppFrame::c8* getData(void);
        const AppFrame::c8* getData(void) const;
        AppFrame::u32 getSize(void) const;

        AppFrame::c8& operator[](const AppFrame::u32& index);
        const AppFrame::c8& operator[](const AppFrame::u32& index) const;

        void set(AppFrame::c8* data, AppFrame::u32 size, bool copy = true);

        void Write(const void* data, AppFrame::u32 size);
        void Write(const bool& data);
        void Write(const AppFrame::c8& data);
        void Write(const AppFrame::u8& data);
        virtual void Write(const AppFrame::s32& data);
        virtual void Write(const AppFrame::u32& data);
        virtual void Write(const AppFrame::s16& data);
        virtual void Write(const AppFrame::u16& data);
        virtual void Write(const AppFrame::f32& data);
        void Write(const AppFrame::string& data);
        void Write(const irr::core::stringc& data);
        void Write(const IDataStream* data);

        IDataStream& operator<<(const bool& data);
        IDataStream& operator<<(const AppFrame::c8& data);
        IDataStream& operator<<(const AppFrame::u8& data);
        IDataStream& operator<<(const AppFrame::s32& data);
        IDataStream& operator<<(const AppFrame::u32& data);
        IDataStream& operator<<(const AppFrame::s16& data);
        IDataStream& operator<<(const AppFrame::u16& data);
        IDataStream& operator<<(const AppFrame::f32& data);
        IDataStream& operator<<(const AppFrame::string& data);
        IDataStream& operator<<(const irr::core::stringc& data);
        IDataStream& operator<<(const IDataStream* data);

        bool Read(void* data, AppFrame::u32 size);
        bool Read(bool& data);
        bool Read(AppFrame::c8& data);
        bool Read(AppFrame::u8& data);
        virtual bool Read(AppFrame::s32& data);
        virtual bool Read(AppFrame::u32& data);
        virtual bool Read(AppFrame::s16& data);
        virtual bool Read(AppFrame::u16& data);
        virtual bool Read(AppFrame::f32& data);
        bool Read(AppFrame::string& data);
        bool Read(irr::core::stringc& data);
        bool Read(IDataStream* data);

        IDataStream& operator>>(bool& data);
        IDataStream& operator>>(AppFrame::c8& data);
        IDataStream& operator>>(AppFrame::u8& data);
        IDataStream& operator>>(AppFrame::s32& data);
        IDataStream& operator>>(AppFrame::u32& data);
        IDataStream& operator>>(AppFrame::s16& data);
        IDataStream& operator>>(AppFrame::u16& data);
        IDataStream& operator>>(AppFrame::f32& data);
        IDataStream& operator>>(AppFrame::string& data);
        IDataStream& operator>>(irr::core::stringc& data);
        IDataStream& operator>>(IDataStream* data);


        CDataStream& operator=(const CDataStream& data);

    protected:
        AppFrame::c8* Data;
        AppFrame::u32 Size;
        AppFrame::u32 WritePos;
        AppFrame::u32 ReadPos;
        bool OwnData;
        bool Encrypted;
    private:
};
}
#endif // CDATASTREAM_H
