#include "EntitySceneNodePlugin.h"


#include "CAttributeNode.h"

#ifdef COMPILE_WITH_CAUDIO
#include "CcAudioSceneNode.h"
#endif

// -------------------------------------------------------------------------------
// Entity factory implementation
// -------------------------------------------------------------------------------

#ifdef COMPILE_IRREDIT_PLUGIN
#include <irredit.h>
__declspec(dllexport) irr::irredit::IIrrEditPlugin* __stdcall createPlugin( irr::irredit::IrrEditServices* services )
{
	// we simple register a new scene node factory here, which is only able to
	// create our example scene node

	irr::scene::ISceneManager* mgr = services->getDevice()->getSceneManager();
	irr::scene::CEntitySceneNodeFactory* factory = new irr::scene::CEntitySceneNodeFactory(mgr, services->getDevice());
	mgr->registerSceneNodeFactory(factory);
	factory->drop();

	return 0;
}
#endif

namespace irr
{
    namespace scene
    {

    	const char* entitySceneNodeTypeName = "Entity";
		const int ENTITY_SCENE_NODE_ID = MAKE_IRR_ID('e','n','t','i');
#ifdef COMPILE_WITH_CAUDIO
		const int cAUDIO_SCENE_NODE_ID = MAKE_IRR_ID('c','a','d','o');
    	const char* audioSceneNodeTypeName = "cAudio";
#endif

		const int CEntitySceneNodeFactory::ENTITYID = ENTITY_SCENE_NODE_ID;

        ESCENE_NODE_TYPE CAttributeNode::getType() const
        {
            return (ESCENE_NODE_TYPE)ENTITY_SCENE_NODE_ID;
        }

#ifdef COMPILE_WITH_CAUDIO
        ESCENE_NODE_TYPE CcAudioSceneNode::getType() const
        {
            return (ESCENE_NODE_TYPE)cAUDIO_SCENE_NODE_ID;
        }
#endif

        CEntitySceneNodeFactory::CEntitySceneNodeFactory(ISceneManager* mgr, IrrlichtDevice* device)
                : Manager(mgr), Device(device)
        {
            // don't grab the manager here, to avoid cyclic references
        }


        CEntitySceneNodeFactory::~CEntitySceneNodeFactory()
        {
        }


//! adds a scene node to the scene graph based on its type id
        ISceneNode* CEntitySceneNodeFactory::addSceneNode(ESCENE_NODE_TYPE type, ISceneNode* parent)
        {
            if (!parent)
                parent = Manager->getRootSceneNode();

            if (type == ENTITY_SCENE_NODE_ID)
            {
                CAttributeNode* node = new CAttributeNode(parent, Device, -1);
                node->drop();
                return node;
            }
#ifdef COMPILE_WITH_CAUDIO
            if (type == cAUDIO_SCENE_NODE_ID)
            {
                CcAudioSceneNode* node = new CcAudioSceneNode(parent, Device->getSceneManager(), -1);
                ///node->setAudioManager(.....); <----TODO
                node->drop();
                return node;
            }
#endif
            return 0;
        }


//! adds a scene node to the scene graph based on its type name
        ISceneNode* CEntitySceneNodeFactory::addSceneNode(const c8* typeName, ISceneNode* parent)
        {
            return addSceneNode( getTypeFromName(typeName), parent );
        }


//! returns amount of scene node types this factory is able to create
        u32 CEntitySceneNodeFactory::getCreatableSceneNodeTypeCount() const
        {
#ifdef COMPILE_WITH_CAUDIO
            return 2;
#else
			return 1;
#endif
        }


//! returns type of a createable scene node type
        ESCENE_NODE_TYPE CEntitySceneNodeFactory::getCreateableSceneNodeType(u32 idx) const
        {
            if (idx==0)
                return (ESCENE_NODE_TYPE)ENTITY_SCENE_NODE_ID;
#ifdef COMPILE_WITH_CAUDIO
            if (idx==1)
                return (ESCENE_NODE_TYPE)cAUDIO_SCENE_NODE_ID;
#endif

            return ESNT_UNKNOWN;
        }


//! returns type name of a createable scene node type
        const c8* CEntitySceneNodeFactory::getCreateableSceneNodeTypeName(u32 idx) const
        {
            if (idx==0)
                return entitySceneNodeTypeName;
#ifdef COMPILE_WITH_CAUDIO
            if (idx==1)
                return audioSceneNodeTypeName;
#endif

            return 0;
        }


//! returns type name of a createable scene node type
        const c8* CEntitySceneNodeFactory::getCreateableSceneNodeTypeName(ESCENE_NODE_TYPE type) const
        {
            if (type == ENTITY_SCENE_NODE_ID)
                return entitySceneNodeTypeName;
#ifdef COMPILE_WITH_CAUDIO
            if (type == cAUDIO_SCENE_NODE_ID)
                return audioSceneNodeTypeName;
#endif

            return 0;
        }


        ESCENE_NODE_TYPE CEntitySceneNodeFactory::getTypeFromName(const c8* name)
        {
            if (!strcmp(name, entitySceneNodeTypeName))
                return (ESCENE_NODE_TYPE)ENTITY_SCENE_NODE_ID;
#ifdef COMPILE_WITH_CAUDIO
            if (!strcmp(name, audioSceneNodeTypeName))
                return (ESCENE_NODE_TYPE)cAUDIO_SCENE_NODE_ID;
#endif

            return ESNT_UNKNOWN;
        }
    }
}
