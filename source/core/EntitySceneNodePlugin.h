#ifndef __ENTITY_SCENE_NODE_PLUGIN_H_INCLUDED__
#define __ENTITY_SCENE_NODE_PLUGIN_H_INCLUDED__

#include <irrlicht.h>

namespace irr
{
    namespace scene
    {
		// own factory implementation

        class CEntitySceneNodeFactory : public ISceneNodeFactory
        {
        public:
			static const int ENTITYID;

            CEntitySceneNodeFactory(ISceneManager* mgr, IrrlichtDevice* device);
            ~CEntitySceneNodeFactory();

            virtual ISceneNode* addSceneNode(ESCENE_NODE_TYPE type, ISceneNode* parent=0);
            virtual ISceneNode* addSceneNode(const c8* typeName, ISceneNode* parent=0);
            virtual u32 getCreatableSceneNodeTypeCount() const;
            virtual const c8* getCreateableSceneNodeTypeName(u32 idx) const;
            virtual ESCENE_NODE_TYPE getCreateableSceneNodeType(u32 idx) const;
            virtual const c8* getCreateableSceneNodeTypeName(ESCENE_NODE_TYPE type) const;

        private:

            ESCENE_NODE_TYPE getTypeFromName(const c8* name);
            ISceneManager* Manager;
            IrrlichtDevice* Device;
        };
    }
}

#endif
