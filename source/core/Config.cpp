/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
//#include "core/ILogger.h"
#include "core/ILog.h"
#include "core/XMLread.h"
#include "Config.h"
#ifdef COMPILE_FOR_WINDOWS
#include <windows.h> // Sleep
#endif
#ifdef COMPILE_FOR_UNIX
#include <unistd.h> // usleep
#endif
#include <irrXML.h>
#include <string>
CConfig::CConfig(irr::c8* filename)//, HINSTANCE hInstance)
{
    m_irrlichparameter.AntiAlias = 1;
    m_irrlichparameter.Bits = 32;
    m_irrlichparameter.DriverType = irr::video::EDT_OPENGL;
    m_irrlichparameter.Fullscreen = false;
    m_irrlichparameter.WindowSize = irr::core::dimension2d<irr::s32>(800,600);
    m_irrlichparameter.Stencilbuffer = false;
    m_irrlichparameter.HighPrecisionFPU = false;
    m_irrlichparameter.IgnoreInput = false;
    m_irrlichparameter.Vsync = false;
    m_irrlichparameter.WithAlphaChannel = false;
    m_irrlichparameter.ZBufferBits = 16;

    //m_hInstance = hInstance;

    loadIrrlichtCFG(filename);
}
CConfig::~CConfig(void)
{}

void CConfig::saveCFG(irr::c8* filename)
{
    AppFrame::xml::XMLread writer(0);
    AppFrame::xml::XMLread::Node* root = writer.addNode("irrlichtconfig");

    AppFrame::xml::XMLread::Node* node = root->addNode("DriverType");

    switch(m_irrlichparameter.DriverType)
    {
        case irr::video::EDT_BURNINGSVIDEO:
            node->AddAttribute("value", "burning");
            break;
        case irr::video::EDT_OPENGL:
            node->AddAttribute("value", "opengl");
            break;
        case irr::video::EDT_NULL:
            node->AddAttribute("value", "null");
            break;
        default:
            node->AddAttribute("value", "opengl");
            break;
    }
    node->AddComment("Driver can be opengl, burning, null");

    node = root->addNode("WindowSize");
    node->AddAttribute("width", m_irrlichparameter.WindowSize.Width);
    node->AddAttribute("height", m_irrlichparameter.WindowSize.Height);

    root->addNode("Fullscreen")->AddAttribute("value", m_irrlichparameter.Fullscreen ? "true" : "false");
    root->addNode("AntiAlias")->AddAttribute("value", m_irrlichparameter.AntiAlias);
    root->addNode("Stencilbuffer")->AddAttribute("value", m_irrlichparameter.Stencilbuffer ? "true" : "false");
    root->addNode("Bits")->AddAttribute("value", m_irrlichparameter.Bits);
    root->addNode("Vsync")->AddAttribute("value", m_irrlichparameter.Vsync ? "true" : "false");
    root->addNode("HighPrecisionFPU")->AddAttribute("value", m_irrlichparameter.HighPrecisionFPU ? "true" : "false");
    root->addNode("IgnoreInput")->AddAttribute("value", m_irrlichparameter.IgnoreInput ? "true" : "false");
    root->addNode("WithAlphaChannel")->AddAttribute("value", m_irrlichparameter.WithAlphaChannel ? "true" : "false");
    root->addNode("Stereobuffer")->AddAttribute("value", m_irrlichparameter.Stereobuffer ? "true" : "false");
    root->addNode("Doublebuffer")->AddAttribute("value", m_irrlichparameter.Doublebuffer ? "true" : "false");
    root->addNode("ZBufferBits")->AddAttribute("value", m_irrlichparameter.ZBufferBits);
    writer.save(filename);
}

void CConfig::loadIrrlichtCFG(irr::c8* filename)
{
    FILE* datei = NULL;
    datei = fopen(filename, "rt");
    if (!datei)
    {
        nlog<<"Write CFG FILE"<<nlfendl;
        saveCFG(filename);
        return;
    }
    else
    {
        fclose(datei);
    }

    AppFrame::xml::XMLread reader(filename);
    //Drivertype
    AppFrame::xml::XMLread::Node* node = reader.getRoot()->getNode("DriverType");
    if (irr::core::stringc("burning") == node->getValue("value"))
    {
        m_irrlichparameter.DriverType = irr::video::EDT_BURNINGSVIDEO;
    }
    else if (irr::core::stringc("null") == node->getValue("value"))
    {
        m_irrlichparameter.DriverType = irr::video::EDT_NULL;
    }
    else
    {
        m_irrlichparameter.DriverType = irr::video::EDT_OPENGL;
    }

    node = reader.getRoot()->getNode("WindowSize");
    m_irrlichparameter.WindowSize.Width = node->getValueAsInt("width");
    m_irrlichparameter.WindowSize.Height = node->getValueAsInt("height");

    m_irrlichparameter.Fullscreen       = reader.getRoot()->getNode("Fullscreen")->getValueAsBool("value");
    m_irrlichparameter.AntiAlias        = reader.getRoot()->getNode("AntiAlias")->getValueAsInt("value");
    m_irrlichparameter.Stencilbuffer    = reader.getRoot()->getNode("Stencilbuffer")->getValueAsBool("value");
    m_irrlichparameter.Bits             = reader.getRoot()->getNode("Bits")->getValueAsInt("value");
    m_irrlichparameter.Vsync            = reader.getRoot()->getNode("Vsync")->getValueAsBool("value");
    m_irrlichparameter.HighPrecisionFPU = reader.getRoot()->getNode("HighPrecisionFPU")->getValueAsBool("value");
    m_irrlichparameter.IgnoreInput      = reader.getRoot()->getNode("IgnoreInput")->getValueAsBool("value");
    m_irrlichparameter.WithAlphaChannel = reader.getRoot()->getNode("WithAlphaChannel")->getValueAsBool("value");
    m_irrlichparameter.Stereobuffer     = reader.getRoot()->getNode("Stereobuffer")->getValueAsBool("value");
    m_irrlichparameter.Doublebuffer     = reader.getRoot()->getNode("Doublebuffer")->getValueAsBool("value");
    m_irrlichparameter.ZBufferBits      = reader.getRoot()->getNode("ZBufferBits")->getValueAsInt("value");
}

void CConfig::setIrrCFG(irr::SIrrlichtCreationParameters& irrlichparameter)
{
    m_irrlichparameter = irrlichparameter;
}

/*LRESULT CALLBACK MessageHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
    case WM_PAINT:

        return 0;
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
        break;

    case WM_CREATE:
        return 0;
        break;

    case WM_LBUTTONDBLCLK:
        return 0;
        break;

    case WM_CLOSE:
        return 0;
        break;
    }
    return DefWindowProc(hWnd, msg, wParam, lParam);
}

HWND CConfig::createWindowsWindow(char* name, int size[2])
{
    WNDCLASSEX wndClass;

    wndClass.cbSize = sizeof(WNDCLASSEX);
    wndClass.style = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

    wndClass.lpfnWndProc = MessageHandler;

    wndClass.cbClsExtra = 0;
    wndClass.cbWndExtra = 0;
    wndClass.hInstance = m_hInstance;

    //wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);

    wndClass.hCursor = LoadCursor(NULL, IDC_HAND);

    wndClass.lpszMenuName = NULL;

    wndClass.lpszClassName = name;

    wndClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);

    wndClass.hIconSm = LoadIcon(NULL, IDI_WINLOGO);

    RegisterClassEx(&wndClass);

    return CreateWindowEx(NULL, name, name, WS_OVERLAPPEDWINDOW | WS_VISIBLE, 0, 0, size[0], size[1], NULL, NULL, m_hInstance, NULL);
}*/

irr::SIrrlichtCreationParameters& CConfig::getIrrlichtParameter(void)
{
    return m_irrlichparameter;
}
