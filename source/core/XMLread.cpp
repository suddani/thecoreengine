/*
  Copyright (C) 2010 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/
#include "XMLread.h"
#include "ILog.h"
#include "xmlwriter/xmlwriter.h"
namespace AppFrame
{
namespace xml
{
XMLread::Val::Val(const irr::c8* name, const irr::c8* value)
{
    Name = name;
    Value = value;
}
XMLread::Val::Val(const Val& v)
{
    Name = v.Name;
    Value = v.Value;
}
XMLread::Val& XMLread::Val::operator=(const Val& v)
{
    Name = v.Name;
    Value = v.Value;
    return *this;
}

XMLread::Node::Node(const irr::c8* name, Node* parent)
{
    Name = name;
    Parent = parent;
}

XMLread::Node::~Node(void)
{
    clear();
}

void XMLread::Node::write(xmlwriter* writer)
{
    for (irr::u32 i=0;i<Comments.size();++i)
        writer->AddComment(Comments[i].c_str());

    for (irr::u32 i=0;i<Values.size();++i)
    {
        writer->AddAtributes(Values[i].Name.c_str(), Values[i].Value.c_str());
    }

    if (Text.size() == 0 && CData.size() == 0 && Unknown.size() == 0 && Nodes.size() == 0)
    {
        writer->CreatetagClosed(Name.c_str());
    }
    else
    {
        if (Nodes.size() > 0)
        {
            writer->Createtag(Name.c_str());
            for (irr::u32 i=0;i<Nodes.size();++i)
                Nodes[i]->write(writer);
            writer->CloseLasttag();
        }
        else
        {
            irr::core::stringc cdat = Text;
            if (CData.size() != 0)
            {
                cdat.append("<![CDATA[ ");
                cdat.append(CData);
                cdat.append(" ]]>");
            }
            writer->CreateChild(Name.c_str(), Text.c_str());
        }
    }
}

void XMLread::Node::AddComment(const irr::c8* comment)
{
    Comments.push_back(comment);
}

void XMLread::Node::AddAttribute(const irr::c8* name, const irr::core::stringc& value)
{
    Values.push_back(XMLread::Val(name, value.c_str()));
}

void XMLread::Node::AddAttribute(const irr::c8* name, const irr::u32& value)
{
    AddAttribute(name, irr::core::stringc(value));
}

void XMLread::Node::AddAttribute(const irr::c8* name, const irr::s32& value)
{
    AddAttribute(name, irr::core::stringc(value));
}

void XMLread::Node::AddAttribute(const irr::c8* name, const irr::f32& value)
{
    AddAttribute(name, irr::core::stringc(value));
}
/*
void XMLread::Node::AddAttribute(const irr::c8* name, const bool& value)
{
    AddAttribute(name, value ? irr::core::stringc("true") : irr::core::stringc("false"));
}
*/

void XMLread::Node::clear(void)
{
    Name    = "";
    Text    = "";
    CData   = "";
    Unknown = "";
    Values.clear();
    for (unsigned int i=0;i<Nodes.size();++i)
    {
        Node* n = Nodes[i];
        delete n;
    }
    Nodes.clear();
}

XMLread::Node* XMLread::Node::addNode(const irr::c8* name)
{
    Node* node = new Node(name, this);
    Nodes.push_back(node);
    return node;
}

XMLread::Node* XMLread::Node::getNode(const irr::c8* name)
{
    if (Name == name)
        return this;
    for (unsigned int i=0;i<Nodes.size();++i)
    {
        if (Nodes[i]->Name == name)
            return Nodes[i];
    }
    return NULL;
}

irr::core::array<XMLread::Node*> XMLread::Node::getNodes(const irr::c8* name)
{
    nlog<<"Getting nodes..."<<nlflush;
    irr::core::array<XMLread::Node*> a;

    for (unsigned int i=0;i<Nodes.size();++i)
    {
        if (Nodes[i]->Name == name)
        {
            a.push_back(Nodes[i]);
        }
    }
    return a;
}

XMLread::Val* XMLread::Node::getValueStruct(const irr::c8* name)
{
    for (unsigned int i=0;i<Values.size();++i)
    {
        if (Values[i].Name == name)
        {
            return &Values[i];
        }
    }
    return NULL;
}

const irr::c8* XMLread::Node::getValue(const irr::c8* name)
{
    XMLread::Val* val = getValueStruct(name);
    if (val)
        return val->Value.c_str();
    return NULL;
}

irr::s32 XMLread::Node::getValueAsInt(const irr::c8* name)
{
    irr::s32 val = -1;
    const irr::c8* v = getValue(name);
    if (v)
        sscanf(v, "%i", &val);
    return val;
}

irr::f32 XMLread::Node::getValueAsFloat(const irr::c8* name)
{
    irr::f32 val = -1;
    const irr::c8* v = getValue(name);
    if (v)
        sscanf(v, "%f", &val);
    return val;
}

bool XMLread::Node::getValueAsBool(const irr::c8* name)
{
    return (irr::core::stringc("true") == getValue(name)) ? true : false;
}

//XMLreader

XMLread::XMLread(const irr::c8* file) : N("", NULL), Current(NULL)
{
    load(file);
}

XMLread::~XMLread(void)
{
    //dtor
}

XMLread::Node* XMLread::addNode(const irr::c8* name, XMLread::Node* parent)
{
    if (!parent)
    {
        N.clear();
        N.Name = name;
        return &N;
    }
    return parent->addNode(name);
}

void XMLread::save(const irr::c8* file)
{
    xmlwriter writer(file);
    N.write(&writer);
}

bool XMLread::load(const irr::c8* file)
{
    //clear tree
    N.clear();

    Loaded = false;
    //int showDebugInfo = 0;

    irr::io::IrrXMLReader* xml = irr::io::createIrrXMLReader(file);
    if (!xml)
    {
        nlerror<<"Loading "<<file<<" is not possible"<<nlfendl;
        return false;
    }

    Current = &N;

    irr::core::array<irr::core::stringc> LastComment;

    while (xml && xml->read() && Current)
    {
        if (!Current)
        {
            nlerror<<"Error in XML file"<<nlfendl;
            break;
        }
        switch (xml->getNodeType())
        {
        case irr::io::EXN_COMMENT:
            LastComment.push_back(xml->getNodeData());
            break;
        case irr::io::EXN_CDATA:
            if (!xml->isEmptyElement())
            {
                //AppFrame::log(showDebugInfo, "CDATA[%s]", xml->getNodeName());
                //AppFrame::log(showDebugInfo, "[%s]", xml->getNodeData());
                //Current->CData.set((const irr::u8*)xml->getNodeData(), strlen(xml->getNodeData()));
                Current->CData = xml->getNodeData();
                //Current->Text = "";
            }
            break;
        case irr::io::EXN_TEXT:
            if (!xml->isEmptyElement())// && Current->CData.size() == 0)
            {
                //AppFrame::log(showDebugInfo, "TEXT[%s]", xml->getNodeName());
                //AppFrame::log(showDebugInfo, "[%s]", xml->getNodeData());
                Current->Text = xml->getNodeData();
            }
            break;
        case irr::io::EXN_ELEMENT:

            if (Current->Name.size() != 0)
                Current = Current->addNode(xml->getNodeName());
            else
                Current->Name = xml->getNodeName();

            for (unsigned int i =0;i<xml->getAttributeCount();++i)
            {
                Current->AddAttribute(xml->getAttributeName(i), xml->getAttributeValue(i));
            }
            //attach comments
            Current->Comments = LastComment;
            LastComment.clear();
            //check if element is empty
            if (xml->isEmptyElement())
                Current = Current->Parent;
            break;
        case irr::io::EXN_ELEMENT_END:
            Current = Current->Parent;
            break;
        case irr::io::EXN_UNKNOWN:
            //AppFrame::log(showDebugInfo, "Unknown[%s]", xml->getNodeName());
            //AppFrame::log(showDebugInfo, "[%s]", xml->getNodeData());
            Current->Unknown = xml->getNodeData();
            break;
        }
    }

    delete xml;

    Loaded = true;

    return Loaded;
}

XMLread::Node* XMLread::getNode(const irr::c8* name, XMLread::Node* parent)
{
    Current = parent;
    if (!Current)
        Current = &N;
    return Current->getNode(name);
}

irr::core::array<XMLread::Node*> XMLread::getNodes(const irr::c8* name, XMLread::Node* parent)
{
    Current = parent;
    if (!Current)
        Current = &N;
    return Current->getNodes(name);
}

XMLread::Node* XMLread::findNode(const irr::c8* name, XMLread::Node* parent)
{
    //TODO: needs to be implemented when needed->i think its the same like getNode
    return getNode(name, parent);
}

irr::core::array<XMLread::Node*> XMLread::findNodes(const irr::c8* name, XMLread::Node* parent)
{
    //TODO: needs to be implemented when needed->i think its the same like getNodes
    irr::core::array<XMLread::Node*> a;
    return getNodes(name, parent);
}
}
}
