#include "CDataStream.h"
namespace AppFrame
{
const AppFrame::u32 ReserveSize = 50;
IDataStream* createDataStream(AppFrame::c8* data, AppFrame::u32 size, bool copy)
{
    return new CDataStream(data, size, copy);
}
IDataStream* createDataStream(AppFrame::u32 reserve)
{
    return new CDataStream(reserve);
}
IDataStream* createDataStream(void)
{
    return new CDataStream();
}

CDataStream& CDataStream::operator=(const CDataStream& data)
{
    if (Size > 0 && Data && OwnData)
        delete [] Data;

    Size        = data.Size;
    WritePos    = data.WritePos;
    ReadPos     = data.ReadPos;
    OwnData     = data.OwnData;
    Encrypted   = data.Encrypted;
    if (OwnData)
    {
        Data = new AppFrame::c8[Size];
        memcpy(Data, data.Data, WritePos);
    }
    else
        Data = data.Data;

    return *this;
}

void CDataStream::set(AppFrame::c8* data, AppFrame::u32 size, bool copy)
{
    if (Size > 0 && Data && OwnData)
        delete [] Data;

    if (copy)
    {
        Data    = new AppFrame::c8[size];
        memcpy(Data, data, size);
    }
    else
        Data    = data;
    Size        = size;
    WritePos    = size;
    ReadPos     = 0;
    OwnData     = copy;
    Encrypted   = false;
}

CDataStream::CDataStream(void)
{
    //ctor
    Data        = NULL;
    Size        = 0;
    WritePos    = 0;
    ReadPos     = 0;
    OwnData     = true;
    Encrypted   = false;
}

CDataStream::CDataStream(const CDataStream& data)
{
    Size        = data.Size;
    WritePos    = data.WritePos;
    ReadPos     = data.ReadPos;
    OwnData     = data.OwnData;
    Encrypted   = data.Encrypted;
    if (OwnData)
    {
        Data = new AppFrame::c8[Size];
        memcpy(Data, data.Data, WritePos);
    }
    else
        Data = data.Data;
}

CDataStream::CDataStream(AppFrame::c8* data, AppFrame::u32 size, bool copy)
{
    if (copy)
    {
        Data    = new AppFrame::c8[size];
        memcpy(Data, data, size);
    }
    else
        Data    = data;
    Size        = size;
    WritePos    = size;
    ReadPos     = 0;
    OwnData     = copy;
    Encrypted   = false;
}

CDataStream::CDataStream(AppFrame::u32 reserve)
{
    Data        = new AppFrame::c8[reserve];
    Size        = reserve;
    WritePos    = 0;
    ReadPos     = 0;
    OwnData     = true;
    Encrypted   = false;
}

CDataStream::~CDataStream()
{
    //dtor
    if (Data && OwnData)
        delete [] Data;

    Data        = NULL;
    Size        = 0;
    WritePos    = 0;
    ReadPos     = 0;
    OwnData     = true;
}

void CDataStream::reset(void)
{
    ResetWriter();
    ResetReader();
}
void CDataStream::ResetReader(void)
{
    ReadPos = 0;
}
void CDataStream::ResetWriter(void)
{
    WritePos = 0;
}

void CDataStream::encrypt(const AppFrame::string& key)
{
    AppFrame::u32 DataPos = 0;
    AppFrame::u32 KeyPos = 0;
    while (DataPos <= WritePos)
    {
        Data[DataPos] ^= key[KeyPos];
        ++DataPos;
        ++KeyPos;
        if (KeyPos >= key.size())
            KeyPos = 0;
    }
    Encrypted = !Encrypted;
}
void CDataStream::dencrypt(const AppFrame::string& key)
{
    encrypt(key);
}

bool CDataStream::isEncrypted(void)
{
    return Encrypted;
}

AppFrame::c8* CDataStream::getData(void)
{
    return Data;
}
const AppFrame::c8* CDataStream::getData(void) const
{
    return Data;
}
AppFrame::u32 CDataStream::getSize(void) const
{
    return WritePos;
}

AppFrame::c8& CDataStream::operator[](const AppFrame::u32& index)
{
    if (index >= WritePos)
        return *((AppFrame::c8*)NULL);
    return Data[index];
}
const AppFrame::c8& CDataStream::operator[](const AppFrame::u32& index) const
{
    if (index >= WritePos)
        return *((AppFrame::c8*)NULL);
    return Data[index];
}

void CDataStream::Write(const void* data, AppFrame::u32 size)
{
    if (WritePos+size >= Size)
    {
        //check if i have to copy data
        if (WritePos > 0)
        {
            AppFrame::c8* tmp = Data;
            Data = new AppFrame::c8[WritePos+size+1+ReserveSize];
            memcpy(Data, tmp, WritePos);
            delete [] tmp;
        }
        else
            Data = new AppFrame::c8[size+1+ReserveSize];
        Size = WritePos+size+1+ReserveSize;
        OwnData = true;
    }
    memcpy(&Data[WritePos], data, size);
    WritePos += size;
}

void CDataStream::Write(const bool& data)
{
    //printf("Write Char\n");
    char byte = data ? 1 : 0;
    Write(&byte, 1);
}
void CDataStream::Write(const AppFrame::c8& data)
{
    //printf("Write Char\n");
    Write(&data, 1);
}
void CDataStream::Write(const AppFrame::u8& data)
{
    //printf("Write Unsigned Char\n");
    Write((AppFrame::c8*)&data, 1);
}
void CDataStream::Write(const AppFrame::s32& data)
{
    //printf("Write Int\n");
    Write((AppFrame::c8*)&data, 4);
}
void CDataStream::Write(const AppFrame::u32& data)
{
    //printf("Write Unsigned Int\n");
    Write((AppFrame::c8*)&data, 4);
}
void CDataStream::Write(const AppFrame::s16& data)
{
    //printf("Write Short\n");
    Write((AppFrame::c8*)&data, 2);
}
void CDataStream::Write(const AppFrame::u16& data)
{
    //printf("Write Unsigned Short\n");
    Write((AppFrame::c8*)&data, 2);
}
void CDataStream::Write(const AppFrame::f32& data)
{
    //printf("Write float\n");
    Write((AppFrame::c8*)&data, 4);
}
void CDataStream::Write(const AppFrame::string& data)
{
    //printf("Write String\n");
    AppFrame::u32 s = data.size()+1;
    Write((AppFrame::c8*)&s, 4);
    Write(data.c_str(), s);
}
void CDataStream::Write(const irr::core::stringc& data)
{
    //printf("Write String\n");
    AppFrame::u32 s = data.size()+1;
    Write((AppFrame::c8*)&s, 4);
    Write(data.c_str(), s);
}
void CDataStream::Write(const IDataStream* data)
{
    //printf("Write DataStream\n");
    AppFrame::u32 s = data->getSize();
    Write((AppFrame::c8*)&s, 4);
    Write(data->getData(), s);
}
IDataStream& CDataStream::operator<<(const bool& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const AppFrame::c8& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const AppFrame::u8& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const AppFrame::s16& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const AppFrame::u16& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const AppFrame::s32& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const AppFrame::u32& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const AppFrame::f32& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const AppFrame::string& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const irr::core::stringc& data)
{
    Write(data);
    return *this;
}
IDataStream& CDataStream::operator<<(const IDataStream* data)
{
    Write(data);
    return *this;
}

bool CDataStream::Read(void* data, AppFrame::u32 size)
{
    if (ReadPos+size > WritePos)
        return false;
    memcpy(data, &Data[ReadPos], size);
    ReadPos += size;
    return true;
}
bool CDataStream::Read(bool& data)
{
    //printf("Read Char\n");
    char byte;
    bool r = Read(&byte, 1);
    data = byte == 1 ? true : false;
    return r;
}
bool CDataStream::Read(AppFrame::c8& data)
{
    //printf("Read Char\n");
    return Read(&data, 1);
}
bool CDataStream::Read(AppFrame::u8& data)
{
    //printf("Read Unsigned Char\n");
    return Read((AppFrame::c8*)&data, 1);
}
bool CDataStream::Read(AppFrame::s32& data)
{
    //printf("Read Int\n");
    return Read((AppFrame::c8*)&data, 4);
}
bool CDataStream::Read(AppFrame::u32& data)
{
    //printf("Read Unsigned Int\n");
    return Read((AppFrame::c8*)&data, 4);
}
bool CDataStream::Read(AppFrame::s16& data)
{
    //printf("Read Short\n");
    return Read((AppFrame::c8*)&data, 2);
}
bool CDataStream::Read(AppFrame::u16& data)
{
    //printf("Read Unsigned Short\n");
    return Read((AppFrame::c8*)&data, 2);
}
bool CDataStream::Read(AppFrame::f32& data)
{
    //printf("Read Float\n");
    return Read((AppFrame::c8*)&data, 4);
}
bool CDataStream::Read(AppFrame::string& data)
{
    //printf("Read String\n");
    AppFrame::u32 s = 0;
    if (!Read((AppFrame::c8*)&s, 4))
        return false;

    AppFrame::c8* d = new AppFrame::c8[s];

    if (!Read(d, s))
        return false;

    data = "";
    data.append(d);
    delete [] d;
    return true;
}
bool CDataStream::Read(irr::core::stringc& data)
{
    //printf("Read String\n");
    AppFrame::u32 s = 0;
    if (!Read((AppFrame::c8*)&s, 4))
        return false;

    AppFrame::c8* d = new AppFrame::c8[s];

    if (!Read(d, s))
        return false;

    data = "";
    data.append(d);
    delete [] d;
    return true;
}
bool CDataStream::Read(IDataStream* data)
{
    //printf("Read DataStream\n");
    AppFrame::u32 s = 0;
    if (!Read((AppFrame::c8*)&s, 4))
        return false;

    AppFrame::c8* d = new AppFrame::c8[s];

    if (!Read(d, s))
        return false;

    data->set(d, s, true);
    delete [] d;
    return true;
}
IDataStream& CDataStream::operator>>(bool& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(AppFrame::c8& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(AppFrame::u8& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(AppFrame::s32& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(AppFrame::u32& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(AppFrame::s16& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(AppFrame::u16& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(AppFrame::f32& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(AppFrame::string& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(irr::core::stringc& data)
{
    Read(data);
    return *this;
}
IDataStream& CDataStream::operator>>(IDataStream* data)
{
    Read(data);
    return *this;
}}
