/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef IDGENERATOR_H
#define IDGENERATOR_H

#include "irrlicht.h"
#include "ILogger.h"
namespace AppFrame
{
	//template<class T>
    class IDGenerator
    {
    public:
        IDGenerator(void* owner = NULL, bool isowner = true, bool withoutID = false) : m_owner(owner), m_isowner(isowner)
        {
        	if(m_owner == NULL)
				m_owner = (void*)this;

			m_assigned = false;

			if(withoutID)
				return;

			if(m_isowner)
			{
				if(getIDs().size() == 0)
				{
					m_ID = 0;
					getIDs().push_back(this);
					m_assigned = true;
				}
				else
				{
					for(irr::u32 i= 0;i < getIDs().size();i++)
					{
						if(getIDs()[i] == NULL)
						{
							getIDs()[i] = this;
							m_ID = i;
							m_assigned = true;
							break;
						}
					}
				}
				if(!m_assigned)
				{
					m_ID = getIDs().size();
					getIDs().push_back(this);
					m_assigned = true;
				}
			}
        }

        virtual ~IDGenerator()
        {
        	if(getIDs().size()>m_ID && getIDs()[m_ID] == this)
        	{
				ILogger::log("Release ID: %i", m_ID);
				getIDs()[m_ID] = NULL;
        	}
        }


        void* getOwner(void)
        {
        	return m_owner;
        }

        irr::u32 getID(void)
        {
        	return m_ID;
        }

        static bool IsIDValid(irr::u32 id)
        {
        	if(getIDs().size()<=id)
				return false;
			else if(getIDs()[id] == NULL)
				return false;

			return true;
        }

        static void* getObject(irr::u32 id)
        {
        	if(getIDs().size() > id && getIDs()[id] != NULL)
				return getIDs()[id]->getOwner();
			else
				return NULL;
        }
        static IDGenerator* getIDGen(irr::u32 id)
        {
        	if(getIDs().size() > id)
				return getIDs()[id];
			else
				return NULL;
        }
    protected:
		void setID(irr::u32 id)
        {
        	if(m_isowner || m_assigned)
				return;

			while(getIDs().size() <= id)
				getIDs().push_back(NULL);

			if(getIDs()[id] == NULL)
			{
				getIDs()[id] = this;
				m_ID = id;
			}
			else
			{
				ILogger::error("ID ALREADY TAKEN");
			}
        }

		void* m_owner;
		irr::u32 m_ID;
		bool m_assigned;
		bool m_isowner;
        //static irr::core::map<irr::u32, CIDGenerator*> m_IDs;
        static irr::core::array<IDGenerator*>& getIDs(void)
        {
        	static irr::core::array<IDGenerator*> IDs;
			return IDs;
        }
    private:
    };
}
#endif // IDGENERATOR_H
