/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#include "CLoadingScreen.h"
#ifdef COMPILE_FOR_WINDOWS
#include <windows.h> // Sleep
#endif
#ifdef COMPILE_FOR_UNIX
#include <unistd.h> // usleep
#endif
namespace irr
{
    namespace gui
    {
        CLoadingScreen::CLoadingScreen(IGUIEnvironment *  environment, IGUIElement *  parent, s32  id) : IGUIElement(EGUIET_ELEMENT, environment, parent, id, core::rect< s32 >(0, 0, environment->getVideoDriver()->getScreenSize().Width, environment->getVideoDriver()->getScreenSize().Height))
        {
            //ctor
            m_draw = false;
        }

        CLoadingScreen::~CLoadingScreen()
        {
            //dtor
        }

        void CLoadingScreen::drawLoadingScreen(core::stringc texture, core::stringc text, s32 percent, irr::u32 sleep)
        {
            setLoadingScreen(texture, text, percent);
            Environment->getVideoDriver()->beginScene(true, true, video::SColor(255,0,0,255));
            draw();
            Environment->getVideoDriver()->endScene();
            #ifdef COMPILE_FOR_WINDOWS
            Sleep(sleep);
            #endif
            #ifdef COMPILE_FOR_UNIX
            usleep(sleep);
            #endif

        }

        void CLoadingScreen::setLoadingScreen(core::stringc texture, core::stringc text, s32 percent)
        {
            m_texture = texture;
            m_text = text;
            m_percent = percent;
            m_draw = true;
        }

        void CLoadingScreen::draw()
        {
            if(!m_draw)
                return;

            video::IVideoDriver* driver = Environment->getVideoDriver();
            video::ITexture* texture = driver->getTexture(m_texture.c_str());
            if (!texture)
				texture = driver->getTexture("#splashscreen");
            IGUIFont* font = Environment->getFont("fonts/matrix_36.bmp");
            u32 width = driver->getScreenSize().Width;
            u32 height = driver->getScreenSize().Height;

            if (texture)
				driver->draw2DImage(texture, core::rect< s32 >(0, 0, width, height), core::rect< s32 >(0,0, texture->getOriginalSize().Width, texture->getOriginalSize().Height), NULL, 0, true);

            if (font)
                font->draw(L"Loading.....", core::rect< s32 >(0,0,width, height*0.2), video::SColor(255,255,255,0), true, true, 0);

            //draw loading bar
            if(m_percent != -1)
            {
                driver->draw2DRectangle(core::rect< s32 >(width*0.2-5, height*0.3-5, width*0.8+5, height*0.3+25), video::SColor(255,128,128,128), video::SColor(255,128,128,128), video::SColor(255,128,128,128), video::SColor(255,128,128,128));
                driver->draw2DRectangle(core::rect< s32 >(width*0.2, height*0.3, width*0.2+(width*0.8-width*0.2)*m_percent/100, height*0.3+20), video::SColor(255,255,0,0), video::SColor(255,255,0,0), video::SColor(255,255,0,0), video::SColor(255,255,0,0));
                core::stringw tx = core::stringw(m_percent).c_str();
                tx.append(L" %");
                Environment->getSkin()->getFont()->draw(tx.c_str(), core::rect< s32 >(width*0.2-5, height*0.3-5, width*0.8+5, height*0.3+25), video::SColor(255,0,0,255), true, true, 0);
                m_percent = -1;
            }

            if(m_text != "")
            {
            	if (font)
					font->draw(core::stringw(m_text.c_str()).c_str(), core::rect< s32 >(0,height*0.4,width, height*0.5), video::SColor(255,255,255,0), true, true, 0);
                m_text = "";
            }

            IGUIElement::draw();

            m_draw = false;
        }
    }
}
