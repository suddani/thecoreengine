/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#include "CAttributeNode.h"

namespace irr
{
    namespace scene
    {
        CAttributeNode::CAttributeNode(ISceneNode* parent, IrrlichtDevice* device, s32 id, core::vector3df pos, core::vector3df rot, core::vector3df scale) : IEntityNode(parent, device, id, pos, rot, scale)
        {
            //ctor
            Device = device;
			Box = irr::core::aabbox3d<irr::f32>(-0.5,-0.5,-0.5,0.5,0.5,0.5);
        }

        CAttributeNode::~CAttributeNode()
        {
            //dtor
        }

        irr::core::array<CAttribute*>& CAttributeNode::getAttributes(void)
        {
            return Attributes;
        }

        void CAttributeNode::setEntity(irr::core::stringc name)
        {
        	if(name != EntityType)
        	{
        		EntityType = name;
				loadAttributes(EntityType.c_str());
        	}
        }

        const irr::core::stringc& CAttributeNode::getEntityType(void)
        {
        	return EntityType;
        }

        void CAttributeNode::deserializeAttributes(io::IAttributes *in, io::SAttributeReadWriteOptions *options)
        {
            ISceneNode::deserializeAttributes(in, options);
            irr::core::stringc newEntity = in->getAttributeAsString("EntityType");
            setEntity(newEntity);
            for (irr::u32 i=0;i<Attributes.size();i++)
                {
                    switch (Attributes[i]->getType())
                    {
                    case CAttribute::ATTRIBUTE_INT:
                        if (in->existsAttribute(Attributes[i]->getName().c_str()))
                            Attributes[i]->getInt() = in->getAttributeAsInt(Attributes[i]->getName().c_str());
                        break;
                    case CAttribute::ATTRIBUTE_FLOAT:
                        if (in->existsAttribute(Attributes[i]->getName().c_str()))
                            Attributes[i]->getFloat() = in->getAttributeAsFloat(Attributes[i]->getName().c_str());
                        break;
                    case CAttribute::ATTRIBUTE_BOOL:
                        if (in->existsAttribute(Attributes[i]->getName().c_str()))
                            Attributes[i]->getBool() = in->getAttributeAsBool(Attributes[i]->getName().c_str());
                        break;
                    case CAttribute::ATTRIBUTE_STRING:
                        if (in->existsAttribute(Attributes[i]->getName().c_str()))
                            Attributes[i]->getString() = in->getAttributeAsString(Attributes[i]->getName().c_str());
                        break;
                    case CAttribute::ATTRIBUTE_VECTOR3D:
                        if (in->existsAttribute(Attributes[i]->getName().c_str()))
                            Attributes[i]->getVector3D() = in->getAttributeAsVector3d(Attributes[i]->getName().c_str());
                        break;
                    }
                }
        }

        void CAttributeNode::serializeAttributes(io::IAttributes *out, io::SAttributeReadWriteOptions *options) const
        {
            ISceneNode::serializeAttributes(out, options);
            out->addString("EntityType", EntityType.c_str());
            for (irr::u32 i=0;i<Attributes.size();i++)
            {
                switch (Attributes[i]->getType())
                {
                case CAttribute::ATTRIBUTE_INT:
                    out->addInt(Attributes[i]->getName().c_str(), Attributes[i]->getInt());
                    break;
                case CAttribute::ATTRIBUTE_FLOAT:
                    out->addFloat(Attributes[i]->getName().c_str(), Attributes[i]->getFloat());
                    break;
                case CAttribute::ATTRIBUTE_BOOL:
                    out->addBool(Attributes[i]->getName().c_str(), Attributes[i]->getBool());
                    break;
                case CAttribute::ATTRIBUTE_STRING:
					{
						const irr::c8* data = Attributes[i]->getString().c_str();
						out->addString(Attributes[i]->getName().c_str(), data);
					}
                    break;
                case CAttribute::ATTRIBUTE_VECTOR3D:
                    out->addVector3d(Attributes[i]->getName().c_str(), Attributes[i]->getVector3D());
                    break;
                }
            }
        }

		void CAttributeNode::OnRegisterSceneNode(void)
		{
			if (IsVisible)
				SceneManager->registerNodeForRendering(this);

			ISceneNode::OnRegisterSceneNode();
		}

		ISceneNode* CAttributeNode::clone(ISceneNode* parent, ISceneManager* smgr)
		{
			CAttributeNode* node = new CAttributeNode(Device->getSceneManager()->getRootSceneNode(), Device, -1, getPosition(), getRotation(), getScale());
			node->EntityType = EntityType;
			node->loadAttributes(EntityType.c_str());
			for(irr::u32 i = 0;i<node->Attributes.size();i++)
			{
				switch(node->Attributes[i]->getType())
				{
					case CAttribute::ATTRIBUTE_BOOL:
						node->Attributes[i]->getBool() = Attributes[i]->getBool();
						break;
					case CAttribute::ATTRIBUTE_INT:
						node->Attributes[i]->getInt() = Attributes[i]->getInt();
						break;
					case CAttribute::ATTRIBUTE_FLOAT:
						node->Attributes[i]->getFloat() = Attributes[i]->getFloat();
						break;
					case CAttribute::ATTRIBUTE_STRING:
						node->Attributes[i]->getString() = Attributes[i]->getString();
						break;
					case CAttribute::ATTRIBUTE_VECTOR3D:
						node->Attributes[i]->getVector3D() = Attributes[i]->getVector3D();
						break;
				}
			}

			irr::core::list<ISceneNode*>::Iterator it = Children.begin();
			while(it!=Children.end())
			{
				(*it)->clone(node, SceneManager);
				++it;
			}

			node->drop();

			return node;
		}

        void CAttributeNode::loadAttributes(const irr::c8* name)
        {
            for (irr::u32 i=0;i<Attributes.size();i++)
                Attributes[i]->clear();
            Attributes.clear();
            irr::core::stringc FileName = "Entities/";
            FileName.append(name);
            FileName.append(".entity");
            irr::io::IXMLReaderUTF8* xml = Device->getFileSystem()->createXMLReaderUTF8(FileName.c_str());

            bool foundEntity = false;

            while (xml && xml->read())
            {
                switch (xml->getNodeType())
                {
                case irr::io::EXN_TEXT:
                    //printf("EXN_TEXT\n");
                    break;
                case irr::io::EXN_ELEMENT:
                    //printf("EXN_ELEMENT: %s\n", xml->getNodeName());
                    if (!strcmp("Entity",xml->getNodeName()))
                    {
                    	foundEntity = true;
                    }
                    else if (!strcmp("Int",xml->getNodeName()) && foundEntity)
                    {
                    	Attributes.push_back(new CAttribute(CAttribute::ATTRIBUTE_INT, xml->getAttributeValue("name")));
                    	Attributes[Attributes.size()-1]->getInt() = xml->getAttributeValueAsInt("value");
                    }
                    else if (!strcmp("Float",xml->getNodeName()) && foundEntity)
                    {
                    	Attributes.push_back(new CAttribute(CAttribute::ATTRIBUTE_FLOAT, xml->getAttributeValue("name")));
                    	Attributes[Attributes.size()-1]->getFloat() = xml->getAttributeValueAsFloat("value");
                    }
                    else if (!strcmp("Bool",xml->getNodeName()) && foundEntity)
                    {
                    	Attributes.push_back(new CAttribute(CAttribute::ATTRIBUTE_BOOL, xml->getAttributeValue("name")));
                    	irr::core::stringc val = xml->getAttributeValue("value");
                    	if (val == "true")
							Attributes[Attributes.size()-1]->getBool() = true;
						else
							Attributes[Attributes.size()-1]->getBool() = false;
                    }
                    else if (!strcmp("Vector3D",xml->getNodeName()) && foundEntity)
                    {
                    	Attributes.push_back(new CAttribute(CAttribute::ATTRIBUTE_VECTOR3D, xml->getAttributeValue("name")));
                    	irr::core::stringc val = xml->getAttributeValue("value");
                    	if (val.size() != 0)
							sscanf(val.c_str(), "%f, %f, %f", &Attributes[Attributes.size()-1]->getVector3D().X, &Attributes[Attributes.size()-1]->getVector3D().Y, &Attributes[Attributes.size()-1]->getVector3D().Z);
                    }
                    else if (!strcmp("String",xml->getNodeName()) && foundEntity)
                    {
                    	Attributes.push_back(new CAttribute(CAttribute::ATTRIBUTE_STRING, xml->getAttributeValue("name")));
                    	Attributes[Attributes.size()-1]->getString() = xml->getAttributeValue("value");
                    }
                    break;
                case irr::io::EXN_ELEMENT_END:
                    //printf("EXN_ELEMENT_END: %s\n", xml->getNodeName());
                    if (!strcmp("Entity", xml->getNodeName()) && foundEntity)
                        break;
                    break;
                case irr::io::EXN_CDATA:
                    //printf("EXN_CDATA\n");
                    break;
                }
            }
            delete xml;
        }
    }
}
