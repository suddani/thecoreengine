/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef CCONFIG_H
#define CCONFIG_H

#include "irrlicht.h"

class CConfig
{
public:
    CConfig(irr::c8* filename = "config.xml");//, HINSTANCE hInstance = NULL);
    virtual ~CConfig(void);

    irr::SIrrlichtCreationParameters& getIrrlichtParameter(void);

    void loadIrrlichtCFG(irr::c8* filename = "config.xml");

    void saveCFG(irr::c8* filename = "config.xml");

    void setIrrCFG(irr::SIrrlichtCreationParameters& irrlichparameter);

protected:

    //HWND createWindowsWindow(char* name, int size[2]);

    //HINSTANCE m_hInstance;

    irr::SIrrlichtCreationParameters m_irrlichparameter;
};
#endif
