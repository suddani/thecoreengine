/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef CATTRIBUTENODE_H
#define CATTRIBUTENODE_H

#include <IEntityNode.h>

namespace irr
{
    namespace scene
    {
        class CAttributeNode : public IEntityNode
        {
        public:
            CAttributeNode(ISceneNode* parent, IrrlichtDevice* device, s32 id = -1, core::vector3df pos = core::vector3df(0,0,0), core::vector3df rot = core::vector3df(0,0,0), core::vector3df scale = core::vector3df(1,1,1));
            virtual ~CAttributeNode();

            void deserializeAttributes(io::IAttributes *in, io::SAttributeReadWriteOptions *options=0);
            void serializeAttributes(io::IAttributes *out, io::SAttributeReadWriteOptions *options=0) const;

            irr::core::array<CAttribute*>& getAttributes(void);

            void setEntity(irr::core::stringc name);

			void OnRegisterSceneNode(void);

            void render(void){}

			ESCENE_NODE_TYPE getType() const;

			const irr::core::stringc& getEntityType(void);

			u32 getMaterialCount()
			{
				return 1;
			}

			video::SMaterial& getMaterial(u32 i)
			{
				return Material;
			}

            const irr::core::aabbox3d<irr::f32>& getBoundingBox(void) const
            {
            	return Box;
            }

            virtual ISceneNode* clone(ISceneNode* newParent=0, ISceneManager* newManager=0);
        protected:
			irr::core::stringc EntityType;

			irr::core::array<CAttribute*> Attributes;

			void loadAttributes(const irr::c8* name);

			IrrlichtDevice* Device;

			irr::core::aabbox3d<irr::f32> Box;

			video::SMaterial Material;
        private:
        };
    }
}
#endif // CATTRIBUTENODE_H
