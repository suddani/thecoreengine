#ifndef CCONSOLE_H
#define CCONSOLE_H

#include "IConsole.h"
#include "appRingBuffer.h"
#include "InputListener.h"
#include "CGUIChatQueue.h"

namespace AppFrame
{
    namespace input
    {
        class CConsole : public IConsole, public IKeyListener, public ILog::ILogReceiver
        {
        public:
            /** Default constructor */
            CConsole(graphic::IGraphicManager* graphic, IInputManager* inputmanager);
            /** Default destructor */
            ~CConsole();

            void setIsConsoleApplication(bool is)
            {
                IsConsoleApplication = is;
            }

            bool isConsoleApplication(void)
            {
                return IsConsoleApplication;
            }

            bool isActive(void)
            {
                return m_consoleActive;
            }

            SCommand getPreviousCommand(void);

            SCommand getNextCommand(void);

            void ExecuteCommand(const SCommand& command);

            void RegisterConsoleCommand(const c8* command, actions::SFunctor1<const SCommand&>* callback, const irr::c8* info = 0, const bool& override = false);

            void unRegisterConsoleCommand(const c8* command, void* classPointer = NULL);

            void UpdateConsoleInput(void);

            //keyinput
            void key_event(const irr::EKEY_CODE& key, const KeyState& state);

            //log input
            void OnLogEvent(const irr::core::stringc& event, ILog::E_LOG_TYPE type, irr::u32 loglevel);

            bool IsConsoleApplication;
        protected:
            IInputManager* InputManager;
            graphic::IGraphicManager* Graphic;
            bool m_consoleActive;

            irr::gui::CGUIChatQueue*    Window;
            irr::gui::IGUIEditBox*      InputLine;
            AppFrame::string            ConsoleLine; //Console Application

            RingBuffer<SCommand, 10> OldCommands;

            class CommandData : public IPointer
            {
            public:
                CommandData(void)
                {
                    Action = NULL;
                }
                CommandData(const CommandData& data)
                {
                    Info = data.Info;
                    if (Action)
                        Action->drop();
                    Action = data.Action;
                    if (Action)
                        Action->grab();
                }
                CommandData(const irr::c8* info, actions::SFunctor1<const SCommand&>* action)
                {
                    Info = info;
                    Action = action;
                }
                ~CommandData(void)
                {
                    if (Action)
                        Action->drop();
                }
                CommandData& operator=(const CommandData& data)
                {
                    nlogl(1)<<"Assigment"<<nlflush;
                    Info = data.Info;

                    if (Action)
                        Action->drop();

                    Action = data.Action;

                    if (Action)
                        Action->grab();

                    return *this;
                }
                irr::core::stringc Info;
                actions::SFunctor1<const SCommand&>* Action;
            };
            irr::core::map<irr::core::stringc, CommandData*> Commands;
            irr::core::map<irr::core::stringc, SCommand> KeyBindings;

            void help(const SCommand& command);

            void bindKey(const SCommand& command);

            void unbindKey(const SCommand& command);

            void echo(const SCommand& command);

            void simpleScript(const SCommand& command);
        private:
        };
    }
}
#endif // CCONSOLE_H
