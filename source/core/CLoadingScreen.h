/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef CLOADINGSCREEN_H
#define CLOADINGSCREEN_H

#include <irrlicht.h>
namespace irr
{
    namespace gui
    {
        class CLoadingScreen : public IGUIElement
        {
        public:
            CLoadingScreen(IGUIEnvironment *  environment, IGUIElement *  parent, s32  id);
            virtual ~CLoadingScreen();

            void setLoadingScreen(core::stringc texture, core::stringc text, s32 percent = -1);

            void drawLoadingScreen(core::stringc texture, core::stringc text, s32 percent = -1, irr::u32 sleep = 100);

            virtual void draw();
        protected:
            core::stringc m_texture;
            core::stringc m_text;
            s32 m_percent;
            bool m_draw;
        private:
        };
    }
}

#endif // CLOADINGSCREEN_H
