#include "CConsole.h"
#include "IInputManager.h"
#include "IGraphicManager.h"
#ifdef win32
#include <conio.h>
#endif
#ifdef linux
#include <termios.h>
int kbhit(void)
{
    struct termios term, oterm;
    int fd = 0;
    int c = 0;
    tcgetattr(fd, &oterm);
    memcpy(&term, &oterm, sizeof(term));
    term.c_lflag = term.c_lflag & (!ICANON);
    term.c_cc[VMIN] = 0;
    term.c_cc[VTIME] = 1;
    tcsetattr(fd, TCSANOW, &term);
    c = getchar();
    tcsetattr(fd, TCSANOW, &oterm);
    if (c != -1)
        ungetc(c, stdin);
    return ((c != -1) ? 1 : 0);
}
int getch(void)
{
    static int ch = -1, fd = 0;
    struct termios neu, alt;
    fd = fileno(stdin);
    tcgetattr(fd, &alt);
    neu = alt;
    neu.c_lflag &= ~(ICANON|ECHO);
    tcsetattr(fd, TCSANOW, &neu);
    ch = getchar();
    tcsetattr(fd, TCSANOW, &alt);
    return ch;
}
#endif
namespace AppFrame
{
namespace input
{
void godMode(const SCommand& command)
{
    nlogl(1)<<"You can't cheat fucker!"<<nlflush;
}

IConsole* createConsole(graphic::IGraphicManager* graphic, IInputManager* input)
{
#ifndef DEBUG
    //AppFrame::ILogger::setLogLevel(1);
    nlogLevel = 1;
#endif
    return new CConsole(graphic, input);
}

CConsole::CConsole(graphic::IGraphicManager* graphic, IInputManager* inputmanager)
{
    //ctor
    Window          = NULL;
    InputLine       = NULL;
    InputManager    = inputmanager;
    m_consoleActive = false;
    if (InputManager)
    {
        InputManager->addListener(this);
        InputManager->grab();
    }
    Graphic = graphic;
    if (Graphic)
    {
        Graphic->grab();
        irr::IrrlichtDevice* m_device = Graphic->getDevice();
        Window = new irr::gui::CGUIChatQueue(m_device->getGUIEnvironment(), m_device->getGUIEnvironment()->getRootGUIElement(), -1, irr::core::rect<irr::s32>(0,0,m_device->getVideoDriver()->getScreenSize().Width,m_device->getVideoDriver()->getScreenSize().Height/2), m_device->getTimer(), m_device->getGUIEnvironment()->getBuiltInFont(), -1, 0, true);
        Window->setEnabled(false);
        Window->setVisible(false);
        InputLine = m_device->getGUIEnvironment()->addEditBox(L"", irr::core::rect<irr::s32>(0,m_device->getVideoDriver()->getScreenSize().Height/2,m_device->getVideoDriver()->getScreenSize().Width,m_device->getVideoDriver()->getScreenSize().Height/2+20));
        InputLine->setEnabled(false);
        InputLine->setVisible(false);
        InputLine->grab();
    }
    RegisterConsoleCommand("/bindkey", new actions::SFunctorReciever1<CConsole, const SCommand&>(this, &CConsole::bindKey), "Bind a KeyboardKey to a ConsoleCommand");
    RegisterConsoleCommand("/unbindkey", new actions::SFunctorReciever1<CConsole, const SCommand&>(this, &CConsole::unbindKey), "unBind a KeyboardKey from a ConsoleCommand");
    RegisterConsoleCommand("/help", new actions::SFunctorReciever1<CConsole, const SCommand&>(this, &CConsole::help), "List all available Console Commands");
    RegisterConsoleCommand("/info", new actions::SFunctorReciever1<CConsole, const SCommand&>(this, &CConsole::help), "List all available Console Commands");
    RegisterConsoleCommand("/echo", new actions::SFunctorReciever1<CConsole, const SCommand&>(this, &CConsole::echo), "Print string to console");
    RegisterConsoleCommand("/script", new actions::SFunctorReciever1<CConsole, const SCommand&>(this, &CConsole::simpleScript), "loads a file and executes the console commands");
    RegisterConsoleCommand("/god", new actions::SFunctionReciever1<const SCommand&>(&godMode), "Aktivate GodMode");

    nlget.addReceiver(this);
    nlogl(1)<<"Initialized ConsoleServer"<<nlflush;
    nlogl(1)<<"Irrlicht "<<IRRLICHT_SDK_VERSION<<nlflush;
}

CConsole::~CConsole()
{
    //dtor
    if (Window)
    {
        Window->remove();
        Window->drop();
    }
    if (InputLine)
    {
        InputLine->remove();
        InputLine->drop();
    }
    if (InputManager)
    {
        InputManager->removeListener(this);
        InputManager->drop();
    }
    if (Graphic)
    {
        Graphic->drop();
    }
    irr::core::map<irr::core::stringc, CommandData*>::Iterator it = Commands.getIterator();
    while (!it.atEnd())
    {
        it->getValue()->drop();
        it++;
    }
    nlget.removeReceiver(this);
}

void CConsole::UpdateConsoleInput(void)
{
    if (!IsConsoleApplication)
        return;

    if (kbhit())
    {
        char c = getch();
        if (c != '\n')
        {
            ConsoleLine.push_back(c);
            printf("%c", c);
        }
        else
        {
            printf("\n");
            nlogl(1)<<"Comannd Parsed: ["<<ConsoleLine.c_str()<<"]"<<nlflush;
            if (ConsoleLine.size() > 0 && ConsoleLine[0] == '/')
                ExecuteCommand(ConsoleLine.c_str());
            //else
            //Frame->Print(ConsoleLine.c_str());
            ConsoleLine.clear();
        }
    }
}

SCommand CConsole::getPreviousCommand(void)
{
    SCommand command;
    OldCommands.readback(command);
    return command;
}

SCommand CConsole::getNextCommand(void)
{
    SCommand command;
    OldCommands.read(command);
    return command;
}

void CConsole::ExecuteCommand(const SCommand& command)
{
    OldCommands.write(command);
    irr::core::map<irr::core::stringc, CommandData*>::Node* node = NULL;
    node = Commands.find(irr::core::stringc(command.Command));
    if (node)
    {
        node->getValue()->Action->call(command);
        return;
    }
    //now check if it is a console variable
    if (command.Parameter.size() > 0)
    {
        irr::core::stringc varName = command.Command;
        varName.erase(0);
        if (setValue(varName.c_str(), command.Parameter[0].Value.c_str()))
            return;
    }
    nlog<<"Command \""<<command.Command.c_str()<<"\" not assigned"<<nlfendl;
}

void CConsole::RegisterConsoleCommand(const c8* command, actions::SFunctor1<const SCommand&>* callback, const irr::c8* info, const bool& override)
{
    irr::core::map<irr::core::stringc, CommandData*>::Node* node = NULL;
    node = Commands.find(irr::core::stringc(command));
    if (node)
    {
        if (override)
        {
            node->getValue()->drop();
        }
        else
        {
            nlogl(1)<<"Command \""<<command<<"\" already taken"<<nlflush;
            return;
        }
    }
    Commands[irr::core::stringc(command)] = new CommandData(info, callback);
    nlog<<"Registered Command \""<<command<<"\""<<nlflush;
}

void CConsole::unRegisterConsoleCommand(const c8* command, void* classPointer)
{
    irr::core::map<irr::core::stringc, CommandData*>::Node* node = NULL;
    node = Commands.find(irr::core::stringc(command));
    if (node && node->getValue()->Action->getClassPointer() == classPointer)
    {
        node->getValue()->drop();
        Commands.remove(irr::core::stringc(command));
        nlog<<"Unregistered Command \""<<command<<"\""<<nlflush;
    }
}

void CConsole::key_event(const irr::EKEY_CODE& key, const KeyState& state)
{
    //nlog<<"KeyInput: "<<key<<" state: "<<(state.pressed ? "down" : "up")<<nlendl;
    if (key == irr::KEY_F1 && state.pressed)
    {
        m_consoleActive = !m_consoleActive;

        nlog<<"Change ConsoleState to "<<(m_consoleActive ? "on" : "off")<<nlendl;

        Window->setEnabled(m_consoleActive);
        Window->setVisible(m_consoleActive);
        InputLine->setEnabled(m_consoleActive);
        InputLine->setVisible(m_consoleActive);
        if (m_consoleActive)
        {
            Graphic->getDevice()->getGUIEnvironment()->getRootGUIElement()->bringToFront(Window);
            Graphic->getDevice()->getGUIEnvironment()->getRootGUIElement()->bringToFront(InputLine);
            Graphic->getDevice()->getGUIEnvironment()->setFocus(InputLine);

            InputManager->restrictListener((IKeyListener*)this);
        }
        else
            InputManager->restrictListener((IKeyListener*)NULL);
        return;
    }
    else if (m_consoleActive)
    {
        if (key == irr::KEY_RETURN)
        {
            irr::core::stringc message = InputLine->getText();
            if (message.size() > 0)
            {
                if (message[0] == '/')
                {
                    ExecuteCommand(message.c_str());
                }
                //else
                //Window->addMessage(InputLine->getText());
            }
            InputLine->setText(L"");
        }
        return;
    }

    irr::core::map<irr::core::stringc, SCommand>::Node* node = KeyBindings.find(InputManager->getKey(key)->Name.c_str());
    if (node)
    {
        node->getValue().print2Log();
        SCommand command = node->getValue();
        command.addParameter(state.pressed == EKS_UP ? "1" : "0");
        ExecuteCommand(command);
    }
}

void CConsole::OnLogEvent(const irr::core::stringc& event, ILog::E_LOG_TYPE type, irr::u32 loglevel)
{
    irr::core::stringc data = type == ILog::ELT_LOG ? "LOG:" : "ERROR:";
    //check if console Command
    if (event.size() > 0)
    {
        if (event[0] == '/')
        {
            ExecuteCommand(event.c_str());
        }
        else
        {
            data.append(" ");
            data.append(event);
            Window->addMessage(data.c_str());
        }
    }
}

void CConsole::help(const SCommand& command)
{
    nlogl(1)<<"Registered Commands"<<nlflush;
    irr::core::stringc saveinfo = nlinfo;
    nlinfo = "";
    irr::core::map<irr::core::stringc, CommandData*>::Iterator it = Commands.getIterator();

    while (!it.atEnd())
    {
        irr::core::map<irr::core::stringc, CommandData*>::Node* n = it.getNode();
        irr::core::stringc a = n->getKey();
        irr::core::stringc b = n->getValue()->Info;
        irr::core::stringc c = "\t-> ";
        c += a;
        c += " - ";
        c += b;
        nlogl(1)<<c<<nlfendl;
        it++;
    }

    nlinfo = saveinfo;
}

void CConsole::bindKey(const SCommand& command)
{
    if (command.Parameter.size() <= 1)
    {
        nlogl(1)<<"No key/command assigned for binding"<<nlfendl;
        return;
    }
    SCommand neu;
    if (command.Parameter[1].Value[0] != '/')
        neu.Command = "/";
    neu.Command.append(command.Parameter[1].Value);
    for (irr::u32 i=2; i<command.Parameter.size(); ++i)
        neu.Parameter.push_back(command.Parameter[i]);

    KeyBindings[command.Parameter[0].Value] = neu;
}

void CConsole::unbindKey(const SCommand& command)
{
    if (command.Parameter.size() <= 0)
    {
        nlogl(1)<<"No key given for unbinding"<<nlfendl;
        return;
    }
    KeyBindings.remove(command.Parameter[0].Value);
}

void CConsole::echo(const SCommand& command)
{
    if (command.Parameter.size() <= 0)
    {
        return;
    }
    Window->addMessage(irr::core::stringw(command.Parameter[0].Value.c_str()).c_str());
    printf("%s\n", command.Parameter[0].Value.c_str());
}

void CConsole::simpleScript(const SCommand& command)
{
    if (command.Parameter.size() <= 0)
    {
        nlogl(1)<<"No file parameter given"<<nlfendl;
        return;
    }
    FILE* datei = fopen(command.Parameter[0].Value.c_str(), "r");
    if (datei)
    {
        nlogl(1)<<"Run script File..."<<nlfendl;
        string buffer;
        while (!feof(datei))
        {
            char c = fgetc(datei);
            if (c != '\n')
                buffer.push_back(c);
            else
            {
                ExecuteCommand(buffer.c_str());
                buffer = "";
            }
        }
        fclose(datei);
    }
    else
        nlogl(1)<<"File does not exist"<<nlfendl;
}
}
}
