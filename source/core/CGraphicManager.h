/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef CGRAPHICMANAGER_H
#define CGRAPHICMANAGER_H

#include <IGraphicManager.h>

namespace AppFrame
{
    namespace graphic
    {
        class CGraphicManager : public IGraphicManager
        {
        public:
            CGraphicManager(irr::IrrlichtDevice* device);

            virtual ~CGraphicManager();

            irr::IrrlichtDevice* getDevice(void);

            void draw(bool post);

            bool isPostProcessActive(const irr::c8* effectName);

            bool isPostProcessActive(IPostProcess* post);

            void deactivateAllPostProcessEffects(void);

            void deactivatePostProcessEffect(const irr::c8* effectName);

            void deactivatePostProcessEffect(IPostProcess* post);

            void activatePostProcessEffect(const irr::c8* effectName);

            void activatePostProcessEffect(IPostProcess* post);

            bool addPostProcess(IPostProcess* post);

            IPostProcess* getPostProcess(const irr::c8* filename);

            IPostProcess* createPostProcess(const irr::c8* filename);

            IPostProcess* createPostProcess(irr::u32 width, irr::u32 height, const irr::c8* effectName);

            IBeamNode* createBeamNode(const irr::c8* side, const irr::c8* front, irr::scene::ISceneNode* parent = NULL, irr::scene::ISceneManager* smgr = NULL);

            IBoltNode* createBoltNode(irr::scene::ISceneNode* parent = NULL, irr::scene::ISceneManager* smgr = NULL);

            irr::scene::IEntityNode* createEntityNode(irr::scene::ISceneNode* parent = NULL, irr::scene::ISceneManager* smgr = NULL, irr::s32 id = -1);

            irr::scene::IParticleSystem* createParticleSystem(irr::scene::ISceneNode* parent, irr::s32 id=-1, const irr::core::vector3df& position = irr::core::vector3df(0,0,0), const irr::core::vector3df& rotation = irr::core::vector3df(0,0,0), const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f, 1.0f));

            irr::gui::IGUIChatQueue* createChatBox(irr::core::rect<irr::s32> rect, irr::gui::IGUIElement* parent = NULL, irr::s32 id = -1, irr::gui::IGUIFont* font = NULL, irr::s32 life = 5000, irr::u32 FadeTime = 500, bool wordwrap = true);

            irr::scene::IObjectSceneNode* addObjectSceneNode(irr::scene::ISceneNode* parent = NULL, const irr::s32& id = -1, const irr::core::vector3df& position = irr::core::vector3df(0,0,0), const irr::core::vector3df& rotation = irr::core::vector3df(0,0,0), const irr::core::vector3df& scale = irr::core::vector3df(1,1,1));

            irr::scene::ISceneNodeFactory* getSceneNodeFactory(void);

            irr::video::E_MATERIAL_TYPE getMaterial(const irr::c8* mat);

            IShaderMaterial* getShaderMaterial(const irr::c8* mat);

            const irr::core::stringc getMaterialName(irr::video::E_MATERIAL_TYPE type);
        protected:
            irr::IrrlichtDevice* m_device;
            irr::scene::ISceneNodeFactory* m_sceneNodeFactory;
            irr::core::array<IShaderMaterial*> ShaderMaterials;
            irr::core::array<IPostProcess*> PostProcessEffects;
            irr::core::array<IPostProcess*> ActivePostProcessEffects;

            void registerExternalMeshLoader(void);
        private:
        };
    }
}
#endif // CGRAPHICMANAGER_H
