#ifndef CENTITY_H
#define CENTITY_H

#include <core/CIDGenerator.h>
#include <core/Tools.h>
#include <core/IRef.h>
#include <core/MessageDispatcher.h>
#include <core/CRunnableList.h>

#include "IComponent.h"

class CEntity;
class CEntityManager;

typedef AppFrame::SharedPointer<CEntity> EntityPtr; /**< Does automatic ref counting for the CEntity */
typedef AppFrame::CIDGenerator<CEntity> EntityIDGenerator; /**< The id generator for CEntity */
typedef AppFrame::CID<CEntity> EntityID; /**< A CEntity id object. Allows casting back and forth */


/** \brief The main Entity class.
 *  These objects represent every object in the game!
 */

class CEntity : public AppFrame::IRef, public AppFrame::actions::MessageDispatcher, public AppFrame::actions::Subscriber, AppFrame::CRunnableList<ComponentPtr>
{
    public:
        CEntity();

        virtual ~CEntity();

        /** \brief Internal use only!
         *  \param m The manager that created this entity and that it belongs to.
         */
        void init(AppFrame::SharedPointer<CEntityManager> m);


        /** \brief Gets a IComponent from the Entity
         *
         * \param comp_id The id of the Component
         * \return The Component or NULL if this Entity doesn't have such a Componet
         *
         */
        IComponent* getComponent(const int& comp_id);

        /** \brief Gets a IComponent from the Entity
         *
         * \param comp_type The string type of the Component
         * \return The Component or NULL if this Entity doesn't have such a Componet
         *
         */
        IComponent* getComponent(const irr::core::stringc& comp_type);

        /** \brief Gets a IComponent from the Entity as COMP
         *  This function returns a component searched by the c++ type and directly casted
         *
         * \return The Component or NULL if this Entity doesn't have such a Componet
         *
         */
        template<class COMP>
        COMP* getComponent()
        {
            return (COMP*)getComponent(COMP::getComponentTypeID_const());
        }

        /** \brief Get the EntityManager
         * \return The CEntityManager of this Entity
         *
         */
        CEntityManager* getManager();

        /** \brief Remove this component
         *  Entity and all components are deleted. Can be called from any update function. Removal should be safe everywhere.
         */
        void remove();

        /** \brief Adds a component to the Entity
         *
         * \param comp The IComponent to add
         * \return The Component added
         *
         */
        IComponent* addComponent(IComponent* comp);

        /** \brief Adds a component to the Entity identified by the string type(Component will be created)
         *
         * \param type The string name of the IComponent
         * \return The Component added
         *
         */
        IComponent* addComponent(const irr::core::stringc& type);


        /** \brief Get the string Type of this entity
         *
         * \return The string type assigned to this entity while loading
         *
         */
        const irr::core::stringc& getType() const;


        /** \brief Get the string Identifier of this entity
         *
         * \return The string ident assigned to this entity
         *
         */
        const irr::core::stringc& getIdent() const;


        /** \brief Set the string Identifier of this entity
         *
         * \param i The string ident assigned to this entity
         *
         */
        void setIdent(const irr::core::stringc& i);
    protected:
        friend class CEntityManager;
        CEntityManager* manager;
        irr::core::stringc ident;
        irr::core::stringc type;
        void onAdd(ComponentPtr comp);
        void onRemove(ComponentPtr comp);
    private:
};

#endif // CENTITY_H
