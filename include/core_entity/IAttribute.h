#ifndef IATTRIBUTE
#define IATTRIBUTE

#include <string>
#include <cstdio>
#include <cassert>

class IAttributeBase
{
public:
    IAttributeBase()
    {

    }
    virtual ~IAttributeBase()
    {

    }

    virtual IAttributeBase* copy() = 0;

    virtual int getType() const = 0;

    virtual const char* getTypeString() const = 0;
};

template<typename T>
class IAttributeBase_ : public IAttributeBase
{
public:
    IAttributeBase_(T attr, const char* type = 0)
    {
        if (IAttributeBase_STRING.empty() && type != 0)
            IAttributeBase_STRING = type;
        else if (IAttributeBase_STRING.empty() && type == 0)
        {
            printf("No name has been given to this attribute type yet\n");
        }

        attribute = attr;
    }

    virtual ~IAttributeBase_()
    {

    }

    IAttributeBase* copy()
    {
        return new IAttributeBase_(attribute, IAttributeBase_STRING.c_str());
    }

    operator T&()
    {
        return attribute;
    }

    operator T&() const
    {
        return attribute;
    }

    int getType() const
    {
        return IAttributeBase_ID;
    }

    const char* getTypeString() const
    {
        return IAttributeBase_STRING.c_str();
    }

    static bool isType(IAttributeBase* base)
    {
        return base->getType() == IAttributeBase_ID;
    }
protected:
    T attribute;

    static int IAttributeBase_ID;
    static std::string IAttributeBase_STRING;
};
template<typename T>
int IAttributeBase_<T>::IAttributeBase_ID = (int)&IAttributeBase_<T>::IAttributeBase_ID;

template<typename T>
std::string IAttributeBase_<T>::IAttributeBase_STRING;



class IAttribute
{
public:
    IAttribute()
    {
        base = 0;
    }
    IAttribute(const IAttribute& attr)
    {
        base = 0;
        *this = attr;
    }
    template<typename T>
    IAttribute(T attr)
    {
        base = 0;
        *this = attr;
    }

    IAttribute& operator=(const IAttribute& attr)
    {
        printf("no template\n");
        if (base && attr.base && base->getType() != attr.base->getType())
            return *this;

        if (base)
            delete base;
        base = 0;
        if (attr.base)
            base = attr.base->copy();
        return *this;
    }
    template<typename T>
    IAttribute& operator=(T attr)
    {
        printf("template\n");
        set(attr);
        return *this;
    }

    template<typename T>
    bool set(T attr, const char* type = 0)
    {
        if (base && !IAttributeBase_<T>::isType(base))
            return false;

        if (base)
            delete base;
        base = new IAttributeBase_<T>(attr, type);
        return true;
    }

    template<typename T>
    operator T& ()
    {
        return getAs<T>();
    }

    template<typename T>
    operator const T& () const
    {
        return getAs<T>();
    }

    template<typename T>
    const T& getAs() const
    {
        static T t;
        if (!checkAttributeType<T>())
        {
            printf("Wrong attribute type\n");
            return t;
        }
        return (*(IAttributeBase_<T>*)base);
    }

    template<typename T>
    T& getAs()
    {
        static T t = T();
        if (!checkAttributeType<T>())
        {
            printf("Wrong attribute type\n");
            return t;
        }
        return (*(IAttributeBase_<T>*)base);
    }

    template<typename T>
    bool get(T& attr) const
    {
        if (!checkAttributeType(attr))
        {
            printf("Wrong attribute type\n");
            return false;
        }
        attr = *(IAttributeBase_<T>*)base;
        return true;
    }

    template<typename T>
    bool checkAttributeType(const T& attr) const
    {
        return IAttributeBase_<T>::isType(base);
    }

    template<typename T>
    bool checkAttributeType() const
    {
        return base && IAttributeBase_<T>::isType(base);
    }

    const char* getType() const
    {
        if (base)
            return base->getTypeString();
        return 0;
    }

    int getTypeID() const
    {
        if (base)
            return base->getType();
        return -1;
    }
protected:
    IAttributeBase* base;
};

#endif // IATTRIBUTE
