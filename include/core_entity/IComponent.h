#ifndef ICOMPONENT_H
#define ICOMPONENT_H

#include <core/MessageDispatcher.h>
#include <core/IRef.h>
#include <core/Tools.h>
#include <core/XMLread.h>

class CEntity;
class IComponent;
class IComponentSystem;
typedef AppFrame::SharedPointer<IComponent> ComponentPtr;

#define COMPONENT_ADD(entity, comp)\
    IComponent::onAdd(entity);

#define COMPONENT_REMOVE(entity, comp)\
    IComponent::onRemove(entity);

#define COMPONENT(comp)\
    entity->getComponent<comp>()

#define CACHE_COMPONENT(var, comp)\
    if (!var)\
        var = COMPONENT(comp);

class IComponent : public AppFrame::actions::Subscriber, public AppFrame::IRef
{
    public:
        IComponent(IComponentSystem* system);
        virtual ~IComponent();

        virtual void onUpdate(const float& delta, const unsigned int& timeMs);

        virtual void onAdd(CEntity* e);
        virtual void onRemove(CEntity* e);

        virtual void entityChanged(void){}

        virtual const int& getComponentTypeID() const = 0;

        virtual irr::core::stringc getComponentType() const;

        IComponent* clone();

        CEntity* getEntity(){return entity;}

        virtual void serialize(AppFrame::xml::XMLread::Node* xml){}
        virtual void deserialize(AppFrame::xml::XMLread::Node* xml){}
    protected:
        CEntity* entity;
        IComponentSystem* componentSystem;
    private:
};

template<class T, class Sys = IComponentSystem>
class IComponent_ : public IComponent
{
public:
    IComponent_(Sys* system) : IComponent((IComponentSystem*)system)
    {
        specialisedSystem = system;
    }

    virtual ~IComponent_(){}

    Sys* getComponentSystem()
    {
        return specialisedSystem;
    }

    virtual const int& getComponentTypeID() const
    {
        return getComponentTypeID_const();
    }

    static const int& getComponentTypeID_const()
    {
        return component_Type_id;
    }
protected:
    Sys* specialisedSystem;
    static const int component_Type_id;
};
template<class T, class S>
const int IComponent_<T,S>::component_Type_id = (int)&IComponent_<T,S>::component_Type_id;

#endif // ICOMPONENT_H
