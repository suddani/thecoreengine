#ifndef CENTITYMANAGER_H
#define CENTITYMANAGER_H

#include <irrlicht.h>
#include <core/CRunnableList.h>
#include <core/MessageDispatcher.h>
#include <core/CGenericFactory.h>
#include <core/IGenericCreator.h>

#include "IComponentSystem.h"
#include "CEntity.h"

class IComponent;
class CEntityManager;

typedef AppFrame::SharedPointer<CEntityManager> EntityManagerPtr; /**< Does automatic ref counting for the CEntityManager */


/** \brief The Entity managing system.
 *  Resposible for creating/spawning entities
 *  creating components from their systems and running systems.
 *
 */

class CEntityManager : public AppFrame::IRef, public AppFrame::actions::MessageDispatcher, public AppFrame::CGenericFactory<IComponent*, irr::core::stringc, irr::core::array<AppFrame::IGenericCreator<IComponent*, irr::core::stringc>* > >, AppFrame::CRunnableList<EntityPtr>
{
    public:
        CEntityManager(irr::ITimer* t=0);
        virtual ~CEntityManager();

        /** \brief Updates all Entities
         *
         *  Should be called on a regular basis(every frame?)
         *
         */
        void update();

        /** \brief spawns a Entity from a template type
         *
         * \param type The Entity template
         * \return The CEntity with all components attached and data set.
         *
         */
        EntityPtr spawnEntity(const irr::core::stringc& type);
        EntityPtr spawnEntity(const irr::core::stringc& type, AppFrame::xml::XMLread& xml);

        /** \brief Creates a new empty Entity
         *
         * \return A new CEntity
         *
         */
        EntityPtr createEntity();

        /** \brief Removes and deletes a Entity and all its IComponent
         *
         * \param e The CEntity to be removed
         *
         */
        void removeEntity(EntityPtr e);

        /** \brief Get Entity by ident type
         *
         * \param ident The string ident to search for
         *
         */
        EntityPtr getEntity(const irr::core::stringc& ident);

        /** \brief Get Entities by ident type
         *
         * \param ident The string ident to search for
         *
         */
        std::vector<EntityPtr> getEntities(const irr::core::stringc& ident);

        /** \brief Adds a new ComponentSystem to the manager
         *
         * \param system The new IComponentSystem
         *
         */

        void addComponentSystem(IComponentSystem* system);


        /** \brief Registers a new IComponent of type Component and creates a standard System to run in.
         *
         * \param type The name the component will go by.
         *
         */
        template<class Component>
        void addComponentSystem(const irr::core::stringc& type)
        {
            addComponentSystem(new CComponentSystem<Component, IComponentSystem>(type));
        }
    protected:
        void onAdd(EntityPtr handler);
        void onRemove(EntityPtr handler);

        AppFrame::CRunnableList<ComponentSystemPtr> componentSystems;

        irr::ITimer* timer;
        irr::u32 lasttime;
    private:
};

#endif // CENTITYMANAGER_H
