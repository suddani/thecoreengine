#ifndef CCOMPONENTSYSTEM_H
#define CCOMPONENTSYSTEM_H

#include <irrlicht.h>
#include <core/CRunnableList.h>
#include <core/CGenericFactory.h>
#include <core/Tools.h>
#include <core/ILog.h>
#include <core/MessageDispatcher.h>

class IComponent;
class CEntity;
class IComponentSystem;
class CEntityManager;

typedef AppFrame::SharedPointer<IComponentSystem> ComponentSystemPtr;

class IComponentSystem : public AppFrame::IGenericCreator<IComponent*, irr::core::stringc>, public AppFrame::actions::Subscriber
{
    public:
        IComponentSystem(const irr::core::stringc& type) : AppFrame::IGenericCreator<IComponent*, irr::core::stringc>(type)
        {}
        virtual ~IComponentSystem(){}

        void setEntityManager(CEntityManager* manager) { entityManager = manager; onInit();}

        CEntityManager* getEntityManager() { return entityManager; }

        virtual void onUpdate(const float& delta, const unsigned int& timeMs){}
    protected:
        friend class IComponent;
        virtual void addComponent(IComponent* comp) = 0;
        virtual void removeComponent(IComponent* comp) = 0;
        virtual void onInit() = 0;
        CEntityManager* entityManager;
};

template<class Component, class System>
class CComponentSystem : public IComponentSystem, public AppFrame::CRunnableList<AppFrame::SharedPointer<Component> >
{
    public:
        CComponentSystem(const irr::core::stringc& type) : IComponentSystem(type) {}
        virtual ~CComponentSystem(){}

        virtual void updateComponent(Component* cmp, const float& delta, const unsigned int& timeMs)
        {
            cmp->onUpdate(delta, timeMs);
        }

        virtual void onUpdate(const float& delta, const unsigned int& timeMs)
        {
            this->lockList();
            typename CComponentSystem::RunnableListIt it = this->Runnables.begin();
            while (it!= this->Runnables.end())
            {
                updateComponent(*it, delta, timeMs);
                it++;
            }
            this->unlockList();
        }

        virtual IComponent* create(void)
        {
            return new Component((System*)this);
        }
    protected:
        virtual void addComponent(IComponent* comp)
        {
            nlogl(1)<<"on add Component to its system"<<nlendl;
            this->addHandler((Component*)comp);
        }
        virtual void removeComponent(IComponent* comp)
        {
            nlogl(1)<<"on remove Component from its system"<<nlendl;
            this->removeHandler((Component*)comp);
        }


        virtual void onAdd(AppFrame::SharedPointer<Component> handler) = 0;
        virtual void onRemove(AppFrame::SharedPointer<Component> handler) = 0;
    private:
};

#endif // CCOMPONENTSYSTEM_H
