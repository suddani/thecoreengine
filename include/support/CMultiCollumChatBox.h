#ifndef CMULTICOLLUMCHATBOX_H
#define CMULTICOLLUMCHATBOX_H

#include <irrlicht.h>


class CMultiCollumChatBox : public irr::gui::IGUIElement
{
    protected:
    public:

        struct SMultiMessage
        {
            irr::core::array<irr::core::stringw> Message;
            irr::u32 StartTime;
            irr::video::SColor Color;
            irr::f32 Visibility;
        };

        /** Default constructor */
        CMultiCollumChatBox(irr::gui::IGUIEnvironment* environment, irr::gui::IGUIElement* parent, irr::s32 id, irr::core::rect<irr::s32> rectangle);
        /** Default destructor */
        virtual ~CMultiCollumChatBox();

        //IGUIElement Functions
        bool OnEvent(const irr::SEvent& event);
        void draw(void);
        void OnPostRender(irr::u32 timeMs);

        //draw order
        void setTop2Bottom(bool t2b);

        //collum management
        void setCollumNameVisible(bool visible);
        void setCollums(irr::u32 count, irr::c8* name[], irr::f32 spacing[], bool allignCenter[]);
        void setCollumBarHeight(irr::u32 height);
        const irr::u32& getCollumCount(void);

        //shows the message even when the element is not visible
        void setMessageVisibleTime(irr::s32 timeMs);
        void setMessageFadeTime(irr::f32 fadetime);

        //message stuff
        irr::u32 getItemCount(void);
        CMultiCollumChatBox::SMultiMessage& getItem(irr::u32 id);
        void setAutoScrollEnabled (bool scroll);

        //message must be at least of the size of CollumCount
        void addMessage(const irr::core::array<irr::core::stringc>& message, const irr::video::SColor& color = irr::video::SColor(255,255,0,0));
        //pass element in form of -> "collum1;collum2;collum3;" the ';' at the end is important
        void addMessage(const irr::c8* message, const irr::video::SColor& color = irr::video::SColor(255,255,0,0));

        //setFont
        void setFont(irr::gui::IGUIFont* font);
        void setHeaderFont(irr::gui::IGUIFont* font);
    protected:

        irr::s32 SelectedElement;

        irr::u32 LastTime;
        irr::f32 FadeTime;
        irr::s32 MessageTimeVisible;

        irr::gui::IGUIFont* Font;
        irr::gui::IGUIFont* HeaderFont;
        bool Top2Bottom;
        bool AutoScroll;
        irr::core::array<SMultiMessage> Messages;

        irr::u32 ScrollBarSize;
        irr::gui::IGUIScrollBar* ScrollBar;

        bool CollumBarVisible;
        irr::u32 CollumBarHeight;
        irr::u32 CollumCount;
        irr::core::array<irr::core::stringw> CollumNames;
        irr::core::array<irr::f32> CollumSpacings;
        irr::core::array<bool> CollumCenter;

        void calc_maxlines(void);
        irr::s32 YDim;
        irr::u32 MaxLines;

        //selecting
        irr::s32 getItem(irr::u32 x, irr::u32 y, irr::s32& collum);
    private:
};

#endif // CMULTICOLLUMCHATBOX_H
