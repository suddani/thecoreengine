#ifndef CPROGRESSBAR_H
#define CPROGRESSBAR_H

#include <irrlicht.h>


class CProgressBar : public irr::gui::IGUIElement
{
    public:
        /** Default constructor */
        CProgressBar(irr::gui::IGUIEnvironment* environment, irr::gui::IGUIElement* parent, irr::s32 id, irr::core::rect<irr::s32> rectangle);
        /** Default destructor */
        virtual ~CProgressBar();

        void draw(void);
        void OnPostRender(irr::u32 timeMs);

        void setFadeOutTime(const irr::f32& time);

        const irr::f32& getProgress(void);
        irr::s32 getProgressPercent(void);
        void setProgress(irr::f32 progress);
        void setProgress(irr::s32 progress);
        void setBorder(irr::s32 border);
        void setBorderColor(irr::video::SColor color);
        void setBarColor(irr::video::SColor color[4]);
        void setOverrideFont(irr::gui::IGUIFont* font);
        void setOverrideColor(irr::video::SColor color);

    protected:
        irr::f32 Progress;
        irr::s32 Border;
        irr::gui::IGUIFont* Font;
        irr::video::SColor OverrideColor;
        irr::video::SColor BorderColor;
        irr::video::SColor BarColor[4];
        irr::f32 FadeOutTime;
        irr::f32 CurrentFadeOutTime;
        irr::u32 LastTime;
    private:
};

#endif // CPROGRESSBAR_H
