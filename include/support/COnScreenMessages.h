#ifndef CONSCREENMESSAGES_H
#define CONSCREENMESSAGES_H

#include <irrlicht.h>


class COnScreenMessages : public irr::gui::IGUIElement
{
    protected:
        struct SMessage
        {
            irr::core::stringw Message;
            irr::u32 StartTime;
            irr::video::SColor Color;
            irr::f32 Visibility;
            irr::gui::IGUIFont* Font;
        };
    public:
        /** Default constructor */
        COnScreenMessages(irr::gui::IGUIEnvironment* environment, irr::gui::IGUIElement* parent, irr::s32 id, irr::core::rect<irr::s32> rectangle);
        /** Default destructor */
        virtual ~COnScreenMessages();

        void draw(void);
        void OnPostRender(irr::u32 timeMs);

        void setVisibleTime(const irr::u32& visTime);
        void setFadeTime(const irr::f32& fadeTime);
        void setFont(irr::gui::IGUIFont* font);

        void addMessage(const irr::c8* message, const irr::video::SColor& color = irr::video::SColor(255,255,0,0), irr::gui::IGUIFont* font = NULL);
    protected:
        irr::u32 VisTime;
        irr::f32 FadeTime;

        irr::u32 LastTime;
        irr::gui::IGUIFont* Font;
        bool Top2Bottom;
        irr::core::list<SMessage> Messages;

        void calc_maxlines(void);
        irr::s32 YDim;
        irr::u32 MaxLines;
    private:
};

#endif // CONSCREENMESSAGES_H
