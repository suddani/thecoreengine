#ifndef CAPPLICATION_H
#define CAPPLICATION_H

#include <irrlicht.h>

//#include <core/ILogger.h>
#include <core/ILog.h>
#include <core/IProcessManager.h>

class CApplication
{
public:
    CApplication(int argc, char* argv[]);

    virtual void Init(void);

    virtual void registerProcess(void) = 0;

    virtual ~CApplication(void);

    void PrintGameVersion(const AppFrame::c8* name);

    virtual void Run(void);

    AppFrame::IProcessManager* getManager(void)
    {
        return Manager;
    }
protected:
    AppFrame::IProcessManager* Manager;
    AppFrame::input::IInputManager* Input;
    AppFrame::graphic::IGraphicManager* Graphic;
    irr::IrrlichtDevice* Device;
    bool Owner;

    //exit
    void exitProcess(const AppFrame::SCommand& command);
};

#endif // CAPPLICATION_H
