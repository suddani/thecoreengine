#ifndef CONSOLEPARAMETER_H_INCLUDED
#define CONSOLEPARAMETER_H_INCLUDED

//#include "ILogger.h"
#include "ILog.h"
#include <irrlicht.h>

namespace AppFrame
{
    class SParameter
    {
    public:
        SParameter(void)
        {
        }

        SParameter(const SParameter& para)
        {
            Name = para.Name;
            Value = para.Value;
        }

        bool operator== (const SParameter& para) const
        {
            return (Name == para.Name && Value == para.Value) ? true : false;
        }

        bool operator!= (const SParameter& para) const
        {
            return (Name == para.Name && Value == para.Value) ? false : true;
        }

        SParameter& operator= (const SParameter& para)
        {
            Name = para.Name;
            Value = para.Value;
            return *this;
        }

        SParameter& operator= (const irr::c8* value)
        {
            set(value);
            return *this;
        }

        SParameter(const irr::c8* value)
        {
            set(value);
        }

        void set(const irr::core::stringc& value)
        {
            Name = "";
            Value = "";
            irr::s32 pos = value.findFirstChar("\"",1);
            if (pos != 0)
            {
                //there might be a name attached
                pos = value.findFirstChar(":",1);
                if (pos != -1)
                {
                    //there is a name attached
                    Name = value.subString(0, pos);
                }
                pos++;
            }
            if (value[pos] == '\"')
                pos++;
            Value = value.subString(pos, value.size()-pos);
            if (Value[Value.size()-1] == '\"')
                Value.erase(Value.size()-1);
        }

        irr::u32 toInt(void) const
        {
            if (Value.size() <= 0)
                return 0;
            irr::u32 i = 0;
            sscanf(Value.c_str(), "%i", &i);
            return i;
        }

        irr::f32 toFloat(void) const
        {
            if (Value.size() <= 0)
                return 0;
            irr::f32 i = 0;
            sscanf(Value.c_str(), "%f", &i);
            return i;
        }

        irr::core::vector2df toVec2(void) const
        {
            irr::core::vector2df i;
            if (Value.size() <= 0)
                return i;
            sscanf(Value.c_str(), "%f, %f", &i.X, &i.Y);
            return i;
        }

        irr::core::vector3df toVec3(void) const
        {
            irr::core::vector3df i;
            if (Value.size() <= 0)
                return i;
            sscanf(Value.c_str(), "%f, %f, %f", &i.X, &i.Y, &i.Z);
            return i;
        }

        irr::core::stringc Value;
        irr::core::stringc Name;
    };
    class SCommand
    {
    public:
        SCommand(void)
        {
        }

        SCommand(const irr::c8* value)
        {
            set(value);
        }

        SCommand(const SCommand& comm)
        {
            Command = comm.Command;
            Parameter = comm.Parameter;
        }

        bool operator== (const SCommand& comm) const
        {
            return (Command == comm.Command && Parameter == comm.Parameter) ? true : false;
        }

        bool operator!= (const SCommand& comm) const
        {
            return (Command == comm.Command && Parameter == comm.Parameter) ? false : true;
        }

        SCommand& operator= (const SCommand& comm)
        {
            Command = comm.Command;
            Parameter = comm.Parameter;
            return *this;
        }

        SCommand& operator= (const irr::c8* value)
        {
            set(value);
            return *this;
        }

        irr::core::stringc getValue(const irr::c8* name) const
        {
            for (irr::u32 i=0;i<Parameter.size();i++)
            {
                if (Parameter[i].Name == name)
                    return Parameter[i].Value;
            }
            return "";
        }

        const SParameter& getParameter(const irr::c8* name) const
        {
            for (irr::u32 i=0;i<Parameter.size();i++)
            {
                if (Parameter[i].Name == name)
                    return Parameter[i];
            }
            return ConstantParameter;
        }

        void removeAllParameter(void)
        {
            Parameter.clear();
        }

        void addParameter(const irr::c8* value)
        {
            if (value != NULL)
                Parameter.push_back(SParameter(value));
        }

        void set(const irr::core::stringc& value)
        {
            Command = "";
            Parameter.clear();

            //the string is empty
            if (value.size() <= 0)
                return;
            //this string is not a command
            if (value[0] == ' ')
                return;

            irr::s32 pos = value.findFirstChar(" ",1);

            //the command has no parameters what so ever
            if (pos < 0)
            {
                Command = value;
                return;
            }

            Command = value.subString(0, pos);
            pos++;

            bool string = false;
            irr::core::stringc data;
            while (pos < (irr::s32)value.size())
            {
                if (value[pos] == '\"')
                    string = !string;

                if (value[pos] == ' ' && !string && data.size() > 0)
                {
                    Parameter.push_back(SParameter(data.c_str()));
                    data = "";
                    pos++;
                    continue;
                }
                data.append(value[pos]);
                pos++;
            }

            //check if i missed the last one
            if (data.size() > 0)
                Parameter.push_back(SParameter(data.c_str()));
        }
        irr::core::stringc Command;
        irr::core::array<SParameter> Parameter;
        const SParameter ConstantParameter;

        void print2Log(void) const
        {
            irr::core::stringc line;
            line = "Command: ";
            line.append(Command);
            line.append("\n");
            for (irr::u32 i=0;i<Parameter.size();i++)
            {
                line.append("\t Name: ");
                line.append(Parameter[i].Name);
                line.append("\n");
                line.append("\t Value: ");
                line.append(Parameter[i].Value);
                line.append("\n");
            }
            //ILogger::log(line.c_str());
            nlog<<line<<nlflush;
        }
    };
}

#endif // CONSOLEPARAMETER_H_INCLUDED
