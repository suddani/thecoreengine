#ifndef ICONSOLE_H
#define ICONSOLE_H

#include "ConsoleParameter.h"
#include "SFunctor.h"
#include "SAttributeContainer.h"
#include "IInputManager.h"

namespace AppFrame
{
    namespace graphic
    {
        class IGraphicManager;
    }
    namespace input
    {
        class IConsole : public IPointer, public SAttributeContainer
        {
        public:
            /** Default constructor */
            IConsole() {}
            /** Default destructor */
            virtual ~IConsole() {}

            virtual void setIsConsoleApplication(bool is) = 0;

            virtual bool isConsoleApplication(void) = 0;
            virtual bool isActive(void) = 0;

            virtual SCommand getPreviousCommand(void) = 0;
            virtual SCommand getNextCommand(void) = 0;

            virtual void ExecuteCommand(const SCommand& command) = 0;
            virtual void RegisterConsoleCommand(const c8* command, actions::SFunctor1<const SCommand&>* callback, const irr::c8* info = 0, const bool& override = false) = 0;
            virtual void unRegisterConsoleCommand(const c8* command, void* classPointer = NULL) = 0;

            virtual void UpdateConsoleInput(void) = 0;

            template<typename T>
            bool BindVar(const c8* name, T* data, const c8& flags = ATTRIBUTE_NORMAL)
            {
                return Bind(name, data, flags);
            }

            template<typename T, typename A>
            bool BindVar(const c8* name, bool (T::*set)(const A&), bool (T::*get)(A&), bool (T::*setStr)(const string&), string (T::*getStr)(), const c8& flags = ATTRIBUTE_NORMAL, T* myClass = NULL)
            {
                return Bind(name, set, get, setStr, getStr, flags, myClass);
            }

            bool ReleaseVar(const c8* name)
            {
                return Release(name);
            }
        protected:
        private:
        };
        IConsole* createConsole(graphic::IGraphicManager* graphic, IInputManager* input);

        #define CONSOLE_CALLBACK_METHOD(MyClass, MyFunc) new AppFrame::actions::SFunctorReciever1<MyClass, const AppFrame::SCommand&>(this, &MyClass::MyFunc)
        #define CONSOLE_CALLBACK_FUNCTION(MyFunc) new AppFrame::actions::SFunctionReciever1<const AppFrame::SCommand&>(MyFunc)
    }
}
#endif // ICONSOLE_H
