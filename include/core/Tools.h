#ifndef APPFRAME_TOOLS_H_INCLUDED
#define APPFRAME_TOOLS_H_INCLUDED

/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#include "Types.h"
#include "ILog.h"
#include <cassert>
namespace AppFrame
{
    template <typename T>
    struct object_counter
    {
        object_counter()
        {
            objects_created++;
            objects_alive++;
        }

        virtual ~object_counter()
        {
            --objects_alive;
        }
        static int objects_created;
        static int objects_alive;
    };
    template <typename T> int object_counter<T>::objects_created( 0 );
    template <typename T> int object_counter<T>::objects_alive( 0 );

    template<class T>
    class smart_ptr
    {
    public:
        smart_ptr(void)
        {
            _t = NULL;
            counter = NULL;
        }
        smart_ptr(T* t)
        {
            counter = NULL;
            _t = t;
            if (_t != NULL)
            {
                counter = new u16;
                *counter = 1;
            }
        }
        smart_ptr(const smart_ptr<T>& other)
        {
            //printf("GRAB 0\n");
            if (other.counter && *other.counter > 0 && other._t != NULL)
            {
                _t = other._t;
                counter = other.counter;
                (*counter)++;
            }
        }
        ~smart_ptr(void)
        {
            reset();
        }
        bool alive(void)
        {
            if (counter != NULL && *counter > 0)
                return true;
            return false;
        }
        ///operatoren
        operator bool()
        {
            return alive();
        }
        T* operator->()
        {
            if (counter && *counter > 0)
                return _t;
            nlerror<<"Smart Pointer is zero"<<nlflush;
            return NULL;
        }
        T& operator*()
        {
            if (!(counter && *counter > 0))
                nlerror<<"Smart Pointer is zero and this function will crash"<<nlflush;
            return *_t;
        }
        /*
        T* operator!()
        {
            if (counter && *counter > 0)
                return _t;
            printf("pointer is zero\n");
            return NULL;
        }
        */
        smart_ptr<T>& operator=(const smart_ptr<T>& other)
        {
            //printf("GRAB 1\n");
            if (this != &other && other.counter && *other.counter > 0 && other._t != NULL)
            {
                reset();
                _t = other._t;
                counter = other.counter;
                (*counter)++;
            }
            return *this;
        }
        smart_ptr<T>& operator=(T* other)
        {
            if (_t != other)
            {
                reset();
                _t = other;
                counter = new u16;
                *counter = 1;
            }
            return *this;
        }
        bool operator==(smart_ptr<T> const& other)
        {
            if (_t == other._t)
                return true;
            return false;
        }
        bool operator!=(smart_ptr<T> const& other)
        {
            if (_t != other._t)
                return true;
            return false;
        }
        bool operator==(T const* other)
        {
            if (_t == other)
                return true;
            return false;
        }
        bool operator!=(T const* other)
        {
            if (_t != other)
                return true;
            return false;
        }
    protected:
        void reset(void)
        {
            //printf("DROP");
            if (counter)
            {
                (*counter)--;
                if (*counter == 0)
                {
                    delete counter;
                    delete _t;
                    //printf("-DELETE");
                }
            }
            counter = NULL;
            _t = NULL;
            //printf("\n");
        }
        T* _t;
        u16* counter;
    };

    class CSmartCounted
    {
    public:
        CSmartCounted(void): killed(NULL), count(NULL)
        {
            if (!count)
            {
                count = new u16;
                *count = 0;
            }

            if (!killed)
            {
                killed = new bool;
                *killed = false;
            }
        }
        virtual ~CSmartCounted(void){}
        virtual void release(void)
        {
            *killed = true;
        }
        smart_ptr<u16>& getCount(void)
        {
            if (!count)
            {
                count = new u16;
                *count = 0;
            }
            return count;
        }
        smart_ptr<bool>& getReleased(void)
        {
            if (!killed)
            {
                killed = new bool;
                *killed = false;
            }
            return killed;
        }
    protected:
        smart_ptr<bool> killed;
        smart_ptr<u16> count;
    };

    template<class T>
    class smart_ptrA
    {
    public:
        smart_ptrA(void)
        {
            ref = NULL;
        }
        smart_ptrA(T* t)
        {
            if (t == NULL || t == 0)
                return;
            ref = (CSmartCounted*)t;
            counter = ((CSmartCounted*)t)->getCount();
            killed = ((CSmartCounted*)t)->getReleased();
            if (ref != NULL)
            {
                grab();
            }
        }
        smart_ptrA(const smart_ptrA<T>& other)
        {
            //printf("GRAB 0\n");
            //other.check();
            //if (other)
            {
                ref = other.ref;
                counter = other.counter;
                killed = other.killed;
                grab();
            }
        }
        ~smart_ptrA(void)
        {
            drop();
        }
        bool alive(void)
        {
            check();
            if (ref && counter && *counter > 0)
                return true;
            return false;
        }
        void check(void)
        {
            if (!counter)
                return;
            //printf("CHECK-");
            if (ref==NULL)
            {
                //printf("\n");
                return;
            }
            if (killed && *killed == true)
            {
                (*counter)--;
                //if (*counter == 0)
                {
                    delete ref;
                    //printf("DELETE");
                    ref = NULL;
                }
            }
            //printf("\n");
        }
        ///operatoren
        operator bool()
        {
            return alive();
        }
        T* operator->()
        {
            check();
            if (ref)
                return (T*)ref;
            printf("pointer is zero\n");
            return NULL;
        }
        T& operator*()
        {
            check();
            if (ref)
                return *ref;
            printf("pointer is zero\n");
        }
        /*
        T* operator!()
        {
        	check();
            if (ref)
                return (T*)ref;
            printf("pointer is zero\n");
            return NULL;
        }
        */
        smart_ptrA<T>& operator=(const smart_ptrA<T>& other)
        {
            //printf("GRAB 1\n");
            //other.check();
            if (this != &other)
            {
                drop();
                ref = other.ref;
                counter = ref->getCount();
                killed = ref->killed;
                grab();
            }
            return *this;
        }
        smart_ptrA<T>& operator=(T* other)
        {
            if (ref != (CSmartCounted*)other && other != NULL && other != 0)
            {
                drop();
                ref = (CSmartCounted*)other;
                counter = ((CSmartCounted*)other)->getCount();
                killed = ((CSmartCounted*)other)->getReleased();
                grab();
            }
            else if (ref != (CSmartCounted*)other && other == NULL)
            {
                drop();
                ref = NULL;
                counter = NULL;
            }
            return *this;
        }
        bool operator==(smart_ptrA<T> const& other)
        {
            if (ref == other.ref)
                return true;
            return false;
        }
        bool operator!=(smart_ptrA<T> const& other)
        {
            if (ref != other.ref)
                return true;
            return false;
        }
        bool operator==(T const* other)
        {
            if (ref == other)
                return true;
            return false;
        }
        bool operator!=(T const* other)
        {
            if (ref != other)
                return true;
            return false;
        }
    protected:
        void drop(void)
        {
            //printf("DROP-");
            check();
            if (ref)
            {
                if (!counter)
                    return;
                (*counter)--;
                if (*counter == 0)
                {
                    *killed = true;
                    delete ref;
                    //printf("DELETE");
                    ref = NULL;
                }
            }
            //printf("\n");
        }
        void grab(void)
        {

            if (!counter)
                return;
            if (ref) (*counter)++;
        }
        CSmartCounted* ref;
        smart_ptr<u16> counter;
        smart_ptr<bool> killed;
    };

    typedef smart_ptr<s16>   s16Ptr;
    typedef smart_ptr<u16>   u16Ptr;
    typedef smart_ptr<s32>   s32Ptr;
    typedef smart_ptr<u32>   u32Ptr;
    typedef smart_ptr<s64>   s64Ptr;
    typedef smart_ptr<u64>   u64Ptr;
    typedef smart_ptr<f32>   f32Ptr;
    typedef smart_ptr<f64>   f64Ptr;
    typedef smart_ptr<c8>    c8Ptr;
    typedef smart_ptr<u8>    u8Ptr;
    typedef smart_ptr<bool>  boolPtr;

    template<class T>
    class SharedPointer
    {
    public:
        SharedPointer(void)
        {
            Pointer = NULL;
        }
        ~SharedPointer(void)
        {
            if (Pointer)
                Pointer->drop();
            Pointer = NULL;
        }
        SharedPointer(T* p)
        {
            Pointer = NULL;
            *this = p;
        }
        SharedPointer(const SharedPointer& other)
        {
            Pointer = NULL;
            *this = other;
        }
        T* operator->(void)
        {
            assert(Pointer);
            return Pointer;
        }
        SharedPointer& operator=(T* p)
        {
            if (Pointer == p)
                return *this;
            if (Pointer)
                Pointer->drop();
            Pointer = p;
            if (Pointer)
                Pointer->grab();
            return *this;
        }
        SharedPointer& operator=(const SharedPointer& other)
        {
            *this = other.Pointer;
            return *this;
        }
        operator T*()
        {
            return Pointer;
        }
    protected:
        T* Pointer;
    };
}
#endif // APPFRAME_TOOLS_H_INCLUDED
