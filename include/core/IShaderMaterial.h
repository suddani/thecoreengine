#ifndef ISHADERMATERIAL_H
#define ISHADERMATERIAL_H

#include "ShaderConstant.h"
namespace AppFrame
{
    namespace graphic
    {
        class IShaderMaterial : public irr::IReferenceCounted
        {
        public:
            virtual void load(const irr::core::stringc& filename) = 0;
            virtual void load(irr::io::IXMLReaderUTF8* xml) = 0;

            virtual void addShaderConstant(ShaderConstant* constant) = 0;
            virtual void addShaderConstant(const irr::core::stringc& name, const irr::core::stringc& type, const irr::core::stringc& value) = 0;
            virtual bool setShaderConstant(const irr::core::stringc& name, const ShaderConstant::E_SHADER_CONSTANT_TYPE& type, const irr::core::stringc& value) = 0;
            virtual bool setShaderConstant(const irr::core::stringc& name, const irr::u32& value) = 0;
            virtual bool setShaderConstant(const irr::core::stringc& name, const irr::f32& value) = 0;
            virtual bool setShaderConstant(const irr::core::stringc& name, const irr::core::vector3df& value) = 0;
            virtual bool setShaderConstant(const irr::core::stringc& name, const irr::core::vector2df& value) = 0;
            virtual bool setShaderConstant(const irr::core::stringc& name, const irr::core::matrix4& value) = 0;

            virtual void setPixelShader(const irr::core::stringc& name) = 0; //desides if it is a file or the shader itsef
            virtual void setPixelShader(irr::io::IReadFile* shader) = 0; //well
            virtual void setPixelShaderType(irr::video::E_PIXEL_SHADER_TYPE type) = 0;
            virtual void setPixelShaderType(const irr::core::stringc& type) = 0;

            virtual void setVertexShader(const irr::core::stringc& name) = 0; //desides if it is a file or the shader itsef
            virtual void setVertexShader(irr::io::IReadFile* shader) = 0; //well
            virtual void setVertexShaderType(irr::video::E_VERTEX_SHADER_TYPE type) = 0;
            virtual void setVertexShaderType(const irr::core::stringc& type) = 0;

            virtual void setBaseMaterial(irr::video::E_MATERIAL_TYPE baseMaterial) = 0;
            virtual void setBaseMaterial(const irr::core::stringc& baseMaterial) = 0;

            virtual bool CompileMaterial(void) = 0;

            virtual bool isCompiled(void) const = 0;

            virtual irr::video::E_MATERIAL_TYPE getMaterial(void) const = 0;

            virtual const irr::core::stringc& getName(void) const = 0;
        protected:
        private:
        };
    }
}

#endif // ISHADERMATERIAL_H
