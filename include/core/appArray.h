/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef APPARRAY_H
#define APPARRAY_H

#include <stdlib.h>
#include "Types.h"

namespace AppFrame
{
    template<typename T>
    class array
    {
    public:
        array(void)
        {
            initNULL();
            initSomeSpace();
        }

        array(const AppFrame::array<T>& a)
        {
            initNULL();
            *this = a;
        }

        array(T* data, u32 size, bool copyData = true)
        {
            initNULL();
            if (!copyData)
            {
                Data     = data;
                SizeUsed = size;
                Size     = size;
            }
            else
            {
                Size = size*2;
                SizeUsed = size;
                Data = new T[Size];
                for (u32 i=0;i<SizeUsed;i++)
                    Data[i] = data[i];
            }
        }

        virtual ~array(void)
        {
            clear();
        }

        bool empty(void) const
        {
            if (!Data || (SizeUsed == 0))
                return true;
            return false;
        }

        u32 size(void) const
        {
            return SizeUsed;
        }

        void clear(void)
        {
            if (Data)
            {
                /*for (u32 i=0;i<Size-1;i++)
                	(&Data[i])->~T();*/
                delete [] Data;
            }
            Data = NULL;
            SizeUsed = 0;
            Size = 0;
            initSomeSpace();
        }

        void erase(u32 index)
        {
            if (SizeUsed <= 0 || !Data)
                return;

            //TODO: make The array erase command more efficent without copying the hole array
            T* tmp = NULL;
            u32 tmpSize = SizeUsed;
            tmp = Data;
            SizeUsed = tmpSize-1;
            Data = new T[Size];
            for (u32 i=0;i<index;i++)
                Data[i] = tmp[i];
            for (u32 i=index;i<tmpSize;i++)
                Data[i] = tmp[i+1];
            delete [] tmp;
        }

        void push_back(const T& character)
        {
            if (SizeUsed == 0)
            {
            	if (Size == 0)
					initSomeSpace();
                Data[0] = character;
                SizeUsed = 1;
                return;
            }

            //check if size is big enough
            if (Size <= SizeUsed+1)
            {
                T* tmp = NULL;
                tmp = Data;
                Size = (SizeUsed+1)*2;
                Data = new T[Size];
                for (u32 i=0;i<SizeUsed;i++)
                    Data[i] = tmp[i];
                Data[SizeUsed] = character;
                delete [] tmp;

                SizeUsed++;
            }
            else
            {
            	Data[SizeUsed] = character;
            	SizeUsed++;
            }
        }

        T* pointer(void)
        {
            return Data;
        }

        const T* const_pointer(void) const
        {
            return Data;
        }

        T& getFirst(void)
        {
            if (SizeUsed <= 0 || !Data)
            {

            }
            else
                return Data[0];
        }

        const T& getFirst(void) const
        {
            if (SizeUsed <= 0 || !Data)
            {

            }
            else
                return Data[0];
        }

        T& getLast(void)
        {
            if (SizeUsed <= 0 || !Data)
            {

            }
            else
                return Data[SizeUsed-1];
        }

        const T& getLast(void) const
        {
            if (SizeUsed <= 0 || !Data)
            {

            }
            else
                return Data[SizeUsed-1];
        }

        //operatoren
        T& operator [](u32 i)
        {
            if (SizeUsed <= i || !Data)
            {

            }
            return Data[i];
        }

        const T& operator [](u32 i) const
        {
            if (SizeUsed <= i || !Data)
            {

            }
            else
                return Data[i];
        }

        bool operator<(const AppFrame::array<T>& s) const
        {
            if (size() < s.size())
                return true;
            return false;
        }

        bool operator>(const AppFrame::array<T>& s) const
        {
            if (size() > s.size())
                return true;
            return false;
        }

        bool operator<=(const AppFrame::array<T>& s) const
        {
            if (size() <= s.size())
                return true;
            return false;
        }

        bool operator>=(const AppFrame::array<T>& s) const
        {
            if (size() >= s.size())
                return true;
            return false;
        }

        AppFrame::array<T>& operator=(const AppFrame::array<T>& s)
        {
            if (Size > 0)
            {
                delete [] Data;
                Data = NULL;
                Size = 0;
                SizeUsed = 0;
            }
            Data = new T[s.Size];
            for (u32 i=0;i<s.SizeUsed;i++)
                Data[i] = s[i];
            SizeUsed = s.SizeUsed;
            Size = s.Size;
            return *this;
        }
    protected:
        void initNULL(void)
        {
            Data = NULL;
            Size = 0;
            SizeUsed = 0;
        }

        void initSomeSpace(void)
        {
            Data = new T[20];
            Size = 20;
            SizeUsed = 0;
        }
        T* Data;
        u32 Size;
        u32 SizeUsed;
    private:
    };
}
#endif // APPARRAY_H
