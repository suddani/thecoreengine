/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef IGRAPHICMANAGER_H
#define IGRAPHICMANAGER_H

#include <irrlicht.h>
namespace irr
{
	namespace gui
	{
		class IGUIChatQueue;
	}
	namespace scene
	{
	    class IObjectSceneNode;
	    class IEntityNode;
	    class IParticleSystem;
	}
}
namespace AppFrame
{
    namespace graphic
    {
    	class IPostProcess;
    	class IBeamNode;
    	class IBoltNode;
    	class IShaderMaterial;
        class IGraphicManager : public irr::IReferenceCounted
        {
        public:
            IGraphicManager() {}

            virtual ~IGraphicManager() {}

            virtual irr::IrrlichtDevice* getDevice(void) = 0;

            virtual void draw(bool post = true) = 0;

            virtual bool isPostProcessActive(const irr::c8* effectName) = 0;

            virtual bool isPostProcessActive(IPostProcess* post) = 0;

            virtual void deactivateAllPostProcessEffects(void) = 0;

            virtual void deactivatePostProcessEffect(const irr::c8* effectName) = 0;

            virtual void deactivatePostProcessEffect(IPostProcess* post) = 0;

            virtual void activatePostProcessEffect(const irr::c8* effectName) = 0;

            virtual void activatePostProcessEffect(IPostProcess* post) = 0;

            virtual bool addPostProcess(IPostProcess* post) = 0;

            virtual IPostProcess* getPostProcess(const irr::c8* filename) = 0;

            virtual IPostProcess* createPostProcess(const irr::c8* filename) = 0;

            virtual IPostProcess* createPostProcess(irr::u32 width, irr::u32 height, const irr::c8* effectName) = 0;

            virtual IBeamNode* createBeamNode(const irr::c8* side, const irr::c8* front, irr::scene::ISceneNode* parent = NULL, irr::scene::ISceneManager* smgr = NULL) = 0;

            virtual IBoltNode* createBoltNode(irr::scene::ISceneNode* parent = NULL, irr::scene::ISceneManager* smgr = NULL) = 0;

            virtual irr::scene::IEntityNode* createEntityNode(irr::scene::ISceneNode* parent = NULL, irr::scene::ISceneManager* smgr = NULL, irr::s32 id = -1) = 0;

            virtual irr::scene::IParticleSystem* createParticleSystem(irr::scene::ISceneNode* parent, irr::s32 id=-1, const irr::core::vector3df& position = irr::core::vector3df(0,0,0), const irr::core::vector3df& rotation = irr::core::vector3df(0,0,0), const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f, 1.0f)) = 0;

            virtual irr::gui::IGUIChatQueue* createChatBox(irr::core::rect<irr::s32> rect, irr::gui::IGUIElement* parent = NULL, irr::s32 id = -1, irr::gui::IGUIFont* font = NULL, irr::s32 life = 5000, irr::u32 FadeTime = 500, bool wordwrap = true) = 0;

            virtual irr::scene::IObjectSceneNode* addObjectSceneNode(irr::scene::ISceneNode* parent = NULL, const irr::s32& id = -1, const irr::core::vector3df& position = irr::core::vector3df(0,0,0), const irr::core::vector3df& rotation = irr::core::vector3df(0,0,0), const irr::core::vector3df& scale = irr::core::vector3df(1,1,1)) = 0;

            virtual irr::scene::ISceneNodeFactory* getSceneNodeFactory(void) = 0;

            virtual irr::video::E_MATERIAL_TYPE getMaterial(const irr::c8* mat) = 0;

            virtual IShaderMaterial* getShaderMaterial(const irr::c8* mat) = 0;

            virtual const irr::core::stringc getMaterialName(irr::video::E_MATERIAL_TYPE type) = 0;

        protected:
        private:
        };

        IGraphicManager* createGraphicManager(irr::IrrlichtDevice* device);
        irr::IrrlichtDevice* createIrrlichtDeviceFromFile(const irr::c8* file);
    }
}
#endif // IGRAPHICMANAGER_H
