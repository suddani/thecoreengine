#ifndef SHARED_PTR_H
#define SHARED_PTR_H

#include <list>
#include <stdio.h>
namespace AppFrame
{
template<class T>
struct CCounter
{
    CCounter()
    {
        count = 0;
        pointer = 0;
    }
    CCounter(T* p)
    {
        count = 1;
        pointer = p;
    }
    CCounter(const CCounter& c)
    {
        count = c.count;
        pointer = c.pointer;
    }
    int count;
    T* pointer;
};

template<class T>
class shared_ptr
{
public:
    shared_ptr()
    {
        pointer = 0;
        myIt = Counter.end();
    }
    shared_ptr(T* p)
    {
        pointer = 0;
        myIt = Counter.end();
        *this = p;
    }
    shared_ptr(const shared_ptr& p)
    {
        pointer = 0;
        myIt = Counter.end();
        *this = p;
    }
    virtual ~shared_ptr()
    {
        release();
    }

    shared_ptr& operator=(T* p)
    {
        setup(p);
        return *this;
    }

    shared_ptr& operator=(const shared_ptr& p)
    {
        setup(p.pointer);
        return *this;
    }

    operator T*() const
    {
        return pointer;
    }

    bool operator==(T* p) const
    {
        return pointer == p;
    }

    bool operator==(const shared_ptr& p) const
    {
        return pointer == p.pointer;
    }

    operator bool() const
    {
        return pointer;
    }

    T* operator->()
    {
        return pointer;
    }

protected:
    typedef typename std::list<CCounter<T>*>::iterator CounterListIt;
    T* pointer;
    CounterListIt myIt;

    void release()
    {
        if (!pointer)
            return;

        if (myIt != Counter.end())
        {
            (*myIt)->count--;
            if ((*myIt)->count == 0)
            {
                delete (*myIt);
                Counter.erase(myIt);
                delete pointer;
            }
            myIt = Counter.end();
        }
        else
        {
            printf("THIS SHOULDN'T BE CALLED AT ALL\n");
            CounterListIt it = Counter.begin();
            while (it != Counter.end())
            {
                if ((*it)->pointer == pointer)
                {
                    (*it)->count--;
                    if ((*it)->count == 0)
                    {
                        delete (*it);
                        Counter.erase(it);
                        //printf("Call delete\n");
                        delete pointer;
                    }
                    break;
                }
                it++;
            }
        }
        pointer = 0;
    }

    void setup(T* p)
    {
        if (pointer == p)
            return;
        release();

        CounterListIt it = Counter.begin();
        while (it != Counter.end())
        {
            if ((*it)->pointer == p)
            {
                //printf("Use existing counter\n");
                (*it)->count++;
                pointer = p;
                myIt = it;
                return;
            }
            it++;
        }
        //printf("Add new counter\n");
        Counter.push_front(new CCounter<T>(p));
        pointer = p;
        myIt = Counter.begin();
    }

    static std::list<CCounter<T>*> Counter;
private:
};
template<class T>
std::list<CCounter<T>*> shared_ptr<T>::Counter;
}
#endif // SHARED_PTR_H
