#ifndef SNETVAR_H
#define SNETVAR_H
#include "IPointer.h"
#include "IDataStream.h"
#include "NetVarUpdater.h"
#include "ILogger.h"
namespace AppFrame
{
namespace network
{
class BaseNetVar
{
public:
    BaseNetVar(void)
    {
        Type = EVC_SEND;
    }
    virtual ~BaseNetVar(void)
    {
    }
    const E_VAR_CHANGED& getMessageType(void) const
    {
        return Type;
    }
    void setMessageType(const E_VAR_CHANGED& t)
    {
        Type = t;
    }
    void callUpdater(void)
    {
        if (!Updater)
            return;
        Updater->receiveNetVarUpdate(this,  getMessageType());
    }
    void Init(NetVarUpdater* updater)
    {
        if (Updater)
            return;
        Updater = updater;
        updater->registerNetVar(this);
    }
    virtual AppFrame::u32 size(void) const
    {
        return 0;
    }
    bool changed(bool reset = false)
    {
        if (!reset)
            return Changed;
        bool s = Changed;
        Changed = false;
        return s;
    }

    virtual void serialize(IDataStream* stream) = 0;
    virtual void deserialize(IDataStream* stream) = 0;
protected:
    E_VAR_CHANGED Type;
    bool Changed;
    NetVarUpdater* Updater;
};
template<typename T>
class SNetVar : public BaseNetVar
{
public:
    SNetVar(void)
    {
        Var = new T;
        Changed = true;
        Updater = NULL;
    }
    SNetVar(T* var)
    {
        Var = var;
        Changed = true;
        Updater = NULL;
    }
    ~SNetVar(void)
    {
        delete Var;
    }
    virtual void serialize(IDataStream* stream)
    {
        stream->Write(*Var);
        Changed = false;
    }
    virtual void deserialize(IDataStream* stream)
    {
        stream->Read(*Var);
    }
    //operatoren
    template<typename A>
    void set(AppFrame::u32 index, const A& data)
    {
        (*Var)[index] = data;
        Changed = true;
        callUpdater();
    }
    template<typename A>
    const A& get(AppFrame::u32 index)
    {
        return (*Var)[index];
    }
    operator T() const
    {
        return *Var;
    }
    const T& operator*() const
    {
        return *Var;
    }
    T& operator*()
    {
        Changed = true;
        return *Var;
    }
    const T& operator->() const
    {
        return *Var;
    }
    T& operator->()
    {
        Changed = true;
        return *Var;
    }
    T& operator=(const T& var)
    {
        *Var = var;
        Changed = true;
        callUpdater();
        return *Var;
    }
    T& operator=(const SNetVar& var)
    {
        *Var = *(var.Var);
        Changed = true;
        callUpdater();
        return *Var;
    }
    T& operator+=(const T& var)
    {
        *Var += var;
        Changed = true;
        callUpdater();
        return *Var;
    }
    T& operator-=(const T& var)
    {
        *Var -= var;
        Changed = true;
        callUpdater();
        return *Var;
    }
    bool operator==(const SNetVar& var) const
    {
        return (*Var == *(var.Var)) && (Changed == var.Changed);
    }
    bool operator==(const T& var) const
    {
        return *Var == var;
    }
protected:
    T* Var;
};

template<>
inline void SNetVar<AppFrame::array<int> >::serialize(IDataStream* stream)
{
    stream->Write(Var->size());
    for (AppFrame::u32 i=0;i<Var->size();++i)
        stream->Write((*Var)[i]);
}
template<>
inline void SNetVar<AppFrame::array<int> >::deserialize(IDataStream* stream)
{
    Var->clear();
    AppFrame::u32 s = 0;
    int data = 0;
    stream->Read(s);
    for (AppFrame::u32 i=0;i<s;++i)
    {
        stream->Read(data);
        Var->push_back(data);
    }
}
}
}
#endif // SNETVAR_H
