#ifndef ILOG_H
#define ILOG_H

#include <irrlicht.h>
#include <stdio.h>
#include <time.h>
#include <string>

namespace AppFrame
{
    class ILog
    {
    public:
        class ILogAble
        {
        public:
            virtual ~ILogAble(void){}
            virtual void toLogString(ILog& logger) const = 0;
        };
        enum ILogParameter
        {
            NLP_FLUSH,
            NLP_ENDL,
            NLP_FLUSH_ENDL
        };

        enum E_LOG_TYPE
        {
            ELT_LOG = 0,
            ELT_ERROR,
            ELT_COUNT
        };

        class ILogReceiver// : public IPointer
        {
        public:
            virtual void OnLogEvent(const irr::core::stringc& event, E_LOG_TYPE type, irr::u32 loglevel) = 0;
        };

        ILog(void)
        {
            FlushTreshold = 50;
            LogLevel = 0;
            UsedLogLevel = 0;
            LogType = ELT_LOG;
            LogInfo = "LOG: ";
            ErrorInfo = "ERROR: ";
            Log_File = NULL;
            Log2Screen = true;
        }
        ~ILog(void)
        {
            flush();
            if (Log_File)
                fclose(Log_File);
            Log_File = NULL;
        }
        void check_flush(void)
        {
            //if (CurrentString.size() >= FlushTreshold)
            //flush();
        }
        void flush(void)
        {
            //flush log
            if (CurrentString.size() > 0)
            {
                irr::core::stringc lstring;
                irr::core::stringc event = CurrentString;
                CurrentString = "";
                if (LogType == ELT_LOG)
                    lstring.append(LogInfo);
                else
                    lstring.append(ErrorInfo);
                lstring.append(event);

                if (Log_File)
                {
                    fprintf(Log_File, "%s\n", lstring.c_str());
                    fflush(Log_File);
                }
                if (Log2Screen)
                {
                    printf("%s\n", lstring.c_str());
                    ///send to receiver
                    for (irr::u32 i=0;i<Receiver.size();++i)
                    {
                        Receiver[i]->OnLogEvent(event, LogType, LogLevel);
                    }
                }
            }
        }
        void addReceiver(ILogReceiver* receiver)
        {
            for (irr::u32 i=0;i<Receiver.size();i++)
                if (Receiver[i] == receiver)
                    return;
            Receiver.push_back(receiver);
        }
        void removeReceiver(ILogReceiver* receiver)
        {
            for (irr::u32 i=0;i<Receiver.size();i++)
                if (Receiver[i] == receiver)
                {
                    Receiver.erase(i);
                    return;
                }
        }

        ILog& operator<<(const ILogParameter& para)
        {
            switch (para)
            {
            case NLP_FLUSH:
                flush();
                return *this;
                break;
            case NLP_ENDL:
                add("\n");
                flush();
                return *this;
                break;
            case NLP_FLUSH_ENDL:
                flush();
                return *this;
                break;
            }
            return *this;
        }
        void add(const irr::core::stringc& s)
        {
            if (LogLevel > UsedLogLevel && LogType == ELT_LOG)
                return;
            CurrentString.append(s);
        }
        ILog& operator<<(const std::string& s)
        {
            add(s.c_str());
            return *this;
        }
        ILog& operator<<(const irr::core::stringc& s)
        {
            add(s);
            return *this;
        }
        ILog& operator<<(const irr::core::stringw& s)
        {
            add(irr::core::stringc(s.c_str()));
            return *this;
        }
        ILog& operator<<(const ILogAble* s)
        {
            s->toLogString(*this);
            return *this;
        }
        ILog& operator<<(const irr::c8* s)
        {
            add(s);
            return *this;
        }
        ILog& operator<<(const irr::u32& s)
        {
            add(irr::core::stringc(s));
            return *this;
        }
        ILog& operator<<(const irr::s32& s)
        {
            add(irr::core::stringc(s));
            return *this;
        }
        ILog& operator<<(const irr::c8 s)
        {
            add(irr::core::stringc(s));
            return *this;
        }
        ILog& operator<<(const irr::f32& s)
        {
            add(irr::core::stringc(s));
            return *this;
        }
        ILog& operator<<(const bool& s)
        {
            if (s)
                add("true");
            else
                add("false");
            return *this;
        }
        void setLogFile(const irr::c8* file, const irr::c8* appname = "")
        {
            flush();
            if (Log_File)
                fclose(Log_File);

            Log_File = fopen(file, "w");
            irr::core::stringc name = appname;
            time_t rawtime;
            tm * timeinfo;

            time ( &rawtime );
            timeinfo = localtime ( &rawtime );
            fprintf(Log_File, "%s Logfile: %s\n\n", name.c_str(), asctime (timeinfo));
        }

        ///variables
        bool Log2Screen;
        irr::u32 FlushTreshold;
        irr::u32 LogLevel;

        irr::core::stringc LogInfo;
        irr::core::stringc ErrorInfo;

        ///get singelton
        static ILog& Instance(irr::u32 loglevel = 0, E_LOG_TYPE type = ELT_LOG, bool justGetInstance = false)
        {
            static ILog* l = new ILog;
            if (!justGetInstance)
            {
                l->flush();
                l->UsedLogLevel = loglevel;
                l->LogType = type;
            }
            return *l;
        }
    protected:
        FILE* Log_File;
        irr::u32 UsedLogLevel;
        E_LOG_TYPE LogType;
        irr::core::stringc CurrentString;
        irr::core::array<ILogReceiver*> Receiver;
    };

#define nlget AppFrame::ILog::Instance(0, AppFrame::ILog::ELT_LOG, true)
#define nlogl(LEVEL) AppFrame::ILog::Instance(LEVEL, AppFrame::ILog::ELT_LOG)
#define nlog AppFrame::ILog::Instance()
#define nlsetFile AppFrame::ILog::Instance(0, AppFrame::ILog::ELT_LOG, true).setLogFile
#define nlogLevel AppFrame::ILog::Instance(0, AppFrame::ILog::ELT_LOG, true).LogLevel
#define nlog2Screen AppFrame::ILog::Instance(0, AppFrame::ILog::ELT_LOG, true).Log2Screen
#define nlflushTreshold AppFrame::ILog::Instance(0, AppFrame::ILog::ELT_LOG, true).FlushTreshold
#define nlinfo AppFrame::ILog::Instance(0, AppFrame::ILog::ELT_LOG, true).LogInfo
#define nlinfoError AppFrame::ILog::Instance(0, AppFrame::ILog::ELT_LOG, true).ErrorInfo
#define nlerror AppFrame::ILog::Instance(0, AppFrame::ILog::ELT_ERROR)
#define nlflush AppFrame::ILog::NLP_FLUSH
#define nlendl AppFrame::ILog::NLP_ENDL
#define nlfendl AppFrame::ILog::NLP_FLUSH_ENDL
}

#endif // ILOG_H
