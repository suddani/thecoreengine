#ifndef CTHREAD_H
#define CTHREAD_H

namespace AppFrame
{
namespace thread
{
class CThread;
#ifdef APPFRAME_USING_THREADS
#include <pthread.h>
class CThread
{
    public:
        CThread(unsigned int mutexcount);
        virtual ~CThread();

        void startThread(void);
        bool stopThread(void);
        bool isThreadStoppped(void);

        virtual bool thread_main(void) = 0;

        bool lockMutex(const unsigned int& id);
        bool trylockMutex(const unsigned int& id);
        bool unlockMutex(const unsigned int& id);
    protected:
        pthread_t ThreadId;
        const unsigned int MutexCount;
        pthread_mutex_t* Mutexes;
        static void* thread_Main(void* data);
        bool StopThread;
    private:
};
#endif
}
}
#endif // CTHREAD_H
