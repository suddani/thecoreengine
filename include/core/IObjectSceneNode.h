#ifndef IOBJECTSCENENODE_H
#define IOBJECTSCENENODE_H
#include <irrlicht.h>
namespace AppFrame
{
    namespace graphic
    {
        class IGraphicManager;
    }
}
namespace irr
{
    namespace scene
    {
        class IObjectSceneNode : public irr::scene::ISceneNode
        {
        public:
            /** Default constructor */
            IObjectSceneNode(scene::ISceneNode *parent, scene::ISceneManager *mgr, s32 id=-1, const core::vector3df &position=core::vector3df(0, 0, 0), const core::vector3df &rotation=core::vector3df(0, 0, 0), const core::vector3df &scale=core::vector3df(1.0f, 1.0f, 1.0f)) : ISceneNode (parent, mgr, id, position, rotation, scale)
            {
            }
            /** Default destructor */
            virtual ~IObjectSceneNode(void) {}

            virtual void render(void) = 0;

            virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox(void) const = 0;

            virtual irr::u32 getMaterialCount(void) const = 0;

            virtual irr::video::SMaterial& getMaterial(const irr::u32& i) = 0;

            virtual void addNext2Animation(const irr::c8* name, const irr::c8* next) = 0;
            virtual void removeNext2Animation(const irr::c8* name, const irr::c8*next) = 0;
            virtual void addAnimation(const irr::c8* name, const irr::u32& start, const irr::u32& end, const irr::f32& speed, const bool& loop) = 0;
            virtual void removeAnimation(const irr::c8* name) = 0;

            virtual void setGraphicManager(AppFrame::graphic::IGraphicManager* graphic) = 0;

            virtual void loadObject(const irr::c8* file) = 0;
            virtual void saveObject(const irr::c8* file) = 0;

            virtual void setAnimation(const u32& start, const u32& end, const f32& speed, const bool& loop) = 0;
            virtual f32 setAnimation(const irr::c8* name, bool now = false) = 0;
            virtual f32 setAnimation(const s32& i, bool now = false, bool same = false) = 0;
            virtual void setAnimationSpeed(const f32& speed) = 0;
            virtual s32 getAnimationCount(void) const = 0;
            virtual const irr::s32& getCurrentAnimation(void) = 0;
            virtual const irr::c8* getAnimationName(const u32& i) = 0;
            virtual void setAnimationFrame(const u32& frame) = 0;
            virtual u32 getCurrentAnimationFrame(void) = 0;
            virtual u32 getMaxAnimationFrame(void) = 0;

            virtual const scene::ISceneNode* getClamp(const irr::c8* name) const = 0;
            virtual const scene::ISceneNode* getClamp(const u32& i) const = 0;
            virtual scene::ISceneNode* getClamp(const irr::c8* name) = 0;
            virtual scene::ISceneNode* getClamp(const u32& i) = 0;
            virtual s32 getClampCount(void) const = 0;
        protected:
        private:
        };
    }
}
#endif // IOBJECTSCENENODE_H
