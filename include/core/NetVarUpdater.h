#ifndef NETVARUPDATER_H
#define NETVARUPDATER_H

#include "appArray.h"
#include "IDataStream.h"
namespace AppFrame
{
namespace network
{
enum E_VAR_CHANGED
{
    EVC_SEND = 0,
    EVC_SEND_ALL,
    EVC_SEND_MAYBE,
    EVC_NOTHING,
    EVC_COUNT
};
class BaseNetVar;
class NetVarUpdater
{
    public:
        NetVarUpdater();
        virtual ~NetVarUpdater();
        void registerNetVar(BaseNetVar* var);
        void receiveNetVarUpdate(BaseNetVar* var, const E_VAR_CHANGED& type);
        void sendUpdate(IDataStream* stream);
        void receiveUpdate(IDataStream* stream);
    protected:
        AppFrame::u32 NextUpdate;
        AppFrame::array<BaseNetVar*> Vars;
        E_VAR_CHANGED State;
    private:
};
}
}
#endif // NETVARUPDATER_H
