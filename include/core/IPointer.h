/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef IPOINTER_H
#define IPOINTER_H

#include "Types.h"
#include "appString.h"
#include "ILog.h"

namespace AppFrame
{
#define AUTO_SIZE virtual u32 Auto_Size(void){return sizeof(*this);}
    class IPointer
    {
    public:
        IPointer()
        {
            Count = 1;
        }
        virtual ~IPointer() {}

        bool drop(void)
        {
            Count--;
            if (Count == 0)
            {
                delete this;
                return true;
            }
            else if (Count < 0)
            {
                nlerror<<"IPointer dropped below"<<nlendl;
                exit(1);
                return true;
            }
            return false;
        }

        bool grab(void)
        {
            if (Count > 0)
            {
                Count++;
                return true;
            }
            nlerror<<"IPointer already deleted don't grab!?!?!"<<nlendl;
            exit(1);
            return false;
        }

        const string& getDebugName(void)
        {
            return DebugName;
        }

        static void dellocate(IPointer** data)
        {
            if (data && *data)
            {
                (*data)->drop();
            }
            *data = NULL;
        }

        AUTO_SIZE
    protected:
        s32 Count;
        string DebugName;
    private:
    };
}
#endif // IPOINTER_H
