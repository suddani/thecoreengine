#ifndef SVar_H
#define SVar_H

#include "SAttribute.h"
//#include "ILogger.h"
#include "ILog.h"
#include "SFunctor.h"

namespace AppFrame
{
    class SVar : public IPointer
    {
        class SetterGetterBasic : public IPointer
        {
        public:
            virtual bool Set(const void* Data) = 0;
            virtual bool Get(void* Data) const = 0;
            virtual bool SetString(const string& Data) = 0;
            virtual string GetString(void) const = 0;
            virtual const string& getTypeName(void) const = 0;
            virtual void setName(const c8* name) = 0;
            virtual const string& getName(void) const = 0;
            virtual void setFlags(const u8& flags) = 0;
            virtual const u8& getFlags(void) const = 0;
        };

        template<typename A>
        class SetterGetter : public SetterGetterBasic
        {
        public:
            SetterGetter(const c8* name, const string& typeName, const u8& flags, actions::SFunctorR1<bool, const A&>* set, actions::SFunctorR1<bool, A&>* get, actions::SFunctorR1<bool, const string&>* setStr, actions::SFunctorR<string>* getStr)
            {
                Name = name;
                TypeName = typeName;
                Flags = flags;
                Setter = set;
                Getter = get;
                SetStr = setStr;
                GetStr = getStr;
            }
            ~SetterGetter(void)
            {
                Setter->drop();
                Getter->drop();
                SetStr->drop();
                GetStr->drop();
            }

            virtual bool Set(const void* Data)
            {
                return Setter->call(*((A*)Data));
            }
            virtual bool Get(void* Data) const
            {
                return Getter->call(*((A*)Data));
            }
            virtual bool SetString(const string& Data)
            {
                return SetStr->call(Data.c_str());
            }
            virtual string GetString(void) const
            {
                return GetStr->call();
            }
            virtual const string& getTypeName(void) const
            {
                return TypeName;
            }
            virtual void setName(const c8* name)
            {
                Name = name;
            }
            virtual const string& getName(void) const
            {
                return Name;
            }
            virtual void setFlags(const u8& flags)
            {
                Flags = flags;
            }
            virtual const u8& getFlags(void) const
            {
                return Flags;
            }

        protected:
            actions::SFunctorR1<bool, const A&>* Setter;
            actions::SFunctorR1<bool, A&>* Getter;
            actions::SFunctorR1<bool,  const string&>* SetStr;
            actions::SFunctorR<string>* GetStr;
            string TypeName;
            string Name;
            u8 Flags;
        };
    public:
        /** Default constructor */
        SVar(void)
        {
            Base    = NULL;
            SetGet  = NULL;
        }
        SVar(const SVar& any)
        {
            //TODO: have to find a good way to solve this problem
            SetGet = any.SetGet;
            if (SetGet)
                SetGet->grab();

            Base = any.Base;
            if (Base)
                Base->grab();
        }
        template<typename T>
        SVar(T any)
        {
            SAttribute<T>* att = new SAttribute<T>("Any", ATTRIBUTE_NORMAL, NULL);
            att->createMem();
            att->setValue(any);
            Base = att;

            SetGet  = NULL;
        }
        /*
        template<typename T>
        SVar(T* any)
        {
            SAttribute<T>* att = new SAttribute<T>("Any", ATTRIBUTE_NORMAL, any);
            Base = att;

            SetGet  = NULL;
        }
        template<typename T>
        SVar(const c8* name, T* any, const u8& flags = ATTRIBUTE_NORMAL)
        {
            SAttribute<T>* att = new SAttribute<T>(name, flags, any);
            Base = att;

            SetGet  = NULL;
        }
        */
        template<typename T>
        SVar(const c8* name, T any, const u8& flags = ATTRIBUTE_NORMAL)
        {
            SAttribute<T>* att = new SAttribute<T>(name, flags, NULL);
            att->createMem();
            att->setValue(any);
            Base = att;

            SetGet  = NULL;
        }
        template<typename T, typename A>
        SVar(const c8* name, T* Class, bool (T::*set)(const A&), bool (T::*get)(A&), bool (T::*setStr)(const string&), string (T::*getStr)(), const u8& flags = ATTRIBUTE_NORMAL)
        {
            Base = NULL;

            actions::SFunctorR1<bool, const A&>* Set = new actions::SFunctorRecieverR1<bool, T, const A&>(Class, set);
            actions::SFunctorR1<bool, A&>* Get = new actions::SFunctorRecieverR1<bool, T, A&>(Class, get);
            actions::SFunctorR1<bool,  const string&>* SetStr = new actions::SFunctorRecieverR1<bool, T, const string&>(Class, setStr);
            actions::SFunctorR<string>* GetStr = new actions::SFunctorRecieverR<string, T>(Class, getStr);

            SetGet = new SetterGetter<A>(name, typeid(A).name(), flags, Set, Get, SetStr, GetStr);
        }
        /** Default destructor */
        virtual ~SVar()
        {
            if (Base)
                Base->drop();
            if (SetGet)
                SetGet->drop();
        }

        u8 getFlags(void)
        {
            if (Base)
                return Base->getFlags();
            else if (SetGet)
                return SetGet->getFlags();
            return 0;
        }

        void setFlags(const u8& flags)
        {
            if (Base)
                Base->setFlags(flags);
            else if (SetGet)
                return SetGet->setFlags(flags);
        }

        string getName(void) const
        {
            if (Base)
                return Base->getName();
            else if (SetGet)
                return SetGet->getName();
            return "$NoName";
        }

        string getTypeName(void) const
        {
            if (Base)
                return Base->getTypeName();
            else if (SetGet)
                return SetGet->getTypeName();
            return "NULL";
        }

        void setName(const c8* name)
        {
            if (Base)
                Base->setName(name);
            else if (SetGet)
                SetGet->setName(name);
        }

        string getValue(void) const
        {
            if (Base)
                return Base->getValue();
            else if (SetGet)
                return SetGet->GetString();
            return "$AnyIsNotInitializedYet";
        }

        bool setValue(const c8* value)
        {
            if (Base)
                return Base->setValue(value);
            else if (SetGet)
                return SetGet->SetString(value);
            return "$AnyIsNotInitializedYet";
        }
        const c8* c_str(void) const
        {
            if (SetGet)
                return SetGet->GetString().c_str();//TODO: This is probably bad

            if (!Base)
                return NULL;
            //BaseValue = Base->getValue();
            return Base->c_str();
        }

        template<typename T>
        SVar& operator=(T any)
        {
            if (SetGet)
            {
                if (SetGet->getTypeName() == typeid(any).name())
                    SetGet->Set(&any);
                return *this;
            }

            if (!Base)
            {
                SAttribute<T>* att = new SAttribute<T>("Any", ATTRIBUTE_NORMAL|ATTRIBUTE_TEMP, new T);
                Base = att;
            }

            if (Base->getTypeName() == typeid(any).name())
            {
                ((SAttribute<T>*)Base)->setValue(any);
            }
            else
                nlerror<<TypeMissmatch.c_str()<<nlflush;

            return *this;
        }
        template<typename T>
        operator T() const //TODO: The callbacks that the var changed won't be called when the returned pointer is changed
        {
            T any;

            if (SetGet)
            {
                if (SetGet->getTypeName() == typeid(any).name())
                    SetGet->Get((void*)&any);
                return any;
            }

            if (!Base)
                return any;

            if (Base->getTypeName() == typeid(any).name())
                ((SAttribute<T>*)Base)->getValue(any);
            else
                nlerror<<TypeMissmatch.c_str()<<nlflush;
            return any;
        }


        SVar& operator=(const SVar& any)
        {
            if (this == &any)
                return *this;

            //TODO: have to find a good way to solve this problem

            if (Base)
            {
                Base->drop();
            }
            Base = any.Base;
            if (Base)
                Base->grab();

            if (SetGet)
            {
                SetGet->drop();
            }
            SetGet = any.SetGet;
            if (SetGet)
                SetGet->grab();
            return *this;
        }

        template<typename T>
        bool operator==(const T& any)
        {
            if (SetGet)
            {
                if (SetGet->getTypeName() == typeid(any).name())
                {
                    T b;
                    SetGet->Get(b);
                    return (b == any) ? true : false;
                }
            }

            if (!Base)
                return false;
            if (Base->getTypeName() != typeid(any).name())
            {
                nlerror<<TypeMissmatch.c_str()<<nlflush;
                return false;
            }
            T a;
            ((SAttribute<T>*)Base)->getValue(a);
            return (a == any) ? true : false;
        }

        template<typename T>
        bool operator!=(const T& any)
        {
            if (SetGet)
            {
                if (SetGet->getTypeName() == typeid(any).name())
                {
                    T b;
                    SetGet->Get(b);
                    return (b == any) ? false : true;
                }
            }

            if (!Base)
                return true;
            if (Base->getTypeName() != typeid(any).name())
            {
                nlerror<<TypeMissmatch.c_str()<<nlflush;
                return true;
            }
            T a;
            ((SAttribute<T>*)Base)->getValue(a);
            return (a == any) ? false : true;
        }

        //standard operators
        template<typename T>
        T operator+(const T& any)
        {
            T out;

            if (SetGet)
            {
                if (SetGet->getTypeName() == typeid(any).name())
                {
                    T in;
                    SetGet->Get(in);
                    return (in + any);
                }
            }

            if (!Base)
                return out;

            if (Base->getTypeName() != typeid(any).name())
            {
                nlerror<<TypeMissmatch.c_str()<<nlflush;
                return out;
            }
            return (*((SAttribute<T>*)Base))+any;
        }
        template<typename T>
        T operator-(const T& any)
        {
            T out;

            if (SetGet)
            {
                if (SetGet->getTypeName() == typeid(any).name())
                {
                    T in;
                    SetGet->Get(in);
                    return (in - any);
                }
            }

            if (!Base)
                return out;

            if (Base->getTypeName() != typeid(any).name())
            {
                nlerror<<TypeMissmatch.c_str()<<nlflush;
                return out;
            }
            return (*((SAttribute<T>*)Base))-any;
        }
        template<typename T>
        T operator*(const T& any)
        {
            T out;

            if (SetGet)
            {
                if (SetGet->getTypeName() == typeid(any).name())
                {
                    T in;
                    SetGet->Get(in);
                    return (in * any);
                }
            }

            if (!Base)
                return out;

            if (Base->getTypeName() != typeid(any).name())
            {
                nlerror<<TypeMissmatch.c_str()<<nlflush;
                return out;
            }
            return (*((SAttribute<T>*)Base))*any;
        }
        template<typename T>
        T operator/(const T& any)
        {
            T out;

            if (SetGet)
            {
                if (SetGet->getTypeName() == typeid(any).name())
                {
                    T in;
                    SetGet->Get(in);
                    return (in / any);
                }
            }

            if (!Base)
                return out;

            if (Base->getTypeName() != typeid(any).name())
            {
                nlerror<<TypeMissmatch.c_str()<<nlflush;
                return out;
            }
            return (*((SAttribute<T>*)Base))/any;
        }

        template<typename T>
        void addCallback(SAttributeCallback<T>* callback)
        {
            if (Base && Base->getTypeName() == typeid(T).name())
                ((SAttribute<T>*)Base)->addCallback(callback);
        }
        template<typename T>
        void removeCallback(SAttributeCallback<T>* callback)
        {
            if (Base && Base->getTypeName() == typeid(T).name())
                ((SAttribute<T>*)Base)->removeCallback(callback);
        }

    protected:
        SAttributeBase* Base;

        SetterGetterBasic* SetGet;
        //string BaseValue;
    private:
    };
}
#endif // SVar_H
