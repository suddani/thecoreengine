/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef IINPUTMANAGER_H
#define IINPUTMANAGER_H

#include "Input.h"
#include "appString.h"
#include "IPointer.h"
namespace AppFrame
{
    namespace input
    {
    	class IKeyListener;
    	class IMouseListener;
    	class IJoystickListener;
    	class IGUIListener;

    	class KeyState
    	{
    		public:
    		KeyState(void)
    		{
    			pressed = EKS_UP;
    		}
    		E_KEY_STATE pressed;
    		string Name;
    	};

    	enum E_MOUSE_EVENT_TYPE
    	{
    		EMET_LEFT,
    		EMET_MIDDLE,
    		EMET_RIGHT,
    		EMET_WHEEL,
    		EMET_MOVE
    	};

    	class MouseState
    	{
    		public:
    		MouseState(void)
    		{
    			LeftPressed   = EKS_UP;
				MiddlePressed = EKS_UP;
				RightPressed  = EKS_UP;
				MouseWheel    = 0.0f;
    		}
    		E_KEY_STATE LeftPressed;
    		E_KEY_STATE MiddlePressed;
    		E_KEY_STATE RightPressed;
    		f32 MouseWheel;
    		f32 RelativePos[2];
    		u32 AbsolutePos[2];
    	};

    	enum E_JOYSTICK_EVENT_TYPE
    	{
    	};

    	class JoystickState
    	{
    	    public:
    	    JoystickState(void)
    	    {
    	    };
    	};
        class IInputManager : public IPointer
        {
        public:
            IInputManager() {}
            virtual ~IInputManager() {}

            virtual const E_KEY_STATE& getKeyState(irr::EKEY_CODE key) = 0;
            virtual const KeyState* getKey(irr::EKEY_CODE key) = 0;
            virtual irr::EKEY_CODE getKeyCode(const char* key) = 0;

            virtual void addListener(IKeyListener* listener) = 0;
            virtual void removeListener(IKeyListener* listener) = 0;
            virtual void restrictListener(IKeyListener* listener) = 0;

            virtual void addListener(IGUIListener* listener) = 0;
            virtual void removeListener(IGUIListener* listener) = 0;

            virtual void addListener(IMouseListener* listener) = 0;
            virtual void removeListener(IMouseListener* listener) = 0;
            virtual void restrictListener(IMouseListener* listener) = 0;

            virtual void addListener(IJoystickListener* listener) = 0;
            virtual void removeListener(IJoystickListener* listener) = 0;
            virtual void restrictListener(IJoystickListener* listener) = 0;
        protected:
        private:
        };

        IInputManager* createInputManager(irr::IrrlichtDevice* device);
    }
}
#endif // IINPUTMANAGER_H
