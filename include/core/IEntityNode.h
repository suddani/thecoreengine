#ifndef IENTITYNODE_H
#define IENTITYNODE_H

#include <irrlicht.h>

namespace irr
{
    namespace scene
    {
        class CAttribute
    	{
		public:
			enum ATTRIBUTE_TYPE
			{
				ATTRIBUTE_INT = 0,
				ATTRIBUTE_FLOAT = 1,
				ATTRIBUTE_BOOL = 2,
				ATTRIBUTE_STRING = 3,
				ATTRIBUTE_VECTOR3D = 4
			};
			CAttribute() : Type(ATTRIBUTE_INT), Name("")
			{
				Int = 0;
				Float = 0;
				Bool = false;
			}
			CAttribute(ATTRIBUTE_TYPE type, irr::core::stringc name) : Type(type), Name(name)
			{
				Int = 0;
				Float = 0;
				Bool = false;
			}
			ATTRIBUTE_TYPE getType(void)
			{
				return Type;
			}
			const irr::core::stringc& getName(void)
			{
				return Name;
			}
			irr::s32& getInt(void){return Int;}
			irr::f32& getFloat(void){return Float;}
			bool& getBool(void){return Bool;}
			irr::core::stringc& getString(void){return String;}
			irr::core::vector3df& getVector3D(void){return Vector3D;}
			void clear(void)
			{
				delete this;
			}
		protected:
			ATTRIBUTE_TYPE Type;
			irr::core::stringc Name;
			irr::s32 Int;
			irr::f32 Float;
			bool Bool;
			irr::core::stringc String;
			irr::core::vector3df Vector3D;
    	};
        class IEntityNode : public ISceneNode
        {
        public:
            /** Default constructor */
            IEntityNode(ISceneNode* parent, IrrlichtDevice* device, s32 id = -1, core::vector3df pos = core::vector3df(0,0,0), core::vector3df rot = core::vector3df(0,0,0), core::vector3df scale = core::vector3df(1,1,1)) : ISceneNode(parent, device->getSceneManager(), id, pos, rot, scale)
            {
            }
            /** Default destructor */
            virtual ~IEntityNode() {}

            virtual irr::core::array<CAttribute*>& getAttributes(void) = 0;

            virtual void setEntity(irr::core::stringc name) = 0;

			virtual const irr::core::stringc& getEntityType(void) = 0;
        protected:
        private:
        };
    }
}
#endif // IENTITYNODE_H
