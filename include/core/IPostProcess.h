/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef IPostProcess_H
#define IPostProcess_H
#include <irrlicht.h>
#include "ILog.h"
#include "ShaderConstant.h"
namespace AppFrame
{
    namespace graphic
    {

        //const irr::core::stringc ESCT_STRINGS[] = {"int", "float", "vec3", "vec2", "mat4", "count"};

        class IPostProcess : public irr::IReferenceCounted
        {
        public:
            class DepthNode
            {
            public:
                DepthNode(irr::scene::ISceneNode* node)
                {
                    Node = node;
                    for (irr::u32 i=0;i<Node->getMaterialCount();i++)
                        Materials.push_back(Node->getMaterial(i));
                }
                irr::scene::ISceneNode* getNode(void)
                {
                    return Node;
                }
                irr::u32 getMaterialType(irr::u32 i)
                {
                    return Materials[i].MaterialType;
                }
            protected:
                irr::scene::ISceneNode* Node;
                irr::core::array<irr::video::SMaterial> Materials;
            };
        class Pipline : public irr::video::IShaderConstantSetCallBack
            {
            public:
                Pipline(irr::video::ITexture* target, irr::video::ITexture* tex0 = NULL, irr::video::ITexture* tex1 = NULL, irr::video::ITexture* tex2 = NULL, irr::video::ITexture* tex3 = NULL) : Target(target)
                {
                    /*for (irr::u32 i=0;i<5;i++)
                        Constants[i] = constants[i];*/
                    Material.MaterialType = irr::video::EMT_SOLID;
                    Material.setTexture(0, tex0);
                    Material.setTexture(1, tex1);
                    Material.setTexture(2, tex2);
                    Material.setTexture(3, tex3);
                    Material.Lighting = false;
                }
                ~Pipline(void)
                {
                	for (irr::u32 i=0;i<Constants.size();i++)
						Constants[i]->drop();
                }
                void addConstant(ShaderConstant* constant)
                {
                	Constants.push_back(constant);
                }
                irr::video::ITexture* getTarget(void)
                {
                    return Target;
                }
                void setShader(irr::video::E_MATERIAL_TYPE shader)
                {
                    Material.MaterialType = shader;
                }
                virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32 userData)
                {

                    //set Textures
                    irr::u32 id0 = 0;
                    irr::u32 id1 = 1;
                    irr::u32 id2 = 2;
                    irr::u32 id3 = 3;

                    services->setVertexShaderConstant("tex0", reinterpret_cast<irr::f32*>(&id0),1);
                    services->setVertexShaderConstant("tex1", reinterpret_cast<irr::f32*>(&id1),1);
                    services->setVertexShaderConstant("tex2", reinterpret_cast<irr::f32*>(&id2),1);
                    services->setVertexShaderConstant("tex3", reinterpret_cast<irr::f32*>(&id3),1);

                    //set Constants
                    for (irr::u32 i=0;i<Constants.size();i++)
						Constants[i]->setConstant(services, Camera);
                }
                const irr::video::SMaterial& getMaterial(void)
                {
                    return Material;
                }
                void setCam(irr::scene::ICameraSceneNode* camera)
                {
                	Camera = camera;
                }
            protected:
				irr::core::array<ShaderConstant*> Constants;
                //irr::f32 Constants[5];
                irr::video::SMaterial Material;
                irr::video::ITexture* Target;
                irr::scene::ICameraSceneNode* Camera;
            };

            virtual ~IPostProcess(){}

            virtual void addRenderTarget(const irr::c8* name, irr::u32 width, irr::u32 height, bool render2target, bool copy2target, const irr::video::ECOLOR_FORMAT& cf = irr::video::ECF_UNKNOWN) = 0;

            virtual IPostProcess::Pipline* addRenderPipeline(irr::core::stringc vert, irr::core::stringc frag, irr::core::stringc target, irr::core::stringc tex0, irr::core::stringc tex1, irr::core::stringc tex2, irr::core::stringc tex3) = 0;

            virtual IPostProcess::Pipline* addRenderPipeline(irr::core::stringc vert, irr::core::stringc frag, irr::core::stringc target, irr::video::ITexture* tex0, irr::video::ITexture* tex1, irr::video::ITexture* tex2, irr::video::ITexture* tex3) = 0;

            virtual irr::video::ITexture* getRenderTarget(const irr::c8* name) = 0;

            virtual irr::video::ITexture* getRenderTarget(void) = 0;

            virtual irr::video::ITexture* getDepthPass(void) = 0;

            virtual void Render(irr::video::ITexture* renderTarget = 0, bool renderScene = true) = 0;

            virtual bool isActive(void) = 0;

            virtual void setActive(bool active) = 0;

            virtual void addAllNodes2DepthPass(void) = 0;

            virtual void addNode2DepthPass(irr::scene::ISceneNode* node) = 0;

            virtual void removeNodeFromDepthPass(irr::scene::ISceneNode* node) = 0;

            virtual void setDepthPass(irr::video::ITexture* target, bool allnodes, float maxfar) = 0;

            virtual void setMainCamera(irr::scene::ICameraSceneNode* camera) = 0;

            virtual const irr::core::stringc& getName(void) = 0;

        protected:
        private:
        };
    }
}
#endif // CPostProcess_H
