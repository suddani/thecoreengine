#ifndef INPUTENUMS_H
#define INPUTENUMS_H

namespace AppFrame
{
    namespace input
    {
        #define SPECIAL_KEY 256
        enum E_MOUSE_KEY
        {
            EMK_LEFT = 0,
            EMK_RIGHT = 1,
            EMK_MIDDLE = 2,
            EMK_COUNT = 3
        };

        enum E_KEY_STATE
        {
            EKS_UP = 0,
            EKS_DOWN = 1,
            EKS_COUNT = 2
        };

        /*
        enum E_KEYBOARD_KEY
        {
            EKK_A = 0x41,
            EKK_B = 0x42,
            EKK_C = 0x43,
            EKK_D = 0x44,
            EKK_E = 0x45,
            EKK_F = 0x46,
            EKK_G = 0x47,
            EKK_H = 0x48,
            EKK_I = 0x49,
            EKK_J = 0x4A,
            EKK_K = 0x4B,
            EKK_L = 0x4C,
            EKK_M = 0x4D,
            EKK_N = 0x4E,
            EKK_O = 0x4F,
            EKK_P = 0x50,
            EKK_Q = 0x51,
            EKK_R = 0x52,
            EKK_S = 0x53,
            EKK_T = 0x54,
            EKK_U = 0x55,
            EKK_V = 0x56,
            EKK_W = 0x57,
            EKK_X = 0x58,
            EKK_Y = 0x59,
            EKK_Z = 0x5A,
            EKK_0 = 0x30,
            EKK_1 = 0x31,
            EKK_2 = 0x32,
            EKK_3 = 0x33,
            EKK_4 = 0x34,
            EKK_5 = 0x35,
            EKK_6 = 0x36,
            EKK_7 = 0x37,
            EKK_8 = 0x38,
            EKK_9 = 0x39,
            EKK_OU = 0xD6,
            EKK_AU = 0xC4,
            EKK_RAUTE = 0x23,
            EKK_ADD = 0x2B,
            EKK_SUBTRACT = 0x2D,
            EKK_UE = 0xDC,
            EKK_SZ = 0xDF,
            EKK_SEMI = 0x60,
            EKK_KOMMA = 0x2C,
            EKK_PERIOD = 0x2E,
            EKK_SMALER = 0x3C,
            EKK_ASTERIX = 0x5E,
            //special keys
            EKK_SPACE = 32,
            EKK_ESC = SPECIAL_KEY+1,
            EKK_F1 = SPECIAL_KEY+2,
            EKK_F2 = SPECIAL_KEY+3,
            EKK_F3 = SPECIAL_KEY+4,
            EKK_F4 = SPECIAL_KEY+5,
            EKK_F5 = SPECIAL_KEY+6,
            EKK_F6 = SPECIAL_KEY+7,
            EKK_F7 = SPECIAL_KEY+8,
            EKK_F8 = SPECIAL_KEY+9,
            EKK_F9 = SPECIAL_KEY+10,
            EKK_F10 = SPECIAL_KEY+11,
            EKK_F11 = SPECIAL_KEY+12,
            EKK_F12 = SPECIAL_KEY+13,
            EKK_F13 = SPECIAL_KEY+14,
            EKK_F14 = SPECIAL_KEY+15,
            EKK_F15 = SPECIAL_KEY+16,
            EKK_F16 = SPECIAL_KEY+17,
            EKK_F17 = SPECIAL_KEY+18,
            EKK_F18 = SPECIAL_KEY+19,
            EKK_F19 = SPECIAL_KEY+20,
            EKK_F20 = SPECIAL_KEY+21,
            EKK_F21 = SPECIAL_KEY+22,
            EKK_F22 = SPECIAL_KEY+23,
            EKK_F23 = SPECIAL_KEY+24,
            EKK_F24 = SPECIAL_KEY+25,
            EKK_F25 = SPECIAL_KEY+26,
            EKK_UP = SPECIAL_KEY+27,
            EKK_DOWN = SPECIAL_KEY+28,
            EKK_LEFT = SPECIAL_KEY+29,
            EKK_RIGHT = SPECIAL_KEY+30,
            EKK_LSHIFT = SPECIAL_KEY+31,
            EKK_RSHIFT = SPECIAL_KEY+32,
            EKK_LCTRL = SPECIAL_KEY+33,
            EKK_RCTRL = SPECIAL_KEY+34,
            EKK_LALT = SPECIAL_KEY+35,
            EKK_RALT = SPECIAL_KEY+36,
            EKK_TAB = SPECIAL_KEY+37,
            EKK_ENTER = SPECIAL_KEY+38,
            EKK_BACKSPACE = SPECIAL_KEY+39,
            EKK_INSERT = SPECIAL_KEY+40,
            EKK_DEL = SPECIAL_KEY+41,
            EKK_PAGEUP = SPECIAL_KEY+42,
            EKK_PAGEDOWN = SPECIAL_KEY+43,
            EKK_HOME = SPECIAL_KEY+44,
            EKK_END = SPECIAL_KEY+45,
            EKK_KP_0 = SPECIAL_KEY+46,
            EKK_KP_1 = SPECIAL_KEY+47,
            EKK_KP_2 = SPECIAL_KEY+48,
            EKK_KP_3 = SPECIAL_KEY+49,
            EKK_KP_4 = SPECIAL_KEY+50,
            EKK_KP_5 = SPECIAL_KEY+51,
            EKK_KP_6 = SPECIAL_KEY+52,
            EKK_KP_7 = SPECIAL_KEY+53,
            EKK_KP_8 = SPECIAL_KEY+54,
            EKK_KP_9 = SPECIAL_KEY+55,
            EKK_KP_DIVIDE = SPECIAL_KEY+56,
            EKK_KP_MULTIPLY = SPECIAL_KEY+57,
            EKK_KP_SUBTRACT = SPECIAL_KEY+58,
            EKK_KP_ADD = SPECIAL_KEY+59,
            EKK_KP_DECIMAL = SPECIAL_KEY+60,
            EKK_KP_EQUAL = SPECIAL_KEY+61,
            EKK_KP_ENTER = SPECIAL_KEY+62,
            EKK_COUNT = 318
        };
        */
    }
}
#endif // INPUTENUMS_H
