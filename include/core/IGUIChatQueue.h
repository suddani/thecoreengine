/*
ChatQueue is copyright g0dsoft 2008
Version: 0.3.1
Code by: Mark Laprairie
Contact: webmaster@g0dsoft.com
Url: www.g0dsoft.com

IGUIChatQueue.h
ChatQueue is a object for simulating half-life/starcraft style chat display
capable of colors, special effects and any custom font.
*/

#ifndef __I_GUI_CHAT_QUEUE_H_INCLUDED__
#define __I_GUI_CHAT_QUEUE_H_INCLUDED__

#include "IGUIElement.h"

namespace irr
{
    namespace gui
    {

        enum chatQueueStyle
        {
            CQS_USEDEFAULT = 0,
            CQS_PLAIN,
            CQS_SHADOW,
            CQS_RAINBOW,
            CQS_MULTIRAINBOW,
            CQS_PULSE,
            CQS_BACKGROUND,
            CQS_CROPBACKGROUND,
            /*!! Warning:
            The following styles are for internal
            use only,  using them may cause your
            program to crash.
            !!*/ //This should some how be moved to CGUI.
            CQS_INTERNALUSEONLY_PULSE,
            CQS_INTERNALUSEONLY_FADE,
            CQS_INTERNALUSEONLY_FADE2
        };
        //! GUI Check box interface.
        class IGUIChatQueue : public IGUIElement
        {
        public:

            //! constructor
            IGUIChatQueue(IGUIEnvironment* environment, IGUIElement* parent, s32 id, const core::rect<s32> &rectangle)
                    : IGUIElement(EGUIET_ELEMENT, environment, parent, id, rectangle) {}

            //! destructor
            virtual ~IGUIChatQueue() {}

            //! Add a message to the Chat Queue
            /** \param text: Irrlicht stringw, wchar_t* or const wchar_t*
            refrencing the text you want displayed
            \param color1: The main color for your text
            \param color2: The special color, used for styles
            ex. if you apply CQS_SHADOW as the style this will
            be the color for the background shadow.
            \param chatQueueStyle: The style the text is to be renderd
            with defaults to a style set with setDefaultStyle or if
            not set,CQS_PLAIN*/
            virtual void addMessage(core::stringw text,chatQueueStyle useStyle =  CQS_USEDEFAULT,video::SColor color1 = video::SColor(255,255,255,255),video::SColor color2 = video::SColor(255,0,0,0)) = 0;

            virtual void addMessage(wchar_t* text,chatQueueStyle useStyle =  CQS_USEDEFAULT,video::SColor color1 =video::SColor(255,255,255,255),video::SColor color2 =video::SColor(255,0,0,0)) = 0;
            virtual void addMessage(const wchar_t* text,chatQueueStyle useStyle =  CQS_USEDEFAULT,video::SColor color1 =video::SColor(255,255,255,255),video::SColor color2 =video::SColor(255,0,0,0)) = 0;

            //! Sets how long text should fade for before removing
            virtual void setFadeTime(u32 fadeTime = 1000) = 0;

            //! Sets the font to be used by Chat Queue
            virtual void setFont(IGUIFont* font) = 0;

            //! Sets a max number of lines that can be visible at a given time
            virtual void setMaxLines(u16 maxLines) = 0;

            //! Makes Chat Queue visible, or invisible
            virtual void setVisible(bool setVisible = true) = 0;

            //! Sets how long a text entry should be fully visible
            virtual void setLife(u32 setLife = 1000) = 0;

            //! Enables/Disables word wrapping
            virtual void setWordWrap(bool setWordWrap = true) = 0;

            //! Modifies the draw area
            virtual void setDrawArea(const core::rect<s32> areaArea) = 0;

            //! Sets a debug mode that makes the draw area visible
            virtual void setDebug(bool setDebug = true) = 0;

            //! Sets a default style to be used when no other style is present
            virtual void setDefaultStyle(chatQueueStyle useStyle) = 0;

            //! Is the current ChatQueue object in debug mode?
            virtual bool isDebug() = 0;

            //! Returns the current fade time
            virtual u32 getFadeTime() = 0;

            //! Returns the current font
            virtual IGUIFont* getFont() = 0;

            //! Returns the current, default style
            virtual chatQueueStyle getDefaultStyle() = 0;

            //! Change drawing Direction
            virtual void drawFromTopToBottom(bool TTB) = 0;

        private:
        };
    } // end namespace gui
} // end namespace irr

#endif
