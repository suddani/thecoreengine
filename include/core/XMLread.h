#ifndef XMLREAD_H
#define XMLREAD_H

#include <irrlicht.h>
class xmlwriter;
namespace AppFrame
{
namespace xml
{
class XMLread
{
public:
    struct Val
    {
        Val(const irr::c8* name, const irr::c8* value);
        Val(const XMLread::Val& v);
        Val& operator=(const XMLread::Val& v);
        irr::core::stringc Name;
        irr::core::stringc Value;
    };
    struct Node
    {
        struct CDATA
        {
            CDATA(void)
            {
                Data = NULL;
                Size = 0;
            }
            ~CDATA(void)
            {
                clear();
            }
            void clear(void)
            {
                if (Data)
                    delete [] Data;
                Data = NULL;
                Size = 0;
            }
            void set(const irr::u8* data, irr::u32 size)
            {
                clear();
                Data = new irr::u8[size];
                for (irr::u32 i=0;i<size;++i)
                    Data[i] = data[i];
                Size = size;
            }
            irr::u8* Data;
            irr::u32 Size;
        };
        Node(const irr::c8* name, Node* parent = NULL);
        ~Node(void);

        //edit functions
        XMLread::Node* addNode(const irr::c8* name);
        void write(xmlwriter* writer);
        void AddComment(const irr::c8* comment);
        void AddAttribute(const irr::c8* name, const irr::core::stringc& value);
        void AddAttribute(const irr::c8* name, const irr::u32& value);
        void AddAttribute(const irr::c8* name, const irr::s32& value);
        void AddAttribute(const irr::c8* name, const irr::f32& value);
        //void AddAttribute(const irr::c8* name, const bool& value);

        //read functions
        XMLread::Node* getNode(const irr::c8* name);
        irr::core::array<XMLread::Node*> getNodes(const irr::c8* name);
        const irr::c8* getValue(const irr::c8* name);
        XMLread::Val* getValueStruct(const irr::c8* name);
        irr::s32 getValueAsInt(const irr::c8* name);
        irr::f32 getValueAsFloat(const irr::c8* name);
        bool getValueAsBool(const irr::c8* name);
        void clear(void);

        Node* Parent;
        irr::core::stringc Name;
        irr::core::stringc Text;
        irr::core::stringc CData;
        irr::core::stringc Unknown;
        irr::core::array<irr::core::stringc> Comments;
        irr::core::array<Val> Values;
        irr::core::array<Node*> Nodes;
    };
    XMLread(const irr::c8* file);
    ~XMLread(void);

    bool load(const irr::c8* file);

    void save(const irr::c8* file);

    XMLread::Node* addNode(const irr::c8* name, XMLread::Node* parent = NULL); ///If parent is null it resets the root element

    XMLread::Node* getNode(const irr::c8* name, XMLread::Node* parent=NULL);
    irr::core::array<XMLread::Node*> getNodes(const irr::c8* name, XMLread::Node* parent=NULL);

    XMLread::Node* findNode(const irr::c8* name, XMLread::Node* parent=NULL);
    irr::core::array<XMLread::Node*> findNodes(const irr::c8* name, XMLread::Node* parent=NULL);

    XMLread::Node* getRoot(void)
    {
        Current = &N;
        return Current;
    }

    bool isLoaded(void){return Loaded;}
protected:
    bool Loaded;
    Node N;
    Node* Current;
};
}
}
#endif // XMLREAD_H
