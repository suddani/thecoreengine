#ifndef SPROFILER_H
#define SPROFILER_H

#include <time.h>
//#include "ILogger.h"
#include "ILog.h"
namespace AppFrame
{
    class SProfiler
    {
        static AppFrame::u32 TotalTime;
        static irr::core::map<AppFrame::string, clock_t> Times;
        AppFrame::string Name;
        clock_t StartTime;
    public:
        static void reset(void)
        {
            TotalTime = 0;
            Times.clear();
        }
        static void Print(void)
        {
            if (TotalTime <= 0)
                return;
            nlogl(1)<<"Application profiler["<<TotalTime<<"]:"<<nlflush;
            irr::core::map<AppFrame::string, clock_t>::Iterator it = Times.getIterator();
            while (!it.atEnd())
            {
                nlogl(1)<<(*it).getKey().c_str()<<" : "<<(int)(*it).getValue()<<" : "<<(float)((*it).getValue())/TotalTime*100.f<<" percent"<<nlflush;
                it++;
            }
        }
        SProfiler(const AppFrame::c8* name)
        {
            Name = name;
            StartTime = clock();
        }
        ~SProfiler(void)
        {
            AppFrame::u32 Time = clock();
            Time -= StartTime;
            irr::core::map<AppFrame::string, clock_t>::Node* n;
            n = Times.find(Name);
            if (n)
            {
                n->setValue(n->getValue()+Time);
            }
            else
            {
                Times[Name] = Time;
            }
            TotalTime += Time;
        }
    };
}
#endif // SPROFILER_H
