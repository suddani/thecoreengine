#ifndef IREF_H
#define IREF_H

#include "Types.h"
#include "appString.h"
#include "ILog.h"

namespace AppFrame
{
    /// IRef is only reference counted after calling grab
    class IRef
    {
    public:
        IRef()
        {
            Count = 0;
        }
        virtual ~IRef() {}

        bool drop(void)
        {
            Count--;
            if (Count == 0)
            {
                delete this;
                return true;
            }
            else if (Count < 0)
            {
                nlerror<<"IRef dropped below"<<nlendl;
                exit(1);
                return true;
            }
            return false;
        }

        bool grab(void)
        {
            if (Count >= 0)
            {
                Count++;
                return true;
            }
            nlerror<<"IRef already deleted don't grab!?!?!"<<nlendl;
            exit(1);
            return false;
        }

        const string& getDebugName(void)
        {
            return DebugName;
        }
    protected:
        s32 Count;
        string DebugName;
    private:
    };
}
#endif // IREF_H
