#ifndef CRUNNABLELIST_H
#define CRUNNABLELIST_H

#include <list>
#include <stdio.h>
namespace AppFrame
{
    template<class T>
    class CRunnableList
    {
    public:
        typedef typename std::list<T>::iterator RunnableListIt;
        typedef typename std::list<T> RunnableList;
        CRunnableList()
        {
            isRunning = false;
        }
        virtual ~CRunnableList()
        {
            clearList();
        }

        void clearList()
        {
            Runnables2Add.clear();
            Runnables2Remove.clear();

            RunnableListIt it = Runnables.begin();
            while (it != Runnables.end())
            {
                T data = *it;
                it = Runnables.erase(it);
                onRemove(data);
            }
            Runnables.clear();
        }
        void addHandler(T handler)
        {
            if (isRunning)
            {
                //printf("Add Later\n");
                Runnables2Add.push_back(handler);
                return;
            }
            //printf("Add now\n");
            Runnables.push_back(handler);
            onAdd(handler);
        }
        void removeHandler(T handler)
        {
            if (isRunning)
            {
                //printf("remove later\n");
                Runnables2Remove.push_back(handler);
            }
            else
            {
                //printf("remove now\n");
                RunnableListIt it = Runnables.begin();
                while (it != Runnables.end())
                {
                    if ((*it) == handler)
                    {
                        Runnables.erase(it);
                        onRemove(handler);
                        return;
                    }
                    it++;
                }
            }
        }

        void lockList()
        {
            isRunning = true;
        }
        void unlockList()
        {
            isRunning = false;

            //add new
            RunnableListIt it = Runnables2Add.begin();
            while (it != Runnables2Add.end())
            {
                addHandler(*it);
                it++;
            }
            Runnables2Add.clear();

            //remove old
            it = Runnables2Remove.begin();
            while (it != Runnables2Remove.end())
            {
                removeHandler(*it);
                it++;
            }
            Runnables2Remove.clear();
        }

        RunnableList Runnables;
    protected:
        virtual void onAdd(T handler)
        {

        }
        virtual void onRemove(T handler)
        {

        }
        virtual void onUpdate(T handler)
        {

        }
        void run_handlers()
        {
            lockList();
            RunnableListIt it = Runnables.begin();
            while(it != Runnables.end())
            {
                onUpdate(*it);
                it++;
            }
            unlockList();
        }
        bool isRunning;
        RunnableList Runnables2Remove;
        RunnableList Runnables2Add;
    private:
    };
}
#endif // CRUNNABLELIST_H
