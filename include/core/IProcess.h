#ifndef IPROCESS_H
#define IPROCESS_H

#include <irrlicht.h>
#include "Types.h"
#include "IPointer.h"

namespace AppFrame
{
    class IProcessManager;
    class IProcess : public IPointer
    {
    public:
        /** Default constructor */
        IProcess(void) : ProcessID(IPROCESS_ID++)
        {
            AlwaysRun = true;
        }
        /** Default destructor */
        virtual ~IProcess() {}

        bool alwaysRun(void)
        {
            return AlwaysRun;
        }

        virtual void Update(void) {}

        virtual bool OnEvent(const irr::SEvent::SGUIEvent& event) { return false;}

        virtual void AttachProcess(IProcessManager* manager)
        {
            Manager = manager;
			onAttach();
        }

        virtual void DetachProcess(IProcessManager* manager)
        {
			onDetach();
            Manager = NULL;
        }

		virtual void onAttach(void) = 0;

		virtual void onDetach(void) = 0;

        virtual void makeMain(void) = 0;

        virtual void makeSlave(void) = 0;

        virtual const c8* getProcessName(void)
        {
            return "NULL";
        }

        virtual const u32& getProcessID(void){return ProcessID;}
    protected:
        IProcessManager* Manager;
        bool AlwaysRun;

    private:
        u32 ProcessID;
        static u32 IPROCESS_ID;
    };
}
#endif // IPROCESS_H
