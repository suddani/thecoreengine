#ifndef CIDGENERATOR_H
#define CIDGENERATOR_H

#include <irrlicht.h>
#include <core/SVar.h>
namespace AppFrame
{
template<class T>
class CIDGenerator
{
public:
    CIDGenerator(void)
    {
        StartID = 0;
    }
    virtual ~CIDGenerator(void)
    {
    }
    int add(T* object)
    {
        if (!object)
            return -1;
        if (Objects_Ids.find(object))
            return Objects_Ids.find(object)->getValue();
        int newId = -1;
        if (FreeIDs.getSize()>0)
        {
            irr::core::list<int>::Iterator it = FreeIDs.begin();
            newId = *it;
            FreeIDs.erase(it);
        }
        else
        {
            newId = StartID++;
        }
        Objects_Ids.insert(object, newId);
        Ids_Objects.insert(newId, object);
        return newId;
    }
    int add(T* object, int id)
    {
        if (!object)
            return -1;
        int newId = -1;
        if (StartID <= id) //easy
        {
            newId = id;
            Objects_Ids.insert(object, newId);
            Ids_Objects.insert(newId, object);
            while (StartID < id)
            {
                if (StartID != id)
                    FreeIDs.push_back(StartID++);
            }
            StartID++;
        }
        else //look for id
        {
            irr::core::list<int>::Iterator it = FreeIDs.begin();
            while (it != FreeIDs.end())
            {
                if (*it == id)
                {
                    newId = id;
                    Objects_Ids.insert(object, newId);
                    Ids_Objects.insert(newId, object);
                    FreeIDs.erase(it);
                    break;
                }
                ++it;
            }
        }
        if (newId == -1) //problem id taken here i reorder a previous entity
        {
            //remove previous object
            T* obj = Ids_Objects[id];
            Objects_Ids.remove(obj);
            Ids_Objects.remove(id);

            //assign for my new object
            newId = id;
            Objects_Ids.insert(object, newId);
            Ids_Objects.insert(newId, object);

            //insert again
            add(obj);
        }
        return newId;
    }
    void remove(T* object)
    {
        if (!object)
            return;
        if (!Objects_Ids.find(object))
            return;
        int i = Objects_Ids.find(object)->getValue();
        FreeIDs.push_back(i);
        Ids_Objects.remove(i);
        Objects_Ids.remove(object);
    }
    T* getObject(int id)
    {
        if (Ids_Objects.find(id))
            return Ids_Objects.find(id)->getValue();
        return NULL;
    }
    int getId(T* object)
    {
        if (!object)
            return -1;
        if (Objects_Ids.find(object))
            return Objects_Ids.find(object)->getValue();
        return -1;
    }
    static CIDGenerator& getGenerator(void)
    {
        static CIDGenerator Generator;
        return Generator;
    }
protected:
    int StartID;
    irr::core::list<int> FreeIDs;
    irr::core::map<int, T*> Ids_Objects;
    irr::core::map<T*, int> Objects_Ids;
private:
};

template<class T>
class CID
{
public:
    CID(void)
    {
        Id = -1;
        Object = NULL;
        id_generator = &CIDGenerator<T>::getGenerator();
    }
    CID(T* object, CIDGenerator<T>* generator = &CIDGenerator<T>::getGenerator())
    {
        id_generator = generator;
        Id = id_generator->getId(object);
        Object = object;
    }
    CID(const int& id, CIDGenerator<T>* generator = &CIDGenerator<T>::getGenerator())
    {
        id_generator = generator;
        Id = id;
        Object = id_generator->getObject(Id);
    }
    CID(const CID& id)
    {
        id_generator = id.id_generator;
        Id = id.Id;
        Object = id_generator->getObject(Id);
    }
    ~CID(void)
    {
    }
    bool isValid(void)
    {
        if (Object == NULL || Id < 0)
            return false;
        T* o = id_generator->getObject(Id);
        int i = id_generator->getId(Object);
        if (Object != o || Id != i)
            return false;
        return true;
    }
    T* operator->(void)
    {
        return Object;
    }
    operator int() const
    {
        return Id;
    }
    operator bool() const
    {
        return isValid();
    }
    operator T*() const
    {
        return Object;
    }
    CID& operator=(const CID& id)
    {
        Id = id.Id;
        id_generator = id.id_generator;
        Object = id_generator->getObject(Id);
    }
    bool operator==(const CID& id)
    {
        if (Id == id.Id && Object == id.Object)
            return true;
        return false;
    }
    int Id;
protected:
    T* Object;
    CIDGenerator<T>* id_generator;
};
}
#endif // CIDGENERATOR_H
