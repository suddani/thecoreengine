#ifndef IDATASTREAM_H
#define IDATASTREAM_H

#include "Types.h"
#include "IPointer.h"
#include "appString.h"
#include <irrlicht.h>
namespace AppFrame
{
class IDataStream : public AppFrame::IPointer
{
    public:
        /** Default constructor */
        IDataStream(void){}
        /** Default destructor */
        virtual ~IDataStream(){}

        virtual void reset(void) = 0; //resets reader and writer
        virtual void ResetReader(void) = 0;
        virtual void ResetWriter(void) = 0;

        virtual void encrypt(const AppFrame::string& key) = 0;
        virtual void dencrypt(const AppFrame::string& key) = 0;
        virtual bool isEncrypted(void) = 0;

        virtual AppFrame::c8* getData(void) = 0;
        virtual const AppFrame::c8* getData(void) const = 0;
        virtual AppFrame::u32 getSize(void) const = 0;

        virtual AppFrame::c8& operator[](const AppFrame::u32& index) = 0;
        virtual const AppFrame::c8& operator[](const AppFrame::u32& index) const = 0;

        virtual void set(AppFrame::c8* data, AppFrame::u32 size, bool copy = true) = 0;

        virtual void Write(const void* data, AppFrame::u32 size) = 0;
        virtual void Write(const bool& data) = 0;
        virtual void Write(const AppFrame::c8& data) = 0;
        virtual void Write(const AppFrame::u8& data) = 0;
        virtual void Write(const AppFrame::s32& data) = 0;
        virtual void Write(const AppFrame::u32& data) = 0;
        virtual void Write(const AppFrame::s16& data) = 0;
        virtual void Write(const AppFrame::u16& data) = 0;
        virtual void Write(const AppFrame::f32& data) = 0;
        virtual void Write(const AppFrame::string& data) = 0;
        virtual void Write(const irr::core::stringc& data) = 0;
        virtual void Write(const IDataStream* data) = 0;

        virtual IDataStream& operator<<(const bool& data) = 0;
        virtual IDataStream& operator<<(const AppFrame::c8& data) = 0;
        virtual IDataStream& operator<<(const AppFrame::u8& data) = 0;
        virtual IDataStream& operator<<(const AppFrame::s32& data) = 0;
        virtual IDataStream& operator<<(const AppFrame::u32& data) = 0;
        virtual IDataStream& operator<<(const AppFrame::s16& data) = 0;
        virtual IDataStream& operator<<(const AppFrame::u16& data) = 0;
        virtual IDataStream& operator<<(const AppFrame::f32& data) = 0;
        virtual IDataStream& operator<<(const AppFrame::string& data) = 0;
        virtual IDataStream& operator<<(const irr::core::stringc& data) = 0;
        virtual IDataStream& operator<<(const IDataStream* data) = 0;

        virtual bool Read(void* data, AppFrame::u32 size) = 0;
        virtual bool Read(bool& data) = 0;
        virtual bool Read(AppFrame::c8& data) = 0;
        virtual bool Read(AppFrame::u8& data) = 0;
        virtual bool Read(AppFrame::s32& data) = 0;
        virtual bool Read(AppFrame::u32& data) = 0;
        virtual bool Read(AppFrame::s16& data) = 0;
        virtual bool Read(AppFrame::u16& data) = 0;
        virtual bool Read(AppFrame::f32& data) = 0;
        virtual bool Read(AppFrame::string& data) = 0;
        virtual bool Read(irr::core::stringc& data) = 0;
        virtual bool Read(IDataStream* data) = 0;

        virtual IDataStream& operator>>(bool& data) = 0;
        virtual IDataStream& operator>>(AppFrame::c8& data) = 0;
        virtual IDataStream& operator>>(AppFrame::u8& data) = 0;
        virtual IDataStream& operator>>(AppFrame::s32& data) = 0;
        virtual IDataStream& operator>>(AppFrame::u32& data) = 0;
        virtual IDataStream& operator>>(AppFrame::s16& data) = 0;
        virtual IDataStream& operator>>(AppFrame::u16& data) = 0;
        virtual IDataStream& operator>>(AppFrame::f32& data) = 0;
        virtual IDataStream& operator>>(AppFrame::string& data) = 0;
        virtual IDataStream& operator>>(irr::core::stringc& data) = 0;
        virtual IDataStream& operator>>(IDataStream* data) = 0;
    protected:
    private:
};

IDataStream* createDataStream(AppFrame::c8* data, AppFrame::u32 size, bool copy = true);
IDataStream* createDataStream(AppFrame::u32 reserve);
IDataStream* createDataStream(void);
}
#endif // IDATASTREAM_H
