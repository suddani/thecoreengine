#ifndef IPROCESSMANAGER_H
#define IPROCESSMANAGER_H

#include "SFunctor.h"
#include "ConsoleParameter.h"

namespace AppFrame
{
    inline u32 ProcessID(c8 a, c8 b, c8 c, c8 d)
    {
        u32 id = 0;
        c8* id_pointer = (c8*)&id;
        id_pointer[0] = a;
        id_pointer[1] = b;
        id_pointer[2] = c;
        id_pointer[3] = d;
        return id;
    }

    inline u32 ProcessID(const c8* str)
    {
        u32 id = 0;
        c8* id_pointer = (c8*)&id;
        string My = str;
        for (u32 i=0;i<4;++i)
        {
            if (My.size() > i)
                id_pointer[i] = My[i];
        }
        return id;
    }

    inline string ProcessIDstr(const u32& id)
    {
        string data;
        c8* id_pointer = (c8*)&id;
        data.push_back(id_pointer[0]);
        data.push_back(id_pointer[1]);
        data.push_back(id_pointer[2]);
        data.push_back(id_pointer[3]);
        data.push_back('\0');
        return data;
    }


    class IProcess;

    class IProcessFactory : public IPointer
    {
    public:
        virtual ~IProcessFactory(void)
        {
        }
        virtual IProcess* create(const SCommand& command) = 0;
    };

    template<class P>
    class IProcessFactorySimple : public IProcessFactory
    {
    public:
        IProcess* create(const SCommand& command)
        {
            return new P(command);
        }
    };

    namespace input
    {
        class IConsole;
        class IInputManager;
    }
    namespace graphic
    {
        class IGraphicManager;
    }

    class IProcessManager : public IPointer
    {
    public:
        IProcessManager(void) {}

        virtual ~IProcessManager(void) {}

        virtual void setMainProcess(const u32& id) = 0;

        virtual bool Run(void) = 0;

        template<class P>
        void registerProcess(const u32& id)
        {
            registerProcess(id, new IProcessFactorySimple<P>());
        }

        virtual void registerProcess(const u32& id, IProcessFactory* factory) = 0;

        IProcess* createProcess(const c8* id, const SCommand& command = "")
        {
            return createProcess(ProcessID(id), command);
        }

        virtual IProcess* createProcess(const u32& id, const SCommand& command = "") = 0;

        virtual bool attachProcess(IProcess* process) = 0;

        virtual bool detachProcess(IProcess* process) = 0;

        virtual void killProcess(const u32& id) = 0;

        virtual void killActiveProcess(void) = 0;

        virtual void killAll(void) = 0;

        virtual input::IConsole* getConsole(void) = 0;

        virtual input::IInputManager* getInputManager(void) = 0;

        virtual graphic::IGraphicManager* getGraphicManager(void) = 0;
    protected:
    private:
    };
    IProcessManager* createProcessManager(graphic::IGraphicManager* graphic, input::IInputManager* input);

    #define PROCESS_CREATE_FUNC(func) new AppFrame::actions::SFunctionRecieverR1<AppFrame::IProcess*, const AppFrame::SCommand&>(&func)
}
#endif // IPROCESSMANAGER_H
