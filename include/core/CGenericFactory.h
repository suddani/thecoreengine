#ifndef CGENERICFACTORY_H
#define CGENERICFACTORY_H

#include <string>
#include <vector>
#include "IGenericCreator.h"
namespace AppFrame
{
    template<class FType, class Ident = std::string, class Container = std::vector<IGenericCreator<FType, Ident>* > >
    class CGenericFactory
    {
        public:
            typedef IGenericCreator<FType, Ident> Creator;

            CGenericFactory() {}
            virtual ~CGenericFactory()
            {
                for (unsigned int i=0; i<Creators.size(); ++i)
                {
                    Creators[i]->drop();
                }
            }

            template<class FTypeIn>
            int registerTypePointer(const Ident& name)
            {
                for (unsigned int i=0; i<Creators.size(); ++i)
                {
                    if (Creators[i]->getIdent() == name)
                    {
                        Creators[i]->drop();
                        Creators[i] = new CGenericCreatorPointer<FTypeIn, FType, Ident>(name);
                        Creators[i]->grab();
			return i;
                    }
                }
                Creator* c = new CGenericCreatorPointer<FTypeIn, FType, Ident>(name);
                c->grab();
                Creators.push_back(c);
		return Creators.size()-1;
            }

            int registerType(IGenericCreator<FType, Ident>* creator)
            {
                if (!creator)
                    return -1;

                creator->grab();

                for (unsigned int i=0; i<Creators.size(); ++i)
                {
                    if (Creators[i]->getIdent() == creator->getIdent())
                    {
                        Creators[i]->drop();
                        Creators[i] = creator;
			return i;
                    }
                }
                Creators.push_back(creator);
		return Creators.size()-1;
            }

            FType createById(const unsigned int& id) const
            {
                Creator* c = getCreatorById(id);
                if (c)
                    return c->create();
                return FType();
            }

            FType createById(const unsigned int& id)
            {
                Creator* c = getCreatorById(id);
                if (c)
                    return c->create();
                return FType();
            }

            FType create(const Ident& ident) const
            {
                Creator* c = getCreator(ident);
                if (c)
                    return c->create();
                return FType();
            }

            FType create(const Ident& ident)
            {
                Creator* c = getCreator(ident);
                if (c)
                    return c->create();
                return FType();
            }

            bool isCreateAble(const Ident& ident) const
            {
                Creator* c = getCreator(ident);
                if (c)
                    return true;
                return false;
            }

            bool isCreateAbleById(const unsigned int& id)
            {
                Creator* c = getCreatorById(id);
                if (c)
                    return true;
                return false;
            }

            unsigned int getCreateAbleCount(void) const
            {
                return Creators.size();
            }

            Creator* getCreatorById(const unsigned int& id) const
            {
                if (id < Creators.size())
                    return Creators[id];
                return 0;
            }

            Creator* getCreatorById(const unsigned int& id)
            {
                if (id < Creators.size())
                    return Creators[id];
                return 0;
            }

            Creator* getCreator(const Ident& ident) const
            {
                for (unsigned int i=0; i<Creators.size(); ++i)
                {
                    if (Creators[i]->checkIdent(ident))
                        return Creators[i];
                }
                return 0;
            }

            Creator* getCreator(const Ident& ident)
            {
                for (unsigned int i=0; i<Creators.size(); ++i)
                {
                    if (Creators[i]->checkIdent(ident))
                        return Creators[i];
                }
                return 0;
            }

            Ident getCreatorIdentById(const unsigned int& id) const
            {
                Creator* c = getCreatorById(id);
                if (c)
                    return c->getIdent();
                return Ident();
            }

            Ident getCreatorIdentById(const unsigned int& id)
            {
                Creator* c = getCreatorById(id);
                if (c)
                    return c->getIdent();
                return Ident();
            }

            int getCreatorIdByIdent(const Ident& ident) const
            {
                for (unsigned int i=0; i<Creators.size(); ++i)
                {
                    if (Creators[i]->checkIdent(ident))
                        return i;
                }
                return -1;
            }

            int getCreatorIdByIdent(const Ident& ident)
            {
                for (unsigned int i=0; i<Creators.size(); ++i)
                {
                    if (Creators[i]->checkIdent(ident))
                        return i;
                }
                return -1;
            }
        protected:
            Container Creators;
        private:
    };
}
#endif // CGENERICFACTORY_H
