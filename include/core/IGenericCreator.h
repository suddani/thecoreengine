#ifndef CGENERICCREATOR_H
#define CGENERICCREATOR_H

namespace AppFrame
{
    template<class FType, class Ident>
    class IGenericCreator
    {
        public:
            IGenericCreator(const Ident& ident)
            {
                Name = ident;
                Counter = 0;
            }
            virtual ~IGenericCreator()
            {
                Counter = -1;
            }

            virtual bool checkIdent(const Ident& ident)
            {
                return getIdent() == ident;
            }

            virtual bool checkIdent(const Ident& ident) const
            {
                return getIdent() == ident;
            }

            const Ident& getIdent(void) const
            {
                return Name;
            }

            virtual FType create(void) = 0;

            void grab(void)
            {
                Counter++;
            }

            bool drop(void)
            {
                Counter--;
                if (Counter == 0)
                {
                    delete this;
                    return true;
                }
                return false;
            }
        protected:
            Ident Name;
            int Counter;
        private:
    };

    template<class FTypeIn, class FType, class Ident>
    class CGenericCreatorPointer : public IGenericCreator<FType, Ident>
    {
        public:
            CGenericCreatorPointer(const Ident& ident) : IGenericCreator<FType, Ident>(ident)
            {
            }
            virtual FType create(void)
            {
                return new FTypeIn;
            }
    };
}
#endif // CGENERICCREATOR_H
