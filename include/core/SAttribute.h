#ifndef SATTRIBUTE_H
#define SATTRIBUTE_H

#ifdef USE_IRRLICHT
    #include <irrlicht.h>
#endif
#include "appString.h"
#include "appArray.h"
#include "IPointer.h"
#include <typeinfo>

namespace AppFrame
{
    class SAttributeContainer;
    class SVar;

    const u8 ATTRIBUTE_READ                = 1;
    const u8 ATTRIBUTE_READ_FROM_NETWORK   = 2;
    const u8 ATTRIBUTE_READ_FROM_STREAM    = 4;
    const u8 ATTRIBUTE_WRITE               = 8;
    const u8 ATTRIBUTE_WRITE_TO_NETWORK    = 16;
    const u8 ATTRIBUTE_WRITE_TO_STREAM     = 32;
    const u8 ATTRIBUTE_TEMP                = 64;
    const u8 ATTRIBUTE_CUSTOME             = 128;
    const u8 ATTRIBUTE_NORMAL              = ATTRIBUTE_READ|ATTRIBUTE_WRITE;

    const string NoValueDefined = "$NoValueDefinedORError";
    const string NoConverterDefined = "$NoStringConverterDefined";
    const string TypeMissmatch = "$TypeMismatchInsideCAttribute";

    template<typename T>
    class SAttributeCallback : public IPointer
    {
    public:
        virtual void attributeChanged(const c8* name, const T& value) = 0;

        virtual void attributeDestroyed(void) = 0;
    };

    class SAttributeBase : public IPointer
    {
    public:
        virtual ~SAttributeBase(void)
        {
        }

        void setName(const c8* name)
        {
            Name = name;
        }

        const string& getName(void) const
        {
            return Name;
        }

        void setFlags(const u8& flags)
        {
            Flags = flags;
        }

        const u8& getFlags(void) const
        {
            return Flags;
        }

        const c8* c_str(void)
        {
            DataValue = getValue();
            return DataValue.c_str();
        }

        virtual string getValue(void) const = 0;

        virtual bool setValue(const c8* value) = 0;

        virtual string getTypeName(void) const = 0;
    protected:
        SAttributeBase(const c8* name, u8 flags) : Name(name), Flags(flags)
        {
        }
        string Name;
        u8 Flags;
        string DataValue;
    };

    template<typename T>
    class SAttribute : public SAttributeBase
    {
    public:

        virtual ~SAttribute(void)
        {
            clearCallbacks();

            delMem();
        }

        virtual string getTypeName(void) const
        {
            return typeid(T).name();//TypeName;
        }

        bool createMem(void)
        {
            if (Data)
                return false;

            Data = new T;
            deleteMemForSure = true;
            return true;
        }

        void setMem(T* mem)
        {
            if (!mem)
                return;
            delMem();
            Data = mem;
        }

        bool setValue(const T& value)
        {
            if (!Data)
                return false;
            *Data = value;

            //update callbacks
            for (u32 i = 0;i < Callbacks.size();i++)
                Callbacks[i]->attributeChanged(Name.c_str(), *Data);

            return true;
        }

        bool getValue(T& value)
        {
            if (!Data)
                return false;
            value = *Data;
            return true;
        }

        virtual string getValue(void) const
        {
            return NoConverterDefined;
        }

        virtual bool setValue(const c8* value) //TODO: i won't receive variable updates from here...has to be set in underlining function
        {
            return false;
        }

        //operator overload
        T operator+(const T& att)
        {
            T out;
            if (!Data)
                return out;
            return (*Data)+(att);
        }
        T operator-(const T& att)
        {
            T out;
            if (!Data)
                return out;
            return (*Data)-(att);
        }
        T operator*(const T& att)
        {
            T out;
            if (!Data)
                return out;
            return (*Data)*(att);
        }
        T operator/(const T& att)
        {
            T out;
            if (!Data)
                return out;
            return (*Data)-(att);
        }
        void addCallback(SAttributeCallback<T>* callback)
        {
            for (u32 i=0;i<Callbacks.size();i++)
            {
                if (Callbacks[i] == callback)
                    return;
            }
            Callbacks.push_back(callback);
            callback->grab();
        }
        void removeCallback(SAttributeCallback<T>* callback)
        {
            for (u32 i=0;i<Callbacks.size();i++)
            {
                if (Callbacks[i] == callback)
                {
                    callback->drop();
                    Callbacks.erase(i);
                    return;
                }
            }
        }
        void clearCallbacks(void)
        {
            for (u32 i = 0;i < Callbacks.size();i++)
            {
                Callbacks[i]->attributeDestroyed();
                Callbacks[i]->drop();
            }
            Callbacks.clear();
        }
    protected:
        friend class SAttributeContainer;
        friend class SVar;
        SAttribute(const c8* name, u8 flags, T* data = NULL) : SAttributeBase(name, flags), Data(data), deleteMemForSure(false)
        {
        }
        SAttribute(const SAttribute& att) : SAttributeBase("NULL", 0), Data(NULL), deleteMemForSure(false)
        {
        }
        void delMem(void)
        {
            if (deleteMemForSure && Data)
            {
                delete Data;
                Data = NULL;
            }
            else if ((Flags & ATTRIBUTE_TEMP) == ATTRIBUTE_TEMP && Data)
            {
                delete Data;
                Data = NULL;
            }
            Data = NULL;
        }
        T* getData(void)
        {
            return Data;
        }
        T* Data;
        bool deleteMemForSure;
        array<SAttributeCallback<T>* > Callbacks;
    };

    //standard implementations for the string copy
    template<>
    inline string SAttribute<f32>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;
        return *Data;
    }
    template<>
    inline bool SAttribute<f32>::setValue(const c8* value)
    {
        if (!Data)
            return false;
        sscanf(value, "%f", Data);

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }

    #ifdef USE_IRRLICHT
    template<>
    inline string SAttribute<irr::core::vector3df>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;
        c8 Buffer[255];
        sprintf(Buffer, "%f, %f, %f", (*Data).X, (*Data).Y, (*Data).Z);
        return Buffer;
    }
    template<>
    inline bool SAttribute<irr::core::vector3df>::setValue(const c8* value)
    {
        if (!Data)
            return false;
        sscanf(value, "%f, %f, %f", &(*Data).X, &(*Data).Y, &(*Data).Z);

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }

    template<>
    inline string SAttribute<irr::core::vector2df>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;
        c8 Buffer[255];
        sprintf(Buffer, "%f, %f", (*Data).X, (*Data).Y);
        return Buffer;
    }
    template<>
    inline bool SAttribute<irr::core::vector2df>::setValue(const c8* value)
    {
        if (!Data)
            return false;
        sscanf(value, "%f, %f", &(*Data).X, &(*Data).Y);

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }
    #endif
    template<>
    inline string SAttribute<bool>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;

        if (*Data)
            return "true";
        else
            return "false";
    }
    template<>
    inline bool SAttribute<bool>::setValue(const c8* value)
    {
        if (!Data)
            return false;
        if (string("true") == value)
            *Data = true;
        else
            *Data = false;

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }

    template<>
    inline string SAttribute<string>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;
        return *Data;
    }
    template<>
    inline bool SAttribute<string>::setValue(const c8* value)
    {
        if (!Data)
            return false;
        *Data = value;

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }

    #ifdef USE_IRRLICHT
    template<>
    inline string SAttribute<irr::core::stringc>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;
        return (*Data).c_str();
    }
    template<>
    inline bool SAttribute<irr::core::stringc>::setValue(const c8* value)
    {
        if (!Data)
            return false;
        *Data = value;

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }
    #endif

    template<>
    inline string SAttribute<c8*>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;
        return *Data;
    }

    template<>
    inline bool SAttribute<c8*>::setValue(const c8* value)
    {
        if (!Data)
            return false;

        delete [] *Data;

        *Data = new c8[strlen(value)];
        strcpy(*Data, value);

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }

    template<>
    inline string SAttribute<c8>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;
        c8 buffer[2];
        sprintf(buffer, "%c", *Data);
        return buffer;
    }
    template<>
    inline bool SAttribute<c8>::setValue(const c8* value)
    {
        if (!Data)
            return false;
        sscanf(value, "%c", Data);

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }

    template<>
    inline string SAttribute<s32>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;
        return *Data;
    }
    template<>
    inline bool SAttribute<s32>::setValue(const c8* value)
    {
        if (!Data)
            return false;
        sscanf(value, "%i", Data);

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }

    template<>
    inline string SAttribute<u32>::getValue(void) const
    {
        if (!Data)
            return NoValueDefined;
        return *Data;
    }
    template<>
    inline bool SAttribute<u32>::setValue(const c8* value)
    {
        if (!Data)
            return false;
        sscanf(value, "%i", Data);

        //update callbacks
        for (u32 i = 0;i < Callbacks.size();i++)
            Callbacks[i]->attributeChanged(Name.c_str(), *Data);
        return true;
    }
}
#endif // SATTRIBUTECONTAINER_H
