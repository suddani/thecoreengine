#ifndef SINGELTONTEMPLATE_H_INCLUDED
#define SINGELTONTEMPLATE_H_INCLUDED

#include <stddef.h>

namespace AppFrame
{
template<class A>
class SingeltonTemplate
{
public:
    SingeltonTemplate(A* i = NULL)
    {
        if (RefCounter == 0)
        {
            Instance = i;
        }
        //if (Instance == NULL) //try calling standard constructor...not a good idea
        //Instance = new A();
        //assert(Instance != NULL);
        if (AutoRemove)
            RefCounter++;
    }
    SingeltonTemplate(const SingeltonTemplate<A>& singelton)
    {
        if (Instance == NULL) //try calling standard constructor
            Instance = new A();
        assert(Instance != NULL);
        if (AutoRemove)
            RefCounter++;
    }
    ~SingeltonTemplate(void)
    {
        if (AutoRemove && Instance)
            RefCounter--;
        if (RefCounter == 0)
        {
            delete Instance;
            Instance = NULL;
        }
        //assert(RefCounter >= 0);
    }
    SingeltonTemplate& operator=(A* i)
    {
        if (RefCounter == 0)
        {
            Instance = i;
        }
        else
        {
            RefCounter = 0;
            if (Instance)
                delete Instance;
            Instance = i;
        }
        if (AutoRemove)
            RefCounter++;

        return *this;
    }
    A* operator->(void)
    {
        return Instance;
    }
    A* operator*(void)
    {
        return Instance;
    }
    operator A*() const
    {
        return Instance;
    }
    const A* operator->(void) const
    {
        return Instance;
    }
    const A* operator*(void) const
    {
        return Instance;
    }
    static void TurnAutoRemoveOff(void)
    {
        AutoRemove = false;
    }
    void Destroy(void)
    {
        AutoRemove = true;

        if (Instance)
            delete Instance;

        Instance = NULL;

        RefCounter = 0;
    }
    static bool Valid(void)
    {
        if ((RefCounter > 0 || !AutoRemove) && Instance)
            return true;
        return false;
    }
    static int GetRefCount(void)
    {
        return RefCounter;
    }
    static A* get()
    {
        return Instance;
    }

    SingeltonTemplate& operator=(const SingeltonTemplate<A>& singelton)
    {
        //dunno but i think i don't have to grab the pointer here
    }
protected:
    static A* Instance;
    static int RefCounter;
    static bool AutoRemove;
};
template<class A> A* SingeltonTemplate<A>::Instance = NULL;
template<class A> int SingeltonTemplate<A>::RefCounter = 0;
template<class A> bool SingeltonTemplate<A>::AutoRemove = true;
}
#endif // SINGELTONTEMPLATE_H_INCLUDED
