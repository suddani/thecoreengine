#ifndef SHADERCONSTANT_H
#define SHADERCONSTANT_H

//#include "ILogger.h"
#include "ILog.h"
#include <irrlicht.h>
#include <time.h>
namespace AppFrame
{
    namespace graphic
    {
        const irr::core::stringc ESCT_STRINGS[] = {"int", "float", "vec3", "vec2", "mat4", "count"};
        class ShaderConstant : public irr::IReferenceCounted
        {
        public:
            enum E_SHADER_CONSTANT_TYPE
            {
                ESCT_INT = 0,
                ESCT_FLOAT = 1,
                ESCT_VEC3 = 2,
                ESCT_VEC2 = 3,
                ESCT_MAT4 = 4,
                ESCT_COUNT = 5
            };
            ShaderConstant(const ShaderConstant& constant) : irr::IReferenceCounted()
            {
                start(constant.Name.c_str(), constant.Pointer);
                switch (constant.Type)
                {
                case ESCT_INT:
                    if (!Pointer)
                    {
                        INT = new irr::u32;
                        *INT = *constant.INT;
                    }
                    else
                        INT = constant.INT;
                    break;
                case ESCT_FLOAT:
                    if (!Pointer)
                    {
                        FLOAT = new irr::f32;
                        *FLOAT = *constant.FLOAT;
                    }
                    else
                        FLOAT = constant.FLOAT;
                    break;
                case ESCT_VEC3:
                    if (!Pointer)
                    {
                        VEC3 = new irr::core::vector3df;
                        *VEC3 = *constant.VEC3;
                    }
                    else
                        VEC3 = constant.VEC3;
                    break;
                case ESCT_VEC2:
                    if (!Pointer)
                    {
                        VEC2 = new irr::core::vector2df;
                        *VEC2 = *constant.VEC2;
                    }
                    else
                        VEC2 = constant.VEC2;
                    break;
                case ESCT_MAT4:
                    if (!Pointer)
                    {
                        MAT4 = new irr::core::matrix4;
                        *MAT4 = *constant.MAT4;
                    }
                    else
                        MAT4 = constant.MAT4;
                    break;
                }
                Manager = constant.Manager;
            }
            ShaderConstant(const irr::c8* name, const irr::c8* type, const irr::c8* value, irr::scene::ISceneManager* manager)
            {
                start(name, false);

                nlog<<"add ShaderConstant: Name["<<name<<"] Type["<<type<<"] Value["<<value<<"]"<<nlflush;

                for (irr::u32 i=0;i<ESCT_COUNT;i++)
                {
                    if (ESCT_STRINGS[i] == type)
                    {
                        Type = (E_SHADER_CONSTANT_TYPE)i;
                        break;
                    }
                }

                switch (Type)
                {
                case ESCT_INT:
                {
                    INT = new irr::u32;
                    sscanf(value, "%i", INT);
                    break;
                }
                case ESCT_FLOAT:
                {
                    if ( irr::core::stringc("Time") != value || irr::core::stringc("deltaTime") != value)
                    {
                        FLOAT = new irr::f32;
                        sscanf(value, "%f", FLOAT);
                    }
                    else
                        Value = value;
                    break;
                }
                case ESCT_VEC3:
                {
                    if (irr::core::stringc("campos0") != value && irr::core::stringc("campos1") != value)
                    {
                        VEC3 = new irr::core::vector3df;
                        sscanf(value, "%f, %f, %f", &VEC3->X, &VEC3->Y, &VEC3->Z);
                    }
                    else
                    {
                        Value = value;
                    }
                    break;
                }
                case ESCT_VEC2:
                {
                    VEC2 = new irr::core::vector2df;
                    sscanf(value, "%f, %f", &VEC2->X, &VEC2->Y);
                    break;
                }
                case ESCT_MAT4:
                {
                    if (irr::core::stringc("worldView0") != value && irr::core::stringc("worldViewInverse0") != value && irr::core::stringc("worldView1") != value && irr::core::stringc("worldViewInverse1") != value && irr::core::stringc("worldMat") != value && irr::core::stringc("worldMatInverse") != value)
                    {
                        MAT4 = new irr::core::matrix4;
                        irr::f32 mat[16];
                        sscanf(value, "%f, %f, %f, %f; %f, %f, %f, %f; %f, %f, %f, %f; %f, %f, %f, %f", &mat[0], &mat[1], &mat[2], &mat[3], &mat[4], &mat[5], &mat[6], &mat[7], &mat[8], &mat[9], &mat[10], &mat[11], &mat[12], &mat[13], &mat[14], &mat[15]);
                        MAT4->setM(mat);
                    }
                    else
                        Value = value;
                    break;
                }
                };

                Manager = manager;
            }
            ShaderConstant(const irr::c8* name, irr::u32* value, bool pointer = false)
            {
                start(name, pointer);

                if (pointer)
                    INT = value;
                else
                {
                    INT = new irr::u32;
                    *INT = *value;
                }
            }
            ShaderConstant(const irr::c8* name, irr::f32* value, bool pointer = false)
            {
                start(name, pointer);

                if (pointer)
                    FLOAT = value;
                else
                {
                    FLOAT = new irr::f32;
                    *FLOAT = *value;
                }
            }
            ShaderConstant(const irr::c8* name, irr::core::vector3df* value, bool pointer = false)
            {
                start(name, pointer);

                if (pointer)
                    VEC3 = value;
                else
                {
                    VEC3 = new irr::core::vector3df;
                    *VEC3 = *value;
                }
            }
            ShaderConstant(const irr::c8* name, irr::core::vector2df* value, bool pointer = false)
            {
                start(name, pointer);

                if (pointer)
                    VEC2 = value;
                else
                {
                    VEC2 = new irr::core::vector2df;
                    *VEC2 = *value;
                }
            }
            ShaderConstant(const irr::c8* name, irr::core::matrix4* value, bool pointer = false)
            {
                start(name, pointer);

                if (pointer)
                    MAT4 = value;
                else
                {
                    MAT4 = new irr::core::matrix4;
                    *MAT4 = *value;
                }
            }
            ~ShaderConstant(void)
            {
                if (!Pointer)
                {
                    switch (Type)
                    {
                    case ESCT_INT:
                    {
                        if (INT)
                            delete INT;
                        break;
                    }
                    case ESCT_FLOAT:
                    {
                        if (FLOAT)
                            delete FLOAT;
                        break;
                    }
                    case ESCT_VEC3:
                    {
                        if (VEC3)
                            delete VEC3;
                        break;
                    }
                    case ESCT_VEC2:
                    {
                        if (VEC2)
                            delete VEC2;
                        break;
                    }
                    case ESCT_MAT4:
                    {
                        if (MAT4)
                            delete MAT4;
                        break;
                    }
                    };
                }
            }
            const E_SHADER_CONSTANT_TYPE& getType(void) const
            {
                return Type;
            }
            const irr::core::stringc getName(void) const
            {
                return Name;
            }
            void setConstant(irr::video::IMaterialRendererServices* services, irr::scene::ICameraSceneNode* own)
            {
                switch (Type)
                {
                case ESCT_INT:
                {
                    services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(INT),1);
                    break;
                }
                case ESCT_FLOAT:
                {
                    if (FLOAT)
                        services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(FLOAT),1);
                    else
                    {
                        if (Value == "Time")
                        {
                            irr::u32 Time = clock();
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(&Time),1);
                        }
                        else if (Value == "deltaTime")
                        {
                            irr::u32 Time = clock();
                            if (Last_Time == 0)
                                Last_Time = Time;
                            irr::f32 deltaTime = Time-Last_Time;
                            deltaTime /= 1000;
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(&deltaTime),1);
                        }
                    }
                    break;
                }
                case ESCT_VEC3:
                {
                    if (VEC3)
                        services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(&VEC3->X),3);
                    else
                    {
                        if (Value == "campos0")
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(&(Manager->getActiveCamera()->getAbsolutePosition().X)),3);
                        else if (Value == "campos1" && own)
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(&(own->getAbsolutePosition().X)),3);
                    }
                    break;
                }
                case ESCT_VEC2:
                {
                    if (VEC2)
                        services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(&VEC2->X),2);
                    break;
                }
                case ESCT_MAT4:
                {
                    if (MAT4)
                        services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(MAT4->pointer()),16);
                    else
                    {
                        if (Value == "worldView0")
                        {
                            irr::core::matrix4 worldViewProj = Manager->getVideoDriver()->getTransform(irr::video::ETS_PROJECTION);
                            worldViewProj *= Manager->getVideoDriver()->getTransform(irr::video::ETS_VIEW);
                            worldViewProj *= Manager->getVideoDriver()->getTransform(irr::video::ETS_WORLD);
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(worldViewProj.pointer()),16);
                        }
                        else if (Value == "worldViewInverse0")
                        {
                            irr::core::matrix4 worldViewProj = Manager->getVideoDriver()->getTransform(irr::video::ETS_PROJECTION);
                            worldViewProj *= Manager->getVideoDriver()->getTransform(irr::video::ETS_VIEW);
                            worldViewProj *= Manager->getVideoDriver()->getTransform(irr::video::ETS_WORLD);
                            worldViewProj.makeInverse();
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(worldViewProj.pointer()),16);
                        }
                        else if (Value == "worldView1" && own)
                        {
                            irr::core::matrix4 worldViewProj = own->getProjectionMatrix();
                            worldViewProj *= own->getViewMatrix();
                            worldViewProj *= Manager->getVideoDriver()->getTransform(irr::video::ETS_WORLD);
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(worldViewProj.pointer()),16);
                        }
                        else if (Value == "worldViewInverse1" && own)
                        {
                            irr::core::matrix4 worldViewProj = own->getProjectionMatrix();
                            worldViewProj *= own->getViewMatrix();
                            worldViewProj *= Manager->getVideoDriver()->getTransform(irr::video::ETS_WORLD);
                            worldViewProj.makeInverse();
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(worldViewProj.pointer()),16);
                        }
                        else if (Value == "worldMat")
                        {
                            irr::core::matrix4 world = Manager->getVideoDriver()->getTransform(irr::video::ETS_WORLD);
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(world.pointer()),16);
                        }
                        else if (Value == "worldMatInverse")
                        {
                            irr::core::matrix4 world = Manager->getVideoDriver()->getTransform(irr::video::ETS_WORLD);
                            world.makeInverse();
                            services->setVertexShaderConstant(Name.c_str(), reinterpret_cast<irr::f32*>(world.pointer()),16);
                        }
                    }
                    break;
                }
                };
            }
            bool getIsPointer(void) const
            {
                return Pointer;
            }
            bool operator==(const ShaderConstant& constant)
            {
                if (Pointer == constant.Pointer && Type == constant.Type && Name == constant.Name)
                {
                    return true;
                }
                else
                    return false;
            }
            ShaderConstant& operator=(const ShaderConstant& constant)
            {
                if (this->Pointer)
                    return *this;
                if (this->Type != constant.getType())
                    return *this;
                if (this->Name != constant.getName())
                    return *this;
                switch (this->Type)
                {
                case ESCT_INT:
                    *INT = *constant.INT;
                    break;
                case ESCT_FLOAT:
                    *FLOAT = *constant.FLOAT;
                    break;
                case ESCT_VEC3:
                    *VEC3 = *constant.VEC3;
                    break;
                case ESCT_VEC2:
                    *VEC2 = *constant.VEC2;
                    break;
                case ESCT_MAT4:
                    *MAT4 = *constant.MAT4;
                    break;
                }

                Manager = constant.Manager;
                return *this;
            }
        protected:
            void start(const irr::c8* name, bool pointer)
            {
                INT   = NULL;
                FLOAT = NULL;
                VEC3  = NULL;
                VEC2  = NULL;
                MAT4  = NULL;
                Type  = ESCT_COUNT;
                Name = name;
                Pointer = pointer;
                Manager = NULL;
                Last_Time = 0;
            }

            E_SHADER_CONSTANT_TYPE Type;
            irr::core::stringc Name;
            irr::u32* INT;
            irr::f32* FLOAT;
            irr::core::vector3df* VEC3;
            irr::core::vector2df* VEC2;
            irr::core::matrix4* MAT4;
            bool Pointer;
            irr::core::stringc Value;
            irr::scene::ISceneManager* Manager;

            irr::u32 Last_Time;
        };
    }
}
#endif // CSHADERCONSTANT_H
