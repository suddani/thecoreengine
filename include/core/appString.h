/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef APPSTRING_H
#define APPSTRING_H

#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "Types.h"

namespace AppFrame
{
    class string
    {
    public:
        string(void)
        {
            initNULL();
            reserve();
        }

        string(const c8* s)
        {
            initNULL();
            Size = strlen(s)+5;
            SizeUsed = strlen(s);
            S = new c8[Size];
            cpyIn(s);
        }

        string(const c8* s, const u32& length)
        {
            initNULL();
            Size = length+5;
            SizeUsed = length;
            S = new c8[Size];
            cpyIn(s);
        }

        string(const AppFrame::string& s)
        {
            initNULL();
            Size = s.size()+5;
            SizeUsed = s.SizeUsed;
            S = new c8[Size];
            cpyIn(s.S);
        }

        string(u32 s)
        {
            initNULL();
            Size = 32;
            S = new c8[Size];
            SizeUsed = sprintf(S, "%i", s);

            //SizeUsed = strlen(S);
        }

        string(s32 s)
        {
            initNULL();
            Size = 32;
            S = new c8[Size];
            SizeUsed = sprintf(S, "%i", s);

            //SizeUsed = strlen(S);
        }

        string(s64 s)
        {
            initNULL();
            Size = 32;
            S = new c8[Size];
            SizeUsed = sprintf(S, "%li", s);

            //SizeUsed = strlen(S);
        }

        string(f32 s)
        {
            initNULL();
            Size = 32;
            S = new c8[Size];
            SizeUsed = sprintf(S, "%f", s);

            //SizeUsed = strlen(S);
        }

        virtual ~string(void)
        {
            clear();
        }

        const c8* c_str(void) const
        {
            return S;
        }

        bool empty(void) const
        {
            if (!S || SizeUsed == 0)
                return true;
            return false;
        }

        u32 size(void) const
        {
            return SizeUsed;
        }

        void erase(u32 i)
        {
            if (SizeUsed <= i || !S)
                return;

            for (u32 a=i;a<SizeUsed;a++)
            {
                if (a+1<SizeUsed)
                {
                    S[a] = S[a+1];
                }
            }
            SizeUsed--;
            S[SizeUsed] = '\0';
        }

        u32 find(c8 character, u32 offset = 0) const
        {
            for (u32 i=offset;i<SizeUsed;i++)
                if (S[i] == character)
                    return i;
        }

        void clear(void)
        {
            SizeUsed = 0;
            Size = 0;
            if (S)
                delete [] S;
            S = NULL;
        }

        void append(const AppFrame::string& s)
        {
            append(s.c_str());
        }

        void append(const c8* s)
        {
            if (Size == 0 || !S)
            {
                Size = strlen(s)+10;
                SizeUsed = strlen(s);
                S = new c8[Size];
                cpyIn(s);
            }
            else
            {
                if (Size >= SizeUsed+strlen(s))
                {
                    u32 SizeUsedTMP = SizeUsed;
                    SizeUsed = strlen(s);
                    memcpy(&S[SizeUsedTMP], s, SizeUsed);
                    SizeUsed = SizeUsedTMP+strlen(s);
                    S[SizeUsed] = '\0';
                }
                else
                {
                    u32 SizeUsedTMP = SizeUsed;
                    SizeUsed = SizeUsed+strlen(s);
                    Size = SizeUsed+10;
                    c8* tmp = S;
                    S = new c8[Size];
                    memcpy(S, tmp, SizeUsedTMP);
                    strcpy(&S[SizeUsedTMP], s);
                    S[SizeUsed] = '\0';
                    delete [] tmp;
                }
            }
        }

        void push_back(c8 character)
        {
            if (Size == 0)
            {
                Size = 32;
                SizeUsed = 1;
                S = new c8[Size];
                S[0] = character;
                S[1] = '\0';
            }
            else
            {
                if (Size >= SizeUsed+2)
                {
                    S[SizeUsed] = character;
                    SizeUsed++;
                    S[SizeUsed] = '\0';
                }
                else
                {
                    u32 SizeUsedTMP = SizeUsed;
                    SizeUsed = SizeUsed+1;
                    Size = SizeUsed+10;
                    c8* tmp = S;
                    S = new c8[Size];
                    strcpy(S, tmp);
                    S[SizeUsedTMP] = character;
                    S[SizeUsed] = '\0';
                    delete [] tmp;
                }
            }
        }

        string subString(u32 pos, u32 len) const
        {
            if (pos+len-1 >= SizeUsed)
                return "";
            c8* b = new c8[len+1];
            memcpy(b, &S[pos], len);
            b[len] = '\0';
            string r = b;
            delete [] b;
            return r;
        }

        ///DEBUG
        void Print(bool newline = false) const
        {
            if (newline)
                printf("%s\n", S);
            else
                printf("%s", S);
        }

        //operatoren
        c8& operator [](u32 i)
        {
            assert(S && i < SizeUsed);
            return S[i];
        }

        const c8& operator [](u32 i) const
        {
            assert(S && i < SizeUsed);
            return S[i];
        }

        bool operator ==(const c8* s) const
        {
            if (strcmp(S, s) != 0)
                return false;
            return true;
        }

        bool operator ==(const AppFrame::string& s) const
        {
            if (strcmp(S, s.c_str()) != 0)
                return false;
            return true;
        }

        bool operator !=(const c8* s) const
        {
            if (strcmp(S, s) != 0)
                return true;
            return false;
        }

        bool operator !=(const AppFrame::string& s) const
        {
            if (strcmp(S, s.c_str()) != 0)
                return true;
            return false;
        }

        AppFrame::string& operator=(const AppFrame::string& s)
        {
            clear();
            Size = s.size();
            SizeUsed = s.SizeUsed;
            S = new c8[Size];
            cpyIn(s.c_str());
            return *this;
        }

        AppFrame::string& operator=(const c8* s)
        {
            clear();
            Size = strlen(s)+10;
            SizeUsed = strlen(s);
            S = new c8[Size];
            cpyIn(s);
            return *this;
        }

        AppFrame::string& operator=(const u32 s)
        {
            clear();
            Size = 32;
            S = new c8[Size];
            SizeUsed = sprintf(S, "%i", s);
            //SizeUsed = strlen(S);
            return *this;
        }

        AppFrame::string operator+(const AppFrame::string& s1) const
        {
            AppFrame::string s = *this;
            s.append(s1);
            return s;
        }

        AppFrame::string operator+(const c8* s1) const
        {
            AppFrame::string s = *this;
            s.append(s1);
            return s;
        }

        AppFrame::string& operator+=(const AppFrame::string& s)
        {
            append(s);
            return *this;
        }

        AppFrame::string& operator+=(const c8* s)
        {
            append(s);
            return *this;
        }

        bool operator<(const AppFrame::string& s) const
        {
            if (size() < s.size())
                return true;
            return false;
        }

        bool operator>(const AppFrame::string& s) const
        {
            if (size() > s.size())
                return true;
            return false;
        }

        bool operator<=(const AppFrame::string& s) const
        {
            if (size() <= s.size())
                return true;
            return false;
        }

        bool operator>=(const AppFrame::string& s) const
        {
            if (size() >= s.size())
                return true;
            return false;
        }

        bool operator<(const c8* s) const
        {
            if (strcmp(c_str(), s) < 0)
                return true;
            return false;
        }

        bool operator>(const c8* s) const
        {
            if (strcmp(c_str(), s) > 0)
                return true;
            return false;
        }

        bool operator<=(const c8* s) const
        {
            if (strcmp(c_str(), s) <= 0)
                return true;
            return false;
        }

        bool operator>=(const c8* s) const
        {
            if (strcmp(c_str(), s) >= 0)
                return true;
            return false;
        }
    protected:

        void initNULL(void)
        {
            S = NULL;
            Size = 0;
            SizeUsed = 0;
        }

        void reserve(void)
        {
            Size = 32;
            SizeUsed = 0;
            S = new c8[Size];
        }

        void cpyIn(const c8* data, u32 offset = 0)
        {
            memcpy(&S[offset], data, SizeUsed);
            S[SizeUsed] = '\0';
        }

        //internal string array
        c8* S;
        u32 Size;
        u32 SizeUsed;
    private:
    };

    inline AppFrame::string makeRelativePath(const AppFrame::string& exe, const AppFrame::string& file)
    {
        AppFrame::u32 folderUp = 0;
        AppFrame::string filepath = file.subString(0, exe.size());
        AppFrame::string exepath = exe;
        while (filepath != exepath)
        {
            if (exepath[exepath.size()-1] == '\\' || exepath[exepath.size()-1] == '/')
                folderUp++;
            exepath.erase(exepath.size()-1);
            filepath = file.subString(0, exepath.size());
        }

        filepath = file.subString(exepath.size(), file.size()-exepath.size());

        if (filepath[0] == '\\' || filepath[0] == '/')
            filepath.erase(0);

        if (exepath.size() > 0 && (exepath[exepath.size()-1] == '\\' || exepath[exepath.size()-1] == '/'))
            folderUp++;

        AppFrame::string final;

        for (AppFrame::u32 i=0;i < (exepath.size() > 0) ? folderUp : 0;i++)
        {
            final += "../";
        }

        final += filepath;

        return final;
    }
}
#endif // APPSTRING_H
