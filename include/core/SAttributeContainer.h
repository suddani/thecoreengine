#ifndef SATTRIBUTECONTAINER_H
#define SATTRIBUTECONTAINER_H

#include "SVar.h"

namespace AppFrame
{
    class StringSetGet
    {
    public:
        class q : public irr::IReferenceCounted
        {
        public:
            q(const c8* name, AppFrame::actions::SFunctorR1<bool, const c8*>* set, AppFrame::actions::SFunctorR<irr::core::stringc>* get)
            {
                Name = name;
                Setter = set;
                Getter = get;
            }
            virtual ~q(void)
            {
                Setter->drop();
                Getter->drop();
            }
            irr::core::stringc getVar(void)
            {
                return Getter->call();
            }
            bool setVar(const c8* value)
            {
                return Setter->call(value);
            }
            const irr::core::stringc& getName(void)
            {
                return Name;
            }
        protected:
            irr::core::stringc Name;
            AppFrame::actions::SFunctorR<irr::core::stringc>* Getter;
            AppFrame::actions::SFunctorR1<bool, const c8*>* Setter;
        };

        virtual ~StringSetGet(void)
        {
            clear();
        }

        irr::core::stringc getAttribute(const c8* name)
        {
            for (irr::u32 i=0;i < Attributes.size();i++)
            {
                if (Attributes[i]->getName() == name)
                {
                    return Attributes[i]->getVar();
                }
            }
        }

        bool setAttribute(const c8* name, const c8* value)
        {
            for (irr::u32 i=0;i < Attributes.size();i++)
            {
                if (Attributes[i]->getName() == name)
                {
                    return Attributes[i]->setVar(value);
                }
            }
        }
    protected:
        StringSetGet(void)
        {
        }

        template<typename C>
        bool Bind(const c8* name, bool (C::*set)(const irr::core::stringc&), irr::core::stringc (C::*get)())
        {
            for (irr::u32 i=0;i < Attributes.size();i++)
            {
                if (Attributes[i]->getName() == name)
                    return false;
            }

            AppFrame::actions::SFunctorR1<bool, const c8*>* Setter = new AppFrame::actions::SFunctorRecieverR1<bool, C, const c8*>((C*)this, set);

            AppFrame::actions::SFunctorR<irr::core::stringc>* Getter = new AppFrame::actions::SFunctorRecieverR<irr::core::stringc, C>((C*)this, get);

            Attributes.push_back(new q(name, Setter, Getter));

            return true;
        }

        bool unBind(const c8* name)
        {
            for (irr::u32 i=0;i < Attributes.size();i++)
            {
                if (Attributes[i]->getName() == name)
                {
                    Attributes[i]->drop();
                    Attributes.erase(i);
                    return true;
                }
            }
            return false;
        }

        void clear(void)
        {
            for (irr::u32 i=0;i < Attributes.size();i++)
                Attributes[i]->drop();
            Attributes.clear();
        }
    private:
        irr::core::array<q*> Attributes;
    };

    class SAttributeContainer
    {
    public:
        /** Default destructor */
        virtual ~SAttributeContainer()
        {
            clear();
        }

        string getValue(const c8* name)
        {
            for (u32 i=0;i<Attributes.size();i++)
                if (Attributes[i]->getName() == name)
                {
                    if ((Attributes[i]->getFlags() & ATTRIBUTE_READ) == ATTRIBUTE_READ)
                        return Attributes[i]->getValue();
                    else
                        return NoValueDefined;
                }
            return NoValueDefined;
        }

        template<typename T>
        bool getValue(const c8* name, T& value)
        {
            for (u32 i=0;i<Attributes.size();i++)
                if (Attributes[i]->getName() == name)
                {
                    if ((Attributes[i]->getFlags() & ATTRIBUTE_READ) == ATTRIBUTE_READ && Attributes[i]->getTypeName() == typeid(value).name())
                    {
                        value = *Attributes[i];
                        return true;
                    }
                    else
                        return false;
                }
            return false;
        }

        bool setValue(const c8* name, const c8* value)
        {
            for (u32 i=0;i<Attributes.size();i++)
                if (Attributes[i]->getName() == name)
                {
                    if ((Attributes[i]->getFlags() & ATTRIBUTE_WRITE) == ATTRIBUTE_WRITE)
                        return Attributes[i]->setValue(value);
                    else
                        return false;
                }
            return false;
        }

        template<typename T>
        bool setValue(const c8* name, const T& value)
        {
            for (u32 i=0;i<Attributes.size();i++)
                if (Attributes[i]->getName() == name)
                {
                    if ((Attributes[i]->getFlags() & ATTRIBUTE_WRITE) == ATTRIBUTE_WRITE && Attributes[i]->getTypeName() == typeid(value).name())
                    {
                        *Attributes[i] = value;
                        return true;
                    }
                    else
                        return false;
                }
            return false;
        }

        SVar& getAttribute(const c8* name)
        {
            for (u32 i=0;i<Attributes.size();i++)
                if (Attributes[i]->getName() == name)
                    return *Attributes[i];
            return TMP;
        }
    protected:
        /** Default constructor */
        SAttributeContainer() {}

        void clear(bool tempOnly = false)
        {
            if (!tempOnly)
            {
                for (u32 i=0;i<Attributes.size();i++)
                    Attributes[i]->drop();
                Attributes.clear();
            }
            else
            {
                u32 i = 0;
                while (i<Attributes.size())
                {
                    if ((Attributes[i]->getFlags() & ATTRIBUTE_TEMP) == ATTRIBUTE_TEMP)
                    {
                        Attributes[i]->drop();
                        Attributes.erase(i);
                    }
                    else
                        i++;
                }
            }
        }

        bool Release(const c8* name)
        {
            for (u32 i=0;i<Attributes.size();i++)
                if (Attributes[i]->getName() == name)
                {
                    Attributes[i]->drop();
                    Attributes.erase(i);
                    return true;
                }
            return false;
        }

        template<typename T>
        bool Bind(const c8* name, T* data, const c8& flags = ATTRIBUTE_NORMAL)
        {
            for (u32 i=0;i<Attributes.size();i++)
                if (Attributes[i]->getName() == name)
                    return false;
            Attributes.push_back(new SVar(name, data, flags));
            return true;
        }

        template<typename T, typename A>
        bool Bind(const c8* name, bool (T::*set)(const A&), bool (T::*get)(A&), bool (T::*setStr)(const string&), string (T::*getStr)(), const c8& flags = ATTRIBUTE_NORMAL, T* myClass = NULL)
        {
            for (u32 i=0;i<Attributes.size();i++)
                if (Attributes[i]->getName() == name)
                    return false;
            if (!myClass)
                myClass = (T*)this;
            Attributes.push_back(new SVar(name, myClass, set, get, setStr, getStr, flags));
            return true;
        }
    private:
        array<SVar*> Attributes;
        SVar TMP;


    private:
    };
}

#endif // SATTRIBUTECONTAINER_H
