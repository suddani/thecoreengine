#ifndef MESSAGEDISPATCHER_H
#define MESSAGEDISPATCHER_H

#include <list>
#include "IRef.h"
#include <map>
#include "ILog.h"
namespace AppFrame
{
class IDataStream;
namespace actions
{
struct Message : public AppFrame::IRef
{
    Message(void)
    {
        getMessageCount()++;
        UserID = -1;
    }
    virtual ~Message(void)
    {
    }
    virtual const int getID(void) const = 0;

    int UserID;

    virtual void serialize(AppFrame::IDataStream* stream){}

    virtual void deserialize(AppFrame::IDataStream* stream){}

    static const int& getTotalMessageCount(void)
    {
        return getMessageCount();
    }
private:
    static int& getMessageCount(void)
    {
        static int MESSAGE_COUNT = 0;
        return MESSAGE_COUNT;
    }
};

template<class M>
struct Message_ : Message
{
    Message_(void)
    {
    }
    static bool checkType(Message* message)
    {
        return Message_<M>::getID_static() == message->getID() ? true : false;
    }
    static M* convert(Message* message)
    {
        if (Message_<M>::getID_static() == message->getID())
            return (M*)message;
        return NULL;
    }
    virtual const int getID(void) const
    {
        return Message_<M>::getID_static();
    }
    static int getID_static(void)
    {
        if (Message_<M>::TypeIDRef_Custom == -1)
            return (int)&Message_<M>::TypeIDRef;
        return Message_<M>::TypeIDRef_Custom;
    }
    static void setID_static(int id)
    {
        Message_<M>::TypeIDRef_Custom = id;
    }
private:
    static const int TypeIDRef;
    static int TypeIDRef_Custom;
};

template<class M>
const int Message_<M>::TypeIDRef = 0;
template<class M>
int Message_<M>::TypeIDRef_Custom = -1;

struct EventHandlerBase
{
    virtual ~EventHandlerBase(void) {}
    virtual void* getClassPointer(void)
    {
        return 0;
    }
    virtual void call(Message* msg) = 0;
};

struct smartPoint
{
    smartPoint(void)
    {
        Base = 0;
    }
    ~smartPoint(void)
    {
        if (Base)
        {
            nlog<<"Release EventHandler\n"<<nlendl;
            delete Base;
        }
        Base = 0;
    }
    EventHandlerBase* Base;
    operator EventHandlerBase*()
    {
        if (!Base)
        {
            nlerror<<"Missing EventHandlerBase"<<nlendl;
            exit(1);
        }
        return Base;
    }
    smartPoint& operator=(EventHandlerBase* base)
    {
        if (Base)
        {
            delete Base;
            nlog<<"Release EventHandler\n"<<nlendl;
        }
        Base = base;
        return *this;
    }
};

struct Subscriber
{
    Subscriber(void) {}
    virtual ~Subscriber(void);
    virtual void receiveMessage(Message* message){/*Needs to be implemented by the user when using MessageDispatcher*/}
protected:
    friend struct MessageDispatcher;
    struct Entry
    {
        Entry(void)
        {
            EventMap = 0;
            List = 0;
        }
        ~Entry(void)
        {
            if (EventMap)
            {
                EventMap->erase(EventMapIt);
            }
            if (List)
            {
                List->erase(ListIt);
            }
        }
        std::map<Subscriber*, smartPoint>* EventMap;
        std::map<Subscriber*, smartPoint>::iterator EventMapIt;
        std::list<Subscriber*>* List;
        std::list<Subscriber*>::iterator ListIt;
    };
    std::list<Entry*> MessageSignals;
    void addSignal(std::map<Subscriber*, smartPoint>* EventMap, std::list<Subscriber*>* List);
    void removeSignal(std::map<Subscriber*, smartPoint>* EventMap);
    void removeSignal(std::list<Subscriber*>* List);
};

struct MessageDispatcher
{
    virtual ~MessageDispatcher(void);

    virtual void send(Message* message);

    template<class MessageType, class T>
    void Bind(T* t)
    {
        Subscriber* l = t;

        //remove the signal incase its already registered
        l->removeSignal(&EventHandlerMap[Message_<MessageType>::getID_static()]);

        EventHandlerMap[Message_<MessageType>::getID_static()][l] = new EventHandler<T, MessageType>(t);
        l->addSignal(&EventHandlerMap[Message_<MessageType>::getID_static()], &ListenerMap[Message_<MessageType>::getID_static()]);
    }
    template<class MessageType>
    void Bind(void (*func)(MessageType* msg))
    {
        //remove the signal incase its already registered
        FunctionSubscriber.removeSignal(&EventHandlerMap[Message_<MessageType>::getID_static()]);

        EventHandlerMap[Message_<MessageType>::getID_static()][&FunctionSubscriber] = new EventHandlerFunction<MessageType>(func);
        FunctionSubscriber.addSignal(&EventHandlerMap[Message_<MessageType>::getID_static()], &ListenerMap[Message_<MessageType>::getID_static()]);
    }
    template<class T, class MessageType>
    void Bind(T* t, void (T::*func)(MessageType* msg))
    {
        Subscriber* l = t;

        //remove the signal incase its already registered
        l->removeSignal(&EventHandlerMap[Message_<MessageType>::getID_static()]);

        EventHandlerMap[Message_<MessageType>::getID_static()][l] = new EventHandlerMethod<T, MessageType>(t, func);
        l->addSignal(&EventHandlerMap[Message_<MessageType>::getID_static()], &ListenerMap[Message_<MessageType>::getID_static()]);
    }
    template<class MessageType, class T>
    void unBind(T* t)
    {
        Subscriber* l = t;
        l->removeSignal(&EventHandlerMap[Message_<MessageType>::getID_static()]);
    }
    template<class MessageType>
    void unBind(void)
    {
        FunctionSubscriber.removeSignal(&EventHandlerMap[Message_<MessageType>::getID_static()]);
    }
private:
    Subscriber FunctionSubscriber;
protected:
    template<class T, class MessageType>
    struct EventHandler : public EventHandlerBase
    {
        EventHandler(T* c)
        {
            C = c;
        }
        void call(Message* message)
        {
            C->receiveMessage(Message_<MessageType>::convert(message));
        }
        void* getClassPointer(void)
        {
            return C;
        }
        T* C;
    };
    template<class MessageType>
    struct EventHandlerFunction : public EventHandlerBase
    {
        typedef void (*Callback)(MessageType*);
        EventHandlerFunction(Callback c)
        {
            C = c;
        }
        void call(Message* message)
        {
            (C)(Message_<MessageType>::convert(message));
        }
        void* getClassPointer(void)
        {
            return NULL;
        }
        Callback C;
    };
    template<class T, class MessageType>
    struct EventHandlerMethod : EventHandlerBase
    {
        typedef void (T::*Callback)(MessageType*);
        EventHandlerMethod(T* object, Callback call)
        {
            Object = object;
            MyCallback = call;
        }
        void call(Message* message)
        {
            (Object->*MyCallback)(Message_<MessageType>::convert(message));
        }
        T* Object;
        Callback MyCallback;
    };
    std::map<int, std::map<Subscriber*, smartPoint> > EventHandlerMap;
    std::map<int, std::list<Subscriber*> > ListenerMap;
};


///premade templates
template<class A>
struct Subscriber_1 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
};
template<class A, class B>
struct Subscriber_2 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (Message_<B>::checkType(message))
            receiveMessage(dynamic_cast<B*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
};
template<class A, class B, class C>
struct Subscriber_3 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (Message_<B>::checkType(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (Message_<C>::checkType(message))
            receiveMessage(dynamic_cast<C*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
};
template<class A, class B, class C, class D>
struct Subscriber_4 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (Message_<B>::checkType(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (Message_<C>::checkType(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (Message_<D>::checkType(message))
            receiveMessage(dynamic_cast<D*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
};
template<class A, class B, class C, class D, class E>
struct Subscriber_5 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (Message_<B>::checkType(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (Message_<C>::checkType(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (Message_<D>::checkType(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (Message_<E>::checkType(message))
            receiveMessage(dynamic_cast<E*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
};
template<class A, class B, class C, class D, class E, class F>
struct Subscriber_6 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (Message_<B>::checkType(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (Message_<C>::checkType(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (Message_<D>::checkType(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (Message_<E>::checkType(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (Message_<F>::checkType(message))
            receiveMessage(dynamic_cast<F*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
};
template<class A, class B, class C, class D, class E, class F, class G>
struct Subscriber_7 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (Message_<B>::checkType(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (Message_<C>::checkType(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (Message_<D>::checkType(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (Message_<E>::checkType(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (Message_<F>::checkType(message))
            receiveMessage(dynamic_cast<F*>(message));
        else if (Message_<G>::checkType(message))
            receiveMessage(dynamic_cast<G*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
    virtual void receiveMessage(G* message) = 0;
};
template<class A, class B, class C, class D, class E, class F, class G, class H>
struct Subscriber_8 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (Message_<B>::checkType(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (Message_<C>::checkType(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (Message_<D>::checkType(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (Message_<E>::checkType(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (Message_<F>::checkType(message))
            receiveMessage(dynamic_cast<F*>(message));
        else if (Message_<G>::checkType(message))
            receiveMessage(dynamic_cast<G*>(message));
        else if (Message_<H>::checkType(message))
            receiveMessage(dynamic_cast<H*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
    virtual void receiveMessage(G* message) = 0;
    virtual void receiveMessage(H* message) = 0;
};
template<class A, class B, class C, class D, class E, class F, class G, class H, class I>
struct Subscriber_9 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (Message_<B>::checkType(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (Message_<C>::checkType(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (Message_<D>::checkType(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (Message_<E>::checkType(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (Message_<F>::checkType(message))
            receiveMessage(dynamic_cast<F*>(message));
        else if (Message_<G>::checkType(message))
            receiveMessage(dynamic_cast<G*>(message));
        else if (Message_<H>::checkType(message))
            receiveMessage(dynamic_cast<H*>(message));
        else if (Message_<I>::checkType(message))
            receiveMessage(dynamic_cast<I*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
    virtual void receiveMessage(G* message) = 0;
    virtual void receiveMessage(H* message) = 0;
    virtual void receiveMessage(I* message) = 0;
};
template<class A, class B, class C, class D, class E, class F, class G, class H, class I, class J>
struct Subscriber_10 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (Message_<A>::checkType(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (Message_<B>::checkType(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (Message_<C>::checkType(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (Message_<D>::checkType(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (Message_<E>::checkType(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (Message_<F>::checkType(message))
            receiveMessage(dynamic_cast<F*>(message));
        else if (Message_<G>::checkType(message))
            receiveMessage(dynamic_cast<G*>(message));
        else if (Message_<H>::checkType(message))
            receiveMessage(dynamic_cast<H*>(message));
        else if (Message_<I>::checkType(message))
            receiveMessage(dynamic_cast<I*>(message));
        else if (Message_<J>::checkType(message))
            receiveMessage(dynamic_cast<J*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
    virtual void receiveMessage(G* message) = 0;
    virtual void receiveMessage(H* message) = 0;
    virtual void receiveMessage(I* message) = 0;
    virtual void receiveMessage(J* message) = 0;
};
}
}
#endif // MESSAGEDISPATCHER_H
