/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef ILOGGER_H
#define ILOGGER_H

#include <stdarg.h>
#include "Types.h"
#include "appString.h"
#include "appArray.h"

#include "IPointer.h"

namespace AppFrame
{
    enum E_LOG_TYPE
    {
        ELT_LOG = 0,
        ELT_ERROR,
        ELT_COUNT
    };

    class ILogReceiver : public IPointer
    {
    public:
        virtual void OnLogEvent(const string& event, E_LOG_TYPE type, u32 loglevel) = 0;
    };

    class ILogger
    {
    public:
        static void addReceiver(ILogReceiver* receiver)
        {
            for (u32 i=0;i<get().Receiver.size();i++)
                if (get().Receiver[i] == receiver)
                    return;
            get().Receiver.push_back(receiver);
        }
        static void removeReceiver(ILogReceiver* receiver)
        {
            for (u32 i=0;i<get().Receiver.size();i++)
                if (get().Receiver[i] == receiver)
                {
                    get().Receiver.erase(i);
                    return;
                }
        }
        virtual ~ILogger()
        {
            if (log_file_normal)
                fclose(log_file_normal);
            if (log_file_error)
                fclose(log_file_error);
        }
        static void setLogLevel(u32 loglevel)
        {
        	//printf("LogLEVEL is: %i\n", getLogLevel());
        	get().log_level = loglevel;
        	//printf("set LogLevel to %i\n", loglevel);
        	//printf("LogLEVEL is now: %i\n", getLogLevel());
        }
        static void setLogFile(const c8* file)
        {
            if (get().log_file_normal)
            {
                fclose(get().log_file_normal);
                get().log_file_normal = NULL;
            }
            if (file != NULL)
            {
                get().log_file_normal = fopen(file, "w+");
            }
        }
        static void setErrorFile(const char* file)
        {
            if (get().log_file_error)
            {
                fclose(get().log_file_error);
                get().log_file_error = NULL;
            }
            if (file != NULL)
            {
                get().log_file_error = fopen(file, "w+");
            }
        }
        static void log(const char* log, ...)
        {
        	if(get().log_level > 0)
				return;
			#ifdef DEBUG_LOGGER
			printf("LogLevel[%i]-MessageSendBy[%i] - ", get().log_level, 0);
			#endif

			char console[255];

            va_list args;
            va_start( args, log );
            if (get().log_file_normal)
            {
            	if(get().log_text.size()>0)
					fprintf( get().log_file_normal, get().log_text.c_str() );
                vfprintf( get().log_file_normal, log, args );
                fprintf( get().log_file_normal, "\n" );
            }
            if (get().log_to_screen)
            {
            	if(get().log_text.size()>0)
					printf(get().log_text.c_str() );

                if (get().Receiver.size()>0)
                {
                    string c;
                    c.append(log);
                    vsprintf(console, c.c_str(), args);
                    for (u32 i=0;i<get().Receiver.size();i++)
                    {
                        get().Receiver[i]->OnLogEvent(console, ELT_LOG, 0);
                    }
                }
                vprintf(log, args );
                printf("\n" );
            }
            va_end( args );
            if (get().log_file_normal)
                fflush(get().log_file_normal);
        }
        static void log(u32 loglevel, const char* log, ...)
        {
        	if(get().log_level > loglevel)
				return;
			#ifdef DEBUG_LOGGER
			printf("LogLevel[%i]-MessageSendBy[%i] - ", get().log_level, 0);
			#endif

			char console[255];

            va_list args;
            va_start( args, log );
            if (get().log_file_normal)
            {
            	if(get().log_text.size()>0)
					fprintf( get().log_file_normal, get().log_text.c_str() );
                vfprintf( get().log_file_normal, log, args );
                fprintf( get().log_file_normal, "\n" );
            }
            if (get().log_to_screen)
            {
            	if(get().log_text.size()>0)
					printf(get().log_text.c_str() );

                if (get().Receiver.size()>0)
                {
                    string c;
                    c.append(log);
                    vsprintf(console, c.c_str(), args);
                    for (u32 i=0;i<get().Receiver.size();i++)
                    {
                        get().Receiver[i]->OnLogEvent(console, ELT_LOG, loglevel);
                    }
                }
                vprintf(log, args );
                printf("\n" );
            }
            va_end( args );
            if (get().log_file_normal)
                fflush(get().log_file_normal);
        }
        static void error(const char* error, ...)
        {
        	if(get().log_level > 0)
				return;
			#ifdef DEBUG_LOGGER
			printf("LogLevel[%i]-MessageSendBy[%i] - ", get().log_level, 0);
			#endif

			char console[255];

            va_list args;
            va_start( args, error );
            if (get().log_file_error)
            {
            	if(get().error_text.size()>0)
					fprintf( get().log_file_error, get().error_text.c_str() );
                vfprintf( get().log_file_error, error, args );
                fprintf( get().log_file_error, "\n" );
            }
            if (get().log_to_screen)
            {
                if(get().error_text.size()>0)
					printf(get().error_text.c_str() );
                if (get().Receiver.size()>0)
                {
                    string c;
                    c.append(error);
                    vsprintf(console, c.c_str(), args);
                    for (u32 i=0;i<get().Receiver.size();i++)
                    {
                        get().Receiver[i]->OnLogEvent(console, ELT_ERROR, 0);
                    }
                }
                vprintf(error, args );
                printf("\n" );
            }
            va_end( args );
            if (get().log_file_error)
                fflush(get().log_file_error);
        }
        static void error(u32 loglevel, const char* error, ...)
        {
        	if(get().log_level > loglevel)
				return;
			#ifdef DEBUG_LOGGER
			printf("LogLevel[%i]-MessageSendBy[%i] - ", get().log_level, 0);
			#endif

			char console[255];

            va_list args;
            va_start( args, error );
            if (get().log_file_error)
            {
            	if(get().error_text.size()>0)
					fprintf( get().log_file_error, get().error_text.c_str() );
                vfprintf( get().log_file_error, error, args );
                fprintf( get().log_file_error, "\n" );
            }
            if (get().log_to_screen)
            {
                if(get().error_text.size()>0)
					printf(get().error_text.c_str() );
                if (get().Receiver.size()>0)
                {
                    string c;
                    c.append(error);
                    vsprintf(console, c.c_str(), args);
                    for (u32 i=0;i<get().Receiver.size();i++)
                    {
                        get().Receiver[i]->OnLogEvent(console, ELT_ERROR, loglevel);
                    }
                }
                vprintf(error, args );
                printf("\n" );
            }
            va_end( args );
            if (get().log_file_error)
                fflush(get().log_file_error);
        }

        static ILogger& get(void)
        {
            static ILogger logger;
            return logger;
        }

        static void setLogText(const c8* text)
        {
            get().log_text = text;
        }

        static void setLogToScreen(bool& l2s)
        {
            get().log_to_screen = l2s;
        }

        const c8* getLogText(void)
        {
            return get().log_text.c_str();
        }

        static void setErrorText(const c8* text)
        {
            get().error_text = text;
        }

        const c8* getErrorText(void)
        {
            return get().error_text.c_str();
        }
    protected:
		ILogger()
        {
            log_file_normal = NULL;
            log_file_error = NULL;
            log_text = "LOG: ";
            error_text = "ERROR: ";
            log_level = 0;
            log_to_screen = true;
        }
        array<ILogReceiver*> Receiver;
        FILE* log_file_error;
        FILE* log_file_normal;
        string log_text;
        string error_text;
        u32 log_level;
        bool log_to_screen;
    private:
    };

    //makes logging easier

    inline void log(const c8* str, ...)
    {
        char console[255];
        va_list args;
        va_start( args, str );
        vsprintf(console, str, args);
        va_end( args );
        ILogger::log(console);
    }

    inline void error(const c8* str, ...)
    {
        char console[255];
        va_list args;
        va_start( args, str );
        vsprintf(console, str, args);
        va_end( args );
        ILogger::error(console);
    }

    inline void log(u32 loglevel, const c8* str, ...)
    {
        char console[255];
        va_list args;
        va_start( args, str );
        vsprintf(console, str, args);
        va_end( args );
        ILogger::log(loglevel, console);
    }

    inline void error(u32 loglevel, const c8* str, ...)
    {
        char console[255];
        va_list args;
        va_start( args, str );
        vsprintf(console, str, args);
        va_end( args );
        ILogger::error(loglevel, console);
    }
}
#endif // ILOGGER_H
