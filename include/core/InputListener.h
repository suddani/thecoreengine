/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef INPUTLISTENER_H
#define INPUTLISTENER_H
#include "IInputManager.h"
namespace AppFrame
{
    namespace input
    {
        class KeyState;
        class IKeyListener
        {
        public:
            IKeyListener(void)
            {
                IKeyListener_Manager = NULL;
            }
            virtual ~IKeyListener(void)
            {
                if (IKeyListener_Manager)
                    IKeyListener_Manager->removeListener(this);
            }
            //if (State)
            //State->removeListener(this);
            virtual void key_event(const irr::EKEY_CODE& key, const AppFrame::input::KeyState& state) = 0;

            IInputManager* IKeyListener_Manager;
        };

        class IGUIListener
        {
        public:
            IGUIListener(void)
            {
                IGUIListener_Manager = NULL;
            }
            virtual ~IGUIListener(void)
            {
                if (IGUIListener_Manager)
                    IGUIListener_Manager->removeListener(this);
            }
            //if (State)
            //State->removeListener(this);
            virtual void gui_event(const irr::SEvent::SGUIEvent& event) = 0;

            IInputManager* IGUIListener_Manager;
        };

        class IMouseListener
        {
        public:
            IMouseListener(void)
            {
                IMouseListener_Manager = NULL;
            }
            virtual ~IMouseListener(void)
            {
                if (IMouseListener_Manager)
                    IMouseListener_Manager->removeListener(this);
            }
            //if (State)
            //State->removeListener(this);
            virtual void mouse_event(const AppFrame::input::E_MOUSE_EVENT_TYPE& type, const AppFrame::input::MouseState& state) = 0;

            IInputManager* IMouseListener_Manager;
        };

        class IJoystickListener
        {
        public:
            IJoystickListener(void)
            {
                IJoystickListener_Manager = NULL;
            }
            virtual ~IJoystickListener(void)
            {
                if (IJoystickListener_Manager)
                    IJoystickListener_Manager->removeListener(this);
            }
            virtual void joystick_event(const AppFrame::input::E_JOYSTICK_EVENT_TYPE& type, const AppFrame::input::JoystickState& state) = 0;
            IInputManager* IJoystickListener_Manager;
        };
    }
}
#endif // INPUTLISTENER_H
