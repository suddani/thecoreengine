/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef APPLIST_H
#define APPLIST_H

#include "Types.h"
#include "IPointer.h"
#include "ILogger.h"

namespace AppFrame
{
    template<typename T>
    class list
    {
    public:
        class Element : public IPointer
        {
        public:
            Element(void)
            {
                Data     = NULL;
                Next     = NULL;
                Previous = NULL;
            }

            Element(T data)
            {
                Data     = data;
                Next     = NULL;
                Previous = NULL;
            }

            Element(const Element& el)
            {
                Data     = el.Data;
                Next     = el.Next;
                Previous = el.Previous;
            }

            Element(const Element* el)
            {
                Data     = el->Data;
                Next     = el->Next;
                Previous = el->Previous;
            }

            virtual ~Element(void)
            {
            }

            bool operator==(const Element* element)
            {
                if (Data = element->Data && Next == element->Next && Previous == element->Previous)
                    return true;
                return false;
            }

            bool operator==(const Element& element)
            {
                if (Data = element.Data && Next == element.Next && Previous == element.Previous)
                    return true;
                return false;
            }

            bool operator!=(const Element* element)
            {
                if (Data = element->Data && Next == element->Next && Previous == element->Previous)
                    return false;
                return true;
            }

            T Data;

            Element* Next;
            Element* Previous;
        };

        class Iterator
        {
        public:
            Iterator(void)
            {
                element = NULL;
            }

            Iterator(Element* el)
            {
                element = el;
            }

            Iterator(const Iterator& it)
            {
                element = it.element;
            }

            Element* get(void)
            {
                return element;
            }

            T& operator*()
            {
                return element->Data;
            }

            T* operator->()
            {
                return element->Data;
            }

            bool operator==(const Iterator& it) const
            {
                if (element == it.element)
                    return true;
                return false;
            }

            Iterator& operator=(const Iterator& it)
            {
                element = it.element;
                return *this;
            }

            Iterator& operator=(Element* it)
            {
                element = it;
                return *this;
            }

            bool operator==(const Element* it) const
            {
                if (element == it)
                    return true;
                return false;
            }

            bool operator==(const Element& it) const
            {
                if (element == it)
                    return true;
                return false;
            }

            bool operator!=(const Iterator& it) const
            {
                if (element != it.element)
                    return true;
                return false;
            }

            inline Iterator& operator++()
            {
                if (element)
                    element = element->Next;
                return *this;
            }

            inline Iterator& operator--()
            {
                if (element)
                    element = element->Previous;
                return *this;
            }
        protected:
            Element* element;
        };


        list(void)
        {
            Start = NULL;
            End   = NULL;
            Size  = 0;
        }
        virtual ~list(void)
        {
            clear();
        }

        void push_front(T data)
        {
            if (Start == NULL)
            {
                Start = new Element(data);
                End   = Start;
            }
            else
            {
                Element* el      = new Element(data);
                el->Next         = Start;
                Start->Previous  = el;
                Start            = el;
            }
            Size++;
        }

        void push_back(T data)
        {
            if (Start == NULL)
            {
                Start = new Element(data);
                End   = Start;
            }
            else
            {
                End->Next           = new Element(data);
                End->Next->Previous = End;
                End                 = End->Next;
            }
            Size++;
        }

        void insert(Iterator& it, T data)
        {
            if (it == last())
            {
                it.get()->Next           = new Element(data);
                it.get()->Next->Previous = last().get();
                End                      = it.get()->Next;
            }
            else
            {
                Element* el    = new Element(data);
                el->Next       = it.get()->Next;
                el->Previous   = it.get();
                it.get()->Next = el;
            }
            Size++;
        }

        bool find(T data, Iterator& i, const Iterator& start = NULL)
        {
            Iterator it;
            if (start != end())
                it = start;
            else
                it = begin();

            while (it != end())
            {
                if ((*it) == data)
                {
                    if (i)
                        i = it;
                    return true;
                }
                ++it;
            }
            return false;
        }

        void erase(T data, bool all = false)
        {
            Iterator it = begin();
            while (it != end())
            {
                if ((*it) == data)
                {
                    erase(it);
                    Size--;
                    if (!all)
                        return;
                }
                ++it;
            }
        }

        void clear(void)
        {
            Iterator it = begin();
            while (it != end())
            {
                erase(it);
            }
            Size = 0;
        }

        void erase(Iterator& it)
        {
            if (it == Start && Start)
            {
                Start = Start->Next;
            }

            if (it == End && End)
            {
                End = End->Previous;
            }

            Element* element = it.get();

            if (element)
            {
                if (element->Previous)
                {
                    element->Previous->Next = element->Next;
                }

                if (element->Next)
                {
                    element->Next->Previous = element->Previous;
                }

                Element* tmp = element->Next;
                element->drop();
                it = tmp;
                Size--;
            }
        }

        Iterator begin(void)
        {
            return Start;
        }

        Iterator last(void)
        {
            return End;
        }

        Iterator end(void)
        {
            return NULL;
        }

        const u32& getSize(void) const
        {
            return Size;
        }

        bool empty(void)
        {
            return Size > 0 ? false : true;
        }
    protected:
        Element* Start;
        Element* End;
        u32 Size;
    private:
    };
}
#endif // APPLIST_H
