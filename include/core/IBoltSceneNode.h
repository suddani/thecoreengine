/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef IBOLTSCENENODE_H
#define IBOLTSCENENODE_H

#include <irrlicht.h>

namespace AppFrame
{
    namespace graphic
    {
        class IBoltNode : public irr::scene::ISceneNode
        {
        public:
            IBoltNode( irr::scene::ISceneNode* parent, irr::scene::ISceneManager *mgr, irr::s32 id) : irr::scene::ISceneNode( parent, mgr, id )
            {
            }

            virtual ~IBoltNode(void)
            {
            }

            virtual void setLine(irr::core::vector3df start, irr::core::vector3df end, irr::u32 updateTime = 300, irr::u32 height = 10, irr::u32 parts = 10, irr::u32 bolts = 1, bool steddyend = true, irr::video::SColor color = irr::video::SColor(255,255,0,0)) = 0;
        protected:
        private:
        };
    }
}
#endif // CBOLTSCENENODE_H
