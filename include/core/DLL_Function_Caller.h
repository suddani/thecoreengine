#ifndef DLL_FUNCTION_CALLER_H
#define DLL_FUNCTION_CALLER_H

#ifdef __GNUWIN32__
#include <windows.h>
#else
//#include <unistd.h> // usleep
#include <dlfcn.h>
#endif

#include "ILog.h"

namespace AppFrame
{
inline void error(const char* str)
{
    nlerror<<str<<nlendl;
}
inline void log(const char* str)
{
    nlog<<str<<nlendl;
}

class DLL_Function_Caller
{
public:
    DLL_Function_Caller(void)
    {
        appDLL = NULL;
    }
    ~DLL_Function_Caller(void)
    {
        reset();
    }
    const char* getFileName(void)
    {
        return DLL_File.c_str();
    }
    void init(const char* file)
    {
        DLL_File = file;
        if (appDLL)
        {
            AppFrame::error("Loader already initialized");
            return;
        }
#ifdef __GNUWIN32__
        appDLL = LoadLibrary(file);
#else
        appDLL = dlopen(file, RTLD_LAZY);
#endif
        if (appDLL == NULL)
        {
            AppFrame::error("Faild to load DLL");
        }
        else
            AppFrame::log("Loaded DLL");
    }
    void reset(void)
    {
        DLL_File = "";
#ifdef __GNUWIN32__
        FreeLibrary(appDLL);
#else
        if (!appDLL)
            dlclose(appDLL);
#endif
        appDLL = NULL;
    }
    void call(const char* function)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            return;
        }
        typedef void (*FunctionPointer)(void);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        if (func)
        {
            AppFrame::log("Found function calling...\n");
            (func)();
            AppFrame::log("done\n");
        }
    }
    template<typename R>
    R callR(const char* function)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            R r;
            return r;
        }
        typedef R (*FunctionPointer)(void);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        return (func)();
    }
    template<typename A>
    void call(const char* function, A a)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            return;
        }
        typedef void (*FunctionPointer)(A);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        if (func)
        {
            AppFrame::log("Found function calling...\n");
            (func)(a);
            AppFrame::log("done\n");
        }
    }
    template<typename R, typename A>
    R callR(const char* function, A a)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            R r;
            return r;
        }
        typedef R (*FunctionPointer)(A);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        return (func)(a);
    }
    template<typename A, typename B>
    void call(const char* function, A a, B b)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            return;
        }
        typedef void (*FunctionPointer)(A, B);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        if (func)
        {
            AppFrame::log("Found function calling...\n");
            (func)(a, b);
            AppFrame::log("done\n");
        }
    }
    template<typename R, typename A, typename B>
    R callR(const char* function, A a, B b)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            R r;
            return r;
        }
        typedef R (*FunctionPointer)(A, B);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        return (func)(a, b);
    }
    template<typename A, typename B, typename C>
    void call(const char* function, A a, B b, C c)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            return;
        }
        typedef void (*FunctionPointer)(A, B, C);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        if (func)
        {
            AppFrame::log("Found function calling...\n");
            (func)(a, b, c);
            AppFrame::log("done\n");
        }
    }
    template<typename R, typename A, typename B, typename C>
    R callR(const char* function, A a, B b, C c)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            R r;
            return r;
        }
        typedef R (*FunctionPointer)(A, B, C);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        return (func)(a, b, c);
    }
    template<typename A, typename B, typename C, typename D>
    void call(const char* function, A a, B b, C c, D d)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            return;
        }
        typedef void (*FunctionPointer)(A, B, C, D);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        if (func)
        {
            AppFrame::log("Found function calling...\n");
            (func)(a, b, c, d);
            AppFrame::log("done\n");
        }
    }
    template<typename R, typename A, typename B, typename C, typename D>
    R callR(const char* function, A a, B b, C c, D d)
    {
        if (!appDLL)
        {
            AppFrame::error("Loader not initialized yet");
            R r;
            return r;
        }
        typedef R (*FunctionPointer)(A, B, C, D);
        FunctionPointer func = 0;
#ifdef __GNUWIN32__
        func = (FunctionPointer)GetProcAddress(appDLL, (LPCSTR)TEXT(function));
#else
        func = (FunctionPointer) dlsym(appDLL, function);
#endif
        return (func)(a, b, c, d);
    }
protected:
    std::string DLL_File;
#ifdef __GNUWIN32__
    HMODULE appDLL;
    //HINSTANCE appDLL;
#else
    void* appDLL;
#endif
};
}
#endif // DLL_FUNCTION_CALLER_H
