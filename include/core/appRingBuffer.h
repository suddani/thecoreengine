#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include "Types.h"
namespace AppFrame
{
    template<typename T, s32 Size>
    class RingBuffer
    {
    public:
        enum E_SEEK_POS
        {
            ESP_BEGIN = 0,
            ESP_END,
            ESP_CURRENT,
            ESP_COUNT
        };
        /** Default constructor */
        RingBuffer(void)
        {
            ReadPos = 0;
            WritePos = 0;
            for (u32 i = 0;i<Size;++i)
                Empty[i] = true;
        }
        /** Default destructor */
        ~RingBuffer(void)
        {
        }
        void write(const T& data)
        {
            Buffer[WritePos] = data;
            Empty[WritePos] = false;
            ++WritePos;
            if (WritePos >= Size)
                WritePos = 0;
        }
        void seek(E_SEEK_POS pos, s32 offset)
        {
            if (pos == ESP_BEGIN)
            {
                ReadPos = WritePos;
                while (Empty[ReadPos])
                {
                    ++ReadPos;
                    if (ReadPos >= Size)
                        ReadPos -= Size;
                }
                ReadPos += offset;
            }
            else if (pos == ESP_END)
            {
                if (WritePos > 0)
                    ReadPos = WritePos-1;
                else
                    ReadPos = Size-1;
                while (Empty[ReadPos])
                {
                    --ReadPos;
                    if (ReadPos < 0)
                        ReadPos += Size;
                }
                ReadPos += offset;
            }
            else if (pos == ESP_CURRENT)
            {
                ReadPos += offset;
            }

            while (ReadPos >= Size)
                ReadPos -= Size;
            while (ReadPos < 0)
                ReadPos += Size;
        }
        bool readLast(T& data)
        {
            seek(ESP_END);
            return read(data);
        }
        bool read(T& data)
        {
            if (Empty[ReadPos])
                return false;
            data = Buffer[ReadPos];
            ++ReadPos;
            if (ReadPos >= Size)
                ReadPos = 0;
            return true;
        }
        bool readback(T& data)
        {
            --ReadPos;
            if (ReadPos < 0)
                ReadPos = Size-1;
            if (Empty[ReadPos])
                return false;
            data = Buffer[ReadPos];
            return true;
        }
    protected:
        T Buffer[Size];
        s32 ReadPos;
        s32 WritePos;
        bool Empty[Size];
    private:
    };
}
#endif // RINGBUFFER_H
