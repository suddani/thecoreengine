#ifndef APPFRAME_TYPES_H_INCLUDED
#define APPFRAME_TYPES_H_INCLUDED

#include <stdlib.h>

/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
namespace AppFrame
{
    typedef int s32;
    typedef unsigned int u32;
    typedef long s64;
    typedef unsigned long u64;
    typedef short s16;
    typedef unsigned short u16;
    typedef float f32;
    typedef double f64;
    typedef char c8;
    typedef unsigned char u8;

    template<typename T>
    T* allocate(u32 size = 1, T** data = NULL)
    {
        T* d = NULL;

        if (size > 1)
            d = new T[size];
        else if (size == 1)
            d = new T;

        if (data)
            *data = d;
        return d;
    }

    template<typename T>
    void dellocate(T** data, bool array = false)
    {
        if (data && *data && array)
        {
            delete [] *data;
        }
        else if (data && *data && !array)
        {
            delete *data;
        }
        *data = NULL;
    }
/*
    inline f32 Q3sqrt(f32 x)
    {
        f32 xhalf = 0.5f*x;
        s32 i = *(s32*)&x; // get bits for floating value
        i = 0x5f375a86- (i>>1); // gives initial guess y0
        x = *(f32*)&i; // convert bits back to float
        x = x*(1.5f-xhalf*x*x); // Newton step, repeating increases accuracy
        x = x*(1.5f-xhalf*x*x); // Newton step, repeating increases accuracy
        x = x*(1.5f-xhalf*x*x); // Newton step, repeating increases accuracy
        return 1.f/x;
    }

    inline f32 Q3sqrt_inv(f32 x)
    {
        f32 xhalf = 0.5f*x;
        s32 i = *(s32*)&x; // get bits for floating value
        i = 0x5f375a86- (i>>1); // gives initial guess y0
        x = *(f32*)&i; // convert bits back to float
        x = x*(1.5f-xhalf*x*x); // Newton step, repeating increases accuracy
        return x;
    }
*/
}

#endif // APPFRAME_TYPES_H_INCLUDED
