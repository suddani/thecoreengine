/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef IBOEAMSCENENODE_H
#define IBOEAMSCENENODE_H

#include <irrlicht.h>

namespace AppFrame
{
    namespace graphic
    {
        class IBeamNode : public irr::scene::ISceneNode
        {
        public:
            IBeamNode( irr::scene::ISceneNode* parent, irr::scene::ISceneManager *mgr, irr::s32 id) : irr::scene::ISceneNode( parent, mgr, id )
            {
            }

            virtual ~IBeamNode(void)
            {
            }

            virtual void setLine(irr::core::vector3df start, irr::core::vector3df end, irr::f32 thickness) = 0;
            virtual irr::f32 getThickness(void) = 0;
            virtual void setThickness(irr::f32 thickness) = 0;
        protected:
        private:
        };
    }
}
#endif // CBEAMSCENENODE_H
