#ifndef IUPDATERECEIVER_H
#define IUPDATERECEIVER_H

#include <irrlicht.h>

namespace AppFrame
{
    namespace network
    {
        class INetMessage;
    }

    namespace components
    {
        class IBaseEntity;
        typedef IBaseEntity* CompEntity;
        typedef irr::s32 CompType;
    }

    class IUpdateReceiver
    {
    public:
        /** Default constructor */
        IUpdateReceiver() {}
        /** Default destructor */
        virtual ~IUpdateReceiver() {}

        virtual void Update(irr::u32 timeMs, irr::f32 timeDiff) {}

        virtual void receive(network::INetMessage* message) {}

        virtual bool SendUpdate(network::INetMessage* message, irr::u32 timeMs)
        {
            return false;
        }
        virtual components::CompEntity getOwner(void) = 0;
        virtual components::CompType getType(void) = 0;
        virtual const irr::c8* getTemplateName(void) = 0;
        virtual bool IsOwner(void) = 0;
    protected:
    private:
    };
}
#endif // IUPDATERECEIVER_H
