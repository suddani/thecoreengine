uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
varying vec2 vTexCoord;

void main(void)
{
	float sU = ( 1.0 / 640.0  );
    float sV = ( 1.0 / 480.0  );

    // The last two components (z,w) are unused. This makes for simpler code, but if
    // constant-storage is limited then it is possible to pack 4 offsets into 2 float4's
    vec4 tcDownSampleOffsets[4];
    tcDownSampleOffsets[0] = vec4(-0.5 * sU, 0.5 * sV, 0.0, 0.0);
    tcDownSampleOffsets[1] = vec4(0.5 * sU, 0.5 * sV, 0.0, 0.0);
    tcDownSampleOffsets[2] = vec4(-0.5 * sU, -0.5 * sV, 0.0, 0.0);
    tcDownSampleOffsets[3] = vec4(0.5 * sU, -0.5 * sV, 0.0, 0.0);

    //brightness
    float fBrightPassThreshold = 0.8;

    vec4 average = { 0.0, 0.0, 0.0, 0.0 };

    for ( int i = 0; i < 4; i++ )
    {
        average += texture2D( tex1, vTexCoord.xy + tcDownSampleOffsets[i].xy);
    }

    average *= 0.25;

	float luminance = max( average.x, max( average.y, average.z ) );

	if( luminance < fBrightPassThreshold )
		average = vec4( 0.0, 0.0, 0.0, 1.0 );

    gl_FragColor = average;
}
