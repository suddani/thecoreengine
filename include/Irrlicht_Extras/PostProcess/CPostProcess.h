/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						                    **
 **										                                        **
 **This software is provided 'as-is', without any express or implied		    **
 **warranty.  In no event will the authors be held liable for any damages	    **
 **arising from the use of this software.					                    **
 **										                                        **
 **Permission is not granted to anyone to use this software for any purpose,	**
 **including private and commercial applications, and to alter it and		    **
 **redistribute it.								                                **
 **										                                        **
 *********************************************************************************/
#ifndef CPostProcess_H
#define CPostProcess_H
#include <irrlicht.h>
#include <ILogger.h>
#include "IPostProcess.h"
namespace AppFrame
{
    namespace graphic
    {
        class CPostProcess : public IPostProcess
        {
        public:
            CPostProcess(irr::u32 width, irr::u32 height, const irr::c8* effectName, irr::IrrlichtDevice* device, const irr::video::ECOLOR_FORMAT& cf = irr::video::ECF_UNKNOWN);

            CPostProcess(const irr::c8* effectFile, irr::IrrlichtDevice* device);

            virtual ~CPostProcess();

            void addRenderTarget(const irr::c8* name, irr::u32 width, irr::u32 height, bool render2target, bool copy2target, const irr::video::ECOLOR_FORMAT& cf = irr::video::ECF_UNKNOWN);

            IPostProcess::Pipline* addRenderPipeline(irr::core::stringc vert, irr::core::stringc frag, irr::core::stringc target, irr::core::stringc tex0, irr::core::stringc tex1, irr::core::stringc tex2, irr::core::stringc tex3);

            IPostProcess::Pipline* addRenderPipeline(irr::core::stringc vert, irr::core::stringc frag, irr::core::stringc target, irr::video::ITexture* tex0, irr::video::ITexture* tex1, irr::video::ITexture* tex2, irr::video::ITexture* tex3);

            irr::video::ITexture* getRenderTarget(const irr::c8* name);

            irr::video::ITexture* getRenderTarget(void);

            irr::video::ITexture* getDepthPass(void);

            void Render(irr::video::ITexture* renderTarget = 0, bool renderScene = true);

            bool isActive(void);

            void setActive(bool active);

            void addAllNodes2DepthPass(void);

            void addNode2DepthPass(irr::scene::ISceneNode* node);

            void removeNodeFromDepthPass(irr::scene::ISceneNode* node);

            void setDepthPass(irr::video::ITexture* target, bool allnodes, float maxfar);

            void setMainCamera(irr::scene::ICameraSceneNode* camera);

            const irr::core::stringc& getName(void);

        protected:
            void SetupCopyMaterial(void);

            irr::core::stringc EffectName;
            irr::IrrlichtDevice* Device;
            irr::scene::ISceneManager* Manager;

            //DepthNodes
            irr::core::array<DepthNode> DepthNodes;
            bool RenderDepthPass;
            void renderDepthPass(void);
            irr::video::ITexture* DepthPassTexture;
            irr::u32 DepthMaterial;
            irr::f32* FarValue;

            //Camera
            irr::scene::ICameraSceneNode* Camera;

            irr::u32 Width;
            irr::u32 Height;

            //irr::video::ITexture* FinalRenderTarget;
            irr::core::array<irr::video::ITexture*> RenderTargets;
            irr::core::array<irr::core::stringc> RenderTargetNames;
            irr::core::array<bool> Render2Target;
            irr::core::array<bool> Copy2Target;

            //Materials
            irr::video::SMaterial CopyMaterial;
            irr::core::array<Pipline*> RenderPipelines;

            //quad
            irr::video::S3DVertex Vertices[6];

            //renderState
            bool Active;
        private:
        };
    }
}
#endif // CPostProcess_H
