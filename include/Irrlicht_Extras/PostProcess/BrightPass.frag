uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
varying vec2 vTexCoord;

void main(void)
{
    vec4 sample0 = texture2D(tex0, vTexCoord);
    vec4 sample1 = texture2D(tex1, vTexCoord);
    vec4 sample2 = texture2D(tex2, vTexCoord);

    float Luminance = 0.099;//0.08
    float fMiddleGray = 0.18;//0.18
    float fWhiteCutoff = 0.8;//0.8

    vec3 ColorOut = sample1.xyz;

    ColorOut *= fMiddleGray / ( Luminance + 0.001f );
    ColorOut *= ( 1.0f + ( ColorOut / ( fWhiteCutoff * fWhiteCutoff ) ) );
    ColorOut -= 5.0f;

    ColorOut = max( ColorOut, 0.0f );

    ColorOut /= ( 10.0 + ColorOut );


    gl_FragColor = vec4(ColorOut, 1.0);
}
