uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
varying vec2 vTexCoord;

void main(void)
{

    vec4 sample0 = texture2D(tex0, vTexCoord);
    vec4 sample1 = texture2D(tex1, vTexCoord);
    vec4 sample2 = texture2D(tex2, vTexCoord);


    gl_FragColor = (sample0+sample2);
}
