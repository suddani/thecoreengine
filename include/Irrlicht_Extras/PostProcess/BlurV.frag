uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;

varying vec2 vTexCoord;

void main()
{
	vec4 baseColor = vec4(0.0, 0.0, 0.0, 0.0);

	float d = 2.5 * 10.0 - 300.0;

	vec2 TexelKernel[13] =
	{
 	  { -6, 0 },
	  { -5, 0 },
  	  { -4, 0 },
  	  { -3, 0 },
  	  { -2, 0 },
  	  { -1, 0 },
  	  {  0, 0 },
  	  {  1, 0 },
  	  {  2, 0 },
 	  {  3, 0 },
 	  {  4, 0 },
  	  {  5, 0 },
	  {  6, 0 },
	};



float BlurWeights[13] =
{
    0.002216,
    0.008764,
    0.026995,
    0.064759,
    0.120985,
    0.176033,
    0.199471,
    0.176033,
    0.120985,
    0.064759,
    0.026995,
    0.008764,
    0.002216,
};

for (int i = 0; i < 13; i++)
    {
        baseColor += texture2D( tex2, vTexCoord.xy + TexelKernel[i].yx /d) * BlurWeights[i] * 5;
    }



	gl_FragColor = baseColor;
}
