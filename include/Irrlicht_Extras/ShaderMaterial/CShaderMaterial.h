#ifndef CSHADERMATERIAL_H
#define CSHADERMATERIAL_H

#include <IShaderMaterial.h>
namespace AppFrame
{
    namespace graphic
    {
        class CShaderMaterial : public irr::video::IShaderConstantSetCallBack, public IShaderMaterial
        {
        public:

            /** Default constructor */
            CShaderMaterial(irr::IrrlichtDevice* device);
            CShaderMaterial(const irr::core::stringc& name, irr::IrrlichtDevice* device);
            /** Default destructor */
            virtual ~CShaderMaterial();

            virtual void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32 userData);
            virtual void OnSetMaterial(const irr::video::SMaterial& material);

            //External Stuff
            void load(const irr::core::stringc& filename);
            //void load(irr::io::IReadFile* file);
            void load(irr::io::IXMLReaderUTF8* xml);

            void addShaderConstant(ShaderConstant* constant);
            void addShaderConstant(const irr::core::stringc& name, const irr::core::stringc& type, const irr::core::stringc& value);
            bool setShaderConstant(const irr::core::stringc& name, const ShaderConstant::E_SHADER_CONSTANT_TYPE& type, const irr::core::stringc& value);
            bool setShaderConstant(const irr::core::stringc& name, const irr::u32& value);
            bool setShaderConstant(const irr::core::stringc& name, const irr::f32& value);
            bool setShaderConstant(const irr::core::stringc& name, const irr::core::vector3df& value);
            bool setShaderConstant(const irr::core::stringc& name, const irr::core::vector2df& value);
            bool setShaderConstant(const irr::core::stringc& name, const irr::core::matrix4& value);

            void setPixelShader(const irr::core::stringc& name); //desides if it is a file or the shader itsef
            void setPixelShader(irr::io::IReadFile* shader); //well
            void setPixelShaderType(irr::video::E_PIXEL_SHADER_TYPE type);
            void setPixelShaderType(const irr::core::stringc& type);

            void setVertexShader(const irr::core::stringc& name); //desides if it is a file or the shader itsef
            void setVertexShader(irr::io::IReadFile* shader); //well
            void setVertexShaderType(irr::video::E_VERTEX_SHADER_TYPE type);
            void setVertexShaderType(const irr::core::stringc& type);

            void setBaseMaterial(irr::video::E_MATERIAL_TYPE baseMaterial);
            void setBaseMaterial(const irr::core::stringc& baseMaterial);

            bool CompileMaterial(void);

            bool isCompiled(void) const;

            irr::video::E_MATERIAL_TYPE getMaterial(void) const;

            const irr::core::stringc& getName(void) const;
        protected:
            void start(void);

            bool Compiled;
            irr::IrrlichtDevice* Device;
            irr::scene::ISceneManager* SceneManager;
            irr::video::IVideoDriver* Driver;
            irr::core::stringc Name;
            irr::core::stringc InternalName;

            irr::io::IReadFile* VertexShader;
            irr::video::E_VERTEX_SHADER_TYPE VertexShaderType;

            irr::io::IReadFile* PixelShader;
            irr::video::E_PIXEL_SHADER_TYPE PixelShaderType;

            irr::video::E_MATERIAL_TYPE BaseMaterial;
            irr::s32 NewMaterial;

            //During Rendering
            irr::video::SMaterial CurrentMaterial;
            irr::core::array<ShaderConstant*> ShaderConstants;
        private:
        };
    }
}
#endif // CSHADERMATERIAL_H
