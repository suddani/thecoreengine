uniform float offset;
void main()
{
	vec3 Pos = gl_Vertex.xyz+normalize(gl_Normal)*offset;
	gl_Position = gl_ModelViewProjectionMatrix*vec4(Pos,1.0);
}