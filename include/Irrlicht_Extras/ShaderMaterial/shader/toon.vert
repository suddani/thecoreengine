uniform vec3 lightPos;
varying vec3 lightDir;
varying vec3 normal;

varying vec2 Texcoord;

void main()
{
	normal = normalize(gl_Normal);
	Texcoord    = gl_MultiTexCoord0.xy;
	lightDir = normalize(lightPos-gl_Vertex.xyz);

	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
