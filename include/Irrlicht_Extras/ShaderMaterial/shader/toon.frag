varying vec3 lightDir;
varying vec3 normal;
uniform sampler2D Texture0;
varying vec2 Texcoord;
void main()
{
	vec3 mlightDir = normalize(lightDir);
	vec3 mnormal = normalize(normal);
	float intensity = dot(mnormal, mlightDir);
	vec4 color = texture2D(Texture0, Texcoord);
	if (intensity > 0.95)
		color = color-vec4(color.x*0.1, color.y*0.1, color.z*0.1, 0.0);
	else if (intensity > 0.50)
		color = color-vec4(color.x*0.3, color.y*0.3, color.z*0.3, 0.0);
	else if (intensity > 0.25)
		color = color-vec4(color.x*0.5, color.y*0.5, color.z*0.5, 0.0);
	else
		color = color-vec4(color.x*0.8, color.y*0.8, color.z*0.8, 0.0);
	gl_FragColor = color;
}
