#ifndef COBJECTSCENENODE_H
#define COBJECTSCENENODE_H

#include <irrlicht.h>
#include <IObjectSceneNode.h>

using namespace irr;
class AnimationSQ
{
public:
    AnimationSQ(void)
    {
        Name = "";
        Start = 0;
        End = 0;
        Speed = 0;
        Loop = false;
    }
    AnimationSQ(const irr::c8* name)
    {
        Name = name;
        Start = 0;
        End = 0;
        Speed = 0;
        Loop = false;
    }
    ~AnimationSQ(void)
    {
        Next.clear();
    }
    bool operator==(const irr::c8* s) const
    {
        if (Name == s)
            return true;
        return false;
    }
    core::stringc Name;
    u32 Start;
    u32 End;
    f32 Speed;
    bool Loop;
    core::array<core::stringc> Next;
};
class ClampSQ
{
public:
    ClampSQ(const irr::c8* name)
    {
        Name = name;
        Node = NULL;
        Scale = core::vector3df(1,1,1);
    }
    ~ClampSQ(void)
    {
        if (Node)
        {
            Node->remove();
            Node->drop();
            Node = NULL;
        }
    }
    void Make(scene::ISceneManager* smgr, scene::ISceneNode* parent)
    {
        if (Node)
            return;
        Node = smgr->addEmptySceneNode(parent);
        Node->setPosition(Position);
        Node->setRotation(Rotation);
        Node->setScale(Scale);
        Node->grab();
    }
    bool operator==(const irr::c8* s) const
    {
        if (Name == s)
            return true;
        return false;
    }
    core::stringc Name;
    core::vector3df Position;
    core::vector3df Rotation;
    core::vector3df Scale;
    core::stringc Bone;
    scene::ISceneNode* Node;
};
class CObjectSceneNode : public irr::scene::IObjectSceneNode, public irr::scene::IAnimationEndCallBack
{
    public:
        /** Default constructor */
        CObjectSceneNode(scene::ISceneNode *parent, scene::ISceneManager *mgr, s32 id=-1, const core::vector3df &position=core::vector3df(0, 0, 0), const core::vector3df &rotation=core::vector3df(0, 0, 0), const core::vector3df &scale=core::vector3df(1.0f, 1.0f, 1.0f));
        /** Default destructor */
        virtual ~CObjectSceneNode();

        void OnAnimationEnd(irr::scene::IAnimatedMeshSceneNode *node);

        void render(void);

        const irr::core::aabbox3d<irr::f32>& getBoundingBox(void) const;

        irr::u32 getMaterialCount(void) const;

        irr::video::SMaterial& getMaterial(const irr::u32& i);

        //external
        void addNext2Animation(const irr::c8* name, const irr::c8* next);
        void removeNext2Animation(const irr::c8* name, const irr::c8* next);
        void addAnimation(const irr::c8* name, const irr::u32& start, const irr::u32& end, const irr::f32& speed, const bool& loop);
        void removeAnimation(const irr::c8* name);

        void setGraphicManager(AppFrame::graphic::IGraphicManager* graphic);

        void loadObject(const irr::c8* file);
        void saveObject(const irr::c8* file);

        void setAnimation(const u32& start, const u32& end, const f32& speed, const bool& loop);
        f32 setAnimation(const irr::c8* name, bool now = false);
        f32 setAnimation(const s32& i, bool now = false, bool same = false);
        void setAnimationSpeed(const f32& speed);
        s32 getAnimationCount(void) const;
        const irr::s32& getCurrentAnimation(void);
        const irr::c8* getAnimationName(const u32& i);
        void setAnimationFrame(const u32& frame);
        u32 getCurrentAnimationFrame(void);
        u32 getMaxAnimationFrame(void);

        const scene::ISceneNode* getClamp(const irr::c8* name) const;
        const scene::ISceneNode* getClamp(const u32& i) const;

        scene::ISceneNode* getClamp(const irr::c8* name);
        scene::ISceneNode* getClamp(const u32& i);
		
        s32 getClampCount(void) const;

    protected:
        s32 findAnimation(const irr::c8* name);

        void cleanUp(void);

        irr::core::aabbox3d<irr::f32> BoundingBox;
        irr::scene::IAnimatedMeshSceneNode* Node;
        irr::core::stringc MeshPath;

        core::array<AnimationSQ> Animations;
        core::array<ClampSQ> Clamps;
        s32 CurrentAnimation;
        s32 NextAnimation;
        u32 MaxFrame;

        //Renderipelines
        irr::core::array<irr::scene::IAnimatedMeshSceneNode*> RenderPipeline;

        AppFrame::graphic::IGraphicManager* GraphicManager;

        //blubb
        irr::video::SMaterial SomeMaterial;
    private:
};

#endif // COBJECTSCENENODE_H
