<Object>
	<Mesh value="show/ninja.b3d"/>
	<Position value="0.0, 0.0, 0.0"/>
	<Rotation value="0.0, 0.0, 0.0"/>
	<Scale value="1.0, 1.0, 1.0"/>

	<MaterialPipeline>
		<Material id="0">
			<MaterialType value=""/>
			<Lightning value="false"/>
		</Material>

		<Material id="1">
			<MaterialType value=""/>
			<Lightning value="false"/>
		</Material>

		<Material id="2">
			<MaterialType value=""/>
			<Lightning value="false"/>
		</Material>

		<Material id="3">
			<MaterialType value=""/>
			<Lightning value="false"/>
		</Material>
	</MaterialPipeline>

	<MaterialPipeline>
		<Material id="0">
			<MaterialType value=""/>
			<Lightning value="false"/>
		</Material>

		<Material id="1">
			<MaterialType value=""/>
			<Lightning value="false"/>
		</Material>

		<Material id="2">
			<MaterialType value=""/>
			<Lightning value="false"/>
		</Material>

		<Material id="3">
			<MaterialType value=""/>
			<Lightning value="false"/>
		</Material>
	</MaterialPipeline>

	<Attachment name="">
		<Position value="0.0, 0.0, 0.0"/>
		<Rotation value="0.0, 0.0, 0.0"/>
		<Scale value="1.0, 1.0, 1.0"/>
		<Bone value=""/>
		<Object value=""/>
	</Attachment>

	<Clamp name="Weapon">
		<Position value="0.0, 0.0, 0.0"/>
		<Rotation value="0.0, 0.0, 0.0"/>
		<Scale value="1.0, 1.0, 1.0"/>
		<Bone value="Head"/> 
	</Clamp>

	<Animation name="Walk">
		<Range value="1" value1="14"/>
		<Speed value="15.0"/>
		<Loop value="true"/>
	</Animation>

	<Animation name="Stealth_Walk">
		<Range value="15" value1="30"/>
		<Speed value="15.0"/>
		<Loop value="true"/>
	</Animation>

	<Animation name="Punch_and_swipe_Sword">
		<Range value="32" value1="44"/>
		<Speed value="10.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="Swipe_and_spin_Sword">
		<Range value="45" value1="59"/>
		<Speed value="10.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="DownCut">
		<Range value="60" value1="68"/>
		<Speed value="5.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="Block_up">
		<Range value="69" value1="72"/>
		<Speed value="1.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="Block_down">
		<Range value="72" value1="69"/>
		<Speed value="1.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="Kick">
		<Range value="73" value1="83"/>
		<Speed value="30.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="PickUp">
		<Range value="84" value1="93"/>
		<Speed value="1.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="Crouch">
		<Range value="87" value1="87"/>
		<Speed value="30.0"/>
		<Loop value="true"/>
	</Animation>

	<Animation name="Jump">
		<Range value="94" value1="102"/>
		<Speed value="5.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="FinishMove">
		<Range value="112" value1="125"/>
		<Speed value="5.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="SideKick">
		<Range value="126" value1="133"/>
		<Speed value="30.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="SpinningSword">
		<Range value="134" value1="145"/>
		<Speed value="10.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="Backflip">
		<Range value="146" value1="158"/>
		<Speed value="10.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="ClimbWall">
		<Range value="159" value1="165"/>
		<Speed value="30.0"/>
		<Loop value="false"/>
	</Animation>

	<Animation name="Death1">
		<Range value="166" value1="173"/>
		<Speed value="5.0"/>
		<Loop value="false"/>
	</Animation>

	<Animation name="Death2">
		<Range value="174" value1="182"/>
		<Speed value="5.0"/>
		<Loop value="false"/>
	</Animation>

	<Animation name="Idle1">
		<Range value="184" value1="205"/>
		<Speed value="10.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="Idle2">
		<Range value="206" value1="250"/>
		<Speed value="10.0"/>
		<Loop value="false"/>
		<Next value="Idle1"/>
		<Next value="Idle2"/>
		<Next value="Idle3"/>
	</Animation>

	<Animation name="Idle3">
		<Range value="251" value1="300"/>
		<Speed value="10.0"/>
		<Loop value="false"/>
		<Next value="Idle3"/>
		<Next value="Idle2"/>
		<Next value="Idle1"/>
	</Animation>
</Object>