#ifndef CGILESWRITER_H
#define CGILESWRITER_H

#include <irrlicht.h>

namespace irr
{
    namespace scene
    {
        class CGilesWriter : public irr::scene::IMeshWriter
        {
        public:
            /** Default constructor */
            CGilesWriter(IrrlichtDevice* device);
            /** Default destructor */
            virtual ~CGilesWriter();

            EMESH_WRITER_TYPE  getType () const;

            bool  writeMesh (io::IWriteFile *file, scene::IMesh *mesh, s32 flags=EMWF_NONE);
        protected:
            IrrlichtDevice* Device;

            s32 writeHeader(io::IWriteFile* file);
            s32 writeAuthor(io::IWriteFile* file, const c8* author);
            s32 writeLightmaps(io::IWriteFile* file);
            s32 writeTextures(io::IWriteFile* file);
            s32 writeMaterials(io::IWriteFile* file);
            s32 writeModels(io::IWriteFile* file, IMesh* mesh);

            void collectMaterials(IMesh* mesh);
            void addMaterial(video::SMaterial mat);

            void addTexture(core::array<video::ITexture*>& textures, video::ITexture* texture);

            core::array<video::SMaterial> Materials;
            core::array<video::ITexture*> LightmapTextures; //have to saved in the file
            core::array<video::ITexture*> Textures;

            s16 getTextureIndex(video::ITexture* tex);
            s16 getLightmapIndex(video::ITexture* tex);


            void clear(void);
        private:
        };
    }
}
#endif // CGILESWRITER_H
