/*
Copyright (c) 2009 Daniel Sudmann

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
   2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
   3. This notice may not be removed or altered from any source distribution.
*/
#ifndef CGILESLOADER_H
#define CGILESLOADER_H

#include <irrlicht.h>

namespace irr
{
    namespace scene
    {
        class CGilesLoader : public IMeshLoader
        {
        public:
            /** Default constructor */
            CGilesLoader(IrrlichtDevice* device);
            /** Default destructor */
            virtual ~CGilesLoader();

            virtual IAnimatedMesh *  createMesh (io::IReadFile *file);

            virtual bool isALoadableFileExtension(const irr::core::string<irr::c8, irr::core::irrAllocator<irr::c8> >& filename) const;

        protected:

            video::IVideoDriver* Driver;

            irr::ILogger* Logger;

            bool readHeader(io::IReadFile *file);

            void loadMaterials(io::IReadFile *file, s32 ChunkSize);

            void loadTextures(io::IReadFile *file, s32 ChunkSize);

            void loadLightmaps(io::IReadFile *file, s32 ChunkSize);

            void loadModels(io::IReadFile *file, s32 ChunkSize);

            void loadModel(io::IReadFile *file, s32 ChunkSize);

            void loadMesh(io::IReadFile *file, s32 ChunkSize);

            void loadSurfaces(io::IReadFile *file, s32 ChunkSize);


            void pushParent(s32 ID);

            void pullParent(void);

            s32 currentParent(void);

            core::array<s32> ParentChunk;

            core::matrix4 VertexTransfrom;

            core::array<video::SMaterial> Materials;
            core::array<video::ITexture*> Lightmaps;
            core::array<video::ITexture*> Textures;

            SMesh*          Mesh;

            void clear(void);

            void readString(core::stringc& data, io::IReadFile *file);

            void skip(s32 Size, io::IReadFile *file);

            bool skipNoParent(s32 ID, s32 Size, io::IReadFile *file);
        private:
        };
    }
}
#endif // CGILESLOADER_H
