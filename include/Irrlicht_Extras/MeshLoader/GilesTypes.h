/*
Copyright (c) 2009 Daniel Sudmann

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
   2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
   3. This notice may not be removed or altered from any source distribution.
*/
#ifndef GILESTYPES_H_INCLUDED
#define GILESTYPES_H_INCLUDED

#include <irrlicht.h>

namespace giles
{
    const irr::s32 GLS_HEADER                = 0xFFFF;

    const irr::s32 GLS_AUTHOR                = 0xF000;

    const irr::s32 GLS_MODELS                = 0x1000;
    const irr::s32 GLS_MODEL                 = 0x1001;
    const irr::s32 GLS_MODEL_NAME            = 0x1002;
    const irr::s32 GLS_MODEL_POSITION        = 0x1003;
    const irr::s32 GLS_MODEL_ROTATION        = 0x1004;
    const irr::s32 GLS_MODEL_SCALE           = 0x1005;
    const irr::s32 GLS_MODEL_HIDDEN          = 0x1008;

    const irr::s32 GLS_MESH                  = 0x2000;
    const irr::s32 GLS_MESH_OVERRIDE         = 0x2001;
    const irr::s32 GLS_MESH_BACKLIGHT        = 0x2002;
    const irr::s32 GLS_MESH_RECEIVESHADOW    = 0x2003;
    const irr::s32 GLS_MESH_CASTSHADOW       = 0x2004;
    const irr::s32 GLS_MESH_RECEIVEGI        = 0x2005;
    const irr::s32 GLS_MESH_AFFECTGI         = 0x2006;

    const irr::s32 GLS_MESH_SURFACES         = 0x2100;
    const irr::s32 GLS_MESH_SURF             = 0x2101;
    const irr::s32 GLS_MESH_SURFVERTS        = 0x2102;
    const irr::s32 GLS_MESH_SURFPOLYS        = 0x2103;
    const irr::s32 GLS_MESH_SURFMATERIAL     = 0x2104;
    const irr::s32 GLS_MESH_SURFVERTFORMAT   = 0x2105;
    const irr::s32 GLS_MESH_SURFVERTDATA     = 0x2106;
    const irr::s32 GLS_MESH_SURFPOLYDATA     = 0x2107;

    const irr::s32 GLS_PIVOT                 = 0x3000;

    const irr::s32 GLS_LIGHT                 = 0x4000;
    const irr::s32 GLS_LIGHT_TYPE            = 0x4001;
    const irr::s32 GLS_LIGHT_ACTIVE          = 0x4002;
    const irr::s32 GLS_LIGHT_CASTSHADOWS     = 0x4003;
    const irr::s32 GLS_LIGHT_INFINITE        = 0x4004;
    const irr::s32 GLS_LIGHT_OVERSHOOT       = 0x4005;
    const irr::s32 GLS_LIGHT_RADIUS          = 0x4006;
    const irr::s32 GLS_LIGHT_RED             = 0x4007;
    const irr::s32 GLS_LIGHT_GREEN           = 0x4008;
    const irr::s32 GLS_LIGHT_BLUE            = 0x4009;
    const irr::s32 GLS_LIGHT_INTENSITY       = 0x400A;
    const irr::s32 GLS_LIGHT_NEAR            = 0x400B;
    const irr::s32 GLS_LIGHT_FAR             = 0x400C;
    const irr::s32 GLS_LIGHT_INNER           = 0x400D;
    const irr::s32 GLS_LIGHT_OUTER           = 0x400E;
    const irr::s32 GLS_LIGHT_TOON            = 0x400F;
    const irr::s32 GLS_LIGHT_TOONLEVELS      = 0x4010;

    const irr::s32 GLS_MATERIALS             = 0x5000;
    const irr::s32 GLS_MAT                   = 0x5001;
    const irr::s32 GLS_MAT_NAME              = 0x5002;
    const irr::s32 GLS_MAT_RED               = 0x5003;
    const irr::s32 GLS_MAT_GREEN             = 0x5004;
    const irr::s32 GLS_MAT_BLUE              = 0x5005;
    const irr::s32 GLS_MAT_ALPHA             = 0x5006;
    const irr::s32 GLS_MAT_SELFILLUMINATION  = 0x5007;
    const irr::s32 GLS_MAT_SHININESS         = 0x5008;
    const irr::s32 GLS_MAT_FX                = 0x5009;
    const irr::s32 GLS_MAT_BLEND             = 0x500A;
    const irr::s32 GLS_MAT_LIGHTMETHOD       = 0x500B;
    const irr::s32 GLS_MAT_LIGHTMAP          = 0x500C;
    const irr::s32 GLS_MAT_RECEIVEBACK       = 0x500D;
    const irr::s32 GLS_MAT_RECEIVESHADOW     = 0x500E;
    const irr::s32 GLS_MAT_CASTSHADOW        = 0x500F;
    const irr::s32 GLS_MAT_RECEIVEGI         = 0x5010;
    const irr::s32 GLS_MAT_AFFECTGI          = 0x5011;
    const irr::s32 GLS_MAT_TEXLAYER          = 0x5012;

    const irr::s32 GLS_TEXTURES              = 0x6000;
    const irr::s32 GLS_TEX                   = 0x6001;
    const irr::s32 GLS_TEX_FILE              = 0x6002;
    const irr::s32 GLS_TEX_SCALEU            = 0x6003;
    const irr::s32 GLS_TEX_SCALEV            = 0x6004;
    const irr::s32 GLS_TEX_OFFSETU           = 0x6005;
    const irr::s32 GLS_TEX_OFFSETV           = 0x6006;
    const irr::s32 GLS_TEX_ANGLE             = 0x6007;
    const irr::s32 GLS_TEX_FLAGS             = 0x6008;
    const irr::s32 GLS_TEX_BLEND             = 0x6009;
    const irr::s32 GLS_TEX_COORDSET          = 0x600A;

    const irr::s32 GLS_LIGHTMAPS             = 0x7000;
    const irr::s32 GLS_LMAP                  = 0x7001;
    const irr::s32 GLS_LMAP_NAME             = 0x7002;
    const irr::s32 GLS_LMAP_FILE             = 0x7003;
    const irr::s32 GLS_LMAP_WIDTH            = 0x7004;
    const irr::s32 GLS_LMAP_HEIGHT           = 0x7005;
    const irr::s32 GLS_LMAP_NONUNIFORM       = 0x7006;
    const irr::s32 GLS_LMAP_USECUSTOMTEXEL   = 0x7007;
    const irr::s32 GLS_LMAP_CUSTOMTEXEL      = 0x7008;
    const irr::s32 GLS_LMAP_REPACK           = 0x7009;
    const irr::s32 GLS_LMAP_DATA             = 0x700A; ///Size = WIDTH*HEIGHT*3, one row at a time


    //RENDER should be at the end of the file
}

#endif // GILESTYPES_H_INCLUDED
