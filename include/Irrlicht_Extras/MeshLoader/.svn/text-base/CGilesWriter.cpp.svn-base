#include "CGilesWriter.h"
#include "GilesTypes.h"
#include <ILogger.h>

namespace irr
{
    namespace scene
    {
        CGilesWriter::CGilesWriter(IrrlichtDevice* device) : Device(device)
        {
            //ctor
        }

        CGilesWriter::~CGilesWriter()
        {
            //dtor
            clear();
        }

        void CGilesWriter::clear(void)
        {
            Materials.clear();
            LightmapTextures.clear();
        }

        EMESH_WRITER_TYPE  CGilesWriter::getType () const
        {
            return (EMESH_WRITER_TYPE)(EMWT_OBJ + 1);
        }

        bool  CGilesWriter::writeMesh (io::IWriteFile *file, scene::IMesh *mesh, s32 flags)
        {
            clear();

            if (!file || !mesh)
                return false;

            AppFrame::log("Write header");
            s32 fileSizePos = writeHeader(file);

            AppFrame::log("Write Author");

            s32 SizePos = writeAuthor(file, "This File is written by Irrlicht");



            AppFrame::log("Collect Materials");
            //collect all Materials
            collectMaterials(mesh);

            //now write out LightmapChunks
            SizePos = writeLightmaps(file);

            //now write out TextureChunks
            SizePos = writeTextures(file);

            //now write out MaterialChunks
            SizePos = writeMaterials(file);

            //now write the models
            SizePos = writeModels(file, mesh);


            //Write Final FileSize
            s32 Size = file->getPos()-4;
            file->seek(fileSizePos, false);
            file->write(&Size, 4);

            return true;
        }

        s32 CGilesWriter::writeHeader(io::IWriteFile* file)
        {
            f32 Version = 1.01f;
            c8* Name = "gile[s]";
            s32 Size = 0; //this is intersting

            file->write(&giles::GLS_HEADER, 4);
            file->write(&Size, 4);
            file->write(Name, 8);
            file->write(&Version, 4);
            return 4;
        }

        s32 CGilesWriter::writeAuthor(io::IWriteFile* file, const c8* author)
        {
            s32 Size = strlen(author);

            file->write(&giles::GLS_AUTHOR, 4);
            s32 SizePos = file->getPos();
            file->write(&Size, 4);
            file->write(author, Size+1);

            return SizePos;
        }

        s32 CGilesWriter::writeLightmaps(io::IWriteFile* file)
        {
            s32 Size = 0; //has to be determined
            s32 SizePos = 0;
            file->write(&giles::GLS_LIGHTMAPS, 4);
            SizePos = file->getPos();
            file->write(&Size, 4);

            //now writing LightmapChunks
            for (u32 i=0;i<LightmapTextures.size();i++)
            {
                s32 MapSize = 0;
                s32 Size = 0;
                file->write(&giles::GLS_LMAP, 4);
                s32 MapSizePos = file->getPos();
                file->write(&MapSize, 4);

                irr::core::stringc data = "Lightmap";
                data.append(i);

                //Write lightmap name
                Size = strlen(data.c_str());
                file->write(&giles::GLS_LMAP_NAME, 4);
                file->write(&Size, 4);
                file->write(data.c_str(), Size+1);

                //Write lightmap Width
                Size = 2;
                file->write(&giles::GLS_LMAP_WIDTH, 4);
                file->write(&Size, 4);
                s16 value = static_cast<s16>(LightmapTextures[i]->getSize().Width);
                file->write(&value, Size);

                //Write lightmap Height
                Size = 2;
                file->write(&giles::GLS_LMAP_HEIGHT, 4);
                file->write(&Size, 4);
                value = static_cast<s16>(LightmapTextures[i]->getSize().Height);
                file->write(&value, Size);

                //Write Lightmap
                video::IImage* lightmap = Device->getVideoDriver()->createImageFromData(LightmapTextures[i]->getColorFormat(), LightmapTextures[i]->getSize(), LightmapTextures[i]->lock());
                LightmapTextures[i]->unlock();
                Size = LightmapTextures[i]->getSize().Width*LightmapTextures[i]->getSize().Height*3;
                c8* colordata = new c8[Size];
                lightmap->copyToScaling(colordata, LightmapTextures[i]->getSize().Width, LightmapTextures[i]->getSize().Height, video::ECF_R8G8B8);
                lightmap->drop();
                file->write(&giles::GLS_LMAP_DATA, 4);
                file->write(&Size, 4);
                file->write(colordata, Size);
                delete [] colordata;

                //Write LightmapSize
                Size = file->getPos()-MapSizePos-4;
                file->seek(MapSizePos);
                file->write(&Size, 4);
                file->seek(Size+MapSizePos+4);
            }
            Size = file->getPos()-SizePos-4;
            file->seek(SizePos);
            file->write(&Size, 4);
            file->seek(Size+SizePos+4);
            return SizePos;
        }

        s32 CGilesWriter::writeTextures(io::IWriteFile* file)
        {
            s32 Size = 0; //has to be determined
            s32 SizePos = 0;
            file->write(&giles::GLS_TEXTURES, 4);
            SizePos = file->getPos();
            file->write(&Size, 4);

            //now writing TextureChunks
            for (u32 i=0;i<Textures.size();i++)
            {
                s32 MapSize = 0;
                s32 Size = 0;
                file->write(&giles::GLS_TEX, 4);
                s32 MapSizePos = file->getPos();
                file->write(&MapSize, 4);


                //write filename
                irr::core::stringc data = Textures[i]->getName();
                Size = data.size();
                file->write(&giles::GLS_TEX_FILE, 4);
                file->write(&Size, 4);
                file->write(data.c_str(), Size+1);



                //Write TextureSize
                Size = file->getPos()-MapSizePos-4;
                file->seek(MapSizePos);
                file->write(&Size, 4);
                file->seek(Size+MapSizePos+4);
            }
            Size = file->getPos()-SizePos-4;
            file->seek(SizePos);
            file->write(&Size, 4);
            file->seek(Size+SizePos+4);
            return SizePos;
        }

        s32 CGilesWriter::writeMaterials(io::IWriteFile* file)
        {
            s32 Size = 0; //has to be determined
            s32 SizePos = 0;
            file->write(&giles::GLS_MATERIALS, 4);
            SizePos = file->getPos();
            file->write(&Size, 4);

            //now writing MaterialChunks
            for (u32 i=0;i<Materials.size();i++)
            {
                s32 MapSize = 0;
                s32 Size = 0;
                file->write(&giles::GLS_MAT, 4);
                s32 MapSizePos = file->getPos();
                file->write(&MapSize, 4);

                //Write GLS_MAT_TEXLAYER
                Size = 3;
                file->write(&giles::GLS_MAT_TEXLAYER, 4);
                file->write(&Size, 4);
                c8 byte = 0;
                file->write(&byte, 1);
                s16 index = getTextureIndex(Materials[i].getTexture(0));
                file->write(&index, 2);

                //Write Lightmethod
                Size = 1;
                file->write(&giles::GLS_MAT_LIGHTMETHOD, 4);
                file->write(&Size, 4);
                if (Materials[i].MaterialType == video::EMT_LIGHTMAP || Materials[i].MaterialType == video::EMT_LIGHTMAP_ADD || Materials[i].MaterialType == video::EMT_LIGHTMAP_LIGHTING || Materials[i].MaterialType == video::EMT_LIGHTMAP_LIGHTING_M2 || Materials[i].MaterialType == video::EMT_LIGHTMAP_LIGHTING_M4 || Materials[i].MaterialType == video::EMT_LIGHTMAP_M2 || Materials[i].MaterialType == video::EMT_LIGHTMAP_M4)
                    byte = 2;
                else
                    byte = 1;
                file->write(&byte, 1);

                //Write Lightmap
                if (Materials[i].MaterialType == video::EMT_LIGHTMAP || Materials[i].MaterialType == video::EMT_LIGHTMAP_ADD || Materials[i].MaterialType == video::EMT_LIGHTMAP_LIGHTING || Materials[i].MaterialType == video::EMT_LIGHTMAP_LIGHTING_M2 || Materials[i].MaterialType == video::EMT_LIGHTMAP_LIGHTING_M4 || Materials[i].MaterialType == video::EMT_LIGHTMAP_M2 || Materials[i].MaterialType == video::EMT_LIGHTMAP_M4)
                {
                    Size = 2;
                    file->write(&giles::GLS_MAT_LIGHTMAP, 4);
                    file->write(&Size, 4);
                    index = getLightmapIndex(Materials[i].getTexture(1))+1;
                    file->write(&index, 2);
                }

                //Write Colors
                Size = 4;

                file->write(&giles::GLS_MAT_RED, 4);
                file->write(&Size, 4);
                f32 color = Materials[i].DiffuseColor.getRed();
                file->write(&color, 4);

                file->write(&giles::GLS_MAT_GREEN, 4);
                file->write(&Size, 4);
                color = Materials[i].DiffuseColor.getGreen();
                file->write(&color, 4);

                file->write(&giles::GLS_MAT_BLUE, 4);
                file->write(&Size, 4);
                color = Materials[i].DiffuseColor.getBlue();
                file->write(&color, 4);


                //Write FX
                Size = 4;
                file->write(&giles::GLS_MAT_FX, 4);
                file->write(&Size, 4);
                s32 flag = 0;
                if (!Materials[i].BackfaceCulling)
                    flag |= 16;
                file->write(&flag, 2);



                //Write MaterialSize
                Size = file->getPos()-MapSizePos-4;
                file->seek(MapSizePos);
                file->write(&Size, 4);
                file->seek(Size+MapSizePos+4);
            }
            Size = file->getPos()-SizePos-4;
            file->seek(SizePos);
            file->write(&Size, 4);
            file->seek(Size+SizePos+4);
            return SizePos;
        }

        s32 CGilesWriter::writeModels(io::IWriteFile* file, IMesh* mesh)
        {
            s32 Size = 0; //has to be determined
            s32 SizePos = 0;
            file->write(&giles::GLS_MODELS, 4);
            SizePos = file->getPos();
            file->write(&Size, 4);

            //now writing ModelChunks
            for (u32 i=0;i<mesh->getMeshBufferCount();i++)
            {
                s32 MapSize = 0;
                s32 Size = 0;
                file->write(&giles::GLS_MODEL, 4);
                s32 MapSizePos = file->getPos();
                file->write(&MapSize, 4);



                //Write ModelSize
                Size = file->getPos()-MapSizePos-4;
                file->seek(MapSizePos);
                file->write(&Size, 4);
                file->seek(Size+MapSizePos+4);
            }
            Size = file->getPos()-SizePos-4;
            file->seek(SizePos);
            file->write(&Size, 4);
            file->seek(Size+SizePos+4);
            return SizePos;
        }

        void CGilesWriter::collectMaterials(IMesh* mesh)
        {
            for (u32 i=0;i<mesh->getMeshBufferCount();i++)
            {
                addMaterial(mesh->getMeshBuffer(i)->getMaterial());
            }
        }

        void CGilesWriter::addMaterial(video::SMaterial mat)
        {
            for (u32 i=0;i<Materials.size();i++)
            {
                if (mat == Materials[i])
                {
                    return;
                }
            }

            Materials.push_back(mat);

            //add the diffuse texture
            if (mat.getTexture(0))
                addTexture(Textures, mat.getTexture(0));

            //add the lightmap if set
            if (mat.MaterialType == video::EMT_LIGHTMAP && mat.getTexture(1))
                addTexture(LightmapTextures, mat.getTexture(1));
        }

        void CGilesWriter::addTexture(core::array<video::ITexture*>& textures, video::ITexture* texture)
        {
            for (u32 i=0;i<textures.size();i++)
            {
                if (textures[i] == texture)
                    return;
            }
            textures.push_back(texture);
        }

        s16 CGilesWriter::getTextureIndex(video::ITexture* tex)
        {
            for (u32 i=0;i<Textures.size();i++)
            {
                if (Textures[i] == tex)
                    return static_cast<s16>(i);
            }
            return -1;
        }

        s16 CGilesWriter::getLightmapIndex(video::ITexture* tex)
        {
            for (u32 i=0;i<LightmapTextures.size();i++)
            {
                if (LightmapTextures[i] == tex)
                    return static_cast<s16>(i);
            }
            return -1;
        }
    }
}
